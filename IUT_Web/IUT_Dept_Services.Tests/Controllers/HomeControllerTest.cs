﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IUT_Dept_Services;
using IUT_Dept_Services.Controllers;

namespace IUT_Dept_Services.Tests.Controllers
{
    public class HomeControllerTest
    {
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
