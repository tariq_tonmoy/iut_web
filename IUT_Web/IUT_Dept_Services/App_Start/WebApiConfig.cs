﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Microsoft.Practices.Unity;
using IUT_Dept_Services.Providers;
using Newtonsoft.Json;
using IUT_Dept_Services.Repositories;
using IUT_Dept_Services.Models;
using IUT_Dept_Services.Factories;
using System.Web.Http.Dispatcher;

namespace IUT_Dept_Services
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IUserDetailsRepository, UserDetailsRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAcademicsRepository, AcademicsRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdminRepository, AdminRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAlumniRepository, AlumniRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IPersonnelRepository<Faculty>, PersonnelRepository<Faculty>>(new HierarchicalLifetimeManager());
            container.RegisterType<IPersonnelRepository<TA>, PersonnelRepository<TA>>(new HierarchicalLifetimeManager());
            container.RegisterType<IPersonnelRepository<Staff>, PersonnelRepository<Staff>>(new HierarchicalLifetimeManager());
            container.RegisterType<IResearchRepository, ResearchRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IStoryRepository, StoryRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IChattingRepository, ChattingRepository>(new HierarchicalLifetimeManager());


            config.DependencyResolver = new UnityResolver(container);

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new UnityControllerFactory(config));
        }
    }
}
