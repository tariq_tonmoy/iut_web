﻿using IUT_Dept_Services.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Data.Entity;
using System.Web;
using Microsoft.Owin.Host.SystemWeb;
using IUT_Dept_Services.Migrations;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace IUT_Dept_Services
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser, string>
    {

        public ApplicationUserManager(IUserStore<ApplicationUser, string> store) : base(store)
        {
        }

        public static ApplicationUserManager Create(
            IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(
                new UserStore<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
                (context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames

            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(
                new ApplicationRoleStore(context.Get<ApplicationDbContext>()));
        }
    }


    internal sealed class ApplicationDbInitializer
        : MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>
    {



        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEF(ApplicationDbContext db)
        {
            ApplicationUserManager userManager;
            ApplicationRoleManager roleManager;
            try
            {
                userManager = new ApplicationUserManager(new UserStore<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(db));
                roleManager = new ApplicationRoleManager(new ApplicationRoleStore(db));
            }
            catch (Exception ex)
            {
                userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            }

            //string[] passwords = new string[]
            //{
            //    "logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)","logan(X1,X2)"
            //};
            //List<ApplicationUser> users = new List<ApplicationUser>() {
            //    new ApplicationUser() {
            //        UserName="admin1",
            //        Email="admin1@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="admin2",
            //        Email="admin2@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="faculty1",
            //        Email="faculty1@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="faculty2",
            //        Email="faculty2@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="staff1",
            //        Email="staff1@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="staff2",
            //        Email="staff2@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="ta1ta1",
            //        Email="ta1@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="ta2ta2",
            //        Email="ta2@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="alumni1",
            //        Email="alumni1@iut-dhaka.edu"
            //    },
            //    new ApplicationUser() {
            //        UserName="alumni2",
            //        Email="alumni2@iut-dhaka.edu"
            //    }
            //};
            //List<UserDetails> details = new List<UserDetails>()
            //{
            //    new UserDetails(){UserID=users[0].Id,PersonalInfo=new PersonalDetails(){UserID=users[0].Id, FirstName="Admin",LastName="One",BloodGroup="A+ve",Nationality="Bangladeshi",DoB=new System.DateTime(1990,12,23) } },
            //    new UserDetails(){UserID=users[1].Id,PersonalInfo=new PersonalDetails(){UserID=users[1].Id,FirstName="Admin",LastName="Two",BloodGroup="B+ve",Nationality="South African",DoB=new System.DateTime(1989,12,23) } },
            //    new UserDetails(){UserID=users[2].Id,PersonalInfo=new PersonalDetails(){UserID=users[2].Id,FirstName="Faculty",LastName="One",BloodGroup="C+ve",Nationality="Saudi",DoB=new System.DateTime(1988,12,23) } },
            //    new UserDetails(){UserID=users[3].Id,PersonalInfo=new PersonalDetails(){UserID=users[3].Id,FirstName="Faculty",LastName="Two",BloodGroup="D+ve",Nationality="Yemeni",DoB=new System.DateTime(1987,12,23) } },
            //    new UserDetails(){UserID=users[4].Id,PersonalInfo=new PersonalDetails(){UserID=users[4].Id,FirstName="Staff",LastName="One",BloodGroup="E+ve",Nationality="Afghani",DoB=new System.DateTime(1986,12,23) } },
            //    new UserDetails(){UserID=users[5].Id,PersonalInfo=new PersonalDetails(){UserID=users[5].Id,FirstName="Staff",LastName="Two",BloodGroup="F+ve",Nationality="Tamil",DoB=new System.DateTime(1985,12,23) } },
            //    new UserDetails(){UserID=users[6].Id,PersonalInfo=new PersonalDetails(){UserID=users[6].Id,FirstName="Teacher's Assistant",LastName="One",BloodGroup="G+ve",Nationality="Bangladeshi",DoB=new System.DateTime(1984,12,23) } },
            //    new UserDetails(){UserID=users[7].Id,PersonalInfo=new PersonalDetails(){UserID=users[7].Id,FirstName="Teacher's Assistant",LastName="Two",BloodGroup="H+ve",Nationality="Bangladeshi",DoB=new System.DateTime(1983,12,23) } },
            //    new UserDetails(){UserID=users[8].Id,PersonalInfo=new PersonalDetails(){UserID=users[8].Id,FirstName="Alumni",LastName="One",BloodGroup="I+ve",Nationality="Bangladeshi",DoB=new System.DateTime(1982,12,23) } },
            //    new UserDetails(){UserID=users[9].Id,PersonalInfo=new PersonalDetails(){UserID=users[9].Id,FirstName="Alumni",LastName="Two",BloodGroup="J+ve",Nationality="Bangladeshi",DoB=new System.DateTime(1981,12,23) } },
            //};
            //List<AcademicDetails> academics = new List<AcademicDetails>()
            //{
            //    new AcademicDetails(){Degree="Degree0",Concentration="Concentration0",Institution="Institution0",IsCurrentAcademicStatus=false,UserID=users[0].Id },
            //    new AcademicDetails(){Degree="Degree1",Concentration="Concentration1",Institution="Institution1",IsCurrentAcademicStatus=true,UserID=users[0].Id},
            //    new AcademicDetails(){Degree="Degree2",Concentration="Concentration2",Institution="Institution2",IsCurrentAcademicStatus=false,UserID=users[1].Id},
            //    new AcademicDetails(){Degree="Degree3",Concentration="Concentration3",Institution="Institution3",IsCurrentAcademicStatus=true,UserID=users[1].Id},
            //    new AcademicDetails(){Degree="Degree4",Concentration="Concentration4",Institution="Institution4",IsCurrentAcademicStatus=false,UserID=users[2].Id},
            //    new AcademicDetails(){Degree="Degree5",Concentration="Concentration5",Institution="Institution5",IsCurrentAcademicStatus=true,UserID=users[2].Id},
            //    new AcademicDetails(){Degree="Degree6",Concentration="Concentration6",Institution="Institution6",IsCurrentAcademicStatus=false,UserID=users[3].Id},
            //    new AcademicDetails(){Degree="Degree7",Concentration="Concentration7",Institution="Institution7",IsCurrentAcademicStatus=true,UserID=users[3].Id},
            //    new AcademicDetails(){Degree="Degree8",Concentration="Concentration8",Institution="Institution8",IsCurrentAcademicStatus=false,UserID=users[4].Id},
            //    new AcademicDetails(){Degree="Degree9",Concentration="Concentration9",Institution="Institution9",IsCurrentAcademicStatus=true,UserID=users[4].Id},
            //    new AcademicDetails(){Degree="Degree10",Concentration="Concentration10",Institution="Institution10",IsCurrentAcademicStatus=false,UserID=users[5].Id},
            //    new AcademicDetails(){Degree="Degree11",Concentration="Concentration11",Institution="Institution11",IsCurrentAcademicStatus=true,UserID=users[5].Id},
            //    new AcademicDetails(){Degree="Degree12",Concentration="Concentration12",Institution="Institution12",IsCurrentAcademicStatus=false,UserID=users[6].Id},
            //    new AcademicDetails(){Degree="Degree13",Concentration="Concentration13",Institution="Institution13",IsCurrentAcademicStatus=true,UserID=users[6].Id},
            //    new AcademicDetails(){Degree="Degree14",Concentration="Concentration14",Institution="Institution14",IsCurrentAcademicStatus=false,UserID=users[7].Id},
            //    new AcademicDetails(){Degree="Degree15",Concentration="Concentration15",Institution="Institution15",IsCurrentAcademicStatus=true,UserID=users[7].Id},
            //    new AcademicDetails(){Degree="Degree16",Concentration="Concentration16",Institution="Institution16",IsCurrentAcademicStatus=false,UserID=users[8].Id},
            //    new AcademicDetails(){Degree="Degree17",Concentration="Concentration17",Institution="Institution17",IsCurrentAcademicStatus=true,UserID=users[8].Id},
            //    new AcademicDetails(){Degree="Degree18",Concentration="Concentration18",Institution="Institution18",IsCurrentAcademicStatus=false,UserID=users[9].Id},
            //    new AcademicDetails(){Degree="Degree19",Concentration="Concentration19",Institution="Institution19",IsCurrentAcademicStatus=true,UserID=users[9].Id},
            //};
            //List<ProfessionalDetails> profs = new List<ProfessionalDetails>()
            //{
            //    new ProfessionalDetails(){Designation="Designation 0",Department="Department1",Institution="Professional Institution 0", IsCurrentProfessionalStatus=false, UserID=users[0].Id},
            //    new ProfessionalDetails(){Designation="Designation 1",Department="Department2",Institution="Professional Institution 2", IsCurrentProfessionalStatus=false, UserID=users[0].Id},
            //    new ProfessionalDetails(){Designation="Designation2",Department="Department3",Institution="Professional Institution 3", IsCurrentProfessionalStatus=false, UserID=users[1].Id},
            //    new ProfessionalDetails(){Designation="Designation3",Department="Department4",Institution="Professional Institution 4", IsCurrentProfessionalStatus=false, UserID=users[1].Id},
            //    new ProfessionalDetails(){Designation="Designation4",Department="Department5",Institution="Professional Institution 5", IsCurrentProfessionalStatus=false, UserID=users[2].Id},
            //    new ProfessionalDetails(){Designation="Designation5",Department="Department6",Institution="Professional Institution 6", IsCurrentProfessionalStatus=false, UserID=users[2].Id},
            //    new ProfessionalDetails(){Designation="Designation6",Department="Department7",Institution="Professional Institution 7", IsCurrentProfessionalStatus=false, UserID=users[3].Id},
            //    new ProfessionalDetails(){Designation="Designation7",Department="Department8",Institution="Professional Institution 8", IsCurrentProfessionalStatus=false, UserID=users[3].Id},
            //    new ProfessionalDetails(){Designation="Designation8",Department="Department9",Institution="Professional Institution 9", IsCurrentProfessionalStatus=false, UserID=users[4].Id},
            //    new ProfessionalDetails(){Designation="Designation9",Department="Department10",Institution="Professional Institution 10", IsCurrentProfessionalStatus=false, UserID=users[4].Id},
            //    new ProfessionalDetails(){Designation="Designation10",Department="Department11",Institution="Professional Institution 11", IsCurrentProfessionalStatus=false, UserID=users[5].Id},
            //    new ProfessionalDetails(){Designation="Designation11",Department="Department12",Institution="Professional Institution 12", IsCurrentProfessionalStatus=false, UserID=users[5].Id},
            //    new ProfessionalDetails(){Designation="Designation12",Department="Department13",Institution="Professional Institution 13", IsCurrentProfessionalStatus=false, UserID=users[6].Id},
            //    new ProfessionalDetails(){Designation="Designation13",Department="Department14",Institution="Professional Institution 14", IsCurrentProfessionalStatus=false, UserID=users[6].Id},
            //    new ProfessionalDetails(){Designation="Designation14",Department="Department15",Institution="Professional Institution 15", IsCurrentProfessionalStatus=false, UserID=users[7].Id},
            //    new ProfessionalDetails(){Designation="Designation15",Department="Department16",Institution="Professional Institution 16", IsCurrentProfessionalStatus=false, UserID=users[7].Id},
            //    new ProfessionalDetails(){Designation="Designation16",Department="Department17",Institution="Professional Institution 17", IsCurrentProfessionalStatus=false, UserID=users[8].Id},
            //    new ProfessionalDetails(){Designation="Designation17",Department="Department18",Institution="Professional Institution 18", IsCurrentProfessionalStatus=false, UserID=users[8].Id},
            //    new ProfessionalDetails(){Designation="Designation18",Department="Department19",Institution="Professional Institution 19", IsCurrentProfessionalStatus=false, UserID=users[9].Id},
            //    new ProfessionalDetails(){Designation="Designation19",Department="Department20",Institution="Professional Institution 1", IsCurrentProfessionalStatus=false, UserID=users[9].Id},
            //};
            List<ApplicationRole> roles = new List<ApplicationRole>();

            List<Department> depts = new List<Department>()
            {
                new Department()
                {
                    DepartmentID="CSE",
                    FullName="Computer Science and Engineering"
                },
                new Department()
                {
                    DepartmentID="CEE",
                    FullName="Civil and Environmental Engineering"
                },
                new Department()
                {
                    DepartmentID="EEE",
                    FullName="Electrical and Electronics Engineering"
                },
                new Department()
                {
                    DepartmentID="MCE",
                    FullName="Mechanical and Chemical Engineering"
                },
                new Department()
                {
                    DepartmentID="TVE",
                    FullName="Technical and Vocational Education"
                }
            };
            int prID = 0;
            try
            {
                foreach (var v in depts)
                {
                    //if (db.Departments.Find(v.DepartmentID) == null)
                    //{
                    //    db.Departments.Add(v);
                    //    List<Program> progs = new List<Program>()
                    //    {
                    //        new Program(){ProgramID=++prID, ProgramName="B.Sc.", Concentration=Guid.NewGuid().ToString(),DepartmentID=v.DepartmentID},
                    //        new Program(){ProgramID=++prID, ProgramName="M.Sc.", Concentration=Guid.NewGuid().ToString(),DepartmentID=v.DepartmentID},
                    //        new Program(){ProgramID=++prID, ProgramName="Ph.D.", Concentration=Guid.NewGuid().ToString(),DepartmentID=v.DepartmentID}
                    //    };
                    //    db.Programs.AddRange(progs);

                    //    List<Course> c = new List<Course>();
                    //    for (int i = 4201; i < 4206; i++)
                    //    {
                    //        c.Add(new Course() { CourseCode = v.DepartmentID + " " + i.ToString(), ProgramID = progs[0].ProgramID, CourseName = v.DepartmentID + " " + progs[0].ProgramName });
                    //    }
                    //    for (int i = 6401; i < 6406; i++)
                    //    {
                    //        c.Add(new Course() { CourseCode = v.DepartmentID + " " + i.ToString(), ProgramID = progs[1].ProgramID, CourseName = v.DepartmentID + " " + progs[1].ProgramName });
                    //    }
                    //    for (int i = 6602; i < 6405; i++)
                    //    {
                    //        c.Add(new Course() { CourseCode = v.DepartmentID + " " + i.ToString(), ProgramID = progs[2].ProgramID, CourseName = v.DepartmentID + " " + progs[2].ProgramName });
                    //    }
                    //    db.Courses.AddRange(c);
                    //}
                }
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }



            foreach (var roleName in CommonVariables.ROLES)
            {
                var role = roleManager.FindByName(roleName.Value);
                if (role == null)
                {
                    role = new ApplicationRole(roleName.Value);
                    var roleresult = roleManager.Create(role);
                }
                roles.Add(role);
            }


            //for (int i = 0; i < users.Count; i++)
            //{
            //    var user = users[i];
            //    int roleIdx = (int)(i / 2);
            //    var role = roles[roleIdx];
            //    var password = passwords[i];
            //    if (userManager.FindByName(user.UserName) == null)
            //    {
            //        var res = userManager.Create(user, password);
            //        res = userManager.SetLockoutEnabled(user.Id, true);
            //        var rolesForUser = userManager.GetRoles(user.Id);
            //        if (!rolesForUser.Contains(role.Name))
            //        {
            //            var result = userManager.AddToRole(user.Id, role.Name);
            //        }

            //        db.UserDetails.Add(details[i]);
            //        db.Locations.Add(new Location() { UserDetails = details[i], Address = "Address: " + Guid.NewGuid().ToString(), City = "City: " + Guid.NewGuid().ToString(), Country = "Country: " + Guid.NewGuid().ToString() });

            //        db.AcademicDetails.Add(academics[i * 2]);
            //        db.AcademicDetails.Add(academics[i * 2 + 1]);
            //        db.Locations.Add(new Location() { AcademicDetails = academics[i * 2], Address = "Address: " + Guid.NewGuid().ToString(), City = "City: " + Guid.NewGuid().ToString(), Country = "Country: " + Guid.NewGuid().ToString() });
            //        db.Locations.Add(new Location() { AcademicDetails = academics[i * 2 + 1], Address = "Address: " + Guid.NewGuid().ToString(), City = "City: " + Guid.NewGuid().ToString(), Country = "Country: " + Guid.NewGuid().ToString() });


            //        db.ProfessionalDetails.Add(profs[i * 2]);
            //        db.ProfessionalDetails.Add(profs[i * 2 + 1]);
            //        db.Locations.Add(new Location() { ProfessionalDetails = profs[i * 2], Address = "Address: " + Guid.NewGuid().ToString(), City = "City: " + Guid.NewGuid().ToString(), Country = "Country: " + Guid.NewGuid().ToString() });
            //        db.Locations.Add(new Location() { ProfessionalDetails = profs[i * 2 + 1], Address = "Address: " + Guid.NewGuid().ToString(), City = "City: " + Guid.NewGuid().ToString(), Country = "Country: " + Guid.NewGuid().ToString() });

            //        if (userManager.GetRoles(user.Id).Contains("Faculty"))
            //        {
            //            Faculty faculty = new Faculty() { DepartmentID = depts[i % 2 == 0 ? 0 : 3].DepartmentID, FacultyID = details[i].UserID };
            //            db.Faculties.Add(faculty);
            //        }
            //        else if (userManager.GetRoles(user.Id).Contains("TA"))
            //        {
            //            TA ta = new TA() { DepartmentID = depts[i % 2 == 0 ? 0 : 3].DepartmentID, TAID = details[i].UserID };
            //            db.TAs.Add(ta);
            //        }
            //        else if (userManager.GetRoles(user.Id).Contains("Alumni"))
            //        {
            //            Alumni alumnus = new Alumni() { UserID = details[i].UserID, PassingYear = new DateTime(2014, 1, 1), ProgramID = 3 };
            //            db.Alumni.Add(alumnus);
            //        }
            //        else if (userManager.GetRoles(user.Id).Contains("Staff"))
            //        {
            //            Staff staff = new Staff() { StaffID = details[i].UserID, Designation = "Worker", DepartmentID = "CSE" };
            //            db.Staffs.Add(staff);
            //        }

            //    }

            //}
            db.SaveChanges();

        }
    }
}
