﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.AspNet.SignalR;
using IUT_Dept_Services.Hubs;
using IUT_Dept_Services.Repositories;
using Microsoft.Owin.Security.OAuth;
using IUT_Dept_Services.Providers;

[assembly: OwinStartup(typeof(IUT_Dept_Services.Startup))]

namespace IUT_Dept_Services
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            ConfigureAuth(app);
            GlobalHost.DependencyResolver.Register(
                typeof(ChatHub),
                () => new ChatHub(new ChattingRepository())
            );
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);

                map.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
                {
                    Provider = new QueryStringOAuthBearerProvider()
                });

                var hubConfiguration = new HubConfiguration
                {
                    Resolver = GlobalHost.DependencyResolver,
                    EnableJavaScriptProxies = false
                };
                map.RunSignalR(hubConfiguration);
            });
        }
    }
}
