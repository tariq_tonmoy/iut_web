﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace IUT_Dept_Services.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDetails>()
                .HasOptional(op => op.Address)
                .WithOptionalPrincipal(op => op.UserDetails)
                .WillCascadeOnDelete();
            modelBuilder.Entity<ProfessionalDetails>()
                .HasOptional(op => op.Address)
                .WithOptionalPrincipal(op => op.ProfessionalDetails);
            modelBuilder.Entity<AcademicDetails>()
                .HasOptional(op => op.Address)
                .WithOptionalPrincipal(op => op.AcademicDetails);
            modelBuilder.Entity<CustomEvent>()
              .HasOptional(op => op.EventLocation)
              .WithOptionalPrincipal(op => op.Event)
              .WillCascadeOnDelete();
            modelBuilder.Entity<Course>()
                 .HasOptional(op => op.Syllabus)
                 .WithRequired(op => op.Course)
                 .WillCascadeOnDelete(true);
            modelBuilder.Entity<Syllabus>()
                 .HasRequired(op => op.Course)
                 .WithOptional(op => op.Syllabus)
                 .WillCascadeOnDelete(true);
            modelBuilder.Entity<CustomEvent>()
                .HasRequired(s => s.Story)
                .WithMany(e => e.Events)
                .HasForeignKey<int>(s => s.StoryID)
                .WillCascadeOnDelete();


            modelBuilder.Entity<Department>().HasMany(d => d.Programs).WithOptional(p => p.Department).HasForeignKey(p => p.DepartmentID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Department>().HasMany(d => d.FacultyMembers).WithOptional(f => f.Department).HasForeignKey(f => f.DepartmentID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Department>().HasMany(d => d.TA_Members).WithOptional(t => t.Department).HasForeignKey(t => t.DepartmentID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Department>().HasMany(d => d.StaffMembers).WithOptional(t => t.Department).HasForeignKey(t => t.DepartmentID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Department>().HasMany(d => d.PendingRequests).WithOptional(t => t.RequestedDepartment).HasForeignKey(t => t.RequestedDepartmentID).WillCascadeOnDelete(true);

            modelBuilder.Entity<Program>().HasMany(p => p.PendingRequests).WithOptional(r => r.RequestedProgram).HasForeignKey(t => t.RequestedProgramID).WillCascadeOnDelete(true);
            modelBuilder.Entity<SyllabusReview>().HasMany(p => p.Syllabus).WithOptional(r => r.SyllabusReview).HasForeignKey(t => t.SyllabusReviewID).WillCascadeOnDelete(true);

            modelBuilder.Entity<UserDetails>().HasMany(group => group.CreatedResearchGroups).WithOptional(c => c.CreatedBy).HasForeignKey(t => t.CreatorID).WillCascadeOnDelete(false);
            modelBuilder.Entity<UserDetails>().HasMany(pub => pub.CreatedPublications).WithOptional(c => c.CreatedBy).HasForeignKey(t => t.CreatorID).WillCascadeOnDelete(false);
            modelBuilder.Entity<UserDetails>().HasMany(interest => interest.CreatedResearchInterests).WithOptional(c => c.CreatedBy).HasForeignKey(t => t.CreatorID).WillCascadeOnDelete(false);

            modelBuilder.Entity<UserDetails>().HasMany(chatGroup => chatGroup.CreatedChatGroups).WithOptional(c => c.Creator).HasForeignKey(t => t.CreatorID).WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }


        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<PendingRequest> PendingRequests { get; set; }
        public DbSet<PersonalDetails> PersonalDetails { get; set; }
        public DbSet<AcademicDetails> AcademicDetails { get; set; }
        public DbSet<ProfessionalDetails> ProfessionalDetails { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }
        public DbSet<SyllabusReview> SyllabusReviews { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<TA> TAs { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<CourseMaterial> CourseMaterials { get; set; }
        public DbSet<Alumni> Alumni { get; set; }
        public DbSet<Publication> Publications { get; set; }
        public DbSet<ResearchGroup> ResearchGroups { get; set; }
        public DbSet<ResearchInterest> ResearchInterests { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<CustomEvent> CustomEvents { get; set; }
        public DbSet<ChatGroup> ChatGroups { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageStatus> MessageStatuses { get; set; }


    }
}