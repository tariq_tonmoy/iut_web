﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class CourseLoad
    {
        public int CourseID { get; set; }
        public String FacultyID { get; set; }
        public int FacultyLoad { get; set; }
    }
}