﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class MessageDTO
    {
        public int MessageID { get; set; }
        public string MessageFileType { get; set; }
        public byte[] MessageFile { get; set; }
        public string MessageFileName { get; set; }
        public string MessageText { get; set; }
        public DateTime TimeStamp { get; set; }
        public int GroupID { get; set; }
        public string SenderID { get; set; }
    }
}