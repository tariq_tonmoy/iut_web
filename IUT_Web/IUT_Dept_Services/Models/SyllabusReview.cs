﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class SyllabusReview
    {
        public int SyllabusReviewID { get; set; }
        public string StartAY { get; set; }
        public string EndAY { get; set; }
        public bool IsCurrentlyActive { get; set; }
        public float Version { get; set; }
        public string Comment { get; set; }

        public int ProgramID { get; set; }
        public virtual ICollection<Syllabus> Syllabus { get; set; }
    }
}