﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class MessageStatus
    {
        [Key, ForeignKey("Message"), Column(Order = 0)]
        public int MessageID { get; set; }
        [Key, ForeignKey("ChatGroupMember"), Column(Order = 1)]
        public string ChatGroupMemberID { get; set; }
        public virtual UserDetails ChatGroupMember { get; set; }
        public virtual Message Message { get; set; }
        public bool HasSeen { get; set; }
        public DateTime? TimeStamp { get; set; }
    }
}