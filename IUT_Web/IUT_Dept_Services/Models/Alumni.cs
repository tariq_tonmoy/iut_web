﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Alumni
    {
        [Key]
        public int AlumniID { get; set; }

        [Required]
        [ForeignKey("UserDetails")]
        public string UserID { get; set; }
        public virtual UserDetails UserDetails { get; set; }
        public int? AdmissionYear { get; set; }
        public bool IsFocused { get; set; }
        public DateTime? PassingYear { get; set; }
        public DateTime? EnrollmentYear { get; set; }
        public string StudentID { get; set; }

        [Required]
        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }
    }

}