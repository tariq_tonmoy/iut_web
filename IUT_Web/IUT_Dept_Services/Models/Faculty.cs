﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Faculty
    {
        public Faculty()
        {
            this.Courses = new List<Course>();
            this.CourseMaterials = new List<CourseMaterial>();

        }
        [Key, ForeignKey("UserDetails")]
        public string FacultyID { get; set; }
        public string Designation { get; set; }
        public string OfficeAddress { get; set; }
        public string Biography { get; set; }
        public int? Priority { get; set; }
        public string DepartmentID { get; set; }
        public virtual Department Department { get; set; }
        public UserDetails UserDetails { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<CourseMaterial> CourseMaterials { get; set; }
        
    }
}