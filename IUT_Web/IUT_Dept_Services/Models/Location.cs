﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Location
    {
        public int LocationID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }


        public virtual UserDetails UserDetails { get; set; }
        public virtual ProfessionalDetails ProfessionalDetails { get; set; }
        public virtual AcademicDetails AcademicDetails { get; set; }
        public virtual CustomEvent Event { get; set; }

    }
}