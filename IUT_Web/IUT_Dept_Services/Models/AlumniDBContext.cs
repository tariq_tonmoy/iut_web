﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class AlumniDBContext : DbContext
    {
        public AlumniDBContext()
            : base("AlumniDBConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        
        public static AlumniDBContext Create()
        {
            return new AlumniDBContext();
        }

        public DbSet<AlumniSummary> AlumniSummaries{ get; set; }
    }
}