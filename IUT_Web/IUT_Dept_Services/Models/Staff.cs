﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Staff
    {
        [Key, ForeignKey("UserDetails")]
        public string StaffID { get; set; }
        public string Designation { get; set; }
        public string OfficeAddress { get; set; }
        public string DepartmentID { get; set; }
        public int? Priority { get; set; }
        public virtual Department Department { get; set; }
        public UserDetails UserDetails { get; set; }
    }
}