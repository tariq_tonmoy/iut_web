﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class AcademicTimeline
    {
        public int AcademicTimelineID { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime MidExamDate { get; set; }
        public DateTime FinalExamDate { get; set; }
        public string Semester { get; set; }

    }
}