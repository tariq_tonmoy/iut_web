﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Program
    {
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public string Concentration { get; set; }


        public string DepartmentID { get; set; }
        public string ProgramDescription { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public virtual ICollection<Alumni> AlumniMembers { get; set; }
        public virtual ICollection<PendingRequest> PendingRequests { get; set; }


    }

    
}