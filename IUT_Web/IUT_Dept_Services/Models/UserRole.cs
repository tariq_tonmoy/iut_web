﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class UserRole
    {
        public string RoleID { get; set; }
        public string RoleName { get; set; }
    }
}