﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class TA
    {
        [Key, ForeignKey("UserDetails")]
        public string TAID { get; set; }
        public float CreditHour { get; set; }
        public string DepartmentID { get; set; }
        public virtual Department Department { get; set; }
        public UserDetails UserDetails { get; set; }
    }
}