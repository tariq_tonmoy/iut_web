﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Department
    {
        public Department()
        {
            this.EstablishedIn = DateTime.UtcNow;
        }
        public string DepartmentID { get; set; }
        public string FullName { get; set; }
        public string Location { get; set; }
        public string Email { get; set; }
        public DateTime EstablishedIn { get; set; }
        public string About { get; set; }
        public string MessageFromHead { get; set; }

        public byte[] Image { get; set; }
        public string ImageType { get; set; }

        public string DepartmentHeadID { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
        public virtual ICollection<Faculty> FacultyMembers { get; set; }
        public virtual ICollection<TA> TA_Members { get; set; }
        public virtual ICollection<Staff> StaffMembers { get; set; }
        public virtual ICollection<PendingRequest> PendingRequests { get; set; }

    }
}