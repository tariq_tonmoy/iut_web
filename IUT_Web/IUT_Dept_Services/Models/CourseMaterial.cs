﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class CourseMaterial
    {
        public CourseMaterial()
        {
            this.Courses = new List<Course>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string MaterialType { get; set; }
        public string MaterialFileName { get; set; }
        public byte[] Material { get; set; }

        [Required]
        [ForeignKey("Owner")]
        public string FacultyID { get; set; }
        public Faculty Owner { get; set; }
        public virtual ICollection<Course> Courses { get; set; }

    }
}