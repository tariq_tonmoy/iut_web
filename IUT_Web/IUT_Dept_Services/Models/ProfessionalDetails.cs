﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ProfessionalDetails
    {
        public int ProfessionalDetailsID { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Institution { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public bool IsCurrentProfessionalStatus { get; set; }
        public string ProfessionType { get; set; }

        public virtual Location Address { get; set; }
        [Required]
        [ForeignKey("UserDetails")]
        public string UserID { get; set; }
        public virtual UserDetails UserDetails { get; set; }
    }
}