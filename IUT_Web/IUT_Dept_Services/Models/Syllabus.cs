﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Syllabus
    {
        public int SyllabusID { get; set; }
       
        public string SyllabusDetails { get; set; }
        public string TextBooks { get; set; }
        
        [ForeignKey("SyllabusReview")]
        public int? SyllabusReviewID { get; set; }

        

        public virtual SyllabusReview SyllabusReview { get; set; }
        public virtual Course Course { get; set; }
    }
}