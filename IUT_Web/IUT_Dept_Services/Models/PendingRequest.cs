﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class PendingRequest
    {
        public PendingRequest()
        {
            this.SubmissionDate = DateTime.UtcNow;
        }
        [Key]
        public int RequestID { get; set; }
        public string MessageToAdmin { get; set; }

        [Required]
        [ForeignKey("RequestedUser")]
        public string RequestUserID { get; set; }
        [Required]
        [ForeignKey("RequestedRole")]
        public string RequestedRoleID { get; set; }
        public virtual ApplicationRole RequestedRole { get; set; }
        public virtual UserDetails RequestedUser { get; set; }

        public string RequestedDepartmentID { get; set; }
        public virtual Department RequestedDepartment { get; set; }
        public int? RequestedProgramID { get; set; }
        public virtual Program RequestedProgram { get; set; }
        public DateTime SubmissionDate { get; set; }
        
    }
}