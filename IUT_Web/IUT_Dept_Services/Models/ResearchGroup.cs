﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ResearchGroup
    {
        public ResearchGroup()
        {
            this.Members = new List<UserDetails>();
            this.Publications = new List<Publication>();
            this.Interests = new List<ResearchInterest>();
        }
        [Key]
        public int GroupID { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(400)]
        public string GroupTitle { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public string ImageType { get; set; }

        public string CreatorID { get; set; }
        public virtual UserDetails CreatedBy { get; set; }

        public virtual ICollection<UserDetails> Members { get; set; }
        public virtual ICollection<Publication> Publications { get; set; }
        public virtual ICollection<ResearchInterest> Interests { get; set; }


    }
    
}