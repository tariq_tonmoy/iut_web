﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Story
    {
        public int StoryID { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool IsFocused { get; set; }
        public string Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Author { get; set; }

        public byte[] StoryImg { get; set; }
        public string ImageType { get; set; }
        public string DescriptionType { get; set; }

        [Required]
        [ForeignKey("Dept")]
        public string DeptID { get; set; }
        public Department Dept { get; set; }
        public virtual ICollection<CustomEvent> Events { get; set; }
    }
    public class CustomEvent
    {
       
        public int CustomEventID { get; set; }
        public string Title { get; set; }
        
        public int StoryID { get; set; }
        public Story Story { get; set; }
        public DateTime? EventStart { get; set; }
        public DateTime? EventEnds { get; set; }
        public virtual Location EventLocation { get; set; }
    }
}