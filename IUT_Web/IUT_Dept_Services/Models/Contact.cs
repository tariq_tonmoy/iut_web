﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Contact
    {
        public int ContactID { get; set; }
        public string ContactType { get; set; }
        public string ContactDetails { get; set; }

        [Required]
        [ForeignKey("UserDetails")]
        public string UserID { get; set; }
        public virtual UserDetails UserDetails { get; set; }
    }
}