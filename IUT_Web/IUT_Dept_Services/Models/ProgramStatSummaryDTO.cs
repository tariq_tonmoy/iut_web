﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ProgramStatSummaryDTO
    {
        public ProgramStatSummaryDTO()
        {
            ProfessionStatusStat = new Dictionary<PROFESSION_STATUS_ENUM, int>()
            {
                {PROFESSION_STATUS_ENUM.EMPLOYED, 0},
                {PROFESSION_STATUS_ENUM.UNEMPLOYED,0},
                {PROFESSION_STATUS_ENUM.STUDYING,0},
            };

            ProfessionTypeStat = new Dictionary<PROFESSION_TYPES_ENUM, int>()
            {
                {PROFESSION_TYPES_ENUM.ACADEMIA, 0},
                {PROFESSION_TYPES_ENUM.INDUSTRY, 0},
                {PROFESSION_TYPES_ENUM.BUSINESS, 0},
                {PROFESSION_TYPES_ENUM.GOVERNMENT, 0},
                {PROFESSION_TYPES_ENUM.RND, 0},
                {PROFESSION_TYPES_ENUM.OTHERS, 0},
            };
        }
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public string DepartmentID { get; set; }
        public Dictionary<PROFESSION_TYPES_ENUM, int> ProfessionTypeStat { get; set; }
        public Dictionary<PROFESSION_STATUS_ENUM, int> ProfessionStatusStat { get; set; }
        public int EnrollmentYear { get; set; }
    }
}