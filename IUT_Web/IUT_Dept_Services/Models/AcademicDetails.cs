﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class AcademicDetails
    {
        public int AcademicDetailsID { get; set; }
        public string Degree { get; set; }
        public string Concentration { get; set; }
        public string Institution { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public bool IsCurrentAcademicStatus { get; set; }

        public virtual Location Address { get; set; }

        [Required]
        [ForeignKey("UserDetails")]
        public string UserID { get; set; }
        public virtual UserDetails UserDetails { get; set; }
    }
}