﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public enum ROLENAMES
    {
        ADMIN, FACULTY, STAFF, TA, ALUMNI, FACULTYONLEAVE
    };
    public enum PROFESSION_TYPES_ENUM
    {
        ACADEMIA, INDUSTRY, RND, GOVERNMENT, BUSINESS, OTHERS
    };
    public enum PROFESSION_STATUS_ENUM
    {
        EMPLOYED, STUDYING, UNEMPLOYED
    };

    public class CommonVariables
    {
        public static string SELF_URL = "";
        public static readonly string[] CLIENT_URLs =
        {
            "http://localhost:4200/",
            "https://csenew.iutoic-dhaka.edu/",
            "https://cse.iutoic-dhaka.edu/",
            "http://cse.iutoic-dhaka.edu/"
        };
        public const string ADMIN = "Admin";
        public const string FACULTY = "Faculty";
        public const string STAFF = "Staff";
        public const string TA = "TA";
        public const string ALUMNI = "Alumni";
        public const string FACULTYONLEAVE = "FacultyOnLeave";

        public const string ACADEMIA = "Academia";
        public const string INDUSTRY = "Industry";
        public const string RND = "Research and Development";
        public const string GOVERNMENT = "Government";
        public const string BUSINESS = "Business / Start Up";
        public const string OTHERS = "Others";

        public const string EMPLOYED = "Employed";
        public const string UNEMPLOYED = "Unemployed";
        public const string STUDYING = "Studying";


        public static Dictionary<ROLENAMES, string> ROLES = new Dictionary<ROLENAMES, string>() {
            { ROLENAMES.ADMIN,ADMIN},
            { ROLENAMES.FACULTY,FACULTY },
            { ROLENAMES.STAFF,STAFF },
            { ROLENAMES.TA,TA },
            { ROLENAMES.ALUMNI,ALUMNI},
            { ROLENAMES.FACULTYONLEAVE,FACULTY}
        };

        public static Dictionary<PROFESSION_TYPES_ENUM, string> PROFESSION_TYPES = new Dictionary<PROFESSION_TYPES_ENUM, string>() {
            { PROFESSION_TYPES_ENUM.ACADEMIA,ACADEMIA},
            { PROFESSION_TYPES_ENUM.INDUSTRY,INDUSTRY},
            { PROFESSION_TYPES_ENUM.RND,RND},
            { PROFESSION_TYPES_ENUM.GOVERNMENT,GOVERNMENT},
            { PROFESSION_TYPES_ENUM.BUSINESS,BUSINESS},
            { PROFESSION_TYPES_ENUM.OTHERS,OTHERS}
        };

        public static Dictionary<PROFESSION_STATUS_ENUM, string> PROFESSION_STATUSES = new Dictionary<PROFESSION_STATUS_ENUM, string>() {
            { PROFESSION_STATUS_ENUM.EMPLOYED,EMPLOYED},
            { PROFESSION_STATUS_ENUM.STUDYING,STUDYING},
            { PROFESSION_STATUS_ENUM.UNEMPLOYED,UNEMPLOYED}
        };

        public static string[] PERSONNEL_ROLES = {
            ROLES[ROLENAMES.FACULTY],
            ROLES[ROLENAMES.TA],
            ROLES[ROLENAMES.STAFF],
            ROLES[ROLENAMES.FACULTYONLEAVE]
        };
        public static string[] RESEARCH_ROLES =
        {
            ROLES[ROLENAMES.FACULTY],
            ROLES[ROLENAMES.ALUMNI]
        };
    }

}