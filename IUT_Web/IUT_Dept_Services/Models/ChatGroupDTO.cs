﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ChatGroupDTO
    {
        public int ChatGroupID { get; set; }
        public string Name { get; set; }
        public string CreatorID { get; set; }
        public  UserDetails Creator { get; set; }
        public IEnumerable<UserDetails> GroupMembers { get; set; }
        public IEnumerable<Message> Messages { get; set; }
    }
}