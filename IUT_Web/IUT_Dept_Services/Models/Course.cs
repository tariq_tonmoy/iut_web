﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Course
    {
        public Course()
        {
            this.CourseMaterials = new List<CourseMaterial>();
            this.FacultyList = new List<Faculty>();

        }
        public int CourseID { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public string Semester { get; set; }
        public bool IsOffered { get; set; }
        public float Credit { get; set; }
        public float CreditHour { get; set; }
        public int TotalSections { get; set; }

        [ForeignKey("Program")]
        [Required]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        public virtual Syllabus Syllabus { get; set; }
        public virtual ICollection<Faculty> FacultyList { get; set; }
        public virtual ICollection<CourseMaterial> CourseMaterials { get; set; }
    }
}