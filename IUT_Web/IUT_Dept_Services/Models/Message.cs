﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Message
    {
        public int MessageID { get; set; }
        public string MessageFileType { get; set; }
        public byte[] MessageFile { get; set; }
        public string MessageFileName { get; set; }
        public string MessageText { get; set; }
        public DateTime TimeStamp { get; set; }

        [ForeignKey("TargetChatGroup")]
        public int GroupID { get; set; }
        [ForeignKey("Sender")]
        public string SenderID { get; set; }
        public virtual ChatGroup TargetChatGroup { get; set; }
        public virtual UserDetails Sender { get; set; }
        public virtual ICollection<MessageStatus> MessageStatuses { get; set; }
    }
}