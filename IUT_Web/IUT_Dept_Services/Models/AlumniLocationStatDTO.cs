﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class LocationStatSummary
    {
        public string City { get; set; }
        public string Country { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int Count { get; set; }
    }
    public class AlumniLocationStatDTO
    {
        public List<LocationStatSummary> CurrentLocationStat { get; set; }
        public List<LocationStatSummary> AcademicLocationStat { get; set; }
        public List<LocationStatSummary> ProfessionLocationStat { get; set; }

    }
}