﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class Publication
    {
        public Publication()
        {
            this.Authors = new List<UserDetails>();
            this.ResearchInterests = new List<ResearchInterest>();
            this.ResearchGroups = new List<ResearchGroup>();
        }

        public int PublicationID { get; set; }
        [Required]
        [Index("IX_Title_PublishedIn_Year", 1, IsUnique = true)]
        [MaxLength(400)]
        public string Title { get; set; }
        [Required]
        [Index("IX_Title_PublishedIn_Year", 2, IsUnique = true)]
        [MaxLength(400)]
        public string PublishedIn { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        [Index("IX_Title_PublishedIn_Year", 3, IsUnique = true)]
        public int Year { get; set; }

        public string AuthorList { get; set; }
        public string Abstract { get; set; }
        public string Url { get; set; }
        public string MaterialName { get; set; }
        public string MaterialType { get; set; }

        public byte[] PublicationMaterial { get; set; }


        public string CreatorID { get; set; }
        public virtual UserDetails CreatedBy { get; set; }

        public virtual ICollection<UserDetails> Authors { get; set; }
        public virtual ICollection<ResearchInterest> ResearchInterests { get; set; }
        public virtual ICollection<ResearchGroup> ResearchGroups { get; set; }


    }
}