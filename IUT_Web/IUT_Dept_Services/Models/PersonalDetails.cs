﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IUT_Dept_Services.Models
{
    public class PersonalDetails
    {
        [Key]
        [ForeignKey("UserDetails")]
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BloodGroup { get; set; }
        public DateTime? DoB { get; set; }
        public byte[] Image { get; set; }
        public string ImageType { get; set; }
        public string Nationality { get; set; }


        public virtual UserDetails UserDetails { get; set; }

    }
}