﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class AlumniSummary
    {
        [Key]
        public int ID { get; set; }
        public string StudentID { get; set; }
        public string Name { get; set; }
        public string EmailPrimary { get; set; }
        public string PhonePrimary { get; set; }
        public int Batch { get; set; }
        public string Department { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public bool? IsNewUser { get; set; }
    }
}