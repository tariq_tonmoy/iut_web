﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class UserDetails
    {
        public UserDetails()
        {
            this.ResearchGroups = new List<ResearchGroup>();
            this.Publications = new List<Publication>();
            this.ResearchInterests = new List<ResearchInterest>();
            this.ChatGroups = new List<ChatGroup>();
        }
        [Key, ForeignKey("User")]
        public string UserID { get; set; }
        public Boolean IsLoggedIn { get; set; }


        public ApplicationUser User { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual TA TA { get; set; }
        public virtual ICollection<Alumni> Alumni { get; set; }
        public virtual Staff Staff { get; set; }

        public virtual Location Address { get; set; }
        public virtual PersonalDetails PersonalInfo { get; set; }



        public virtual ICollection<ProfessionalDetails> WorkPlaces { get; set; }
        public virtual ICollection<AcademicDetails> AcademicPlaces { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<PendingRequest> PendingRequests { get; set; }

        public virtual ICollection<Publication> Publications { get; set; }
        public virtual ICollection<ResearchInterest> ResearchInterests { get; set; }
        public virtual ICollection<ResearchGroup> ResearchGroups { get; set; }

        public virtual ICollection<Publication> CreatedPublications { get; set; }
        public virtual ICollection<ResearchInterest> CreatedResearchInterests { get; set; }
        public virtual ICollection<ResearchGroup> CreatedResearchGroups { get; set; }

        public virtual ICollection<ChatGroup> CreatedChatGroups { get; set; }
        public virtual ICollection<ChatGroup> ChatGroups { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<MessageStatus> MessageStatuses { get; set; }
    }
}