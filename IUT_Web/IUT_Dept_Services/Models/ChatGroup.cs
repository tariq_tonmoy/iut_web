﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ChatGroup
    {
        public ChatGroup()
        {
            if (this.GroupMembers == null)
                this.GroupMembers = new List<UserDetails>();
        }
        public int ChatGroupID { get; set; }
        public string Name { get; set; }
        public string CreatorID { get; set; }
        public virtual UserDetails Creator { get; set; }

        public virtual ICollection<UserDetails> GroupMembers { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}