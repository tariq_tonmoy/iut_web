﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Models
{
    public class ResearchInterest
    {
        public ResearchInterest()
        {
            this.Members = new List<UserDetails>();
            this.Publications = new List<Publication>();
            this.ResearchGroups = new List<ResearchGroup>();
        }
        [Key]
        public int InterestID { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(400)]
        public string InterestTitle { get; set; }
        public string InterestDescription { get; set; }


        public string CreatorID { get; set; }
        public virtual UserDetails CreatedBy { get; set; }

        public virtual ICollection<UserDetails> Members { get; set; }
        public virtual ICollection<ResearchGroup> ResearchGroups { get; set; }
        public virtual ICollection<Publication> Publications { get; set; }


    }
}