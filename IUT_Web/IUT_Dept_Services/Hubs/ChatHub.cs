﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IUT_Dept_Services.Models;
using IUT_Dept_Services.Providers;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;

namespace IUT_Dept_Services.Hubs
{
    [AuthorizeClaims]
    public class ChatHub : Hub
    {
        IChattingRepository chattingRepository;
        private readonly static ConnectionMapping _connections =
            new ConnectionMapping();
        public ChatHub(IChattingRepository repository) : base()
        {
            chattingRepository = repository;
        }
        public override Task OnConnected()
        {
            var c = Context.ConnectionId;
            var id = Context.User.Identity.GetUserId();
            _connections.Add(id, c);
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            var id = Context.User.Identity.GetUserId();

            if (!_connections.GetConnections(id).Contains(Context.ConnectionId))
            {
                var c = Context.ConnectionId;
                _connections.Add(id, Context.ConnectionId);
                addUserToChatGroups(id);
            }

            return base.OnReconnected();
        }


        public override Task OnDisconnected(bool stopCalled)
        {
            var id = Context.User.Identity.GetUserId();
            _connections.Remove(id, Context.ConnectionId);
            removeUserFromChatGroups(id, Context.ConnectionId);

            if (!_connections.HasKey(id))
            {
                Clients.All.loggedOut(id);
            }

            return base.OnDisconnected(stopCalled);
        }
        public void LoggedIn()
        {
            var id = Context.User.Identity.GetUserId();
            Clients.All.loggedIn(_connections.GetAllConnectedUsers());
            addUserToChatGroups(id);
        }

        private async Task addUserToChatGroups(string id)
        {
            List<ChatGroup> groups = await chattingRepository.GetChatGroupsAsync(id, Context.User.Identity.GetUserId());
            foreach (var group in groups)
            {
                JoinRoom(id, group.ChatGroupID.ToString());
            }
        }

        private async Task removeUserFromChatGroups(string userID, string connectionID)
        {
            List<ChatGroup> groups = await chattingRepository.GetChatGroupsAsync(userID, Context.User.Identity.GetUserId());
            foreach (var group in groups)
            {
                await Groups.Remove(connectionID, group.ChatGroupID.ToString());
            }
        }

        public void AddUsersToChatGroup(List<string> userIDs, int ChatGroupID)
        {
            foreach (var v in userIDs)
            {
                if (_connections.HasKey(v))
                {
                    JoinRoom(v, ChatGroupID.ToString());
                    foreach (var conn in _connections.GetConnections(v))
                        Clients.Client(conn).addedToNewGroup(ChatGroupID);
                }
            }
            Clients.Group(ChatGroupID.ToString()).addUsersToGroup(ChatGroupID, userIDs);
        }

        public void RemoveUsersFromChatGroup(List<string> userIDs, int ChatGroupID)
        {
            foreach (var userID in userIDs)
            {
                if (_connections.HasKey(userID))
                {
                    foreach (var conn in _connections.GetConnections(userID))
                    {
                        Groups.Remove(conn, ChatGroupID.ToString());
                        Clients.Client(conn).removedFromChatGroup(ChatGroupID);
                    }
                }
            }
            Clients.Group(ChatGroupID.ToString()).removeUsersFromGroup(ChatGroupID,userIDs);
        }

        public void JoinRoom(string userID, string chatRoomName)
        {
            foreach (var conn in _connections.GetConnections(userID))
                Groups.Add(conn, chatRoomName);
        }

        public void SendMessage(MessageDTO message)
        {
            Clients.Group(message.GroupID.ToString()).messageReceived(message);
        }

        public void DeleteMessage(MessageDTO message)
        {
            Clients.Group(message.GroupID.ToString()).deleteMessage(message.MessageID, message.GroupID);
        }

        public void DeleteChatGroup(ChatGroupDTO chatGroup)
        {
            Clients.Group(chatGroup.ChatGroupID.ToString()).removedGroup(chatGroup.ChatGroupID);
        }

        public void EditChatGroup(int chatGroupID,object chatGroup)
        {
            Clients.Group(chatGroupID.ToString()).editedGroup(chatGroup);
        }
    }
}