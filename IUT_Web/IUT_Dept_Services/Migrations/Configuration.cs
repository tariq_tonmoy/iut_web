namespace IUT_Dept_Services.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IUT_Dept_Services.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "IUT_Dept_Services.Models.ApplicationDbContext";
        }

        protected override void Seed(IUT_Dept_Services.Models.ApplicationDbContext context)
        {
            ApplicationDbInitializer.InitializeIdentityForEF(context);
        }
    }
}
