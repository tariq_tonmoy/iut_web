﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace IUT_Dept_Services.Repositories
{
    public class PersonnelRepository<T> : IPersonnelRepository<T>
    {
        ApplicationDbContext db = new ApplicationDbContext();
        ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

        public bool DeletePersonnel( T personnel)
        {
            try
            {
                var type = typeof(T);
                if (type == typeof(Faculty))
                {
                    var faculty = (Faculty)Convert.ChangeType(personnel, typeof(Faculty));
                    db.Faculties.Remove(db.Faculties.FirstOrDefault(x => x.FacultyID == faculty.FacultyID));
                    userManager.RemoveFromRole(faculty.FacultyID, CommonVariables.ROLES[ROLENAMES.FACULTY]);
                }
                else if (type == typeof(TA))
                {
                    var ta = (TA)Convert.ChangeType(personnel, typeof(TA));

                    db.TAs.Remove(db.TAs.FirstOrDefault(x => x.TAID == ta.TAID));
                    userManager.RemoveFromRole(ta.TAID, CommonVariables.ROLES[ROLENAMES.TA]);

                }
                else if (type == typeof(Staff))
                {
                    var staff = (Staff)Convert.ChangeType(personnel, typeof(Staff));

                    db.Staffs.Remove(db.Staffs.FirstOrDefault(x => x.StaffID == staff.StaffID ));
                    userManager.RemoveFromRole(staff.StaffID, CommonVariables.ROLES[ROLENAMES.STAFF]);

                }
                else return false;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<T> GetPersonnelByDepartment(Department department)
        {
            var type = typeof(T);
            try
            {
                if (type == typeof(Faculty))
                {
                    var v = (from x in db.Faculties
                             where x.DepartmentID == department.DepartmentID
                             select x).AsNoTracking().OrderBy(x => x.Priority).ToList().Where(y => userManager.GetRoles(y.FacultyID).Contains(CommonVariables.ROLES[ROLENAMES.FACULTY]));
                    return (IEnumerable<T>)v;
                }
                else if (type == typeof(TA))
                {
                    var v = (from x in db.TAs
                             where x.DepartmentID == department.DepartmentID
                             select x).AsNoTracking().ToList().Where(y => userManager.GetRoles(y.TAID).Contains(CommonVariables.ROLES[ROLENAMES.TA]));
                    return (IEnumerable<T>)v;
                }
                else if (type == typeof(Staff))
                {
                    var v = (from x in db.Staffs
                             where x.DepartmentID == department.DepartmentID
                             select x).AsNoTracking().OrderBy(x => x.Priority).ToList().Where(y => userManager.GetRoles(y.StaffID).Contains(CommonVariables.ROLES[ROLENAMES.STAFF]));
                    return (IEnumerable<T>)v;
                }
            }
            catch (Exception)
            {

                return null;
            }
            return null;
        }

        public IEnumerable<T> GetPersonnelDetails(string userID)
        {
            var type = typeof(T);
            try
            {
                if (type == typeof(Faculty))
                {
                    var v = (from f in db.Faculties
                             where f.FacultyID == userID
                             select f).AsNoTracking().ToList();
                    return (IEnumerable<T>)v;
                }
                else if (type == typeof(TA))
                {
                    var v = (from f in db.TAs
                             where f.TAID == userID
                             select f).AsNoTracking().ToList();
                    return (IEnumerable<T>)v;
                }
                else if (type == typeof(Staff))
                {
                    var v = (from f in db.Staffs
                             where f.StaffID == userID
                             select f).AsNoTracking().ToList();
                    return (IEnumerable<T>)v;
                }
            }
            catch (Exception)
            {


            }

            return null;

        }

        public bool IsValidPersonnelUpdateRequest(T personnel)
        {
            var type = typeof(T);
            try
            {
                if (type == typeof(Faculty))
                {
                    var faculty = (Faculty)Convert.ChangeType(personnel, typeof(Faculty));
                    var v = (from f in db.Faculties
                             where f.FacultyID == faculty.FacultyID
                             && f.Priority == faculty.Priority
                             && f.DepartmentID == faculty.DepartmentID
                             && db.Departments.Contains(db.Departments.FirstOrDefault(x => x.DepartmentID == faculty.DepartmentID))
                             select f).AsNoTracking().ToList().Where(y => userManager.GetRoles(y.FacultyID).Contains(CommonVariables.ROLES[ROLENAMES.FACULTY])).ToList();
                    return v.Count > 0 ? true : false;
                }
                else if (type == typeof(TA))
                {
                    var ta = (TA)Convert.ChangeType(personnel, typeof(TA));
                    var v = (from f in db.TAs
                             where f.TAID == ta.TAID
                             && f.DepartmentID == ta.DepartmentID
                             && db.Departments.Contains(db.Departments.FirstOrDefault(x => x.DepartmentID == ta.DepartmentID))
                             select f).AsNoTracking().ToList().Where(y => userManager.GetRoles(y.TAID).Contains(CommonVariables.ROLES[ROLENAMES.TA])).ToList();
                    return v.Count > 0 ? true : false;
                }
                else if (type == typeof(Staff))
                {
                    var staff = (Staff)Convert.ChangeType(personnel, typeof(Staff));
                    var v = (from f in db.Staffs
                             where f.StaffID == staff.StaffID
                             && f.Priority == staff.Priority
                             && f.DepartmentID == staff.DepartmentID
                             && db.Departments.Contains(db.Departments.FirstOrDefault(x => x.DepartmentID == staff.DepartmentID))
                             select f).AsNoTracking().ToList().Where(y => userManager.GetRoles(y.StaffID).Contains(CommonVariables.ROLES[ROLENAMES.STAFF])).ToList();
                    return v.Count > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {


            }
            return false;
        }

        public bool UpdatePersonnelDetails(string userID, T personnel)
        {
            var type = typeof(T);
            try
            {
                if (type == typeof(Faculty))
                {
                    var faculty = (Faculty)Convert.ChangeType(personnel, typeof(Faculty));

                    if (userID == faculty.FacultyID && IsValidPersonnelUpdateRequest(personnel))
                    {
                        try
                        {
                            db.Entry(faculty).State = EntityState.Modified;
                            db.SaveChanges();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    else return false;
                }
                else if (type == typeof(TA))
                {
                    var ta = (TA)Convert.ChangeType(personnel, typeof(TA));

                    if (userID == ta.TAID && IsValidPersonnelUpdateRequest(personnel))
                    {
                        try
                        {
                            db.Entry(ta).State = EntityState.Modified;
                            db.SaveChanges();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    else return false;
                }
                else if (type == typeof(Staff))
                {
                    var staff = (Staff)Convert.ChangeType(personnel, typeof(Staff));

                    if (userID == staff.StaffID && IsValidPersonnelUpdateRequest(personnel))
                    {
                        try
                        {
                            db.Entry(staff).State = EntityState.Modified;
                            db.SaveChanges();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    else return false;
                }
            }
            catch (Exception)
            {


            }
            return false;
        }

        public bool UpdatePersonnelPriority(string userID, int? priority)
        {

            var type = typeof(T);
            try
            {
                if (type == typeof(Faculty))
                {

                    var faculty = (from f in db.Faculties
                                   where f.FacultyID == userID
                                   select f).AsNoTracking().FirstOrDefault();
                    if (faculty == null) return false;
                    if (priority != null)
                    {
                        var faculties = (from f in db.Faculties
                                         where f.DepartmentID == faculty.DepartmentID
                                         && f.Priority != null
                                         && f.FacultyID != userID
                                         && f.Priority == priority
                                         select f).ToList();
                        if (faculties.Count != 0)
                            return false;
                    }
                    try
                    {
                        faculty.Priority = priority;
                        db.Entry(faculty).State = EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }

                }
                else if (type == typeof(Staff))
                {

                    var staff = (from s in db.Staffs
                                 where s.StaffID == userID
                                 select s).AsNoTracking().FirstOrDefault();
                    if (staff == null) return false;
                    if (priority != null)
                    {
                        var staffs = (from s in db.Staffs
                                      where s.DepartmentID == s.DepartmentID
                                      && s.Priority != null
                                      && s.StaffID != userID
                                      && s.Priority == priority
                                      select s).ToList();
                        if (staffs.Count != 0)
                            return false;
                    }
                    try
                    {
                        staff.Priority = priority;
                        db.Entry(staff).State = EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {


            }
            return false;
        }
    }
}