﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IAdminRepository
    {
        IQueryable<PendingRequest> FindPendingRequests();
        IQueryable<PendingRequest> FindPendingRequests(UserDetails userWithID);
        IQueryable<PendingRequest> FindPendingRequests(UserDetails userWithID, ApplicationRole roleWithID);
        IQueryable<PendingRequest> FindPendingRequests(ApplicationRole roleWithID);

        bool ApproveRequest(int RequestID);
        bool ApproveRequest(PendingRequest request);

        bool RemoveRequest(int RequestID);
        bool RemoveRequest(PendingRequest request);
        bool RevokeRole(string UserID, ApplicationRole role);
    }
}