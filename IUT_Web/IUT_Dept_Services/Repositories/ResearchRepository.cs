﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Diagnostics;

namespace IUT_Dept_Services.Repositories
{
    public class ResearchRepository : IResearchRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();
        ApplicationRoleManager roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();




        private bool IsValidPublication(int publicationID)
        {
            try
            {
                return db.Publications.FirstOrDefault(x => x.PublicationID == publicationID) == null ? false : true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        private bool IsValidResearchInterest(int interestID)
        {
            try
            {
                return db.ResearchInterests.AsNoTracking().FirstOrDefault(x => x.InterestID == interestID) == null ? false : true;

            }
            catch (Exception)
            {

                return false;
            }
        }
        private bool IsValidResearchGroup(int researchGroupID)
        {
            try
            {
                return db.ResearchGroups.AsNoTracking().FirstOrDefault(x => x.GroupID == researchGroupID) == null ? false : true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        private bool IsValidResearcher(string userID)
        {
            try
            {
                IList<string> v = null;
                return (v = userManager.GetRoles(userID)).Count == 0 ? false : v.Intersect(CommonVariables.RESEARCH_ROLES).Count() == 0 ? false : true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }



        public bool AddAuthorsToPublication(string[] userIDs, int publicationID, string ownerID)
        {
            if (userIDs.Length > 0 && IsValidPublication(publicationID))
            {
                var pub = db.Publications.Find(publicationID);
                if (pub.CreatorID != ownerID)
                    return false;

                foreach (var userID in userIDs)
                {
                    if (pub.CreatorID == userID)
                        continue;
                    if (IsValidResearcher(userID))
                    {
                        try
                        {
                            var user = db.UserDetails.Find(userID);

                            user.Publications.Add(pub);
                            pub.Authors.Add(user);

                            db.Entry(pub).State = EntityState.Modified;
                            db.Entry(user).State = EntityState.Modified;
                        }
                        catch (Exception ex) { }
                    }
                }
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                }
            }

            return false;
        }
        public bool AddPublicationToResearchGroup(int[] publicationIDs, int groupID, string ownerID)
        {
            if (publicationIDs.Length > 0 && IsValidResearchGroup(groupID))
            {
                var group = db.ResearchGroups.Find(groupID);
                if (group.CreatorID != ownerID)
                    return false;

                foreach (var pubID in publicationIDs)
                {
                    if (IsValidPublication(pubID))
                    {
                        try
                        {
                            var pub = db.Publications.Find(pubID);
                            group.Publications.Add(pub);
                        }
                        catch (Exception ex) { }
                    }
                }
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                }
            }

            return false;
        }

        public bool AddMembersToResearchGroup(string[] userIDs, int groupID, string ownerID)
        {

            if (userIDs.Length > 0 && IsValidResearchGroup(groupID))
            {
                var group = db.ResearchGroups.Find(groupID);
                if (group.CreatorID != ownerID)
                    return false;
                foreach (var userID in userIDs)
                {
                    if (userID == group.CreatorID)
                        continue;
                    if (IsValidResearcher(userID))
                    {
                        try
                        {
                            var user = db.UserDetails.Find(userID);

                            group.Members.Add(user);


                        }
                        catch (Exception ex) { }
                    }
                }
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                }
            }

            return false;
        }

        public int AddNewPublication(string userID, Publication publication)
        {
            if (IsValidResearcher(userID))
            {
                try
                {
                    var user = db.UserDetails.Find(userID);
                    publication.CreatorID = userID;

                    publication.Authors.Add(user);
                    user.Publications.Add(publication);

                    db.Publications.Add(publication);
                    db.Entry(user).State = EntityState.Modified;

                    db.SaveChanges();
                    return publication.PublicationID;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            return 0;
        }

        public int AddNewResearchGroup(string userID, ResearchGroup researchGroup)
        {
            if (IsValidResearcher(userID))
            {
                try
                {
                    var user = db.UserDetails.Find(userID);
                    researchGroup.CreatorID = userID;

                    researchGroup.Members.Add(user);
                    user.ResearchGroups.Add(researchGroup);

                    db.ResearchGroups.Add(researchGroup);
                    db.Entry(user).State = EntityState.Modified;

                    db.SaveChanges();
                    return researchGroup.GroupID;

                }
                catch (Exception ex)
                {

                }
            }
            return 0;
        }

        public bool AddNewResearchInterest(string userID, ResearchInterest interest)
        {
            if (IsValidResearcher(userID))
            {
                try
                {
                    var user = db.UserDetails.Find(userID);
                    interest.CreatorID = userID;

                    interest.Members.Add(user);
                    user.ResearchInterests.Add(interest);

                    db.ResearchInterests.Add(interest);
                    db.Entry(user).State = EntityState.Modified;

                    db.SaveChanges();
                    return true;
                }
                catch (Exception) { }
            }
            return false;
        }

        public bool AddResearchInterestToMember(int[] interestIDs, string userID)
        {
            if (interestIDs.Length > 0 && IsValidResearcher(userID))
            {
                var user = db.UserDetails.Find(userID);
                foreach (var id in interestIDs)
                {
                    if (IsValidResearchInterest(id))
                    {
                        var interest = db.ResearchInterests.Find(id);

                        user.ResearchInterests.Add(interest);
                        interest.Members.Add(user);

                        db.Entry(user).State = EntityState.Modified;
                        db.Entry(interest).State = EntityState.Modified;
                    }
                }
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch (Exception) { }
            }

            return false;
        }

        public bool DeletePublication(string userID, Publication publication)
        {
            if (IsValidPublication(publication.PublicationID))
            {
                var pub = db.Publications.Find(publication.PublicationID);
                if (pub.CreatorID != userID)
                    return false;
                try
                {
                    db.Publications.Remove(pub);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex) { }
            }
            return false;
        }

        public bool DeleteResearchInterest(string userID, ResearchInterest interest)
        {
            if (IsValidResearchInterest(interest.InterestID))
            {
                var r = db.ResearchInterests.Find(interest.InterestID);
                if (r.CreatorID != userID)
                    return false;
                try
                {
                    db.ResearchInterests.Remove(r);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception) { }
            }
            return false;
        }

        public bool DeleteResearchGroup(string userID, ResearchGroup researchGroup)
        {
            if (IsValidResearchGroup(researchGroup.GroupID))
            {
                var r = db.ResearchGroups.Find(researchGroup.GroupID);
                if (r.CreatorID != userID)
                    return false;
                try
                {
                    db.ResearchGroups.Remove(r);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception) { }
            }
            return false;
        }

        public bool RemoveAuthorFromPublication(string[] userIDs, int publicationID, string ownerID)
        {
            foreach (var userID in userIDs)
            {
                if (IsValidResearcher(userID) && IsValidPublication(publicationID))
                {
                    var pub = db.Publications.Find(publicationID);
                    if (pub.CreatorID != ownerID)
                        return false;
                    try
                    {
                        var user = db.UserDetails.FirstOrDefault(x => x.UserID == userID);
                        db.Entry(pub).Collection("Authors").Load();
                        pub.Authors.Remove(user);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public bool RemoveMmeberFromResearchGroup(string[] userIDs, int groupID, string ownerID)
        {
            foreach (var userID in userIDs)
            {
                if (IsValidResearcher(userID) && IsValidResearchGroup(groupID))
                {
                    var group = db.ResearchGroups.Find(groupID);
                    if (group.CreatorID != ownerID)
                        return false;
                    try
                    {
                        var user = db.UserDetails.FirstOrDefault(x => x.UserID == userID);
                        db.Entry(group).Collection("Members").Load();

                        group.Members.Remove(user);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public bool RemovePublicationFromResearchGroup(int[] pubIDs, int groupID, string ownerID)
        {
            foreach (var pubID in pubIDs)
            {
                if (IsValidPublication(pubID) && IsValidResearchGroup(groupID))
                {
                    var group = db.ResearchGroups.Find(groupID);
                    if (group.CreatorID != ownerID)
                        return false;
                    try
                    {
                        var pub = db.Publications.FirstOrDefault(x => x.PublicationID == pubID);
                        db.Entry(group).Collection("Publications").Load();

                        group.Publications.Remove(pub);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool RemoveResearchInterestFromMember(int interestID, string userID)
        {

            if (IsValidResearcher(userID) && IsValidResearchInterest(interestID))
            {
                try
                {

                    var interest = db.ResearchInterests.Find(interestID);
                    var user = db.UserDetails.Find(userID);
                    db.Entry(user).Collection("ResearchInterests").Load();
                    user.ResearchInterests.Remove(interest);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex) { }
            }
            return false;
        }

        public bool UpdatePublication(string userID, Publication publication)
        {
            if (IsValidPublication(publication.PublicationID))
            {
                var v = db.Publications.Find(publication.PublicationID);
                if (v.CreatorID != userID)
                    return false;
                try
                {
                    v.CreatorID = publication.CreatorID ?? v.CreatorID;
                    v.Title = publication.Title ?? v.Title;
                    v.Url = publication.Url ?? v.Url;
                    v.Year = publication.Year < 1700 ? v.Year : publication.Year;
                    v.PublicationMaterial = publication.PublicationMaterial ?? v.PublicationMaterial;
                    v.PublishedIn = publication.PublishedIn ?? v.PublishedIn;
                    v.Abstract = publication.Abstract ?? v.Abstract;
                    v.AuthorList = publication.AuthorList ?? v.AuthorList;
                    v.MaterialName = publication.MaterialName ?? v.MaterialName;
                    v.MaterialType = publication.MaterialType ?? v.MaterialType;
                    db.Entry(v).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                }
            }
            return false;
        }

        public bool UpdateResearhInterest(string userID, ResearchInterest interest)
        {

            if (IsValidResearchInterest(interest.InterestID))
            {
                var v = db.ResearchInterests.Find(interest.InterestID);
                if (v.CreatorID != userID)
                    return false;
                try
                {
                    v.InterestTitle = interest.InterestTitle ?? v.InterestTitle;
                    v.InterestDescription = interest.InterestDescription ?? v.InterestDescription;
                    v.CreatorID = interest.CreatorID ?? v.CreatorID;

                    db.Entry(v).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                }
            }
            return false;
        }

        public bool UpdateResearchGroup(string userID, ResearchGroup researchGroup)
        {
            if (IsValidResearchGroup(researchGroup.GroupID))
            {
                var v = db.ResearchGroups.Find(researchGroup.GroupID);
                if (v.CreatorID != userID)
                    return false;
                try
                {
                    v.Description = researchGroup.Description ?? v.Description;
                    v.GroupTitle = researchGroup.GroupTitle ?? v.GroupTitle;
                    v.CreatorID = researchGroup.CreatorID ?? v.CreatorID;
                    v.Image = researchGroup.Image ?? v.Image;
                    v.ImageType = researchGroup.ImageType ?? v.ImageType;

                    db.Entry(v).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                }
            }
            return false;

        }

        public IEnumerable<Object> GetPublicationByDepartment(string departmentID)
        {
            try
            {
                var v = (from fs in db.Faculties
                         where fs.DepartmentID == departmentID
                         join users in db.UserDetails
                         on fs.FacultyID equals users.UserID
                         from p in users.Publications
                         select new
                         {
                             PublicationID = p.PublicationID,
                             CreatorID = p.CreatorID,
                             Abstract = p.Abstract,
                             MaterialName = p.MaterialName,
                             MaterialType = p.MaterialType,
                             AuthorList = p.AuthorList,
                             Title = p.Title,
                             PublishedIn = p.PublishedIn,
                             ResearchGroups = p.ResearchGroups,
                             ResearchInterests = p.ResearchInterests,
                             Authors = p.Authors,
                             CreatedBy = p.CreatedBy,
                             Type = p.Type,
                             Url = p.Url,
                             Year = p.Year
                         }).AsNoTracking().GroupBy(x => x.PublicationID).Select(x => x.FirstOrDefault()).OrderByDescending(x => x.Year);
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public IQueryable<ResearchGroup> GetResearchGroupByDepartment(string departmentID)
        {
            var v = (from p in db.ResearchGroups
                     let faculties = from f in db.Faculties
                                     where f.DepartmentID == departmentID
                                     select f.FacultyID
                     where faculties.Contains(p.CreatorID)
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<ResearchInterest> GetResearchInterestByDepartment(string departmentID)
        {
            var v = (from p in db.ResearchInterests
                     let faculties = from f in db.Faculties
                                     where f.DepartmentID == departmentID
                                     select f.FacultyID
                     where faculties.Contains(p.CreatorID)
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<Object> GetPublicationByUser(string userID)
        {
            var v = (from p in db.Publications
                     where p.Authors.Contains(db.UserDetails.FirstOrDefault(x => x.UserID == userID))
                     select new
                     {
                         PublicationID = p.PublicationID,
                         CreatorID = p.CreatorID,
                         Abstract = p.Abstract,
                         MaterialName = p.MaterialName,
                         MaterialType = p.MaterialType,
                         AuthorList = p.AuthorList,
                         Title = p.Title,
                         PublishedIn = p.PublishedIn,
                         ResearchGroups = p.ResearchGroups,
                         ResearchInterests = p.ResearchInterests,
                         Authors = p.Authors,
                         CreatedBy = p.CreatedBy,
                         Type = p.Type,
                         Url = p.Url,
                         Year = p.Year
                     }).OrderByDescending(x => x.Year).AsNoTracking();
            return v;
        }

        public IQueryable<ResearchGroup> GetResearchGroupByUser(string userID)
        {
            var v = (from p in db.ResearchGroups
                     where p.Members.Contains(db.UserDetails.FirstOrDefault(x => x.UserID == userID))
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<ResearchInterest> GetResearchInterestByUser(string userID)
        {
            var v = (from p in db.ResearchInterests
                     where p.Members.Contains(db.UserDetails.FirstOrDefault(x => x.UserID == userID))
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<ResearchInterest> GetResearchInterestByResearchGroup(int groupID)
        {
            var v = (from p in db.ResearchInterests
                     where p.ResearchGroups.Contains(db.ResearchGroups.FirstOrDefault(x => x.GroupID == groupID))
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<ResearchInterest> GetResearchInterestByPublication(int publicationID)
        {
            var v = (from p in db.ResearchInterests
                     where p.Publications.Contains(db.Publications.FirstOrDefault(x => x.PublicationID == publicationID))
                     select p).AsNoTracking();
            return v;
        }

        public IQueryable<Object> GetPublicationByResearchGroup(int groupID)
        {
            var v = (from p in db.Publications
                     where p.ResearchGroups.Contains(db.ResearchGroups.FirstOrDefault(x => x.GroupID == groupID))
                     select new
                     {
                         PublicationID = p.PublicationID,
                         CreatorID = p.CreatorID,
                         Abstract = p.Abstract,
                         MaterialName = p.MaterialName,
                         MaterialType = p.MaterialType,
                         AuthorList = p.AuthorList,
                         Title = p.Title,
                         PublishedIn = p.PublishedIn,
                         ResearchGroups = p.ResearchGroups,
                         ResearchInterests = p.ResearchInterests,
                         Authors = p.Authors,
                         CreatedBy = p.CreatedBy,
                         Type = p.Type,
                         Url = p.Url,
                         Year = p.Year
                     }).OrderByDescending(x => x.Year).AsNoTracking();
            return v;
        }

        public IQueryable<UserDetails> GetUsersByPublication(int publicationID)
        {
            var v = (from u in db.UserDetails
                     where u.Publications.Contains(db.Publications.FirstOrDefault(x => x.PublicationID == publicationID))
                     select u).AsNoTracking();

            return v;
        }

        public IQueryable<UserDetails> GetUsersByResearchGroup(int groupID)
        {
            var v = (from u in db.UserDetails
                     where u.ResearchGroups.Contains(db.ResearchGroups.FirstOrDefault(x => x.GroupID == groupID))
                     select u).AsNoTracking();

            return v;
        }

        public Object GetPublicationByPubID(int pubID)
        {
            var v = (from p in db.Publications
                     where p.PublicationID == pubID
                     select new
                     {
                         PublicationID = p.PublicationID,
                         CreatorID = p.CreatorID,
                         Abstract = p.Abstract,
                         MaterialName = p.MaterialName,
                         MaterialType = p.MaterialType,
                         AuthorList = p.AuthorList,
                         Title = p.Title,
                         PublishedIn = p.PublishedIn,
                         ResearchGroups = p.ResearchGroups,
                         ResearchInterests = p.ResearchInterests,
                         Authors = p.Authors,
                         CreatedBy = p.CreatedBy,
                         Type = p.Type,
                         Url = p.Url,
                         Year = p.Year
                     }).FirstOrDefault();
            return v;
        }

        public ResearchGroup GetResearchGroupByResearchGroupID(int groupID)
        {
            var v = (from r in db.ResearchGroups
                     where r.GroupID == groupID
                     select r).FirstOrDefault();
            return v;
        }

        public byte[] GetPublicationMaterial(int pubID)
        {
            var v = (from p in db.Publications
                     where p.PublicationID == pubID
                     select p.PublicationMaterial).AsNoTracking().FirstOrDefault();
            return v;
        }

        public IQueryable<object> GetPublicationYears(string departmentID)
        {

            try
            {
                var v = (from fs in db.Faculties
                         where fs.DepartmentID == departmentID
                         join users in db.UserDetails
                         on fs.FacultyID equals users.UserID
                         from p in users.Publications
                         select new
                         {
                             Year = p.Year
                         }).AsNoTracking().GroupBy(x => x.Year).Select(x => x.FirstOrDefault()).OrderByDescending(x => x.Year);
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public IQueryable<object> GetPublicationStats(string departmentID)
        {

            try
            {
                var v = (from fs in db.Faculties
                         where fs.DepartmentID == departmentID
                         join users in db.UserDetails
                         on fs.FacultyID equals users.UserID
                         from p in users.Publications
                         group p by new
                         {
                             p.Type,
                             p.Year
                         } into stats
                         select new
                         {
                             type=stats.Key.Type,
                             year=stats.Key.Year,
                             count=stats.Select(x=>x.PublicationID).Distinct().Count()
                         }).AsNoTracking().OrderByDescending(x=>x.year);
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}