﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IResearchRepository
    {
        int AddNewPublication(string userID, Publication publication);
        bool AddNewResearchInterest(string userID, ResearchInterest interest);
        int AddNewResearchGroup(string userID, ResearchGroup researchGroup);

        bool UpdatePublication(string userID, Publication publication);
        bool UpdateResearchGroup(string userID, ResearchGroup researchGroup);
        bool UpdateResearhInterest(string userID, ResearchInterest interest);

        bool DeletePublication(string userID, Publication publication);
        bool DeleteResearchGroup(string userID, ResearchGroup researchGroup);
        bool DeleteResearchInterest(string userID, ResearchInterest interest);

        bool AddAuthorsToPublication(string[] userIDs, int publicationID, string ownerID);
        bool AddMembersToResearchGroup(string[] userIDs, int groupID, string ownerID);
        bool AddResearchInterestToMember(int[] interestIDs, string userID);
        bool AddPublicationToResearchGroup(int[] publicationIDs, int groupID, string ownerID);


        bool RemoveAuthorFromPublication(string[] userIDs, int publicationID, string ownerID);
        bool RemoveMmeberFromResearchGroup(string[] userIDs, int groupID, string ownerID);
        bool RemoveResearchInterestFromMember(int interestID, string userID);
        bool RemovePublicationFromResearchGroup(int[] pubIDs, int groupID, string ownerID);

        IEnumerable<Object> GetPublicationByDepartment(string departmentID);
        IQueryable<Object> GetPublicationByUser(string userID);
        IQueryable<Object> GetPublicationByResearchGroup(int groupID);

        IQueryable<ResearchGroup> GetResearchGroupByDepartment(string departmentID);
        IQueryable<ResearchGroup> GetResearchGroupByUser(string userID);

        IQueryable<ResearchInterest> GetResearchInterestByUser(string userID);
        IQueryable<ResearchInterest> GetResearchInterestByResearchGroup(int groupID);
        IQueryable<ResearchInterest> GetResearchInterestByPublication(int publicationID);
        IQueryable<ResearchInterest> GetResearchInterestByDepartment(string departmentID);

        IQueryable<UserDetails> GetUsersByPublication(int publicationID);
        IQueryable<UserDetails> GetUsersByResearchGroup(int groupID);

        Object GetPublicationByPubID(int pubID);
        ResearchGroup GetResearchGroupByResearchGroupID(int groupID);
        byte[] GetPublicationMaterial(int pubID);
        IQueryable<object> GetPublicationYears(string departmentID);
        IQueryable<object> GetPublicationStats(string departmentID);
    }
}