﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;

namespace IUT_Dept_Services.Repositories
{
    /// <summary>
    /// In this class, use Raw SQL queries to access the "CourseFaculties" table in database.
    /// This table is a junction table between Course and Faculty. It also has a extra column: CourseFacultySection.
    /// This column lists the faculty and course wise how many sections a faculty has in his load.
    /// </summary>
    public class CourseLoadRepository : ICourseLoadRepository
    {
        ApplicationDbContext db = null;
        public CourseLoadRepository()
        {
            this.db = new ApplicationDbContext();
        }

        public CourseLoad GetSectionsPerCoursePerFaculty(Faculty faculty, Course course)
        {
            throw new NotImplementedException();
        }

        public float GetTotalCreditHourPerFaculty(Faculty faculty)
        {
            throw new NotImplementedException();
        }

        public float GetTotalCreditPerFaculty(Faculty faculty)
        {
            throw new NotImplementedException();
        }

        public void SetSectionsPerCoursePerFaculty(CourseLoad load)
        {
            throw new NotImplementedException();
        }
    }
}