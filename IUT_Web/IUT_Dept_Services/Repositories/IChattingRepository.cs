﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IChattingRepository
    {
        Task<int> AddNewChatGroup(ChatGroup group, string UserID);

        Task<bool> AddUserToChatGroup(int groupID, string memberID, string[] newMemberIDs);
        Task<bool> RemoveUserFromChatGroup(int groupID, string memberID, string[] removableMemberIDs);
        Task<bool> EditChatGroup(ChatGroup chatGroup, string creatorID);
        Task<bool> DeleteChatGroup(ChatGroup chatGroup, string creatorID);
        Task<bool> AddNewMessageAsync(Message message, string userID);

        Task<bool> DeleteMessage(int messageID, string userID);

        Task<List<ChatGroup>> GetChatGroupsAsync(string userID, string identityID);
        Task<ChatGroupDTO> GetChatGroupDetailsAsync(int groupID, string identityID);
        List<UserDetails> GetUserDetailsByGroup(int groupID, string userID);
        List<Message> GetChatGroupMessages(int groupID, string identityID);
        byte[] GetMessageFile(int messageID, string memberID);
    }
}