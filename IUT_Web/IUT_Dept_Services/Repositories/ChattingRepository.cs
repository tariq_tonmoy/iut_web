﻿using IUT_Dept_Services.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public class ChattingRepository : IChattingRepository
    {
        ApplicationDbContext db = null;
        HubConnection hubConnection = null;
        string token = "";
        IHubProxy chatHubProxy = null;
        public ChattingRepository()
        {
            db = new ApplicationDbContext();

        }
        private async Task<bool> ConnectToHub()
        {
            this.token = HttpContext.Current.Request.Headers["Authorization"];
            if (hubConnection != null && hubConnection.State == ConnectionState.Connected)
                return true;
            if (String.IsNullOrWhiteSpace(token))
                return false;
            if (hubConnection == null)
                hubConnection = new HubConnection(CommonVariables.SELF_URL + "/signalr", token);
            hubConnection.Headers.Add("Authorization", token);
            chatHubProxy = hubConnection.CreateHubProxy("ChatHub");
            hubConnection.Error += HubConnection_Error;
            try
            {
                await hubConnection.Start();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void HubConnection_Error(Exception obj)
        {
            hubConnection = null;
        }

        public async Task<int> AddNewChatGroup(ChatGroup group, string UserID)
        {
            try
            {
                if (group.CreatorID != UserID)
                    return -1;
                var user = (from u in db.UserDetails
                            where u.UserID == UserID
                            select u).FirstOrDefault();
                if (user == null)
                    return -1;
                group.Creator = user;
                group.GroupMembers = new List<UserDetails>();
                group.GroupMembers.Add(user);
                db.ChatGroups.Add(group);
                db.SaveChanges();
                if (await ConnectToHub())
                {
                    await chatHubProxy.Invoke("AddUsersToChatGroup", new List<string>() { UserID }, group.ChatGroupID);
                    this.hubConnection.Stop();
                }
                return group.ChatGroupID;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }

        public async Task<bool> AddUserToChatGroup(int groupID, string memberID, string[] newMemberIDs)
        {
            newMemberIDs = newMemberIDs.Distinct<string>().ToArray();
            if (newMemberIDs == null || newMemberIDs.Length == 0)
                return false;
            List<UserDetails> members = new List<UserDetails>();
            try
            {
                members = (from grp in db.ChatGroups
                           let _member = (from u in db.UserDetails
                                          where u.UserID == memberID
                                          select u).FirstOrDefault()
                           where grp.ChatGroupID == groupID
                           && grp.GroupMembers.Contains(_member)
                           let users = (from u in db.UserDetails
                                        where newMemberIDs.Contains(u.UserID)
                                        select u)
                           let _members = (from grp_member in grp.GroupMembers
                                           select grp_member)
                           select users.Except(_members)).FirstOrDefault().ToList();
            }
            catch (Exception ex)
            {

                return false;
            }

            ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var _tempMembers = new List<UserDetails>(members);
            for (int i = 0; i < members.Count; i++)
            {
                if (userManager.GetRoles(members[i].UserID).Count == 0)
                {
                    _tempMembers.Remove(members[i]);
                }
            }
            members = new List<UserDetails>(_tempMembers);
            if (members == null)
                return false;
            var originalGroup = db.ChatGroups.Find(groupID);
            if (String.IsNullOrWhiteSpace(originalGroup.Name))
            {
                originalGroup.Name = "";
                foreach (var v in members)
                {
                    var person = (from u in db.PersonalDetails
                                  where u.UserID == v.UserID
                                  select u).FirstOrDefault();
                    var seperator = (members.IndexOf(v) == members.Count - 1) ? "" : ", ";
                    originalGroup.Name += person != null ? person.FirstName + seperator : "";
                }
                var creator = (from u in db.PersonalDetails
                               where u.UserID == memberID
                               select u).FirstOrDefault();
                originalGroup.Name = creator != null ? creator.FirstName + ", " + originalGroup.Name : originalGroup.Name;
            }
            List<string> membersToAdd = new List<string>();
            foreach (var v in members)
            {
                originalGroup.GroupMembers.Add(v);
                membersToAdd.Add(v.UserID);
            }
            db.Entry(originalGroup).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
                var grpDto = new
                {
                    ChatGroupID = originalGroup.ChatGroupID,
                    CreatorID = originalGroup.CreatorID,
                    Name = originalGroup.Name
                };

                if (await ConnectToHub())
                {
                    await chatHubProxy.Invoke("AddUsersToChatGroup", membersToAdd, originalGroup.ChatGroupID);
                    await chatHubProxy.Invoke("EditChatGroup", originalGroup.ChatGroupID, grpDto);
                    this.hubConnection.Stop();
                }


            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> AddNewMessageAsync(Message message, string userID)
        {
            try
            {
                var grp = await db.ChatGroups.Include(x => x.GroupMembers).SingleOrDefaultAsync(x => x.ChatGroupID == message.GroupID);
                if (grp.GroupMembers.SingleOrDefault(x => x.UserID == userID) != null
                    && message.SenderID == userID)
                {
                    message.TimeStamp = DateTime.UtcNow;
                    db.Messages.Add(message);
                    await db.SaveChangesAsync();
                    int id = message.MessageID;
                    var others = grp.GroupMembers.Except(db.UserDetails.Where(x => x.UserID == userID));
                    List<MessageStatus> statuses = new List<MessageStatus>();
                    foreach (var v in others)
                    {
                        var status = new MessageStatus()
                        {
                            ChatGroupMemberID = v.UserID,
                            MessageID = id,
                            HasSeen = false
                        };
                        statuses.Add(status);
                    }
                    db.MessageStatuses.AddRange(statuses);
                    await db.SaveChangesAsync();
                    if (await ConnectToHub())
                    {
                        var msg = (from m in db.Messages
                                   where m.MessageID == message.MessageID
                                   select new MessageDTO()
                                   {
                                       MessageID = m.MessageID,
                                       GroupID = m.GroupID,
                                       MessageFileType = m.MessageFileType,
                                       MessageText = m.MessageText,
                                       MessageFileName=m.MessageFileName,
                                       SenderID = m.SenderID,
                                       MessageFile = null,
                                       TimeStamp = m.TimeStamp
                                   }).FirstOrDefault();

                        await chatHubProxy.Invoke("SendMessage", msg);
                        this.hubConnection.Stop();
                    }
                }
                else return false;
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<bool> RemoveUserFromChatGroup(int groupID, string memberID, string[] removableMemberIDs)
        {
            removableMemberIDs = removableMemberIDs.Distinct<string>().ToArray();
            IQueryable<string> removableMemberList = removableMemberIDs.AsQueryable();
            try
            {
                var originalGroup = db.ChatGroups
                    .Include(grp => grp.GroupMembers)
                    .SingleOrDefault(x => x.ChatGroupID == groupID);
                if (originalGroup == null)
                    return false;
                var removableMembers = (from grp in db.ChatGroups
                                        let _member = (from u in db.UserDetails
                                                       where u.UserID == memberID
                                                       select u).FirstOrDefault()
                                        let users = (from u in db.UserDetails
                                                     where removableMemberIDs.Contains(u.UserID)
                                                     select u)
                                        let _members = (from grp_member in grp.GroupMembers
                                                        where memberID == grp.CreatorID
                                                        || (removableMemberIDs.Count() == 1
                                                        && removableMemberList.Contains(memberID))
                                                        select grp_member)
                                        where grp.ChatGroupID == groupID
                                        && grp.GroupMembers.Contains(_member)
                                        select users.Intersect(_members)).FirstOrDefault();

                if (removableMembers == null)
                    return false;
                var removableUserIDs = new List<string>();
                foreach (var removableMember in removableMembers)
                {
                    originalGroup.GroupMembers.Remove(removableMember);
                    removableUserIDs.Add(removableMember.UserID);
                }
                db.Entry(originalGroup).State = EntityState.Modified;

                await db.SaveChangesAsync();
                if (removableUserIDs.Count > 0 && await ConnectToHub())
                {
                    await chatHubProxy.Invoke("RemoveUsersFromChatGroup", removableUserIDs, originalGroup.ChatGroupID);
                    if (originalGroup.GroupMembers.Count() >= 2) this.hubConnection.Stop();
                }

                if (originalGroup.GroupMembers.Count() < 2)
                {
                    return await DeleteChatGroup(originalGroup, originalGroup.CreatorID);
                }
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

        public async Task<bool> EditChatGroup(ChatGroup chatGroup, string creatorID)
        {
            var grp = db.ChatGroups.SingleOrDefault(x => x.ChatGroupID == chatGroup.ChatGroupID && x.CreatorID == creatorID);
            if (grp == null)
                return false;
            try
            {

                db.Entry(grp).State = EntityState.Detached;
                db.Entry(chatGroup).State = EntityState.Modified;

                var grpDto = new
                {
                    ChatGroupID = chatGroup.ChatGroupID,
                    CreatorID = chatGroup.CreatorID,
                    Name = chatGroup.Name
                };
                await db.SaveChangesAsync();
                if (await ConnectToHub())
                {
                    await chatHubProxy.Invoke("EditChatGroup", grp.ChatGroupID, grpDto);
                    this.hubConnection.Stop();
                }
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;

        }

        public async Task<bool> DeleteChatGroup(ChatGroup chatGroup, string creatorID)
        {
            var grp = db.ChatGroups.SingleOrDefault(x => x.ChatGroupID == chatGroup.ChatGroupID && x.CreatorID == creatorID);
            if (grp == null)
                return false;
            try
            {
                var grpDto = new ChatGroupDTO()
                {
                    ChatGroupID = chatGroup.ChatGroupID,
                };
                db.ChatGroups.Remove(grp);
                await db.SaveChangesAsync();
                if (await ConnectToHub())
                {
                    await chatHubProxy.Invoke("DeleteChatGroup", grpDto);
                    this.hubConnection.Stop();
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public async Task<bool> DeleteMessage(int messageID, string userID)
        {
            var msg = db.Messages.SingleOrDefault(x => x.MessageID == messageID && x.SenderID == userID);
            if (msg == null)
                return false;
            var grp = db.ChatGroups
                .Include(x => x.GroupMembers)
                .SingleOrDefault(x => x.ChatGroupID == msg.GroupID)
                .GroupMembers.SingleOrDefault(y => y.UserID == userID);
            if (grp == null)
                return false;
            try
            {
                db.Messages.Remove(msg);
                var msgDto = new MessageDTO()
                {
                    MessageID = msg.MessageID,
                    GroupID = msg.GroupID
                };
                await db.SaveChangesAsync();
                if (await ConnectToHub())
                {

                    await chatHubProxy.Invoke("DeleteMessage", msgDto);
                    this.hubConnection.Stop();
                }

            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public async Task<List<ChatGroup>> GetChatGroupsAsync(string userID, string identityID)
        {
            try
            {
                var v = (await (from u in db.UserDetails.Include(x => x.ChatGroups)
                                where u.UserID == userID
                                && userID == identityID
                                select u.ChatGroups).FirstOrDefaultAsync() as ICollection<ChatGroup>).ToList();
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public async Task<ChatGroupDTO> GetChatGroupDetailsAsync(int groupID, string identityID)
        {
            try
            {
                var v = await (from grp in db.ChatGroups
                            .Include(x => x.GroupMembers)
                               where grp.ChatGroupID == groupID
                               && grp.GroupMembers.Where(x => x.UserID == identityID).Count() > 0
                               select new ChatGroupDTO()
                               {
                                   ChatGroupID = grp.ChatGroupID,
                                   CreatorID = grp.CreatorID,
                                   Name = grp.Name
                               }).FirstOrDefaultAsync();
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public List<UserDetails> GetUserDetailsByGroup(int groupID, string userID)
        {
            try
            {
                var t = (from g in db.ChatGroups.Include(x => x.GroupMembers)
                         let user = (from u in db.UserDetails
                                     where u.UserID == userID
                                     select u).FirstOrDefault()
                         where g.GroupMembers.Contains(user)
                         && g.ChatGroupID == groupID
                         select g.GroupMembers).FirstOrDefault().ToList();
                return t;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Message> GetChatGroupMessages(int groupID, string identityID)
        {
            try
            {
                var v = (from grp in db.ChatGroups
                               .Include(x => x.GroupMembers)
                               .Include(x => x.Messages)
                         where grp.ChatGroupID == groupID
                         && grp.GroupMembers.Where(x => x.UserID == identityID).Count() > 0
                         select grp)
                              .FirstOrDefault()
                              .Messages.Select(items => new Message()
                              {
                                  MessageID = items.MessageID,
                                  GroupID = items.GroupID,
                                  MessageText = items.MessageText,
                                  MessageFileName=items.MessageFileName,
                                  MessageFileType = items.MessageFileType,
                                  SenderID = items.SenderID,
                                  TimeStamp = items.TimeStamp
                              }).ToList();
                return v;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public byte[] GetMessageFile(int messageID, string memberID)
        {
            try
            {
                var message = (from m in db.Messages
                               where m.MessageID == messageID
                               select m).FirstOrDefault();
                var grp = (from g in db.ChatGroups.Include(x => x.GroupMembers)
                           where g.ChatGroupID == message.GroupID
                           select g).FirstOrDefault();

                var mem = (from u in db.UserDetails.Include(x => x.ChatGroups)
                           where u.UserID == memberID
                           select u).FirstOrDefault().ChatGroups.Contains(grp);

                return mem ? message.MessageFile : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}