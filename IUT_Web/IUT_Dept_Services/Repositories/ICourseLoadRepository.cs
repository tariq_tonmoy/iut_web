﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface ICourseLoadRepository
    {
        void SetSectionsPerCoursePerFaculty(CourseLoad load);
        CourseLoad GetSectionsPerCoursePerFaculty(Faculty faculty, Course course);
        float GetTotalCreditPerFaculty(Faculty faculty);
        float GetTotalCreditHourPerFaculty(Faculty faculty);

    }
}