﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;

namespace IUT_Dept_Services.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();
        ApplicationRoleManager roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();


        /// <summary>
        /// Checks if user is present in UserDetails table based on UserID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns>true; if present</returns>
        private bool IsValidUser(string UserID)
        {
            return db.UserDetails.Find(UserID) == null ? false : true;
        }

        /// <summary>
        /// Checks if Department exists based on DepartmentID 
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        private bool IsValidDepartment(string DepartmentID)
        {
            return db.Departments.Find(DepartmentID) == null ? false : true;

        }

        /// <summary>
        /// Checks if Program exists based on ProgramID
        /// </summary>
        /// <param name="ProgramID"></param>
        /// <returns></returns>
        private bool IsValidProgram(int ProgramID)
        {
            return db.Programs.Find(ProgramID) == null ? false : true;

        }

        /// <summary>
        /// Checks if a role name belongs to the list of roles in PersonnelRole
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        private bool IsValidPersonnelRole(string role)
        {
            return CommonVariables.PERSONNEL_ROLES.Contains(role);
        }

        /// <summary>
        /// Checks if a user in UserDetails table has a role. It also checks if the role has a valid Id or Name
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        private bool IsInRole(string userID, ApplicationRole role)
        {

            var user = db.UserDetails.Find(userID);
            role = roleManager.FindByName(role.Name) ?? (roleManager.FindByName(role.Id) ?? null);
            if (user == null || role == null)
                return false;
            if (userManager.GetRoles(userID).Contains(role.Name))
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Checks if RequestID belongs to a valid Pending request which should include a valid User and Application Role
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        private bool IsValidRequest(int RequestID)
        {
            return db.PendingRequests.Find(RequestID) == null ?
                false : roleManager.FindById(db.PendingRequests.Find(RequestID).RequestedRoleID) == null ||
                        !IsValidUser(db.PendingRequests.Find(RequestID).RequestUserID) ?
                            false : true;
        }

        /// <summary>
        /// Checks if userid and roleid is present in a request object
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool IsValidRequest(PendingRequest request)
        {
            if (IsValidRequest(request.RequestID))
                return true;
            else if (request.RequestUserID == null || request.RequestedRoleID == null)
                return false;
            else return true;
        }

        private void CreateRoleSpecificProfile(string UserID, string DepartmentID, string PersonnelRoleName)
        {
            if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.FACULTY])
            {
                Faculty faculty = new Faculty()
                {
                    FacultyID = UserID,
                    DepartmentID = DepartmentID
                };
                db.Faculties.Add(faculty);
                db.SaveChanges();
            }
            else if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.STAFF])
            {
                Staff staff = new Staff()
                {
                    StaffID = UserID,
                    DepartmentID = DepartmentID
                };
                db.Staffs.Add(staff);
                db.SaveChanges();
            }
            else if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.TA])
            {
                TA ta = new TA()
                {
                    TAID = UserID,
                    DepartmentID = DepartmentID
                };
                db.TAs.Add(ta);
                db.SaveChanges();
            }
        }

        private void UpdateRoleSpecificProfile(string UserID, string DepartmentID, string PersonnelRoleName)
        {
            if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.FACULTY])
            {
                var v = from f in db.Faculties
                        where f.FacultyID == UserID
                        select f;
                if (v.Count() > 0)
                {
                    v.First().DepartmentID = DepartmentID;
                    db.Entry(v.First()).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else throw new Exception();



            }
            else if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.STAFF])
            {
                var v = from s in db.Staffs
                        where s.StaffID == UserID
                        select s;
                if (v.Count() > 0)
                {
                    v.First().DepartmentID = DepartmentID;
                    db.Entry(v.First()).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else throw new Exception();

            }
            else if (PersonnelRoleName == CommonVariables.ROLES[ROLENAMES.TA])
            {
                var v = from t in db.TAs
                        where t.TAID == UserID
                        select t;
                if (v.Count() > 0)
                {
                    v.First().DepartmentID = DepartmentID;
                    db.Entry(v.First()).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else throw new Exception();

            }
        }

        private bool ApproveAdminRole(string UserID)
        {
            if (IsValidUser(UserID) && !IsInRole(UserID, roleManager.FindByName(CommonVariables.ROLES[ROLENAMES.ADMIN])))
            {
                userManager.AddToRole(UserID, CommonVariables.ROLES[ROLENAMES.ADMIN]);
                return true;
            }
            return false;
        }


        private bool ApproveAlumniRole(string UserID, int ProgramID)
        {
            if (IsValidUser(UserID) && IsValidProgram(ProgramID))
            {
                if (!IsInRole(UserID, roleManager.FindByName(CommonVariables.ROLES[ROLENAMES.ALUMNI])))
                {
                    userManager.AddToRole(UserID, CommonVariables.ROLES[ROLENAMES.ALUMNI]);
                }
                var v = from alm in db.Alumni
                        where alm.UserID == UserID
                        && alm.ProgramID == ProgramID
                        select alm;
                if (v.Count() > 0)
                    return false;
                Alumni alumni = new Alumni()
                {
                    UserID = UserID,
                    ProgramID = (int)ProgramID
                };

                try
                {
                    db.Alumni.Add(alumni);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception) { }
            }

            return false;
        }


        private bool ApprovePersonnelRole(string UserID, string PersonnelRoleName, string DepartmentID)
        {
            if (IsValidPersonnelRole(PersonnelRoleName) &&
                IsValidUser(UserID) &&
                IsValidDepartment(DepartmentID))
            {
                if (!IsInRole(UserID, roleManager.FindByName(PersonnelRoleName)))
                {
                    try
                    {
                        userManager.AddToRole(UserID, PersonnelRoleName);
                        CreateRoleSpecificProfile(UserID, DepartmentID, PersonnelRoleName);
                        foreach (var v in CommonVariables.PERSONNEL_ROLES)
                        {
                            if (v != PersonnelRoleName && IsInRole(UserID, roleManager.FindByName(v)))
                            {
                                userManager.RemoveFromRole(UserID, v);
                            }
                        }
                        return true;

                    }
                    catch (Exception) { }
                }
                else
                {
                    try
                    {
                        UpdateRoleSpecificProfile(UserID, DepartmentID, PersonnelRoleName);
                        return true;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            return false;
        }

        private bool RoleSpecificApproveRequest(PendingRequest request)
        {
            var role = roleManager.FindById(request.RequestedRoleID);
            var user = db.UserDetails.Find(request.RequestUserID);
            if (role == null || user == null)
                return false;
            var res = false;
            IQueryable<PendingRequest> reqs = null;
            if (!String.IsNullOrEmpty(role.Name) && role.Name == CommonVariables.ROLES[ROLENAMES.ADMIN])
            {
                res = ApproveAdminRole(request.RequestUserID);
                reqs = FindPendingRequests(user, role);
                if (reqs.Count() > 0)
                {
                    var lsReqs = reqs.ToList();
                    foreach (var v in lsReqs)
                    {
                        res = res && RemoveRequest(v);
                    }
                }
            }
            else if (!String.IsNullOrEmpty(role.Name) && role.Name == CommonVariables.ROLES[ROLENAMES.ALUMNI])
            {

                res = request.RequestedProgramID == null ? false : ApproveAlumniRole(request.RequestUserID, request.RequestedProgramID.Value);
                if (res == false)
                    return false;
                reqs = from r in FindPendingRequests(user, role)
                       where r.RequestedProgramID == request.RequestedProgramID
                       select r;
                if (reqs.Count() > 0)
                {
                    var lsReqs = reqs.ToList();
                    foreach (var v in lsReqs)
                    {
                        res = res && RemoveRequest(v);
                    }
                }
            }
            else if (!String.IsNullOrEmpty(role.Name) && CommonVariables.PERSONNEL_ROLES.Contains(role.Name))
            {
                res = request.RequestedDepartmentID == null ? false : ApprovePersonnelRole(request.RequestUserID, role.Name, request.RequestedDepartmentID);
                if (res == false)
                    return false;
                reqs = from r in FindPendingRequests(user, role)
                       where r.RequestedDepartmentID == request.RequestedDepartmentID
                       select r;
                if (reqs.Count() > 0)
                {
                    var lsReqs = reqs.ToList();
                    foreach (var v in lsReqs)
                    {
                        res = res && RemoveRequest(v);
                    }
                }
            }
            else res = false;

            return res;
        }



        public bool ApproveRequest(int RequestID)
        {
            if (!IsValidRequest(RequestID))
                return false;
            var v = db.PendingRequests.Find(RequestID);
            return RoleSpecificApproveRequest(v);
        }

        public bool ApproveRequest(PendingRequest request)
        {
            if (!IsValidRequest(request))
                return false;
            return RoleSpecificApproveRequest(request);
        }


        public IQueryable<PendingRequest> FindPendingRequests(UserDetails userWithID)
        {
            var v = from p in db.PendingRequests
                    where userWithID.UserID == p.RequestUserID
                    select p;
            return v;
        }

        public IQueryable<PendingRequest> FindPendingRequests(UserDetails userWithID, ApplicationRole roleWithID)
        {
            var v = from p in db.PendingRequests
                    where userWithID.UserID == p.RequestUserID
                    && roleWithID.Id == p.RequestedRoleID
                    select p;
            return v;
        }

        public IQueryable<PendingRequest> FindPendingRequests(ApplicationRole roleWithID)
        {
            var v = from p in db.PendingRequests
                    where roleWithID.Id == p.RequestedRoleID
                    select p;
            return v;
        }

        public IQueryable<PendingRequest> FindPendingRequests()
        {
            return db.PendingRequests;
        }

        public bool RemoveRequest(int RequestID)
        {
            var p = db.PendingRequests.Find(RequestID);
            if (p != null)
            {
                try
                {
                    db.PendingRequests.Remove(p);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                }
            }
            return false;
        }

        public bool RemoveRequest(PendingRequest request)
        {
            try
            {
                db.PendingRequests.Remove(db.PendingRequests.FirstOrDefault(x => x.RequestID == request.RequestID));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }


        public bool RevokeRole(string UserID, ApplicationRole role)
        {
            var user = db.UserDetails.Find(UserID);
            role = roleManager.FindByName(role.Name) ?? (roleManager.FindByName(role.Id) ?? null);
            if (user == null || role == null || !IsInRole(UserID, role)) return false;
            try
            {
                userManager.RemoveFromRole(user.UserID, role.Name);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}