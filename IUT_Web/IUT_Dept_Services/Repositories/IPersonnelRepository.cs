﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IPersonnelRepository<T>
    {
        bool IsValidPersonnelUpdateRequest(T personnel);
        bool UpdatePersonnelDetails(string userID, T personnel);
        IEnumerable<T> GetPersonnelByDepartment(Department department);
        IEnumerable<T> GetPersonnelDetails(string userID);
        bool UpdatePersonnelPriority(string userID,int? priority);
        bool DeletePersonnel(T personnel);
    }
}