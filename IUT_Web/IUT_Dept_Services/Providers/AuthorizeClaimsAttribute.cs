﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace IUT_Dept_Services.Providers
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public class AuthorizeClaimsAttribute : AuthorizeAttribute
    {
        protected override bool UserAuthorized(System.Security.Principal.IPrincipal user)
        {
            if (user == null)
            {
                return false;
            }
            var userIdentity = (ClaimsIdentity)user.Identity;
            if (userIdentity.AuthenticationType.ToLower() != "Bearer".ToLower())
                return false;
            var claims = userIdentity.Claims;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            if (user.Identity.IsAuthenticated && roles.Count()>0)
                return true;
            else return false;
        }
    }
}