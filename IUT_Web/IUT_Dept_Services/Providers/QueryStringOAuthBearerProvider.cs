﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IUT_Dept_Services.Providers
{
    public class QueryStringOAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            var value = context.Request.Query.Get("access_token");

            if (!string.IsNullOrEmpty(value))
            {
                context.Token = value;
            }

            else if (context.Request.QueryString.HasValue)
            {
                var queryStrings = Uri.UnescapeDataString(context.Request.QueryString.Value);
                var queryAr = queryStrings.Split('&');
                var bearer = "";
                for (int i = 2; i < queryAr.Length; i++)
                {
                    bearer += queryAr[i];
                    if (String.IsNullOrEmpty(queryAr[i]))
                    {
                        bearer += '&';
                    }
                }
                if (!String.IsNullOrEmpty(bearer))
                    context.Token = value;
            }
            return Task.FromResult<object>(null);
        }
    }
}