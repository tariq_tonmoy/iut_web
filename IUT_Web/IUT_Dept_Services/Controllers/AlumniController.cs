﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/alumni")]
    public class AlumniController : ApiController
    {
        private IAlumniRepository repository;
        public AlumniController(IAlumniRepository alumniRepository)
        {
            this.repository = alumniRepository;
        }
        [HttpGet]
        [Route("{programID}/pass/{passingYear}")]
        public IQueryable<Alumni> GetAlumniByPassingYear(int programID, int passingYear)
        {
            return repository.GetAlumniByProgramPassing(new Program() { ProgramID = programID }, passingYear);
        }
        [HttpGet]
        [Route("{programID}/enroll/{enrollmentYear}")]
        public IQueryable<Alumni> GetAlumniByEnrollmentYear(int programID, int enrollmentYear)
        {
            return repository.GetAlumniByProgramEnrollment(new Program() { ProgramID = programID }, enrollmentYear);
        }
        [HttpGet]
        [Route("{programID}/enroll")]
        public IQueryable<Alumni> GetAlumniByProgram(int programID)
        {
            return repository.GetAlumniByProgram(programID);
        }

        [HttpGet]
        [Route("{userID}")]
        public IQueryable<Alumni> GetAlumniByID(string userID)
        {
            return repository.GetAlumniDetails(userID);
        }

        [HttpGet]
        [Route("generate/{programName}")]
        [Authorize(Roles = CommonVariables.ADMIN)]
        public bool GenerateAlumniAccounts([FromUri] string programName)
        {
            return repository.GenerateAlumniAccounts(programName);
        }

        [HttpGet]
        [Route("alumni-category/{deptID}")]
        public IQueryable<Object> GenerateAlumniCategories([FromUri] string deptID)
        {
            return repository.GetAlumniCategoriesByDepartment(deptID);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("send-email/{fromEmail}")]
        public HttpResponseMessage SendEmailsToAlumni([FromUri]string fromEmail, [FromBody]string fromPassword) 
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.SendAlumniCredentialsViaEmail(fromEmail, fromPassword))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ALUMNI)]
        [Route("update")]
        public HttpResponseMessage UpdateAlumni(Alumni alumni)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdateAlumniDetails(User.Identity.GetUserId(), alumni))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ALUMNI)]
        [Route("delete")]
        public HttpResponseMessage DeleteAlumni(Alumni alumni)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteAlumni(User.Identity.GetUserId(), alumni.AlumniID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }



    }
}
