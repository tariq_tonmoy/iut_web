﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/iutacademics")]
    public class IutAcademicsController : ApiController
    {
        private IAcademicsRepository academicsRepository;
        public IutAcademicsController(IAcademicsRepository academicsRepository)
        {
            this.academicsRepository = academicsRepository;
        }

        [HttpGet]
        [Route("departments")]
        public IQueryable<Department> GetDepartments()
        {
            return academicsRepository.GetDepartments();
        }
        [HttpGet]
        [Route("department/{deptID}")]
        public Department GetDepartment(string deptID)
        {
            return academicsRepository.GetDepartmentByID(deptID);
        }
        [HttpGet]
        [Route("department-head/{deptID}")]
        public Faculty GetDepartmentHead(string deptID)
        {
            return academicsRepository.GetDepartmentHead(deptID);
        }



        [HttpGet]
        [Route("department-programs/{deptID}")]
        public IQueryable<Program> GetPrograms(string deptID)
        {
            return academicsRepository.GetProgramByDepartment(deptID);
        }
        [HttpGet]
        [Route("programs-summary/{deptID}")]
        public IQueryable<Object> GetProgramSummary(string deptID)
        {
            return academicsRepository.GetProgramSummaryByDepartment(deptID);
        }


        [HttpGet]
        [Route("course-syllabus/{courseID}")]
        public Syllabus GetCourseSyllabus(int courseID)
        {
            return academicsRepository.GetSyllabusByCourse(courseID);
        }
        [HttpGet]
        [Route("program/{programID}")]
        public Program GetProgram(int programID)
        {
            return academicsRepository.GetProgram(programID);
        }

        [HttpGet]
        [Route("program-courses/{programID}")]
        public IQueryable<Course> GetCourses(int programID)
        {
            return academicsRepository.GetCourseByProgram(programID);
        }
        [HttpGet]
        [Route("faculty-courses/{facultyID}")]
        public IQueryable<Course> GetCourses(string facultyID)
        {
            return academicsRepository.GetCourseByFaculty(facultyID);
        }
        [HttpGet]
        [Route("course-faculties/{courseID}")]
        public IQueryable<Faculty> GetFaculties(int CourseID)
        {
            return academicsRepository.GetFacultyByCourse(CourseID);
        }
        [HttpGet]
        [Route("coursematerial/{facultyID}")]
        public IQueryable<CourseMaterial> GetCourseMaterials(string facultyID)
        {
            return academicsRepository.GetCourseMaterial(facultyID);
        }
        [HttpGet]
        [Route("coursematerial/{facultyID}/{courseID}")]
        public IQueryable<Object> GetCourseMaterials(string facultyID, int courseID)
        {
            return academicsRepository.GetCourseMaterialByCourse(courseID, facultyID);
        }

        [HttpGet]
        [Route("coursematerial-file/{materialID}")]
        public byte[] GetCourseMaterialFile(int materialID)
        {
            return academicsRepository.GetCourseMaterialFile(materialID);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("coursematerial/{courseID}")]
        public HttpResponseMessage AddCourseMaterial([FromUri]int courseID, [FromBody]CourseMaterial courseMaterial)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewCourseMaterial(courseID, User.Identity.GetUserId(), courseMaterial))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus/{courseID}")]
        public HttpResponseMessage AddSyllabus([FromUri] int courseID, [FromBody] Syllabus syllabus)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewSyllabus(courseID, syllabus))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("course/{programID}")]
        public HttpResponseMessage AddCourse([FromUri] int programID, [FromBody] Course course)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewCourse(programID, course))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("program/{deptID}")]
        public HttpResponseMessage AddProgram([FromUri] string deptID, [FromBody] Program program)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewProgram(deptID, program))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("department")]
        public HttpResponseMessage AddDepartment([FromBody] Department department)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewDepartment(department))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }



        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ADMIN)]
        [Route("assign-faculty/{facultyID}")]
        public HttpResponseMessage AssignFaculty([FromUri]string facultyID, [FromBody] Course course)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddFacultyToCourse(facultyID, course.CourseID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);

        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ADMIN)]
        [Route("remove-faculty/{facultyID}")]
        public HttpResponseMessage RemoveFacultyFromCourse([FromUri]string facultyID, [FromBody] Course course)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteFacultyFromCourse(facultyID, course.CourseID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }


        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("coursematerial")]
        public HttpResponseMessage EditCourseMaterial([FromBody]CourseMaterial courseMaterial)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditCourseMaterial(User.Identity.GetUserId(), courseMaterial))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus")]
        public HttpResponseMessage EditSyllabus([FromBody] Syllabus syllabus)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditSyllabus(syllabus))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("course")]
        public HttpResponseMessage EditCourse([FromBody] Course course)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditCourse(course))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("program")]
        public HttpResponseMessage EditProgram([FromBody] Program program)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditProgram(program))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("department")]
        public HttpResponseMessage EditDepartment([FromBody] Department department)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditDepartment(department))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }



        [HttpDelete]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("coursematerial")]
        public HttpResponseMessage DeleteCourseMaterial([FromBody]CourseMaterial courseMaterial)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteCourseMaterial(User.Identity.GetUserId(), courseMaterial))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus")]
        public HttpResponseMessage DeleteSyllabus([FromBody] Syllabus syllabus)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteSyllabus(syllabus))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("course")]
        public HttpResponseMessage DeleteCourse([FromBody] Course course)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteCourse(course))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("program")]
        public HttpResponseMessage DeleteProgram([FromBody] Program program)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteProgram(program))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("department")]
        public HttpResponseMessage DeleteDepartment([FromBody] Department department)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteDepartment(department))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("syllabus-review")]
        public IQueryable<SyllabusReview> GetSyllabusReviews()
        {
            return academicsRepository.GetSyllabusReviews();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("syllabus-review-courses/{progID}/{reviewID}")]
        public IQueryable<Course> GetCourseBySyllabusReview([FromUri]int progID, [FromUri]int reviewID)
        {
            return academicsRepository.GetCoursesBySyllabusReview(progID, reviewID);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("program-syllabus-reviews/{progID}")]
        public IQueryable<SyllabusReview> GetSyllabusReviewsByProgram([FromUri]int progID)
        {
            return academicsRepository.GetSyllabusReviewByProgram(progID);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus-review")]
        public HttpResponseMessage DeleteSyllabusReview([FromBody] SyllabusReview syllabusReview)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.DeleteSyllabusReview(syllabusReview.SyllabusReviewID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus-review")]
        public HttpResponseMessage AddSyllabusReview([FromBody] SyllabusReview review)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.AddNewSyllabusReview(review))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("syllabus-review")]
        public HttpResponseMessage EditSullabusReview([FromBody] SyllabusReview review)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (academicsRepository.EditSyllabusReview(review))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }


    }
}
