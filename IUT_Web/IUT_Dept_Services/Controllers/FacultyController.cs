﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/faculty")]
    public class FacultyController : ApiController
    {
        private IPersonnelRepository<Faculty> repository;
        public FacultyController(IPersonnelRepository<Faculty> facultyRepository)
        {
            this.repository = facultyRepository;
        }
        [HttpGet]
        [Route("department/{departmentID}")]
        public IEnumerable<Faculty> GetFacultyByDepartment(string departmentID)
        {
            return repository.GetPersonnelByDepartment(new Department() { DepartmentID = departmentID });
        }
        [HttpGet]
        [Route("{facultyID}")]
        public IEnumerable<Faculty> GetFacultyByID(string facultyID)
        {
            return repository.GetPersonnelDetails(facultyID);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("update")]
        public HttpResponseMessage UpdateFaculty(Faculty faculty)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePersonnelDetails(User.Identity.GetUserId(), faculty))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("update/{FacultyID}")]
        public HttpResponseMessage UpdateStaffPriority([FromUri]string FacultyID, [FromBody]int? priority)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePersonnelPriority(FacultyID, priority))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("delete")]
        public HttpResponseMessage DeleteFaculty(Faculty faculty)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeletePersonnel(faculty))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

    }
}
