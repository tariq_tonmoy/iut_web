﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;

namespace IUT_Dept_Services.Controllers
{

    [RoutePrefix("api/user")]
    public class UserDetailsController : ApiController
    {
        private IUserDetailsRepository userDetailsRepository;

        public UserDetailsController(IUserDetailsRepository userDetailsRepository)
        {
            this.userDetailsRepository = userDetailsRepository;
        }
        [HttpGet]
        [Route("userId/{userName}/")]
        public IHttpActionResult GetUserID(string userName)
        {
            var v = userDetailsRepository.GetUserIDFromUserName(userName);
            if (v == null)
                return NotFound();
            return Ok(v);
        }
        [HttpGet]
        [Route("userName/{userId}")]
        public IHttpActionResult GetUserName(string userId)
        {
            var v = userDetailsRepository.GetUserNameFromUserID(userId);
            if (v == null)
                return NotFound();
            return Ok(v);
        }
        [HttpGet]
        [Route("isdoctor/{userId}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult isDoctor(string userId)
        {
            var v = userDetailsRepository.isDoctor(userId);
            return Ok(v);
        }
        [HttpGet]
        [Route("isprofessor/{userId}")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult isProfessor(string userId)
        {
            var v = userDetailsRepository.isProfessor(userId);
            return Ok(v);
        }

        [HttpGet]
        [Route("email/{userID}")]
        public IHttpActionResult GetUserEmail(string userID)
        {
            var v = userDetailsRepository.GetUserEmail(userID);
            if (v == null)
                return NotFound();
            return Ok(v);
        }
        [HttpGet]
        [Route("roles")]
        public IEnumerable<UserRole> GetAllRoles()
        {
            return userDetailsRepository.GetUserRoles();
        }

        [HttpGet]
        [Route("roles/{userID}")]
        public IEnumerable<UserRole> GetRolesByUser(string userID)
        {
            return userDetailsRepository.GetRolesByUser(userID);
        }

        [HttpGet]
        [Route("personal/{id}")]
        [ResponseType(typeof(PersonalDetails))]
        public IHttpActionResult GetPersonalDetails(string id)
        {
            var v = userDetailsRepository.GetPersonalDetails(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }

        [HttpGet]
        [Route("personal-no-pic/{id}")]
        [ResponseType(typeof(object))]
        public IHttpActionResult GetPersonalDetailsNoProfilePicture(string id)
        {
            var v = userDetailsRepository.GetPersonalDetailsNoProPic(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }


        [HttpGet]
        [Route("profile-picture/{id}")]
        [ResponseType(typeof(byte[]))]
        public IHttpActionResult GetProfilePicture(string id)
        {
            var v = userDetailsRepository.GetProfilePicture(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }


        [Authorize]
        [HttpGet]
        [Route("personal-search/{searchTerm}")]
        [ResponseType(typeof(IQueryable<PersonalDetails>))]
        public IHttpActionResult GetPersonalDetailsBySearch(string searchTerm)
        {
            var v = userDetailsRepository.SearchPersonalDetailsByName(searchTerm);
            if (v == null || v.Count() == 0)
                return NotFound();
            return Ok(v);
        }
        [HttpGet]
        [Route("academic/{id}")]
        [ResponseType(typeof(AcademicDetails))]
        public IQueryable<AcademicDetails> GetAcademicDetails(string id)
        {
            return userDetailsRepository.GetAcademicDetails(id);
        }
        [HttpGet]
        [Route("profession/{id}")]
        [ResponseType(typeof(ProfessionalDetails))]
        public IQueryable<ProfessionalDetails> GetProfessionalDetails(string id)
        {
            return userDetailsRepository.GetProfessionalDetails(id);
        }

        [HttpGet]
        [Route("contact/{id}")]
        [ResponseType(typeof(Contact))]
        public IQueryable<Contact> GetContacts(string id)
        {
            return userDetailsRepository.GetContacts(id);
        }

        [HttpGet]
        [Route("personal/location/{id}")]
        [ResponseType(typeof(Location))]
        public IHttpActionResult GetUserLocation(string id)
        {
            var v = userDetailsRepository.GetUserLocations(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }

        [HttpGet]
        [Route("academic/location/{id}")]
        [ResponseType(typeof(Location))]
        public IHttpActionResult GetAcademicLocation(int id)
        {
            var v = userDetailsRepository.GetAcademicLocations(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }

        [HttpGet]
        [Route("profession/location/{id}")]
        [ResponseType(typeof(Location))]
        public IHttpActionResult GetProfessionalLocation(int id)
        {
            var v = userDetailsRepository.GetProfessionalLocations(id);
            if (v == null)
                return NotFound();
            return Ok(v);
        }

        [Authorize]
        [Route("personal")]
        [HttpPost]
        public HttpResponseMessage PostPersonalDetails(PersonalDetails personalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.AddPersonalDetails(personalDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);

        }

        [Authorize]
        [Route("academic")]
        [HttpPost]
        public HttpResponseMessage PostAcademicDetails(AcademicDetails academicDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.AddAcademicDetails(academicDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("profession")]
        [HttpPost]
        public HttpResponseMessage PostProfessionalDetails(ProfessionalDetails professionalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.AddProfessionalDetails(professionalDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("contact")]
        [HttpPost]
        public HttpResponseMessage PostContactDetails(Contact contact)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.AddContact(contact, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("personal/location")]
        [HttpPost]
        public HttpResponseMessage PostUserLocation(Location location)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.AddUserLocation(location, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("request-role")]
        [HttpPost]
        public HttpResponseMessage RequestNewRole([FromBody] PendingRequest request)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.RequestNewRole(User.Identity.GetUserId(), request))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("personal")]
        [HttpPut]
        public HttpResponseMessage EditPersonalDetails(PersonalDetails personalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.EditPersonalDetails(personalDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);

        }

        [Authorize]
        [Route("academic")]
        [HttpPut]
        public HttpResponseMessage EditAcademicDetails(AcademicDetails academicDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.EditAcademicDetails(academicDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("profession")]
        [HttpPut]
        public HttpResponseMessage EditProfessionalDetails(ProfessionalDetails professionalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.EditProfessionalDetails(professionalDetails, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("contact")]
        [HttpPut]
        public HttpResponseMessage EditContactDetails(Contact contact)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.EditContact(contact, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("personal/location")]
        [HttpPut]
        public HttpResponseMessage EditUserLocation(Location location)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.EditLocation(location, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("personal")]
        [HttpDelete]
        public HttpResponseMessage RemovePersonalDetails(PersonalDetails personalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeletePersonalDetails(personalDetails.UserID,personalDetails.UserID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);

        }

        [Authorize]
        [Route("academic")]
        [HttpDelete]
        public HttpResponseMessage RemoveAcademicDetails(AcademicDetails academicDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeleteAcademicDetails(academicDetails.AcademicDetailsID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("profession")]
        [HttpDelete]
        public HttpResponseMessage RemoveProfessionalDetails(ProfessionalDetails professionalDetails)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeleteProfessionalDetails(professionalDetails.ProfessionalDetailsID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("contact")]
        [HttpDelete]
        public HttpResponseMessage RemoveContactDetails(Contact contact)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeleteContact(contact.ContactID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize]
        [Route("personal/location")]
        [HttpDelete]
        public HttpResponseMessage RemoveUserLocation(Location location)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeleteLocation(location.LocationID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [Authorize(Roles =CommonVariables.ADMIN)]
        [Route("personal/user/{UserName}")]
        [HttpDelete]
        public HttpResponseMessage RemoveUser([FromUri]string UserName)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (userDetailsRepository.DeleteUser(UserName))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

    }
}