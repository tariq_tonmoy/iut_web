﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/ta")]
    public class TeacherAssistantController : ApiController
    {
        private IPersonnelRepository<TA> repository;
        public TeacherAssistantController(IPersonnelRepository<TA> taRepository)
        {
            this.repository = taRepository;
        }
        [HttpGet]
        [Route("department/{departmentID}")]
        public IEnumerable<TA> GetTAByDepartment(string departmentID)
        {
            return repository.GetPersonnelByDepartment(new Department() { DepartmentID = departmentID });
        }
        [HttpGet]
        [Route("{TAID}")]
        public IEnumerable<TA> GetTAByID(string TAID)
        {
            return repository.GetPersonnelDetails(TAID);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.TA)]
        [Route("update")]
        public HttpResponseMessage UpdateTA(TA TA)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePersonnelDetails(User.Identity.GetUserId(), TA))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        
        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("delete")]
        public HttpResponseMessage DeleteTA(TA TA)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeletePersonnel(TA))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
    }
}
