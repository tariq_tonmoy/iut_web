﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/research")]
    public class ResearchController : ApiController
    {
        IResearchRepository repository = null;
        public ResearchController(IResearchRepository repo)
        {
            this.repository = repo;
        }

        #region HttpGet
        [HttpGet]
        [Route("user-publications/{userID}")]
        public IQueryable<Object> GetPublicationsByUser(string userID)
        {
            return repository.GetPublicationByUser(userID);
        }

        [HttpGet]
        [Route("publication-years/{deptID}")]
        public IQueryable<Object> GetPublicationYearsByDepartment(string deptID)
        {
            return repository.GetPublicationYears(deptID);
        }

        [HttpGet]
        [Route("publication-pub-id/{pubID}")]
        public Object GetPublicationBypubID(int pubID)
        {
            return repository.GetPublicationByPubID(pubID);
        }

        [HttpGet]
        [Route("publication-material/{pubID}")]
        public byte[] GetPublicationMaterialBypubID(int pubID)
        {
            return repository.GetPublicationMaterial(pubID);
        }


        [HttpGet]
        [Route("group-group-id/{groupID}")]
        public ResearchGroup GetResearchGroupBygroupID(int groupID)
        {
            return repository.GetResearchGroupByResearchGroupID(groupID);
        }

        [HttpGet]
        [Route("user-interests/{userID}")]
        public IQueryable<ResearchInterest> GetResearchInterestsByUser(string userID)
        {
            return repository.GetResearchInterestByUser(userID);
        }
        [HttpGet]
        [Route("user-groups/{userID}")]
        public IQueryable<ResearchGroup> GetResearchGroupsByUser(string userID)
        {
            return repository.GetResearchGroupByUser(userID);
        }
        [HttpGet]
        [Route("group-members/{groupID}")]
        public IQueryable<UserDetails> GetResearchGroupMembers(int groupID)
        {
            return repository.GetUsersByResearchGroup(groupID);
        }
        [HttpGet]
        [Route("group-publications/{groupID}")]
        public IQueryable<Object> GetResearchGroupPublications(int groupID)
        {
            return repository.GetPublicationByResearchGroup(groupID);
        }
        [HttpGet]
        [Route("group-interests/{groupID}")]
        public IQueryable<ResearchInterest> GetResearchGroupInterests(int groupID)
        {
            return repository.GetResearchInterestByResearchGroup(groupID);
        }
        [HttpGet]
        [Route("publication-authors/{publicationID}")]
        public IQueryable<UserDetails> GetPublicationAuthors(int publicationID)
        {
            return repository.GetUsersByPublication(publicationID);
        }
        [HttpGet]
        [Route("publication-interests/{publicationID}")]
        public IQueryable<ResearchInterest> GetPublicationInterests(int publicationID)
        {
            return repository.GetResearchInterestByPublication(publicationID);
        }
        [HttpGet]
        [Route("department-publications/{departmentID}")]
        public IEnumerable<Object> GetDepartmentPublications(string departmentID)
        {
            return repository.GetPublicationByDepartment(departmentID);
        }
        [HttpGet]
        [Route("publication-stats/{departmentID}")]
        public IEnumerable<Object> GetPublicationStatsByDepartment(string departmentID)
        {
            return repository.GetPublicationStats(departmentID);
        }
        [HttpGet]
        [Route("department-groups/{departmentID}")]
        public IQueryable<ResearchGroup> GetDepartmentResearchGroups(string departmentID)
        {
            return repository.GetResearchGroupByDepartment(departmentID);
        }
        [HttpGet]
        [Route("department-interests/{departmentID}")]
        public IQueryable<ResearchInterest> GetDepartmentResearchInterests(string departmentID)
        {
            return repository.GetResearchInterestByDepartment(departmentID);
        }
        #endregion HttpGet

        #region HttpPost
        [HttpPost]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ALUMNI)]
        [Route("publication")]
        public HttpResponseMessage AddPublication([FromBody]Publication publication)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var v = repository.AddNewPublication(User.Identity.GetUserId(), publication);
            if (v > 0)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, v);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPost]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group")]
        public HttpResponseMessage AddResearchGroup([FromBody]ResearchGroup group)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var v = repository.AddNewResearchGroup(User.Identity.GetUserId(), group);
            if (v > 0)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, v);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPost]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-interest")]
        public HttpResponseMessage AddResearchInterest([FromBody]ResearchInterest interest)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddNewResearchInterest(User.Identity.GetUserId(), interest))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        #endregion HttpPost

        #region HttpPut
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ALUMNI)]
        [Route("publication")]
        public HttpResponseMessage UpdatePublication([FromBody]Publication publication)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePublication(User.Identity.GetUserId(), publication))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group")]
        public HttpResponseMessage UpdateResearchGroup([FromBody]ResearchGroup researchGroup)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdateResearchGroup(User.Identity.GetUserId(), researchGroup))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-interest")]
        public HttpResponseMessage UpdateResearchInterest([FromBody]ResearchInterest interest)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdateResearhInterest(User.Identity.GetUserId(), interest))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ALUMNI)]
        [Route("publication/add-users/{publicationID}")]
        public HttpResponseMessage AddPublicationAuthors([FromUri]int publicationID, [FromBody]string[] userIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddAuthorsToPublication(userIDs, publicationID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group/add-pub/{groupID}")]
        public HttpResponseMessage AddGroupPublicatoins([FromUri]int groupID, [FromBody]int[] publicationIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddPublicationToResearchGroup(publicationIDs, groupID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group/add-users/{groupID}")]
        public HttpResponseMessage AddResearchGroupMembers([FromUri]int groupID, [FromBody]string[] userIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddMembersToResearchGroup(userIDs, groupID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-interest/add/{userID}")]
        public HttpResponseMessage AddResearchInterestsToMembers([FromUri]string userID, [FromBody]int[] interestIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddResearchInterestToMember(interestIDs, userID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ALUMNI)]
        [Route("publication/remove-user/{publicationID}")]
        public HttpResponseMessage RemovePublicationMembers([FromUri]int publicationID, [FromBody]string[] userIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemoveAuthorFromPublication(userIDs, publicationID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group/remove-user/{groupID}")]
        public HttpResponseMessage RemoveResearchGroupMembers([FromUri]int groupID, [FromBody]string[] userIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemoveMmeberFromResearchGroup(userIDs, groupID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group/remove-pub/{groupID}")]
        public HttpResponseMessage RemoveGroupPublicatoins([FromUri]int groupID, [FromBody]int[] publicationIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemovePublicationFromResearchGroup(publicationIDs, groupID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-interest/remove/{userID}")]
        public HttpResponseMessage RemoveResearchInterestsToMembers([FromUri]string userID, [FromBody]int interestID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemoveResearchInterestFromMember(interestID, userID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        #endregion HttpPut

        #region HttpDelete
        [HttpDelete]
        [Authorize(Roles = CommonVariables.FACULTY + "," + CommonVariables.ALUMNI)]
        [Route("publication")]
        public HttpResponseMessage DeletePublication([FromBody]int publicationID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeletePublication(User.Identity.GetUserId(), new Publication() { PublicationID = publicationID }))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpDelete]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-group")]
        public HttpResponseMessage DeleteResearchGroup([FromBody]int researchGroupID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteResearchGroup(User.Identity.GetUserId(), new ResearchGroup() { GroupID = researchGroupID }))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpDelete]
        [Authorize(Roles = CommonVariables.FACULTY)]
        [Route("research-interest")]
        public HttpResponseMessage DelteResearchInterest([FromBody]int researchInterestID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteResearchInterest(User.Identity.GetUserId(), new ResearchInterest() { InterestID = researchInterestID }))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        #endregion HttpDelete
    }
}
