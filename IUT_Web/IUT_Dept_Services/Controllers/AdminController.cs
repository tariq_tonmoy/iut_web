﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [Authorize(Roles = CommonVariables.ADMIN)]
    [RoutePrefix("api/admin")]
    public class AdminController : ApiController
    {
        IAdminRepository repository = null;
        public AdminController(IAdminRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("requests")]
        public IQueryable<PendingRequest> GetPendingRequestList()
        {
            return repository.FindPendingRequests();
        }

        [HttpGet]
        [Route("requests/{userID}/{roleID}")]
        public IQueryable<PendingRequest> GetPendingRequestList(string userID, string roleID)
        {
            UserDetails user = new UserDetails() { UserID = userID };
            ApplicationRole appRole = new ApplicationRole() { Id = roleID };
            return repository.FindPendingRequests(user, appRole);
        }
        [HttpGet]
        [Route("requests/{userID}")]
        public IQueryable<PendingRequest> GetPendingRequestList(string userID)
        {
            UserDetails user = new UserDetails() { UserID = userID };
            return repository.FindPendingRequests(user);
        }

        /// <summary>
        /// ADMIN can Use this api to approve new role for a user who HAVE NOT APPLIED
        /// ADMIN can create new pending request and send it here to allocate someone with some role
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("request/approve")]
        public HttpResponseMessage ApproveUserRequest(PendingRequest request)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.ApproveRequest(request))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Route("request/approve/{requestID}")]
        public HttpResponseMessage ApproveUserRequest([FromUri] int requestID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.ApproveRequest(requestID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Route("request/remove")]
        public HttpResponseMessage RemoveUserRequest([FromBody]PendingRequest request)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemoveRequest(request))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Route("request/remove/{requestID}")]
        public HttpResponseMessage RemoveUserRequest([FromUri] int requestID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RemoveRequest(requestID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Route("request/revoke/{userID}")]
        public HttpResponseMessage RevokeUserRole([FromUri] string userID, [FromBody]ApplicationRole role)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.RevokeRole(userID,role))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
    }
}
