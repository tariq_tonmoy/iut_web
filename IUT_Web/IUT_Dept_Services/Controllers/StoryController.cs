﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [Authorize(Roles = CommonVariables.ADMIN)]
    [RoutePrefix("api")]
    public class StoryController : ApiController
    {
        IStoryRepository repository;
        public StoryController(IStoryRepository repo)
        {
            this.repository = repo;
        }

        #region HttpGet
        [HttpGet]
        [Route("stories/{departmentID}")]
        [AllowAnonymous]
        public IQueryable<Object> GetStoriesByDepartment(string departmentID)
        {
            return repository.GetAllStories(departmentID);
        }
        [HttpGet]
        [Route("stories/focused/{departmentID}")]
        [AllowAnonymous]
        public IQueryable<Object> GetFocusedStoriesByDepartment(string departmentID)
        {
            return repository.GetFocusedStories(departmentID);
        }
        [HttpGet]
        [Route("story/image/{storyID}")]
        [AllowAnonymous]
        public byte[] GetStoryImage(int storyID)
        {
            return repository.GetImageByStoryID(storyID);
        }
        [HttpGet]
        [Route("events/{departmentID}")]
        [AllowAnonymous]
        public IQueryable<CustomEvent> GetEventsByDepartment(string departmentID)
        {
            return repository.GetAllEvents(departmentID);
        }
        [HttpGet]
        [Route("events/focused/{departmentID}")]
        [AllowAnonymous]
        public IQueryable<CustomEvent> GetFocusedEventsByDepartment(string departmentID)
        {
            return repository.GetFocusedEvents(departmentID);
        }
        [HttpGet]
        [Route("event/location/{eventID}")]
        [AllowAnonymous]
        public Location GetEventLocation(int eventID)
        {
            return repository.GetEventLocation(eventID);
        }

        [HttpGet]
        [Route("event/{eventID}")]
        [AllowAnonymous]
        public CustomEvent GetEvent(int eventID)
        {
            return repository.GetCustomEvent(eventID);
        }
        [HttpGet]
        [Route("story/{storyID}")]
        [AllowAnonymous]
        public Story GetStory(int storyID)
        {
            return repository.GetStory(storyID);
        }

        [HttpGet]
        [Route("has-events/{storyID}")]
        [AllowAnonymous]
        public bool HasEvents(int storyID)
        {
            return repository.HasEvent(storyID);
        }




        #endregion HttpGet

        #region HttpPost
        [HttpPost]
        [Route("story")]
        public HttpResponseMessage AddNewStory([FromBody]Story story)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddNewStory(story))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPost]
        [Route("event")]
        public HttpResponseMessage AddNewEvent([FromBody]CustomEvent customEvent)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddNewEvent(customEvent))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPost]
        [Route("event/location/{eventID}")]
        public HttpResponseMessage AddEventLocation([FromUri]int eventID, [FromBody]Location location)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.AddEventLocation(eventID, location))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        #endregion HttpPost

        #region HttpPut
        [HttpPut]
        [Route("story")]
        public HttpResponseMessage EditStory([FromBody]Story story)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.EditStory(story))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Route("event")]
        public HttpResponseMessage EditEvent([FromBody]CustomEvent customEvent)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.EditEvent(customEvent))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Route("event/location/{eventID}")]
        public HttpResponseMessage EditEventLocation([FromUri]int eventID, [FromBody]Location location)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.EditEventLocation(eventID, location))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        #endregion HttpPut

        #region HttpDelete
        [HttpDelete]
        [Route("story")]
        public HttpResponseMessage DeleteStory([FromBody]int StoryID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteStory(StoryID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpDelete]
        [Route("event")]
        public HttpResponseMessage DeleteEvent([FromBody]CustomEvent customEvent)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteEvent(customEvent.CustomEventID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpDelete]
        [Route("event/location/{EventID}")]
        public HttpResponseMessage DeleteEventLocation([FromUri]int EventID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeleteEventLocation(EventID))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        #endregion HttpDelete 

    }
}
