﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [RoutePrefix("api/staff")]
    public class StaffController : ApiController
    {
        private IPersonnelRepository<Staff> repository;
        public StaffController(IPersonnelRepository<Staff> staffRepository)
        {
            this.repository = staffRepository;
        }
        [HttpGet]
        [Route("department/{departmentID}")]
        public IEnumerable<Staff> GetStaffByDepartment(string departmentID)
        {
            return repository.GetPersonnelByDepartment(new Department() { DepartmentID = departmentID });
        }
        [HttpGet]
        [Route("{StaffID}")]
        public IEnumerable<Staff> GetStaffByID(string StaffID)
        {
            return repository.GetPersonnelDetails(StaffID);
        }

        [HttpPut]
        [Authorize(Roles = CommonVariables.STAFF)]
        [Route("update")]
        public HttpResponseMessage UpdateStaff(Staff Staff)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePersonnelDetails(User.Identity.GetUserId(), Staff))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }


        [HttpPut]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("update/{StaffID}")]
        public HttpResponseMessage UpdateStaffPriority([FromUri]string StaffID,[FromBody]int? priority)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.UpdatePersonnelPriority(StaffID, priority))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Authorize(Roles = CommonVariables.ADMIN)]
        [Route("delete")]
        public HttpResponseMessage DeleteStaff(Staff Staff)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (repository.DeletePersonnel(Staff))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
    }
}
