﻿using IUT_Dept_Services.Models;
using IUT_Dept_Services.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IUT_Dept_Services.Controllers
{
    [Authorize]
    [RoutePrefix("api/chatting")]
    public class ChattingController : ApiController
    {
        private IChattingRepository chattingRepository;
        public ChattingController(IChattingRepository chattingRepository)
        {
            this.chattingRepository = chattingRepository;
        }

        [HttpGet]
        [Route("chat-groups/{userID}")]
        public async Task<List<ChatGroup>> GetChattingGroupAsync([FromUri] string userID)
        {
            return await chattingRepository.GetChatGroupsAsync(userID, User.Identity.GetUserId());
        }
        [HttpGet]
        [Route("message-file/{messageID}")]
        public byte[] GetMessageFile([FromUri] int messageID)
        {
            return chattingRepository.GetMessageFile(messageID, User.Identity.GetUserId());
        }

        [HttpGet]
        [Route("chat-group-details/{groupID}")]
        public async Task<ChatGroupDTO> GetChatGroupDetailsAsync([FromUri] int groupID)
        {
            return await chattingRepository.GetChatGroupDetailsAsync(groupID, User.Identity.GetUserId());
        }
        [HttpGet]
        [Route("chat-group-user-details/{groupID}")]
        public List<UserDetails> GetUserDetailsByChatGroup([FromUri] int groupID)
        {
            return chattingRepository.GetUserDetailsByGroup(groupID, User.Identity.GetUserId());
        }
        [HttpGet]
        [Route("get-messages/{groupID}")]
        public List<Message> GetMessagesByChatGroup([FromUri] int groupID)
        {
            return chattingRepository.GetChatGroupMessages(groupID, User.Identity.GetUserId());
        }


        [HttpPost]
        [Route("chat-group")]
        public async Task<HttpResponseMessage> AddChattingGroupAsync([FromBody] ChatGroup ChattingGroup)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var v = await chattingRepository.AddNewChatGroup(ChattingGroup, User.Identity.GetUserId());
            if (v > 0)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, v);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPost]
        [Route("send-message")]
        public async Task<HttpResponseMessage> AddNewMessageAsync([FromBody] Message message)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.AddNewMessageAsync(message, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Route("add-member/{groupID}")]
        public async Task<HttpResponseMessage> AddMemberToChattingGroupAsync([FromUri] int groupID, [FromBody]string[] newMemberIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.AddUserToChatGroup(groupID, User.Identity.GetUserId(), newMemberIDs))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
        [HttpPut]
        [Route("remove-member/{groupID}")]
        public async Task<HttpResponseMessage> RemoveMemberToChattingGroupAsync([FromUri] int groupID, [FromBody]string[] newMemberIDs)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.RemoveUserFromChatGroup(groupID, User.Identity.GetUserId(), newMemberIDs))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpPut]
        [Route("edit-group")]
        public async Task<HttpResponseMessage> EditChattingGroupAsync([FromBody]ChatGroup chatGroup)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.EditChatGroup(chatGroup, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Route("delete-group")]
        public async Task<HttpResponseMessage> DeleteChattingGroupAsync([FromBody]ChatGroup chatGroup)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.DeleteChatGroup(chatGroup, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

        [HttpDelete]
        [Route("delete-message/{messageID}")]
        public async Task<HttpResponseMessage> DeleteMessageAsync([FromUri] int messageID)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            if (await chattingRepository.DeleteMessage(messageID, User.Identity.GetUserId()))
            {
                var response = Request.CreateResponse(HttpStatusCode.Created);
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Conflict);
        }

    }
}
