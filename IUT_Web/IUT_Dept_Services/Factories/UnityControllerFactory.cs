﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace IUT_Dept_Services.Factories
{
    public class UnityControllerFactory : DefaultHttpControllerSelector
    {
        public UnityControllerFactory(HttpConfiguration config) : base(config)
        {

        }
        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var p = request.RequestUri.Scheme;
            var v = request.Headers.Host;
            if (!v.Contains("localhost"))
                v += "/services";
            CommonVariables.SELF_URL = p + "://" + v;
            return base.SelectController(request);
        }
    }
}