﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IStoryRepository
    {
        bool AddNewStory(Story story);
        bool EditStory(Story story);
        bool DeleteStory(int storyID);

        bool AddNewEvent(CustomEvent customEvent);
        bool EditEvent(CustomEvent customEvent);
        bool DeleteEvent(int eventID);

        bool AddEventLocation(int eventID,Location location);
        bool EditEventLocation(int eventID, Location location);
        bool DeleteEventLocation(int eventID);

        IQueryable<Object> GetAllStories(string deptID);
        IQueryable<CustomEvent> GetAllEvents(string deptID);

        IQueryable<Object> GetFocusedStories(string deptID);
        IQueryable<CustomEvent> GetFocusedEvents(string deptID);

        Location GetEventLocation(int eventID);
        Story GetStory(int StoryID);
        CustomEvent GetCustomEvent(int CustomEventID);
        bool HasEvent(int StoryID);

        byte[] GetImageByStoryID(int StoryID);
    }
}