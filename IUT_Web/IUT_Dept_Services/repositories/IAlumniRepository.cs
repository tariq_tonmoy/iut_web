﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IAlumniRepository
    {
        bool IsValidAlumniUpdateRequest(Alumni alumni);
        bool UpdateAlumniDetails(string userID, Alumni alumni);
        IQueryable<Alumni> GetAlumniByProgramEnrollment(Program program, int enrollmentYear);
        IQueryable<Alumni> GetAlumniByProgramPassing(Program program, int passingYear);
        IQueryable<Alumni> GetAlumniDetails(string userID);
        IQueryable<Alumni> GetAlumniByProgram(int ProgramID);
        Alumni GetAlumniDetails(int alumniID);
        Alumni GetAlumniDetails(string userID, int programID);
        bool DeleteAlumni(string userID, int AlumniID);
        bool GenerateAlumniAccounts(string programName);
        bool SendAlumniCredentialsViaEmail(string fromEmail, string fromPassword);
        IQueryable<Object> GetAlumniCategoriesByDepartment(string deptID);
    }
}