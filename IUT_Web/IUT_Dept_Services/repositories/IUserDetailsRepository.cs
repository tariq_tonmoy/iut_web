﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IUserDetailsRepository
    {
        string GetUserIDFromUserName(string userName);
        string GetUserNameFromUserID(string userId);
        string GetUserEmail(string UserID);
        IEnumerable<UserRole> GetUserRoles();
        IEnumerable<UserRole> GetRolesByUser(string userID);
        Location GetUserLocations(string UserID);
        Location GetAcademicLocations(int ID);
        Location GetProfessionalLocations(int ID);
        bool isProfessor(string userId);
        bool isDoctor(string userId);


        IQueryable<Contact> GetContacts(string UserID);
        IQueryable<ProfessionalDetails> GetProfessionalDetails(string UserID);
        IQueryable<AcademicDetails> GetAcademicDetails(string UserID);
        object GetPersonalDetailsNoProPic(string UserID);
        PersonalDetails GetPersonalDetails(string UserID);
        byte[] GetProfilePicture(string userID);
        IQueryable<PersonalDetails> SearchPersonalDetailsByName(string searchTerm);

        bool AddUserLocation(Location address, string UserID);
        bool AddAcademicLocation(Location address, int ID);
        bool AddProfessionalLocation(Location address, int ID);

        bool AddContact(Contact contact, string UserID);
        bool AddProfessionalDetails(ProfessionalDetails professionalDetails, string UserID);
        bool AddAcademicDetails(AcademicDetails academicDetails, string UserID);
        bool AddPersonalDetails(PersonalDetails personalDetails, string UserID);
        bool AddUserDetails(string UserID);

        bool EditLocation(Location address, string UserID);
        bool EditContact(Contact contact, string UserID);
        bool EditProfessionalDetails(ProfessionalDetails professionalDetails, string UserID);
        bool EditAcademicDetails(AcademicDetails academicDetails, string UserID);
        bool EditPersonalDetails(PersonalDetails personalDetails, string UserID);

        bool DeleteLocation(int locationID, string UserID);
        bool DeleteContact(int ContactID, string UserID);
        bool DeleteProfessionalDetails(int professionID, string UserID);
        bool DeleteAcademicDetails(int AcademicID, string UserID);
        bool DeletePersonalDetails(string UserID, string currentUser);

        bool DeleteUser(string UserID);

        bool RequestNewRole(string userID, PendingRequest request);

    }
}