﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using System.Data.Entity;

namespace IUT_Dept_Services.Repositories
{
    public class AcademicsRepository : IAcademicsRepository
    {
        ApplicationDbContext db = null;
        public AcademicsRepository()
        {
            db = new ApplicationDbContext();

        }

        public bool AddFacultyToCourse(string facultyID, int courseID)
        {
            var f = db.Faculties.FirstOrDefault(x => x.FacultyID == facultyID);
            var c = db.Courses.FirstOrDefault(x => x.CourseID == courseID);
            if (f != null && c != null && f.DepartmentID == db.Programs.First(x => x.ProgramID == c.ProgramID).DepartmentID)
            {
                try
                {
                    f.Courses.Add(c);
                    c.FacultyList.Add(f);

                    db.Entry(f).State = EntityState.Modified;
                    db.Entry(c).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;
        }

        public bool AddNewCourse(int programID, Course course)
        {
            var v = db.Programs.FirstOrDefault(x => x.ProgramID == programID);
            if (v == null)
                return false;
            course.ProgramID = programID;
            try
            {
                db.Courses.Add(course);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool AddNewCourseMaterial(int courseID, string facultyID, CourseMaterial courseMaterial)
        {
            IQueryable<Course> courses = (from c in db.Courses
                                          where c.CourseID == courseID
                                          select c).AsNoTracking();
            if (courses.Count() <= 0)
                return false;
            var v = (from f in db.Faculties
                     where facultyID == f.FacultyID &&
                     f.Courses.Contains(courses.FirstOrDefault())
                     select f).AsNoTracking();
            if (v.Count() <= 0)
                return false;

            try
            {
                var course = courses.First();
                var faculty = v.First();

                courseMaterial.FacultyID = faculty.FacultyID;
                db.CourseMaterials.Add(courseMaterial);

                faculty.CourseMaterials.Add(courseMaterial);
                course.CourseMaterials.Add(courseMaterial);

                db.Entry(faculty).State = EntityState.Modified;
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }



        }

        public bool AddNewDepartment(Department department)
        {
            if (department.DepartmentID == null)
                return false;
            var v = db.Departments.FirstOrDefault(x => x.DepartmentID == department.DepartmentID);
            if (v != null)
                return false;
            try
            {
                db.Departments.Add(department);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNewProgram(string deptID, Program program)
        {
            var v = from d in db.Departments
                    where d.DepartmentID == deptID &&
                    d.Programs.Contains(db.Programs.FirstOrDefault(x => x.ProgramName.ToLower() == program.ProgramName.ToLower() && x.Concentration.ToLower() == program.Concentration.ToLower()))
                    select d;
            if (v.Count() > 0)
                return false;

            program.DepartmentID = deptID;
            try
            {

                db.Programs.Add(program);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNewSyllabus(int courseID, Syllabus syllabus)
        {
            var v = db.Courses.FirstOrDefault(x => x.CourseID == courseID);
            if (v == null) return false;

            try
            {
                syllabus.Course = v;
                db.Syllabus.Add(syllabus);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteCourse(Course course)
        {
            var v = db.Courses.FirstOrDefault(x => x.CourseID == course.CourseID);
            if (v == null) return false;


            try
            {
                db.Courses.Remove(v);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteCourseMaterial(string facultyID, CourseMaterial courseMaterial)
        {
            var v = db.CourseMaterials.FirstOrDefault(x => x.ID == courseMaterial.ID && x.FacultyID == facultyID);
            if (v == null) return false;

            try
            {
                db.CourseMaterials.Remove(v);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteDepartment(Department department)
        {
            var dept = db.Departments.FirstOrDefault(x => x.DepartmentID == department.DepartmentID);
            if (dept == null) return false;

            var progs = (from p in db.Programs
                         where p.DepartmentID == department.DepartmentID
                         select p).AsNoTracking();
            var fs = (from f in db.Faculties
                      where f.DepartmentID == department.DepartmentID
                      select f).AsNoTracking();
            var ts = (from t in db.TAs
                      where t.DepartmentID == department.DepartmentID
                      select t).AsNoTracking();
            var sfs = (from s in db.Staffs
                       where s.DepartmentID == department.DepartmentID
                       select s).AsNoTracking();

            try
            {
                foreach (var v in progs)
                {
                    v.DepartmentID = null;
                    db.Entry(v).State = EntityState.Modified;
                }
                foreach (var v in fs)
                {
                    v.DepartmentID = null;
                    db.Entry(v).State = EntityState.Modified;
                }
                foreach (var v in ts)
                {
                    v.DepartmentID = null;
                    db.Entry(v).State = EntityState.Modified;
                }
                foreach (var v in sfs)
                {
                    v.DepartmentID = null;
                    db.Entry(v).State = EntityState.Modified;
                }

                db.Departments.Remove(dept);

                db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteFacultyFromCourse(string facultyID, int courseID)
        {
            var f = db.Faculties.FirstOrDefault(x => x.FacultyID == facultyID);
            var c = db.Courses.FirstOrDefault(x => x.CourseID == courseID);
            if (f != null && c != null && f.DepartmentID == db.Programs.First(x => x.ProgramID == c.ProgramID).DepartmentID)
            {
                try
                {

                    db.Entry(f).Collection("Courses").Load();
                    f.Courses.Remove(c);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;
        }

        public bool DeleteProgram(Program program)
        {
            var v = db.Programs.FirstOrDefault(x => x.ProgramID == program.ProgramID);
            if (v == null) return false;

            try
            {
                db.Programs.Remove(v);
                db.SaveChanges();
                var queriable = from s in db.SyllabusReviews
                                where s.ProgramID == program.ProgramID
                                select s;
                if (queriable.Count() > 0)
                {
                    foreach (var s in queriable)
                        db.SyllabusReviews.Remove(s);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteSyllabus(Syllabus syllabus)
        {
            var v = db.Syllabus.FirstOrDefault(x => x.SyllabusID == syllabus.SyllabusID);
            if (v == null) return false;

            try
            {
                db.Syllabus.Remove(v);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditCourse(Course course)
        {
            var v = db.Courses.AsNoTracking().FirstOrDefault(x => x.CourseID == course.CourseID);
            if (v == null) return false;

            try
            {
                course.ProgramID = v.ProgramID;
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditCourseMaterial(string facultyID, CourseMaterial courseMaterial)
        {
            var v = db.CourseMaterials.AsNoTracking().FirstOrDefault(x => x.ID == courseMaterial.ID && x.FacultyID == facultyID);
            if (v == null) return false;

            try
            {
                db.Entry(courseMaterial).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditDepartment(Department department)
        {
            var v = db.Departments.AsNoTracking().FirstOrDefault(x => x.DepartmentID == department.DepartmentID);
            if (v == null) return false;

            try
            {
                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EditProgram(Program program)
        {
            var v = db.Programs.AsNoTracking().FirstOrDefault(x => x.ProgramID == program.ProgramID);
            if (v == null) return false;

            try
            {
                db.Entry(program).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditSyllabus(Syllabus syllabus)
        {
            var v = db.Syllabus.AsNoTracking().FirstOrDefault(x => x.SyllabusID == syllabus.SyllabusID);
            if (v == null) return false;

            try
            {
                db.Entry(syllabus).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IQueryable<Course> GetCourseByFaculty(string userID)
        {
            IQueryable<Course> v = (from c in db.Courses
                                    where c.FacultyList.Contains(db.Faculties.FirstOrDefault(x => x.FacultyID == userID))
                                    select c).AsNoTracking();
            return v;

        }

        public IQueryable<Course> GetCourseByProgram(int programID)
        {
            IQueryable<Course> course = (from p in db.Programs
                                         where p.ProgramID == programID
                                         join c in db.Courses
                                         on p.ProgramID equals c.ProgramID
                                         select c).AsNoTracking();
            return course;
        }

        public IQueryable<CourseMaterial> GetCourseMaterial(string facultyID)
        {
            IQueryable<CourseMaterial> queryable = (from f in db.Faculties
                                                    where f.FacultyID == facultyID
                                                    join c in db.CourseMaterials
                                                    on f.FacultyID equals c.FacultyID
                                                    select c).AsNoTracking();
            return queryable;
        }


        public IQueryable<Object> GetCourseMaterialByCourse(int courseID, string facultyID)
        {
            try
            {
                IQueryable<Object> queryable = (from f in db.Faculties
                                                where f.FacultyID == facultyID
                                                join c in db.CourseMaterials
                                                on f.FacultyID equals c.FacultyID
                                                where f.Courses.Contains(db.Courses.FirstOrDefault(x => x.CourseID == courseID))
                                                where c.Courses.Contains(db.Courses.FirstOrDefault(x => x.CourseID == courseID))
                                                select new
                                                {
                                                    c.FacultyID,
                                                    c.ID,
                                                    c.Name,
                                                    c.Summary,
                                                    c.MaterialType,
                                                    c.MaterialFileName
                                                }).AsNoTracking();
                return queryable;


            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public byte[] GetCourseMaterialFile(int materialID)
        {
            byte[] queryable = (from m in db.CourseMaterials
                                where m.ID == materialID
                                select m.Material).AsNoTracking().FirstOrDefault();
            return queryable;
        }


        public Program GetProgram(int programID)
        {
            Program pg = (from p in db.Programs
                          where p.ProgramID == programID
                          select p).FirstOrDefault();
            return pg;
        }
        public IQueryable<Department> GetDepartments()
        {
            IQueryable<Department> queryable = (from d in db.Departments
                                                select d).AsNoTracking();
            return queryable;
        }

        public Faculty GetDepartmentHead(string DeptID)
        {
            var queryable = (from d in db.Departments
                             where d.DepartmentID == DeptID
                             join f in db.Faculties
                             on d.DepartmentHeadID equals f.FacultyID
                             select f).AsNoTracking().FirstOrDefault();
            return queryable;
        }
        public IQueryable<Program> GetProgramByDepartment(string deptID)
        {
            IQueryable<Program> queryable = (from p in db.Programs
                                             where p.DepartmentID == deptID && p.ProgramName != null
                                             select p).AsNoTracking();
            return queryable;
        }

        public Syllabus GetSyllabusByCourse(int courseID)
        {
            Syllabus queryable = (from s in db.Syllabus
                                  where s.Course.CourseID == courseID
                                  select s).AsNoTracking().FirstOrDefault();
            return queryable;
        }

        public IQueryable<SyllabusReview> GetSyllabusReviews()
        {
            IQueryable<SyllabusReview> queryable = (from s in db.SyllabusReviews
                                                    select s).AsNoTracking();
            return queryable;
        }

        public IQueryable<Object> GetProgramSummaryByDepartment(string deptID)
        {
            var v = (from p in db.Programs
                     where p.DepartmentID == deptID
                     select new
                     {
                         p.ProgramID,
                         p.ProgramName
                     }).AsNoTracking();
            return v;
        }

        public bool DeleteSyllabusReview(int syllabusReviewID)
        {

            var v = db.SyllabusReviews.FirstOrDefault(x => x.SyllabusReviewID == syllabusReviewID);
            if (v == null) return false;

            try
            {
                db.SyllabusReviews.Remove(v);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNewSyllabusReview(SyllabusReview review)
        {
            if (review.SyllabusReviewID < 0)
                return false;
            var v = db.SyllabusReviews.FirstOrDefault(x => x.SyllabusReviewID == review.SyllabusReviewID);
            if (v != null)
                return false;
            try
            {
                db.SyllabusReviews.Add(review);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool EditSyllabusReview(SyllabusReview review)
        {
            var v = db.SyllabusReviews.AsNoTracking().FirstOrDefault(x => x.SyllabusReviewID == review.SyllabusReviewID);
            if (v == null) return false;

            try
            {
                db.Entry(review).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public IQueryable<Course> GetCoursesBySyllabusReview(int progID, int syllabusReviewID)
        {
            IQueryable<Course> queryable = (from syl in db.Syllabus
                                            where syl.SyllabusReviewID == syllabusReviewID
                                            join c in db.Courses
                                            on syl.Course.CourseID equals c.CourseID
                                            select c).AsNoTracking();
            return queryable;
        }

        public IQueryable<Faculty> GetFacultyByCourse(int CourseID)
        {
            var queryable = (from f in db.Faculties
                             where f.Courses.Contains(db.Courses.FirstOrDefault(x => x.CourseID == CourseID))
                             select f).AsNoTracking();
            return queryable;
        }

        public IQueryable<SyllabusReview> GetSyllabusReviewByProgram(int progID)
        {
            IQueryable<SyllabusReview> queryable = (from syl in db.SyllabusReviews
                                                    where syl.ProgramID == progID
                                                    select syl).AsNoTracking();
            return queryable;
        }

        public Department GetDepartmentByID(string DeptID)
        {
            return (from dept in db.Departments
                    where dept.DepartmentID == DeptID
                    select dept).AsNoTracking().FirstOrDefault();
        }
    }
}