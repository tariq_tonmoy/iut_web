﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using System.Data.Entity;
using System.Diagnostics;

namespace IUT_Dept_Services.Repositories
{
    public class StoryRepository : IStoryRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public bool AddEventLocation(int eventID, Location location)
        {
            try
            {
                var v = db.CustomEvents.FirstOrDefault(x => x.CustomEventID == eventID);
                if (v != null)
                {
                    location.Event = v;
                    db.Locations.Add(location);
                    v.EventLocation = location;
                    db.Entry(v).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool AddNewEvent(CustomEvent customEvent)
        {
            try
            {
                db.CustomEvents.Add(customEvent);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNewStory(Story story)
        {
            try
            {
                db.Stories.Add(story);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteEvent(int eventID)
        {
            try
            {
                var v = db.CustomEvents.FirstOrDefault(x => x.CustomEventID == eventID);
                db.CustomEvents.Remove(v);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteEventLocation(int eventID)
        {
            try
            {
                var v = (from ev in db.CustomEvents
                         join loc in db.Locations
                         on ev.CustomEventID equals loc.Event.CustomEventID
                         where ev.CustomEventID == eventID
                         select loc).AsNoTracking().FirstOrDefault();

                if (v == null)
                    return false;
                var d = db.Locations.FirstOrDefault(x => x.LocationID == v.LocationID);
                db.Locations.Remove(d);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteStory(int storyID)
        {
            try
            {
                var s = db.Stories.FirstOrDefault(x => x.StoryID == storyID);
                if (s == null)
                    return false;
                db.Stories.Remove(s);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EditEvent(CustomEvent customEvent)
        {
            try
            {
                var ev = db.CustomEvents.AsNoTracking().FirstOrDefault(x => x.CustomEventID == customEvent.CustomEventID);
                if (ev == null)
                    return false;
                db.Entry(customEvent).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditStory(Story story)
        {
            try
            {
                var st = db.Stories.AsNoTracking().FirstOrDefault(x => x.StoryID == story.StoryID);
                if (st == null)
                    return false;
                db.Entry(story).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IQueryable<CustomEvent> GetAllEvents(string deptID)
        {
            var v = (from ev in db.CustomEvents
                     join st in db.Stories
                     on ev.StoryID equals st.StoryID
                     where st.DeptID == deptID
                     select ev).OrderBy(x => x.EventEnds).AsNoTracking();
            return v;

        }

        public IQueryable<Object> GetAllStories(string deptID)
        {
            return (from s in db.Stories
                    where s.DeptID == deptID
                    select new {
                        s.StoryID,
                        s.Subtitle,
                        s.Title,
                        s.PublishDate,
                        s.IsFocused,
                        s.ImageType,
                        s.Description,
                        s.DescriptionType,
                        s.DeptID,
                        s.Author
                    }).OrderByDescending(x => x.PublishDate).AsNoTracking();
        }

        public Location GetEventLocation(int eventID)
        {
            try
            {
                var v = from ev in db.CustomEvents
                        join loc in db.Locations
                        on ev.CustomEventID equals loc.Event.CustomEventID
                        where ev.CustomEventID == eventID
                        select loc;

                var d = v.FirstOrDefault();
                return v.FirstOrDefault();
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public IQueryable<CustomEvent> GetFocusedEvents(string deptID)
        {
            try
            {

                var v = (from ev in db.CustomEvents
                         join st in db.Stories
                         on ev.StoryID equals st.StoryID
                         where st.DeptID == deptID
                         select ev)
                         .Where(x=>x.EventEnds>=System.Data.Entity.DbFunctions.AddDays(DateTime.UtcNow,-1))
                         .OrderBy(x => x.EventEnds).AsNoTracking();
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }
            
        }

        public IQueryable<Object> GetFocusedStories(string deptID)
        {
            return (from s in db.Stories
                    where s.DeptID == deptID
                    && s.IsFocused
                    select new {
                        s.StoryID,
                        s.Subtitle,
                        s.Title,
                        s.PublishDate,
                        s.IsFocused,
                        s.ImageType,
                        s.Description,
                        s.DescriptionType,
                        s.DeptID,
                        s.Author
                    }).OrderByDescending(x => x.PublishDate).AsNoTracking();
        }

        public bool EditEventLocation(int eventID, Location location)
        {
            try
            {
                var v = (from ev in db.CustomEvents
                         join loc in db.Locations
                         on ev.CustomEventID equals loc.Event.CustomEventID
                         where ev.CustomEventID == eventID
                         select loc).AsNoTracking().FirstOrDefault();
                if (v == null) return false;

                db.Entry(location).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Story GetStory(int StoryID)
        {
            try
            {
                var v = db.Stories.FirstOrDefault(x => x.StoryID == StoryID);
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public CustomEvent GetCustomEvent(int CustomEventID)
        {
            try
            {
                var v = db.CustomEvents.FirstOrDefault(x => x.CustomEventID == CustomEventID);
                return v;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public bool HasEvent(int StoryID)
        {
            try
            {
                var v = (from ev in db.CustomEvents
                         where ev.StoryID == StoryID
                         select ev).Count();
                return v > 0 ? true : false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public byte[] GetImageByStoryID(int StoryID)
        {
            try
            {
                var v = (from s in db.Stories
                         where s.StoryID == StoryID
                         select s.StoryImg).AsNoTracking().FirstOrDefault();
                return v;
            }
            catch (Exception ex)
            {

                return null; 
            }
        }
    }
}