﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Text.RegularExpressions;

namespace IUT_Dept_Services.Repositories
{
    public class UserDetailsRepository : IUserDetailsRepository
    {
        ApplicationDbContext db = null;
        ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        ApplicationRoleManager roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();

        public UserDetailsRepository()
        {
            db = new ApplicationDbContext();
        }

        public bool AddAcademicDetails(AcademicDetails academicDetails, string UserID)
        {
            academicDetails.UserID = UserID;

            if (AddUserDetails(UserID))
            {
                try
                {
                    db.AcademicDetails.Add(academicDetails);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;


        }

        public bool AddContact(Contact contact, string UserID)
        {
            contact.UserID = UserID;
            if (AddUserDetails(UserID))
            {
                try
                {
                    db.Contacts.Add(contact);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;

        }

        public bool AddPersonalDetails(PersonalDetails personalDetails, string UserID)
        {
            personalDetails.UserID = UserID;

            if (AddUserDetails(UserID))
            {
                db.PersonalDetails.Add(personalDetails);
                var v = db.UserDetails.FirstOrDefault(x => x.UserID == UserID);
                if (v == null) return false;
                v.PersonalInfo = personalDetails;
                try
                {
                    db.Entry(v).State = EntityState.Modified;

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;

        }


        public bool AddProfessionalDetails(ProfessionalDetails professionalDetails, string UserID)
        {
            professionalDetails.UserID = UserID;
            if (AddUserDetails(UserID))
            {
                try
                {
                    db.ProfessionalDetails.Add(professionalDetails);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return false;
        }

        public bool AddUserDetails(string UserID)
        {
            var v = db.UserDetails.FirstOrDefault(x => x.UserID == UserID);

            if (v == null)
            {
                UserDetails user = new UserDetails() { UserID = UserID };
                try
                {
                    db.UserDetails.Add(user);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else return true;

        }

        public bool AddUserLocation(Location address, string UserID)
        {
            if (AddUserDetails(UserID))
            {
                var v = db.UserDetails.FirstOrDefault(x => x.UserID == UserID);

                db.Entry(v).Reference("Address").Load();
                if (v.Address != null) return false;
                address.UserDetails = v;
                try
                {
                    db.Locations.Add(address);

                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }


        public bool AddAcademicLocation(Location address, int ID)
        {
            var v = db.AcademicDetails.FirstOrDefault(x => x.AcademicDetailsID == ID);
            db.Entry(v).Reference("Address").Load();

            if (v == null || v.Address != null)
                return false;
            address.AcademicDetails = v;
            try
            {
                db.Locations.Add(address);

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool AddProfessionalLocation(Location address, int ID)
        {
            var v = db.ProfessionalDetails.FirstOrDefault(x => x.ProfessionalDetailsID == ID);
            db.Entry(v).Reference("Address").Load();

            if (v == null || v.Address != null)
                return false;
            address.ProfessionalDetails = v;
            try
            {
                db.Locations.Add(address);

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public IQueryable<AcademicDetails> GetAcademicDetails(string UserID)
        {
            var v = (from academic in db.AcademicDetails
                     where academic.UserID == UserID
                     orderby academic.IsCurrentAcademicStatus descending, academic.To descending
                     select academic).AsNoTracking();
            return v;
        }

        public IQueryable<Contact> GetContacts(string UserID)
        {
            var v = (from contact in db.Contacts
                     where contact.UserID == UserID
                     select contact).AsNoTracking();
            return v;
        }

        public PersonalDetails GetPersonalDetails(string userID)
        {
            var v = (from person in db.PersonalDetails
                     where person.UserID == userID
                     select person).AsNoTracking().FirstOrDefault();
            return v;

        }
        public object GetPersonalDetailsNoProPic(string UserID)
        {
            var v = (from person in db.PersonalDetails
                     where person.UserID == UserID
                     select new
                     {
                         FirstName = person.FirstName,
                         BloodGroup = person.BloodGroup,
                         DoB = person.DoB,
                         ImageType = person.ImageType,
                         LastName = person.LastName,
                         Nationality = person.Nationality,
                         UserID = person.UserID
                     }).AsNoTracking().FirstOrDefault();
            return v;
        }

        public byte[] GetProfilePicture(string userID)
        {
            var v = (from person in db.PersonalDetails
                     where person.UserID == userID
                     select person.Image).AsNoTracking().FirstOrDefault();
            return v;
        }
        public IQueryable<PersonalDetails> SearchPersonalDetailsByName(string searchTerm)
        {
            var v = (from person in db.PersonalDetails
                     where person.FirstName.Contains(searchTerm)
                     || person.LastName.Contains(searchTerm)
                     select person).AsNoTracking();
            return v;
        }


        public IQueryable<ProfessionalDetails> GetProfessionalDetails(string UserID)
        {
            var v = (from profession in db.ProfessionalDetails
                     where profession.UserID == UserID
                     orderby profession.IsCurrentProfessionalStatus descending, profession.To descending
                     select profession).AsNoTracking();
            return v;
        }

        public Location GetUserLocations(string UserID)
        {
            var v = (from address in db.Locations
                     where address.UserDetails.UserID == UserID
                     select address).AsNoTracking();
            return v.FirstOrDefault();
        }
        public Location GetAcademicLocations(int ID)
        {
            var v = (from address in db.Locations
                     where address.AcademicDetails.AcademicDetailsID == ID
                     select address).AsNoTracking();
            return v.FirstOrDefault();
        }

        public Location GetProfessionalLocations(int ID)
        {
            var v = (from address in db.Locations
                     where address.ProfessionalDetails.ProfessionalDetailsID == ID
                     select address).AsNoTracking();
            return v.FirstOrDefault();
        }

        public bool EditLocation(Location address, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join addr in db.Locations
                     on user.UserID equals addr.UserDetails.UserID
                     where addr.LocationID == address.LocationID
                     select addr).AsNoTracking();

            if (v.Count() == 0)
                return false;
            try
            {
                address.UserDetails = db.UserDetails.Find(UserID);
                db.Entry(address).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditContact(Contact contact, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.Contacts
                     on user.UserID equals obj.UserID
                     where obj.ContactID == contact.ContactID
                     select obj).AsNoTracking();

            if (v.Count() == 0)
                return false;
            contact.UserID = UserID;
            try
            {
                db.Entry(contact).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditProfessionalDetails(ProfessionalDetails professionalDetails, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.ProfessionalDetails
                     on user.UserID equals obj.UserID
                     where obj.ProfessionalDetailsID == professionalDetails.ProfessionalDetailsID
                     select obj).AsNoTracking();
            var loc = from locs in db.Locations
                      where locs.LocationID == professionalDetails.Address.LocationID
                      && locs.ProfessionalDetails.ProfessionalDetailsID == professionalDetails.ProfessionalDetailsID
                      select locs;
            if (v.Count() == 0)
                return false;
            professionalDetails.UserID = UserID;
            try
            {
                db.Entry(professionalDetails).State = EntityState.Modified;
                if (loc.Count() <= 0)
                {
                    this.AddProfessionalLocation(professionalDetails.Address, professionalDetails.ProfessionalDetailsID);
                }
                else
                {
                    db.Entry(professionalDetails.Address).State = EntityState.Modified;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditAcademicDetails(AcademicDetails academicDetails, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.AcademicDetails
                     on user.UserID equals obj.UserID
                     where obj.AcademicDetailsID == academicDetails.AcademicDetailsID
                     select obj).AsNoTracking();
            var loc = from locs in db.Locations
                      where locs.LocationID == academicDetails.Address.LocationID
                      && locs.AcademicDetails.AcademicDetailsID == academicDetails.AcademicDetailsID
                      select locs;



            if (v.Count() == 0)
                return false;
            academicDetails.UserID = UserID;
            try
            {
                db.Entry(academicDetails).State = EntityState.Modified;
                if (loc.Count() <= 0)
                {
                    this.AddAcademicLocation(academicDetails.Address, academicDetails.AcademicDetailsID);
                }
                else
                {
                    db.Entry(academicDetails.Address).State = EntityState.Modified;
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EditPersonalDetails(PersonalDetails personalDetails, string UserID)
        {
            var userDetails = db.UserDetails.FirstOrDefault(x => x.UserID == UserID);
            if (userDetails == null)
                return false;
            personalDetails.UserID = UserID;
            try
            {
                db.Entry(personalDetails).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool DeleteLocation(int locationID, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join addr in db.Locations
                     on user.UserID equals addr.UserDetails.UserID
                     where addr.LocationID == locationID
                     select addr).AsNoTracking();

            if (v.Count() == 0)
                return false;
            try
            {
                var t = v.First();
                db.Locations.Attach(t);
                db.Locations.Remove(t);

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteContact(int ContactID, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.Contacts
                     on user.UserID equals obj.UserDetails.UserID
                     where obj.ContactID == ContactID
                     select obj).AsNoTracking();

            if (v.Count() == 0)
                return false;
            try
            {
                var t = v.First();
                db.Contacts.Attach(t);
                db.Contacts.Remove(t);

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteProfessionalDetails(int professionID, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.ProfessionalDetails
                     on user.UserID equals obj.UserDetails.UserID
                     where obj.ProfessionalDetailsID == professionID
                     select obj).AsNoTracking();

            if (v.Count() == 0)
                return false;
            try
            {
                var t = v.First();
                var loc = from locs in db.Locations
                          where locs.ProfessionalDetails.ProfessionalDetailsID == t.ProfessionalDetailsID
                          select locs;
                foreach (var l in loc)
                {
                    db.Locations.Attach(l);
                    db.Locations.Remove(l);
                }
                db.ProfessionalDetails.Attach(t);
                db.ProfessionalDetails.Remove(t);

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteAcademicDetails(int AcademicID, string UserID)
        {
            var v = (from user in db.UserDetails
                     where user.UserID == UserID
                     join obj in db.AcademicDetails
                     on user.UserID equals obj.UserDetails.UserID
                     where obj.AcademicDetailsID == AcademicID
                     select obj).AsNoTracking();

            if (v.Count() == 0)
                return false;
            try
            {
                var t = v.First();
                var loc = from locs in db.Locations
                          where locs.AcademicDetails.AcademicDetailsID == t.AcademicDetailsID
                          select locs;
                foreach (var l in loc)
                {
                    db.Locations.Attach(l);
                    db.Locations.Remove(l);
                }

                db.AcademicDetails.Attach(t);
                db.AcademicDetails.Remove(t);

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public bool DeletePersonalDetails(string UserID, string currentUser)
        {
            var v = db.PersonalDetails.FirstOrDefault(x => x.UserID == UserID);
            if (v == null)
                return false;
            try
            {
                db.PersonalDetails.Attach(v);
                db.PersonalDetails.Remove(v);


                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private bool HandlePersonnelRequest(UserDetails userDetails, string departmentID, string roleID, PendingRequest pendingRequest)
        {

            var d = db.Departments.FirstOrDefault(x => x.DepartmentID == departmentID);
            if (d == null)
                return false;
            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var role = roleManager.FindById(roleID);
            if (CommonVariables.PERSONNEL_ROLES.Contains(role.Name))
            {
                if (db.PendingRequests.FirstOrDefault(x => x.RequestedRoleID == role.Id && x.RequestedDepartmentID == departmentID && x.RequestUserID == userDetails.UserID) != null)
                    return false;
                var userRoles = userManager.GetRoles(userDetails.UserID);
                if (userRoles.Contains(role.Name))
                    return false;

                PendingRequest request = new PendingRequest()
                {
                    RequestedDepartmentID = departmentID,
                    RequestUserID = userDetails.UserID,
                    RequestedRoleID = role.Id,
                    MessageToAdmin = pendingRequest.MessageToAdmin,
                    SubmissionDate = DateTime.UtcNow
                };
                try
                {
                    db.PendingRequests.Add(request);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                }
            }
            return false;
        }
        private bool HandleAlumniRequest(UserDetails userDetails, int? programID, PendingRequest pendingRequest)
        {
            var p = db.Programs.FirstOrDefault(x => x.ProgramID == programID);
            if (p == null)
                return false;
            if (db.PendingRequests.FirstOrDefault(x => x.RequestedProgramID == programID && x.RequestUserID == userDetails.UserID) != null)
                return false;

            var chk = from v in db.Alumni
                      where v.UserID == userDetails.UserID
                      && v.ProgramID == programID
                      select v;
            if (chk.Count() > 0)
                return false;

            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            var role = roleManager.FindByName(CommonVariables.ROLES[ROLENAMES.ALUMNI]);

            PendingRequest request = new PendingRequest()
            {
                RequestedProgramID = programID,
                RequestUserID = userDetails.UserID,
                RequestedRoleID = role.Id,
                MessageToAdmin = pendingRequest.MessageToAdmin

            };

            try
            {
                db.PendingRequests.Add(request);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }

        private bool HandleAdminRequest(UserDetails userDetails, PendingRequest pendingRequest)
        {
            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var role = roleManager.FindByName(CommonVariables.ROLES[ROLENAMES.ADMIN]);
            if (db.PendingRequests.FirstOrDefault(x => x.RequestedRoleID == role.Id && x.RequestUserID == userDetails.UserID) != null)
                return false;

            var rolesForUser = userManager.GetRoles(userDetails.UserID);
            if (rolesForUser.Contains(role.Name))
                return false;
            PendingRequest request = new PendingRequest()
            {
                RequestUserID = userDetails.UserID,
                RequestedRoleID = role.Id,
                MessageToAdmin = pendingRequest.MessageToAdmin
            };

            try
            {
                db.PendingRequests.Add(request);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool RequestNewRole(string userID, PendingRequest request)
        {
            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();

            var role = roleManager.FindById(request.RequestedRoleID);
            if (role == null)
                return false;
            if (AddUserDetails(userID))
            {
                var user = db.UserDetails.FirstOrDefault(x => x.UserID == userID);
                if (user == null)
                    return false;
                if (role.Name == CommonVariables.ROLES[ROLENAMES.ADMIN])
                    return HandleAdminRequest(user, request);
                else if (role.Name == CommonVariables.ROLES[ROLENAMES.ALUMNI])
                    return HandleAlumniRequest(user, request.RequestedProgramID, request);
                else return HandlePersonnelRequest(user, request.RequestedDepartmentID, request.RequestedRoleID, request);
            }
            else return false;

        }

        public string GetUserIDFromUserName(string userName)
        {
            try
            {
                return userManager.FindByName(userName).Id;

            }
            catch (Exception)
            {

                return null;
            }
        }
        public string GetUserNameFromUserID(string userId)
        {
            try
            {
                return userManager.FindById(userId).UserName;

            }
            catch (Exception)
            {

                return null;
            }

        }

        public IEnumerable<UserRole> GetUserRoles()
        {
            List<UserRole> roles = new List<UserRole>();
            foreach (var role in this.roleManager.Roles)
            {
                string id = role.Id;
                string name = role.Name;
                UserRole userRole = new UserRole() { RoleID = id, RoleName = name };
                roles.Add(userRole);
            }
            return roles.AsEnumerable();
        }

        public IEnumerable<UserRole> GetRolesByUser(string userID)
        {
            List<UserRole> roles = new List<UserRole>();
            foreach (var roleName in this.userManager.GetRoles(userID))
            {
                var role = this.roleManager.FindByName(roleName);
                if (role == null)
                    continue;
                string id = role.Id;
                string name = role.Name;
                UserRole userRole = new UserRole() { RoleID = id, RoleName = name };
                roles.Add(userRole);
            }
            return roles.AsEnumerable();
        }

        public string GetUserEmail(string UserID)
        {
            return userManager.GetEmail(UserID);
        }

        public bool isProfessor(string userId)
        {
            var patterns = new string[]
            {
                @"^\s*professor\s*\.?\s*$",
                @"^\s*prof\s*\.?\s*$"
            };
            var rx = new Regex(String.Join("|", patterns), RegexOptions.IgnoreCase);
            var querableProfessional = db.ProfessionalDetails.Where(
               p => p.Designation != null
               && p.UserID == userId).ToList();
            var querableFaculty = db.Faculties.Where(
               p => p.Designation != null
               && p.FacultyID == userId).ToList();


            return querableProfessional.Where(x => rx.IsMatch(x.Designation)).FirstOrDefault() == null ? (querableFaculty.Where(x => rx.IsMatch(x.Designation)).FirstOrDefault() == null ? false : true) : true;
        }

        public bool isDoctor(string userId)
        {
            var patterns = new string[]
            {
                @"^\s*doctor\s*of\s*philosophy\s*$",
                @"^\s*doctorate\s*of\s*philosophy\s*$",
                @"^\s*ph\s*.?\s*d\.?\s*$"
            };
            var rx = new Regex(String.Join("|", patterns), RegexOptions.IgnoreCase);
            var querable = db.AcademicDetails.Where(
                p => p.Degree != null
                && p.UserID == userId).ToList();
            return querable.Where(x => rx.IsMatch(x.Degree) && !x.IsCurrentAcademicStatus).FirstOrDefault() == null ? false : true;
        }

        public bool DeleteUser(string UserName)
        {
            string UserID = this.GetUserIDFromUserName(UserName);
            var loc = this.GetUserLocations(UserID);
            if (loc != null)
            {
                this.DeleteLocation(loc.LocationID, UserID);
            }
            this.DeletePersonalDetails(UserID, UserID);

            var profs = this.GetProfessionalDetails(UserID).ToList();
            foreach (var v in profs)
            {
                this.DeleteProfessionalDetails(v.ProfessionalDetailsID, UserID);
            }

            var academics = this.GetAcademicDetails(UserID).ToList();
            foreach (var v in academics)
            {
                this.DeleteAcademicDetails(v.AcademicDetailsID, UserID);
            }

            var contacts = this.GetContacts(UserID).ToList();

            foreach (var contact in contacts)
            {
                this.DeleteContact(contact.ContactID, UserID);
            }
            List<UserRole> roles = null;
            try
            {
                roles = this.GetRolesByUser(UserID).ToList();
            }
            catch (Exception)
            {

            }
            if (roles != null)
            {
                foreach (var role in roles)
                {
                    if (role.RoleName.ToUpper() == "FACULTY")
                    {
                        try
                        {
                            var faculty = db.Faculties.First(x => x.FacultyID == UserID);
                            db.Faculties.Attach(faculty);
                            db.Faculties.Remove(faculty);
                            db.SaveChanges();


                        }
                        catch (Exception ex)
                        {
                            return false;

                        }
                    }
                    else if (role.RoleName.ToUpper() == "STAFF")
                    {
                        try
                        {
                            var staff = db.Staffs.First(x => x.StaffID == UserID);
                            db.Staffs.Attach(staff);
                            db.Staffs.Remove(staff);
                            db.SaveChanges();

                        }
                        catch (Exception ex)
                        {

                            return false;
                        }
                    }
                    else if (role.RoleName.ToUpper() == "TA")
                    {
                        try
                        {
                            var ta = db.TAs.First(x => x.TAID == UserID);
                            db.TAs.Attach(ta);
                            db.TAs.Remove(ta);
                            db.SaveChanges();

                        }
                        catch (Exception ex)
                        {
                            return false;

                        }
                    }
                    else if (role.RoleName.ToUpper() == "ALUMNI")
                    {
                        try
                        {
                            var alumni = db.Alumni.Where(x => x.UserID == UserID).ToList();
                            foreach (var v in alumni)
                            {
                                db.Alumni.Attach(v);
                                db.Alumni.Remove(v);
                                db.SaveChanges();

                            }
                        }
                        catch (Exception ex)
                        {
                            return false;

                        }
                    }
                    userManager.RemoveFromRole(UserID, role.RoleName);
                }
            }
            try
            {
                var user = db.UserDetails.FirstOrDefault(x => x.UserID == UserID);
                if (user != null)
                {
                    var pubs = db.Publications.Where(x => x.CreatorID == UserID).ToList();
                    foreach (var pub in pubs)
                    {
                        pub.CreatedBy = null;
                        pub.CreatorID = null;
                        db.Entry(pub).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var rgs = db.ResearchGroups.Where(x => x.CreatorID == UserID).ToList();
                    foreach (var rg in rgs)
                    {
                        rg.CreatedBy = null;
                        rg.CreatorID = null;
                        db.Entry(rg).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var ris = db.ResearchInterests.Where(x => x.CreatorID == UserID).ToList();
                    foreach (var ri in ris)
                    {
                        ri.CreatedBy = null;
                        ri.CreatorID = null;
                        db.Entry(ri).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                    db.UserDetails.Attach(user);
                    db.UserDetails.Remove(user);
                    db.SaveChanges();
                }

                ApplicationUser AppUser = userManager.Users.First(x => x.Id == UserID);
                if (AppUser != null)
                    userManager.Delete(AppUser);

            }
            catch (Exception ex)
            {
                return false;

            }

            return true;
        }

    }
}