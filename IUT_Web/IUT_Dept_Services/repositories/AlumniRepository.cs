﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IUT_Dept_Services.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace IUT_Dept_Services.Repositories
{
    public class AlumniRepository : IAlumniRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();
        ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        AlumniDBContext dbAlumni = new AlumniDBContext();
        public bool DeleteAlumni(string userID, int AlumniID)
        {
            try
            {
                db.Alumni.Remove(db.Alumni.First(x => x.AlumniID == AlumniID && x.UserID == userID));
                db.SaveChanges();
                if (db.Alumni.FirstOrDefault(x => x.UserID == userID) == null)
                    userManager.RemoveFromRole(userID, CommonVariables.ROLES[ROLENAMES.ALUMNI]);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool GenerateAlumniAccounts(string programName)
        {
            var alumnies = (from al in dbAlumni.AlumniSummaries
                            where String.IsNullOrEmpty(al.Username)
                            select al).AsNoTracking().ToList();
            var newAlumnies = (from v in alumnies
                               where userManager.FindByEmail(v.EmailPrimary) == null
                               && userManager.FindByName(v.EmailPrimary.Split('@')[0]) == null
                               select v).ToList();
            var oldAlumnies = (from v in alumnies
                               where userManager.FindByName(v.EmailPrimary.Split('@')[0]) != null
                               || userManager.FindByEmail(v.EmailPrimary) != null
                               select v).ToList();

            foreach (var v in newAlumnies)
            {
                v.Username = v.EmailPrimary.Split('@')[0];
                if (v.Username.Length < 6)
                {
                    var len = v.Username.Length;
                    for (int i = 0; i < 6 - len; i++)
                    {
                        v.Username += i.ToString();
                    }
                }


                var user = new ApplicationUser()
                {
                    UserName = v.Username,
                    Email = v.EmailPrimary
                };

                var nameAr = v.Name.Split(' ');
                if (nameAr.Length == 1)
                {
                    nameAr = v.Name.Split('-');
                    if (nameAr.Length == 1)
                        nameAr = v.Name.Split('.');
                }
                var ln = nameAr[(nameAr.Length - 1)];
                var fn = "";
                for (int i = 0; i < nameAr.Length - 1; i++)
                {
                    fn += nameAr[i] + ' ';
                }
                int c = 0;
                while (true)
                {
                    try
                    {
                        if (c > 100)
                        {
                            c = 0;
                            break;
                        }
                        v.Password = System.Web.Security.Membership.GeneratePassword(12, 2);
                        var res = userManager.Create(user, v.Password);
                        res = userManager.SetLockoutEnabled(user.Id, false);
                        var result = userManager.AddToRole(user.Id, CommonVariables.ALUMNI);
                        v.IsNewUser = true;
                        dbAlumni.Entry(v).State = EntityState.Modified;
                        var userDetails = new UserDetails()
                        {
                            UserID = user.Id,
                            PersonalInfo = new PersonalDetails()
                            {
                                UserID = user.Id,
                                FirstName = fn,
                                LastName = ln
                            }
                        };
                        db.UserDetails.Add(userDetails);
                        var program = (from p in db.Programs
                                       where p.DepartmentID == v.Department
                                       && (p.ProgramName.Replace(" ", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Replace(".", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Replace(". ", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Contains(programName.ToLower()))
                                       select p).FirstOrDefault();
                        if (program != null)
                        {
                            var alumniDetails = new Alumni() { StudentID = v.StudentID, UserID = user.Id, AdmissionYear = v.Batch, ProgramID = program.ProgramID };
                            db.Alumni.Add(alumniDetails);
                        }
                        var contact = new Contact() { ContactType = "Mobile", ContactDetails = v.PhonePrimary, UserID = user.Id };
                        db.Contacts.Add(contact);
                        Debug.WriteLine(v.ID);
                        break;
                    }
                    catch (Exception ex)
                    {
                        c++;
                    }
                }

            }

            foreach (var v in oldAlumnies)
            {
                var user = userManager.FindByEmail(v.EmailPrimary);
                v.Username = v.EmailPrimary.Split('@')[0];
                v.IsNewUser = false;
                dbAlumni.Entry(v).State = EntityState.Modified;
                if (user == null) user = userManager.FindByName(v.Username);
                if (user != null)
                {
                    v.Username = user.UserName;
                    if (!userManager.GetRoles(user.Id).Contains(CommonVariables.ALUMNI))
                    {
                        var result = userManager.AddToRole(user.Id, CommonVariables.ALUMNI);
                        var program = (from p in db.Programs
                                       where p.DepartmentID == v.Department
                                       && (p.ProgramName.Replace(" ", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Replace(".", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Replace(". ", "").ToLower().Contains(programName.ToLower())
                                       || p.ProgramName.Contains(programName.ToLower()))
                                       select p).FirstOrDefault();
                        if (program != null)
                        {

                            var alumniDetails = new Alumni() { StudentID = v.StudentID, UserID = user.Id, AdmissionYear = v.Batch, ProgramID = program.ProgramID };
                            db.Alumni.Add(alumniDetails);
                        }
                        var contact = new Contact() { ContactType = "Mobile", ContactDetails = v.PhonePrimary, UserID = user.Id };
                        db.Contacts.Add(contact);
                        if (userManager.FindByEmail(v.EmailPrimary) == null)
                        {
                            contact = new Contact() { ContactType = "Email", ContactDetails = v.EmailPrimary, UserID = user.Id };
                            db.Contacts.Add(contact);
                        }
                    }
                }
            }
            try
            {
                db.SaveChanges();
                dbAlumni.SaveChanges();
            }

            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

        public IQueryable<Alumni> GetAlumniByProgram(int ProgramID)
        {
            var v = (from alm in db.Alumni
                     where alm.EnrollmentYear != null
                    && alm.ProgramID == ProgramID
                     select alm).AsNoTracking().OrderByDescending(x => x.EnrollmentYear.Value.Year);
            return v;
        }

        public IQueryable<Alumni> GetAlumniByProgramEnrollment(Program program, int enrollmentYear)
        {
            var v = (from alm in db.Alumni
                     where (alm.EnrollmentYear != null
                     || alm.AdmissionYear != null)
                     && (alm.EnrollmentYear.Value.Year == enrollmentYear
                     || alm.AdmissionYear == enrollmentYear)
                     && alm.ProgramID == program.ProgramID
                     select alm).OrderBy(x => x.StudentID).AsNoTracking();
            return v;
        }

        public IQueryable<Alumni> GetAlumniByProgramPassing(Program program, int passingYear)
        {
            var v = (from alm in db.Alumni
                     where alm.PassingYear != null
                     && alm.PassingYear.Value.Year == passingYear
                     && alm.ProgramID == program.ProgramID
                     select alm).AsNoTracking();
            return v;
        }

        public IQueryable<Alumni> GetAlumniDetails(string userID)
        {
            var v = (from alm in db.Alumni
                     where alm.UserID == userID
                     select alm).AsNoTracking();
            return v;
        }

        public Alumni GetAlumniDetails(int alumniID)
        {
            var v = (from alm in db.Alumni
                     where alm.AlumniID == alumniID
                     select alm).AsNoTracking();
            return v.Count() > 0 ? v.First() : null;
        }

        public Alumni GetAlumniDetails(string userID, int programID)
        {
            var v = (from alm in db.Alumni
                     where alm.UserID == userID
                     && alm.ProgramID == programID
                     select alm).AsNoTracking();
            return v.Count() > 0 ? v.First() : null;
        }

        public bool IsValidAlumniUpdateRequest(Alumni alumni)
        {
            var v = (from alm in db.Alumni
                     where alumni.UserID != null
                     && alm.UserID == alumni.UserID
                     && alm.ProgramID == alumni.ProgramID
                     && alm.IsFocused == alumni.IsFocused
                     select alm).AsNoTracking();
            return v.Count() > 0 ? true : false;
        }

        public bool UpdateAlumniDetails(string userID, Alumni alumni)
        {
            if (IsValidAlumniUpdateRequest(alumni))
            {
                var alm = GetAlumniDetails(alumni.UserID, alumni.ProgramID);
                if (alm == null || alm.UserID != userID)
                    return false;
                alumni.AlumniID = alm.AlumniID;
                try
                {
                    db.Entry(alumni).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public IQueryable<Object> GetAlumniCategoriesByDepartment(string deptID)
        {
            var v = (from p in db.Alumni
                     where p.Program.DepartmentID == deptID
                     && p.AdmissionYear != null
                     select new
                     {
                         p.ProgramID,
                         p.AdmissionYear
                     }).Distinct().OrderBy(x => x.AdmissionYear).AsNoTracking();
            return v;
        }

        public bool SendAlumniCredentialsViaEmail(string fromEmail, string _fromPassword)
        {
            var fromAddress = new MailAddress(fromEmail);
            string fromPassword = _fromPassword;
            var new_alumni = (from al in dbAlumni.AlumniSummaries
                              where al.IsNewUser == true
                              && al.Password != null
                              && al.Username != null
                              && al.EmailPrimary != null
                              select al).AsNoTracking().ToList();
            foreach (var v in new_alumni)
            {
                var toAddress = new MailAddress(v.EmailPrimary);
                string subject = "IUT-CSE Departmental Website Account Credentials";
                string body = "Dear " + v.Username + ",\n"
                                + "Hope this finds you well. I am writing to you to inform you that the Department of CSE, IUT is about to launch a new Website. There is an account created using your email address in the website. Below you can find the link to the website and your credentials to log in. You can update your profile with your current academic and professional achievements.";
                body += "\n\nWebsite: https://csenew.iutoic-dhaka.edu \n";
                body += "Username: " + v.Username + "\n";
                body += "Password: " + v.Password + "\n\n";
                body += "If the website goes live, you can visit it in https://cse.iutoic-dhaka.edu \n";
                body += "Thanks";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    try
                    {
                        smtp.Send(message);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }
            }
            return true;
        }
    }
}