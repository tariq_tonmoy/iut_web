﻿using IUT_Dept_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUT_Dept_Services.Repositories
{
    public interface IAcademicsRepository
    {
        Syllabus GetSyllabusByCourse(int courseID);
        IQueryable<Course> GetCourseByProgram(int programID);
        IQueryable<Course> GetCourseByFaculty(string userID);

        IQueryable<Program> GetProgramByDepartment(string deptID);
        IQueryable<Object> GetCourseMaterialByCourse(int courseID, string facultyID);
        IQueryable<CourseMaterial> GetCourseMaterial(string facultyID);

        IQueryable<Department> GetDepartments();
        IQueryable<SyllabusReview> GetSyllabusReviews();
        IQueryable<Object> GetProgramSummaryByDepartment(string deptID);
        IQueryable<Course> GetCoursesBySyllabusReview(int progID, int syllabusReviewID);
        IQueryable<SyllabusReview> GetSyllabusReviewByProgram(int progID);
        IQueryable<Faculty> GetFacultyByCourse(int CourseID);
        byte[] GetCourseMaterialFile(int materialID);
        Faculty GetDepartmentHead(string DeptID);
        Program GetProgram(int programID);
        Department GetDepartmentByID(string DeptID);

        bool AddNewSyllabus(int courseID, Syllabus syllabus);
        bool AddNewCourse(int programID, Course course);
        bool AddNewProgram(string deptID, Program program);
        bool AddFacultyToCourse(string facultyID, int courseID);
        bool AddNewCourseMaterial(int courseID, string facultyID, CourseMaterial courseMaterial);
        bool AddNewDepartment(Department department);
        bool AddNewSyllabusReview(SyllabusReview review);

        bool EditSyllabus(Syllabus syllabus);
        bool EditProgram(Program program);
        bool EditDepartment(Department department);
        bool EditCourse(Course course);
        bool EditCourseMaterial(string facultyID, CourseMaterial courseMaterial);
        bool EditSyllabusReview(SyllabusReview review);

        bool DeleteSyllabus(Syllabus syllabus);
        bool DeleteProgram(Program program);
        bool DeleteDepartment(Department department);
        bool DeleteCourse(Course course);
        bool DeleteCourseMaterial(string facultyID, CourseMaterial courseMaterial);
        bool DeleteFacultyFromCourse(string facultyID, int courseID);
        bool DeleteSyllabusReview(int syllabusReviewID);
    }
}