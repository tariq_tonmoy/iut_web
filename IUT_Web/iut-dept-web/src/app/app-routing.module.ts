import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

import {ErrorComponent} from "./error.component";
import {ExternalAuthComponent} from "./components/external-auth/external-auth.component";


const routes: Routes = [
  {path: "error", component: ErrorComponent},
  {path: "external-auth", component: ExternalAuthComponent},
  {path: "", redirectTo: "cse/home", pathMatch: "full"},
  {path: "**", redirectTo: "/error", pathMatch: "full"}];


@NgModule(
  {
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })

export class AppRoutingModule {

}
