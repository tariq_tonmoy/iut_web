import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AcademicsService} from "../../service/academics.service";
import {AlumniService} from "../../service/alumni.service";
import {AlumniDataStruct} from "../../view-model/alumni-data-struct";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cse-dept-alumni',
  templateUrl: './cse-dept-alumni.component.html',
  styleUrls: ['./cse-dept-alumni.component.css']
})
export class CseDeptAlumniComponent implements OnInit, AfterViewInit {

  constructor(private academicsService: AcademicsService,
              private alumniService: AlumniService,
              private personalService: PersonalDetailsService,
              private router: Router) {
  }


  public alumniCategoryList: AlumniDataStruct[] = [];
  public programSummaryList: ProgramDataStruct[] = [];

  public defaultAlumniCategory: AlumniDataStruct;
  public alumniList: AlumniDataStruct[] = [];
  public profileList: PersonalDetailsDataStruct[] = [];

  ngOnInit() {
    window.scroll(0, 0);

    this.InitializeSummaryByDept("CSE");
    this.InitializeSummaryByDept("CIT");
  }

  private InitializeSummaryByDept(deptID: string): void {
    this.alumniService.GetAlumniCategories(deptID).then(resp => {
      this.alumniCategoryList = this.alumniCategoryList.concat(resp).sort((a, b) => a.AdmissionYear < b.AdmissionYear ? 1 : a.AdmissionYear > b.AdmissionYear ? -1 : 0);
    }).catch(err => {
      console.log(err);
    });
    this.academicsService.GetProgramsSummary(deptID).then(resp => {
      this.programSummaryList = this.programSummaryList.concat(resp);
    }).catch(err => {
      console.log(err);
    });

  }

  public findProgramName(programID: number): string {
    try {
      var v = this.programSummaryList.find(x => x.ProgramID === programID);
      return v === null ? "" : v.ProgramName;
    } catch (err) {
      return "";
    }
  }

  public findProfileForView(userID: string): PersonalDetailsDataStruct {
    try {
      var v = this.profileList.find(x => x.UserID === userID);
      return v;
    } catch (err) {
      return null;
    }
  }

  private getEmail(userID: string): void {
    this.personalService.GetUserEmail(userID).then(email => {
      var person = this.profileList.find(x => x.UserID === userID);
      if (person)
        person.Email = email;
    });
  }

  private getProfilePic(userID: string): void {
    this.personalService.GetProfilePicture(userID)
      .then(pic => {
        if (pic) {
          var person = this.profileList.find(x => x.UserID === userID);
          if (person)
            person.Image = pic;
        }
      })
      .catch(err => {

      });
  }

  private findProfileDetails(userID: string): void {
    this.personalService.GetProfileDetails(userID).then(resp => {
      this.profileList.push(resp);
      this.getEmail(resp.UserID);
      this.getProfilePic(resp.UserID);
    }).catch(err => {
      console.log(err);
    });
  }

  public onProfileDetailsClicked(userID: string): void {
    this.personalService.getUserNameFromUserID(userID).then(res => {
      this.router.navigate(['cse/profile/' + encodeURIComponent(res)]).then(resp => {

      }).catch(err => {
        console.log(err);
      });
    }).catch(err_s => {
      console.log(err_s);
    });
  }

  public onCategoryClicked(al: AlumniDataStruct): void {

    this.profileList = [];
    this.alumniList = [];
    if (this.defaultAlumniCategory && this.defaultAlumniCategory === al) {
      this.defaultAlumniCategory = null;
    }
    else {
      this.defaultAlumniCategory = al;
      this.alumniService.GetAlumniByProgramEnrollemnt(al.ProgramID, al.AdmissionYear).then(resp => {
        this.alumniList = resp;
        for (let p of this.alumniList)
          this.findProfileDetails(p.UserID);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  public onUpClicked(): void {
    window.scroll(0, 0);
  }

  ngAfterViewInit(): void {
    window.onscroll = function () {
      try {
        var el = document.getElementById("top_button");
        if (el) {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            el.style.display = "block";
          } else {
            el.style.display = "none";
          }
        }

      } catch (e) {

      }
    };
  }


}
