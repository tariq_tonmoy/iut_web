import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptAlumniComponent } from './cse-dept-alumni.component';

describe('CseDeptAlumniComponent', () => {
  let component: CseDeptAlumniComponent;
  let fixture: ComponentFixture<CseDeptAlumniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptAlumniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptAlumniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
