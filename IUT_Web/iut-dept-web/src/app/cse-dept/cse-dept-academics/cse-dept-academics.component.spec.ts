import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptAcademicsComponent } from './cse-dept-academics.component';

describe('CseDeptAcademicsComponent', () => {
  let component: CseDeptAcademicsComponent;
  let fixture: ComponentFixture<CseDeptAcademicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptAcademicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptAcademicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
