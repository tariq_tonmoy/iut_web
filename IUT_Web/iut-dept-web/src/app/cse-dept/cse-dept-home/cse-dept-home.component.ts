import {Component, OnInit, ViewChild} from '@angular/core';
import {NewsService} from "../../service/news.service";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {ActivatedRoute, Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {ModalDirective} from "ngx-bootstrap";
import {ExternalUserInfoDataStruct} from "../../view-model/external-user-info-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AuthenticationService} from "../../service/authentication.service";

declare var $: any;

@Component({
  selector: 'app-cse-dept-home',
  templateUrl: './cse-dept-home.component.html',
  styleUrls: ['./cse-dept-home.component.css']
})
export class CseDeptHomeComponent implements OnInit {

  public dept: DepartmentDataStruct = new DepartmentDataStruct('', '');
  public isLoaded: boolean = false;
  public regErrorMessage: string = "";

  constructor(private newsService: NewsService,
              private academicService: AcademicsService,
              private profileService: PersonalDetailsService,
              private authService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private ngxSpinner: NgxSpinnerService) {
  }

  @ViewChild('autoShownModal') autoShownModal: ModalDirective;
  public isRegModalShown: boolean = false;

  ngOnInit() {
    window.scroll(0, 0);
    this.ngxSpinner.show();
    this.academicService.GetDepartment("CSE").then(resp => {
      this.dept = resp;
      this.isLoaded = true;
      this.ngxSpinner.hide();
    }).catch(err => {
      console.log(err)
    });
    this.hasEvents = true;
    var userInfo = this.route.snapshot.queryParams['userInfo']
    if (userInfo) {
      var userInfoDS: ExternalUserInfoDataStruct = JSON.parse(userInfo);
      if (!userInfoDS.HasRegistered) {
        this.isRegModalShown = true;
      }
      else {
        this.profileService.getUserNameFromUserID(userInfoDS.UserID).then(resp => {
          this.authService.setUserName(resp);
          this.router.navigate(["/cse/"]).then(resp => {
          }).catch(err => {
            console.log(err);
          });
        })
      }
    }
  }

  onAboutClicked(): void {
    this.router.navigate(["cse/information"], {queryParams: {status: 2}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });
  }

  public hasEvents: boolean;

  onNoEvents(): void {
    this.hasEvents = false;
  }

  onRegCancelClicked(): void {
    this.isRegModalShown = false;
    this.router.navigate(["/cse/"]).then(resp => {
    }).catch(err => {
      console.log(err);
    });
  }

  onRegSubmit(success: boolean): void {
    if (!success) {
      this.regErrorMessage = "Oops! Something went wrong. Try Different Username and/or Email!";
    }
    else {
      this.isRegModalShown = false;
      this.regErrorMessage = "";
      this.router.navigate(["/cse/"]).then(resp => {
      }).catch(err => {
        console.log(err);
      });
    }
  }
}
