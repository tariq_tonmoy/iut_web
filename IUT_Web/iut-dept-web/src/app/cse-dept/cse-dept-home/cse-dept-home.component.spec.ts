import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptHomeComponent } from './cse-dept-home.component';

describe('CseDeptHomeComponent', () => {
  let component: CseDeptHomeComponent;
  let fixture: ComponentFixture<CseDeptHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
