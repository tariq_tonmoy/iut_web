import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptNewsComponent } from './cse-dept-news.component';

describe('CseDeptNewsComponent', () => {
  let component: CseDeptNewsComponent;
  let fixture: ComponentFixture<CseDeptNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
