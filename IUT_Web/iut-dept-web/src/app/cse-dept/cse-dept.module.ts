import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {CseDeptHomeComponent} from "./cse-dept-home/cse-dept-home.component";
import {CseDeptRoutingModule} from "./cse-dept-routing.module";
import {CseDeptPersonnelComponent} from "./cse-dept-personnel/cse-dept-personnel.component";
import {CseDeptNewsComponent} from "./cse-dept-news/cse-dept-news.component";
import {CseDeptAlumniComponent} from "./cse-dept-alumni/cse-dept-alumni.component";
import {CseDeptResearchComponent} from "./cse-dept-research/cse-dept-research.component";
import {CseDeptAcademicsComponent} from "./cse-dept-academics/cse-dept-academics.component";
import {CseDeptNavComponent} from "./cse-dept-nav/cse-dept-nav.component";
import {ComponentsModule} from "../components/components.module";
import {CseHomeCarousalComponent} from "./cse-home-carousal/cse-home-carousal.component";
import {CseHomeEventsComponent} from "./cse-home-events/cse-home-events.component";
import {CseHomeStoriesComponent} from "./cse-home-stories/cse-home-stories.component";
import {CseHeadMessageComponent} from "./cse-head-message/cse-head-message.component";
import {CseProfileComponent} from "./cse-profile/cse-profile.component";
import {TooltipModule, BsDatepickerModule, CarouselModule, ModalModule} from "ngx-bootstrap";

import {NgxMasonryModule} from "ngx-masonry";
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
  declarations: [CseDeptHomeComponent, CseDeptPersonnelComponent, CseDeptNewsComponent, CseDeptAlumniComponent, CseDeptResearchComponent, CseDeptAcademicsComponent, CseDeptNavComponent, CseHomeCarousalComponent, CseHomeEventsComponent, CseHomeStoriesComponent, CseHeadMessageComponent, CseProfileComponent],
  imports: [ModalModule, NgxSpinnerModule, BrowserModule, ComponentsModule, CseDeptRoutingModule, TooltipModule, BsDatepickerModule, CarouselModule, NgxMasonryModule],
  exports: [ComponentsModule],
  entryComponents: [CseDeptNavComponent]
})

export class CseDeptModule {

}
