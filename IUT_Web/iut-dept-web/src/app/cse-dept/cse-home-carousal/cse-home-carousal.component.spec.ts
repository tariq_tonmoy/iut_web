import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseHomeCarousalComponent } from './cse-home-carousal.component';

describe('CseHomeCarousalComponent', () => {
  let component: CseHomeCarousalComponent;
  let fixture: ComponentFixture<CseHomeCarousalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseHomeCarousalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseHomeCarousalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
