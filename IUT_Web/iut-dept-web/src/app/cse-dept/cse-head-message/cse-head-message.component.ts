import {AfterViewInit, Component, EventEmitter, Input, Output} from "@angular/core";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {Router} from "@angular/router";
import {CommonConversionService} from "../../service/common-conversion.service";

declare var $: any;

@Component({
  selector: "app-cse-head-message",
  templateUrl: "./cse-head-message.component.html",
  styleUrls: ["./cse-head-message.component.css"]
})
export class CseHeadMessageComponent implements AfterViewInit {

  public _dept: DepartmentDataStruct = new DepartmentDataStruct("", "");
  @Input() set dept(value: DepartmentDataStruct) {
    this._dept = value;

  }

  @Output() onCSE_DetailsClicked = new EventEmitter<void>();

  public head: PersonalDetailsDataStruct = new PersonalDetailsDataStruct("", "", "", "", "", "");


  constructor() {
  }


  onDetailsClicked(): void {
    this.onCSE_DetailsClicked.emit();
  }

  ngAfterViewInit(): void {
  }

}
