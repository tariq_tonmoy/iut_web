import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseHomeStoriesComponent } from './cse-home-stories.component';

describe('MceHomeStoriesComponent', () => {
  let component: CseHomeStoriesComponent;
  let fixture: ComponentFixture<CseHomeStoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseHomeStoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseHomeStoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
