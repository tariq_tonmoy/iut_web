import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {CseDeptHomeComponent} from "./cse-dept-home/cse-dept-home.component";
import {IutDeptRoutesEnum, IutDeptRoutesStr} from "../view-model/routing-data";
import {CseDeptPersonnelComponent} from "./cse-dept-personnel/cse-dept-personnel.component";
import {CseDeptNewsComponent} from "./cse-dept-news/cse-dept-news.component";
import {CseDeptAlumniComponent} from "./cse-dept-alumni/cse-dept-alumni.component";
import {CseDeptResearchComponent} from "./cse-dept-research/cse-dept-research.component";
import {CseDeptAcademicsComponent} from "./cse-dept-academics/cse-dept-academics.component";
import {CseProfileComponent} from "./cse-profile/cse-profile.component";

const routes: Routes = [
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/home",
    component: CseDeptHomeComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/personnel",
    component: CseDeptPersonnelComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/news",
    component: CseDeptNewsComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/academics",
    component: CseDeptAcademicsComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/research",
    component: CseDeptResearchComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/alumni",
    component: CseDeptAlumniComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/information",
    component: CseDeptNewsComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/profile/:username",
    component: CseProfileComponent,
    data: {class: 'IUT-CSE', dept: 'cse'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.CSE],
    redirectTo: IutDeptRoutesStr[IutDeptRoutesEnum.CSE] + "/home",
    pathMatch: "full"
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CseDeptRoutingModule {

}
