import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CustomEventDataStruct} from "../../view-model/custom-event-data-struct";
import {NewsService} from "../../service/news.service";
import {TimeServiceService} from "../../service/time-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cse-home-events',
  templateUrl: './cse-home-events.component.html',
  styleUrls: ['./cse-home-events.component.css']
})
export class CseHomeEventsComponent implements OnInit {

  public events: CustomEventDataStruct[] = [];
  private eventsInHome: number = 8;
  @Output() onNoEvents = new EventEmitter<void>();

  constructor(private newsService: NewsService,
              private timeService: TimeServiceService,
              private router: Router) {
  }

  ngOnInit() {
    this.newsService.GetFocusedEventByDepartment("CSE").then(resp => {
      this.events = resp.slice(0, this.eventsInHome);
      if (this.events.length === 0)
        this.onNoEvents.emit();
    }).catch(err => {
      console.log(err);
    });
  }

  getLocalDate(localDate: Date): Date {
    return this.timeService.FormatDate(localDate);
  }

  onEventClicked(ev: CustomEventDataStruct): void {
    this.router.navigate(["cse/news"], {queryParams: {eventID: ev.CustomEventID}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });
  }

}
