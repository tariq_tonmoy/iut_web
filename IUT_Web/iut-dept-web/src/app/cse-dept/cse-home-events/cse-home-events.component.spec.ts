import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseHomeEventsComponent } from './cse-home-events.component';

describe('MceHomeEventsComponent', () => {
  let component: CseHomeEventsComponent;
  let fixture: ComponentFixture<CseHomeEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseHomeEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseHomeEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
