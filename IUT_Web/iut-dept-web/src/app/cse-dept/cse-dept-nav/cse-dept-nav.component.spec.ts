import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptNavComponent } from './cse-dept-nav.component';

describe('CseDeptNavComponent', () => {
  let component: CseDeptNavComponent;
  let fixture: ComponentFixture<CseDeptNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
