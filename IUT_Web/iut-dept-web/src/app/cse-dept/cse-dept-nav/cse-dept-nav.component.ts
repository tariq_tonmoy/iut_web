import { Component, OnInit, TemplateRef } from "@angular/core";
import { AuthenticationService } from "../../service/authentication.service";
import { CommonConversionService } from "../../service/common-conversion.service";
import { Router } from "@angular/router";
import { PersonalDetailsService } from "../../service/personal-details.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ExternalAPIDataStruct } from "../../view-model/external-api-data-struct";
import { SignalRService } from "../../service/signal-r.service";


declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-cse-dept-nav",
  templateUrl: "./cse-dept-nav.component.html",
  styleUrls: ["./cse-dept-nav.component.css"]
})
export class CseDeptNavComponent implements OnInit {
  modalRef: BsModalRef;
  username: string;
  isConnectedToHub: boolean = false;
  errorMessage: string = "";
  animEnd: string = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
  public googleApiInfo: ExternalAPIDataStruct;

  openLoginComponent(template: TemplateRef<any>) {
    if (!this.username)
      this.modalRef = this.modalService.show(template,
        Object.assign({}, { class: "modal-lg" }));
    else this.modalRef = this.modalService.show(template);

    $("div[name=nav-login-form]").addClass("d-inline");
    $("div[name=nav-reg-form]").addClass("d-none");
  }

  constructor(private authService: AuthenticationService,
    private common: CommonConversionService,
    private router: Router,
    private personalDetailsService: PersonalDetailsService,
    private modalService: BsModalService,
    private signalRService: SignalRService) {
  }

  connectToHub(isConnected: boolean) {
    this.isConnectedToHub = isConnected;
    if (isConnected && this.username) {
      this.signalRService.login();
    }
  }

  ngOnInit() {
    this.authService.currentUsername.subscribe((username) => {
      this.username = username;
    });
    if (!this.isConnectedToHub && this.username)
      this.signalRService.startConnection(true);

    this.signalRService.currentIsConnected.subscribe((isConnected) => {
      this.connectToHub(isConnected);
    });
    this.authService.getExternalLogins().then(resp => {
      this.googleApiInfo = resp.find(x => x.Name === "Google");
    }).catch(err => {
      console.log(err);
    });
  }

  onSignUpButtonClick(): void {
    this.errorMessage = "";
    $("div[name=nav-login-form]").removeClass("d-inline");
    $("div[name=nav-reg-form]").removeClass("d-none");
    $("div[name=nav-login-form]").addClass("animated fadeOutLeft").one(this.animEnd, function () {
      $(this).removeClass("animated fadeOutLeft");
      $("div[name=nav-login-form]").addClass("d-none");
    });
    $("div[name=nav-reg-form]").addClass("animated fadeInRight").one(this.animEnd, function () {
      $(this).removeClass("animated fadeInRight");
      $("div[name=nav-reg-form]").addClass("d-inline");
    });
  }

  onLogInButtonClick(isLoggedIn: boolean): void {
    if (!isLoggedIn) {
      this.errorMessage = "Oops! Something went wrong. Check the Username and Password!";
    }
    else {
      this.modalRef.hide();
      this.errorMessage = "";
      if (!this.isConnectedToHub)
        this.signalRService.startConnection(true);
      else this.signalRService.login();
    }
  }

  onRegSubmit(success: boolean): void {
    if (!success) {
      this.errorMessage = "Oops! Something went wrong. Try Different Username and/or Email!";
    }
    else {
      this.modalRef.hide();

      this.errorMessage = "";


    }
  }

  regCancelClick(): void {
    this.errorMessage = "";
    this.modalRef.hide();

  }

  hasAccount(res: boolean): void {
    event.preventDefault();
    this.errorMessage = "";
    $("div[name=nav-login-form]").removeClass("d-none");
    $("div[name=nav-reg-form]").removeClass("d-inline");
    $("div[name=nav-login-form]").addClass("animated fadeInRight").one(this.animEnd, function () {
      $(this).removeClass("animated fadeInRight");
      $("div[name=nav-login-form]").addClass("d-inline");
    });
    $("div[name=nav-reg-form]").addClass("animated fadeOutLeft").one(this.animEnd, function () {
      $(this).removeClass("animated fadeOutLeft");
      $("div[name=nav-reg-form]").addClass("d-none");
    });
  }

  onSignOutClick(): void {
    this.modalRef.hide();

    if (this.isConnectedToHub) {
      this.signalRService.stopConnection();
    }
    this.authService.logoutService();
    this.router.navigate(["cse/home"]);
  }

  onProfileDetailsClick(sentParams: any): void {
    if (sentParams.hasProfile) {
      this.modalRef.hide();
      this.router.navigate(["cse/profile", sentParams.personalDetails.Username]);
    }
  }
}
