import {Component, NgZone, OnInit} from '@angular/core';
import {PersonnelService} from "../../service/personnel.service";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {StaffDataStruct} from "../../view-model/staff-data-struct";
import {IutDeptRoutesEnum, IutDeptRoutesStr} from "../../view-model/routing-data";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AcademicsService} from "../../service/academics.service";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {CommonVariables} from "../../view-model/common-variables";
import {CommonConversionService} from "../../service/common-conversion.service";
import {TeacherAssistantDataStruct} from "../../view-model/teacher-assistant-data-struct";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {SignalRService} from "../../service/signal-r.service";


@Component({
  selector: 'app-cse-dept-personnel',
  templateUrl: './cse-dept-personnel.component.html',
  styleUrls: ['./cse-dept-personnel.component.css']
})
export class CseDeptPersonnelComponent implements OnInit {

  public facultyList: FacultyDataStruct[] = [];
  public staffList: StaffDataStruct[] = [];
  public personList: PersonalDetailsDataStruct[] = [];
  public taList: TeacherAssistantDataStruct[] = [];
  public isFacultyLoaded: boolean = false;
  public isStaffLoaded: boolean = false;
  public isTALoaded: boolean = false;

  public userStatuses: any[] = [];

  public designationList = [];
  public head: FacultyDataStruct = new FacultyDataStruct("", "");
  public id: string = 'personnel_f';
  public status: number = 0;

  constructor(private personnelService: PersonnelService,
              private personService: PersonalDetailsService,
              private academicService: AcademicsService,
              private commonService: CommonConversionService,
              private router: Router,
              private spinner: NgxSpinnerService,
              private signalRService: SignalRService,
              private nz: NgZone) {
  }


  private getEmail(userID: string): void {
    this.personService.GetUserEmail(userID).then(email => {
      var person = this.personList.find(x => x.UserID === userID);
      if (person)
        person.Email = email;
    });
  }

  private getProfilePic(userID: string): void {
    this.personService.GetProfilePicture(userID)
      .then(pic => {
        if (pic) {
          var person = this.personList.find(x => x.UserID === userID);
          if (person)
            person.Image = pic;
        }
      })
      .catch(err => {

      });
  }

  private getFacultyMembers(): void {
    this.spinner.show();
    this.personnelService.GetFacultyByDept(IutDeptRoutesStr[IutDeptRoutesEnum.CSE])
      .then((resp) => {
        this.facultyList = resp;
        for (let f of this.facultyList) {
          this.personService.GetPersonalDetailsNoPicture(f.FacultyID).then((presp) => {
            this.personList.push(presp);
            this.getEmail(presp.UserID);
            this.getProfilePic(presp.UserID);
          }).catch((err) => {
            console.log(err)
          });
        }
        this.isFacultyLoaded = true;
        this.hideSpinner();

      }).catch((err) => {
      console.log(err);
    });
  }

  private getStaffMembers(): void {
    this.spinner.show();
    this.personnelService.GetStaffByDept(IutDeptRoutesStr[IutDeptRoutesEnum.CSE])
      .then((resp) => {
        this.staffList = resp;
        for (let s of this.staffList) {
          this.personService.GetProfileDetails(s.StaffID).then((presp) => {
            this.personList.push(presp);
            this.getEmail(presp.UserID);
            this.getProfilePic(presp.UserID);
          }).catch((err) => {
            console.log(err)
          });
        }
        this.isStaffLoaded = true;
        this.hideSpinner();
      }).catch((err) => {
      console.log(err);
    });
  }

  private getTAMembers(): void {
    this.spinner.show();
    this.personnelService.GetTAByDept(IutDeptRoutesStr[IutDeptRoutesEnum.CSE])
      .then((resp) => {
        this.taList = resp;
        for (let t of this.taList) {
          this.personService.GetProfileDetails(t.TAID).then((presp) => {
            this.personList.push(presp);
            this.getEmail(presp.UserID);
            this.getProfilePic(presp.UserID);
          }).catch((err) => {
            console.log(err)
          });
        }
        this.isTALoaded = true;
        this.hideSpinner();
      }).catch((err) => {
      console.log(err);
    });
  }

  hideSpinner(): void {
    this.spinner.hide();
  }


  ngOnInit() {
    window.scroll(0, 0);
    this.spinner.show();
    this.designationList = CommonVariables.facultyDesignations;
    this.signalRService.loginStatusSource.subscribe(val => {
      for (let v of this.userStatuses) {
        if (val.indexOf(v) === -1) {
          this.nz.run(() => {
            this.userStatuses.splice(this.userStatuses.indexOf(v), 1);
          });
        }
      }
      for (let v of val) {
        if (this.userStatuses.indexOf(v) === -1) {
          this.nz.run(() => {
            this.userStatuses.push(v);
          });
        }
      }
    });

    this.academicService.GetDepartmentHead("CSE").then((resp) => {
      this.head = resp;
    }).catch((err) => {
      console.log(err);
    });

    this.getFacultyMembers();
  }

  getPerson(userID: string): PersonalDetailsDataStruct {
    return this.personList.find(x => x.UserID == userID);
  }

  showFaculty(mode: string) {
    if (mode === 's') {
      this.status = 1;
      this.id = 'personnel_s';
      if (!this.isStaffLoaded)
        this.getStaffMembers();
    }
    else if (mode === 'f') {
      this.status = 0;
      this.id = 'personnel_f';
      if (!this.isFacultyLoaded)
        this.getFacultyMembers();
    }
    else if (mode === 't') {
      this.status = 2;
      this.id = 'personnel_t';
      if (!this.isTALoaded)
        this.getTAMembers();
    }
  }

  getSpecificFaculties(desig: string): FacultyDataStruct[] {
    return this.facultyList.filter(x => x.Designation === desig);
  }

  onUserDetailsClicked(id: string): void {
    this.personService.getUserNameFromUserID(id).then(res => {
      this.router.navigate(['cse/profile/' + this.commonService.replaceQuotes(res)]).then(resp => {

      }).catch(err => {
        console.log(err);
      });
    }).catch(err_s => {
      console.log(err_s);
    });

  }
}
