import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptPersonnelComponent } from './cse-dept-personnel.component';

describe('CseDeptPersonnelComponent', () => {
  let component: CseDeptPersonnelComponent;
  let fixture: ComponentFixture<CseDeptPersonnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptPersonnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptPersonnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
