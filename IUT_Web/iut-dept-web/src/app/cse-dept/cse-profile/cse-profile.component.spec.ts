import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseProfileComponent } from './cse-profile.component';

describe('MceProfileComponent', () => {
  let component: CseProfileComponent;
  let fixture: ComponentFixture<CseProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
