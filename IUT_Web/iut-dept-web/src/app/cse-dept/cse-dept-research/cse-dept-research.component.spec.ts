import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseDeptResearchComponent } from './cse-dept-research.component';

describe('CseDeptResearchComponent', () => {
  let component: CseDeptResearchComponent;
  let fixture: ComponentFixture<CseDeptResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseDeptResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseDeptResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
