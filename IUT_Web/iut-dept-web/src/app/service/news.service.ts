import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StoryDataStruct} from "../view-model/story-data-struct";
import {CustomEventDataStruct} from "../view-model/custom-event-data-struct";
import {LocationDataStruct} from "../view-model/location-data-struct";

@Injectable()
export class NewsService {
  private storiesUrl: string = "api/stories/";
  private eventsUrl: string = "api/events/";
  private storyUrl: string = "api/story/";
  private eventUrl: string = "api/event/";


  constructor(private http: HttpClient) {
  }

  GetStoryByDepartment(id: string): Promise<StoryDataStruct[]> {
    return this.http.get(this.storiesUrl + id)
      .toPromise()
      .then((response: StoryDataStruct[]) => {
        return response;
      });
  }

  GetImageByStoryID(storyID: number): Promise<Blob> {
    return this.http.get(this.storyUrl + "image/" + storyID.toString())
      .toPromise()
      .then((response: Blob) => {
        return response;
      });
  }

  GetFocusedStoryByDepartment(id: string): Promise<StoryDataStruct[]> {
    return this.http.get(this.storiesUrl + "focused/" + id)
      .toPromise()
      .then((response: StoryDataStruct[]) => {
        return response;
      });
  }

  GetEventByDepartment(id: string): Promise<CustomEventDataStruct[]> {
    return this.http.get(this.eventsUrl + id)
      .toPromise()
      .then((response: CustomEventDataStruct[]) => {
        return response;
      });
  }

  GetFocusedEventByDepartment(id: string): Promise<CustomEventDataStruct[]> {
    return this.http.get(this.eventsUrl + "focused/" + id)
      .toPromise()
      .then((response: CustomEventDataStruct[]) => {
        return response;
      });
  }

  GetEventLocationByEvent(id: number): Promise<LocationDataStruct> {
    return this.http.get(this.eventUrl + "location/" + id)
      .toPromise()
      .then((response: LocationDataStruct) => {
        return response;
      });
  }

  GetEvent(id: number): Promise<CustomEventDataStruct> {
    return this.http.get(this.eventUrl + id)
      .toPromise()
      .then((response: CustomEventDataStruct) => {
        return response;
      });
  }

  GetStory(id: number): Promise<StoryDataStruct> {
    return this.http.get(this.storyUrl + id)
      .toPromise()
      .then((response: StoryDataStruct) => {
        return response;
      });
  }

  HasEvent(id: number): Promise<boolean> {
    return this.http.get("api/has-events/" + id)
      .toPromise()
      .then((response: boolean) => {
        return response;
      });
  }


  PostStory(data: StoryDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.storyUrl, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PostEvent(data: CustomEventDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.eventUrl, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PostEventLocation(id: number, data: LocationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.eventUrl + 'location/' + id, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutStory(data: StoryDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.storyUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutEvent(data: CustomEventDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.eventUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutEventLocation(id: number, data: LocationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.eventUrl + 'location/' + id, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteStory(data: StoryDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.storyUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data.StoryID
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteEvent(data: CustomEventDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.eventUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteEventLocation(id: number): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.eventUrl + 'location/' + id, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


}
