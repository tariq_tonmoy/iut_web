import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/toPromise';
import {DepartmentDataStruct} from "../view-model/department-data-struct";
import {ProgramDataStruct} from "../view-model/program-data-struct";
import {RoleRequestDataStruct} from "../view-model/role-request-data-struct";


@Injectable()
export class AdminService {
  requestUrl: string = 'api/admin/requests/';
  approveUrl: string = 'api/admin/request/approve/';
  removeUrl: string = 'api/admin/request/remove/';
  revokeUrl: string = 'api/admin/request/revoke/';


  constructor(private http: HttpClient) {

  }


  GetRequestList(): Promise<RoleRequestDataStruct[]> {
    return this.http.get(this.requestUrl)
      .toPromise()
      .then((response: RoleRequestDataStruct[]) => {
        return response;
      });
  }

  GetRequestPerUserPerRole(userID: string, roleID: string): Promise<RoleRequestDataStruct> {
    return this.http.get(this.requestUrl + userID + "/" + roleID)
      .toPromise()
      .then((response: RoleRequestDataStruct) => {
        return response;
      });
  }

  GetRequestPerUser(userID: string): Promise<RoleRequestDataStruct> {
    return this.http.get(this.requestUrl + userID)
      .toPromise()
      .then((response: RoleRequestDataStruct) => {
        return response;
      });
  }

  ApproveRequest(requestID: number): Promise<any> {
    return this.http.post(this.approveUrl + requestID, '')
      .toPromise()
      .then((response: any) => {
        return response;
      }).catch((err:HttpErrorResponse) => {
        console.log("Error: ");
        console.log(err.status);
        return err.status;
      });
  }

  ApproveRequestFromRole(role: RoleRequestDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.approveUrl, role, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  RemoveRequest(requestID: number): Promise<any> {
    return this.http.delete(this.removeUrl + requestID)
      .toPromise()
      .then((response: any) => {
        return response;
      }).catch((err:HttpErrorResponse) => {
        console.log("Error: ");
        console.log(err.status);
        return err.status;
      });
  }

  RemoveRequestUsingRole(role: RoleRequestDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.request('DELETE', this.removeUrl, {
      headers: head,
      responseType: 'text',
      withCredentials: true,
      body: role
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  RevokeRequestUsingRole(userID: string, role: RoleRequestDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.request('PUT', this.revokeUrl+userID, {
      headers: head,
      responseType: 'text',
      withCredentials: true,
      body: role
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

}
