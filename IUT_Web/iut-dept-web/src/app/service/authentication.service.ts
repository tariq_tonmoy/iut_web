import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LoginDataStruct} from "../view-model/login-data-struct";
import {RegistrationDataStruct} from "../view-model/registration-data-struct";
import {CommonConversionService} from "./common-conversion.service";
import "rxjs/add/operator/toPromise";
import {ChangePasswordBindingModel} from "../view-model/change-password-binding-model";
import {BehaviorSubject} from "rxjs";
import {CommonVariables, RegisterExternalBindingModel} from "../view-model/common-variables";
import {ExternalUserInfoDataStruct} from "../view-model/external-user-info-data-struct";
import {ExternalAPIDataStruct} from "../view-model/external-api-data-struct";
import {ActivatedRoute} from "@angular/router";


@Injectable()
export class AuthenticationService {

  loginUrl: string = "Token";
  externalUserUrl: string = 'api/account/UserInfo';
  registrationUrl: string = "api/account/register";
  changePasswordUrl: string = "api/Account/ChangePassword";
  externLoginDetailsUrl: string = '/api/Account/ExternalLogins?returnUrl=%2F&generateState=true';
  regExternalUrl: string = 'api/Account/RegisterExternal';
  private tokenSource = new BehaviorSubject("");
  public currentToken = this.tokenSource.asObservable();
  private usernameSource = new BehaviorSubject("");
  public currentUsername = this.usernameSource.asObservable();

  private username: string;
  private token: string;

  constructor(private http: HttpClient,
              private commonConversion: CommonConversionService,
              private route: ActivatedRoute) {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.tokenSource.next(currentUser && currentUser.token);
    this.usernameSource.next(currentUser && currentUser.username);
    this.currentToken.subscribe(token => this.token = token);
    this.currentUsername.subscribe(username => this.username = username);
  }


  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  loginService(login: LoginDataStruct): Promise<boolean> {
    var head;
    var data;
    head = new HttpHeaders().append("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(this.loginUrl, this.commonConversion.convertJsonToXWUE(login), {
      headers: head,
      withCredentials: true
    }).toPromise()
      .then((response: Response) => {
        this.tokenSource.next(response["access_token"]);
        this.usernameSource.next(response["userName"]);
        localStorage.setItem("currentUser", JSON.stringify({username: this.username, token: this.token}));
        return true;
      })
      .catch(this.handleError);
  }

  setAcessToken(token: string): void {
    this.tokenSource.next(token);
    localStorage.setItem("currentUser", JSON.stringify({username: this.username, token: token}));
  }

  setUserName(username: string): void {
    this.usernameSource.next(username);
    localStorage.setItem("currentUser", JSON.stringify({username: username, token: this.token}));

  }

  registrationService(registration: RegistrationDataStruct): Promise<boolean> {
    var head;
    var data;
    head = new HttpHeaders().append("Content-Type", "application/x-www-form-urlencoded");

    return this.http.post(this.registrationUrl, this.commonConversion.convertJsonToXWUE(registration), {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: string) => {
        return true;
      })
      .catch(this.handleError);
  }

  logoutService(): void {
    this.tokenSource.next(null);
    this.usernameSource.next(null);
    localStorage.setItem("currentUser", null);
  }

  changePassword(data: ChangePasswordBindingModel) {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.changePasswordUrl, data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  getExternalLogins(): Promise<ExternalAPIDataStruct[]> {
    return this.http.get(this.externLoginDetailsUrl)
      .toPromise()
      .then((response: ExternalAPIDataStruct[]) => {
        return response;
      });
  }

  getExternalUserRegInfo(googleApi: ExternalAPIDataStruct): void {
    var dept: string = this.route.root.firstChild.snapshot.data['dept'];
    window.location.href = (CommonVariables.urlString +
      'api/Account/ExternalLogin?provider=' +
      googleApi.Name +
      '&response_type=token&client_id=self&redirect_uri=' +
      CommonVariables.externalRedirectUrlString +
      'dept=' + dept +
      '&state=' + googleApi.State);
  }

  getExternalUserRegInfoForAddExternal(googleApi: ExternalAPIDataStruct, id: string): void {
    var dept: string = this.route.root.firstChild.snapshot.data['dept'];
    window.location.href = (CommonVariables.urlString +
      'api/Account/ExternalLogin?provider=' +
      googleApi.Name +
      '&response_type=token&client_id=self&redirect_uri=' +
      CommonVariables.externalRedirectUrlString +
      'dept=' + dept +
      '%26id=' + id +
      '&state=' + googleApi.State);
  }

  getExternalUserInfo(): Promise<ExternalUserInfoDataStruct> {
    return this.http.get(this.externalUserUrl)
      .toPromise()
      .then((response: ExternalUserInfoDataStruct) => {
        return response;
      });
  }

  addExternalLogin(token: object): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post('api/Account/AddExternalLogin', {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  registrationExternal(object: RegisterExternalBindingModel): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.regExternalUrl, object, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }
}
