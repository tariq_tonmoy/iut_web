import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/toPromise';
import {FacultyDataStruct} from "../view-model/faculty-data-struct";
import {StaffDataStruct} from "../view-model/staff-data-struct";
import {TeacherAssistantDataStruct} from "../view-model/teacher-assistant-data-struct";

@Injectable()
export class PersonnelService {
  academicsUrl: string = 'api/iutacademics/';
  facultyUrl: string = 'api/faculty/';
  staffUrl: string = 'api/staff/';
  taUrl: string = 'api/ta/';

  constructor(private http: HttpClient) {

  }

  GetFacultyByDept(deptID: string): Promise<FacultyDataStruct[]> {
    return this.http.get(this.facultyUrl + "department/" + deptID)
      .toPromise()
      .then((response: FacultyDataStruct[]) => {
        return response;
      });
  }


  GetStaffByDept(deptID: string): Promise<StaffDataStruct[]> {
    return this.http.get(this.staffUrl + "department/" + deptID)
      .toPromise()
      .then((response: StaffDataStruct[]) => {
        return response;
      });
  }

  GetTAByDept(deptID: string): Promise<TeacherAssistantDataStruct[]> {
    return this.http.get(this.taUrl + "department/" + deptID)
      .toPromise()
      .then((response: TeacherAssistantDataStruct[]) => {
        return response;
      });
  }

  GetFacultyInfo(facultyID: string): Promise<FacultyDataStruct> {
    return this.http.get(this.facultyUrl + facultyID)
      .toPromise()
      .then((response: FacultyDataStruct[]) => {
        return response.length > 0 ? response[0] : null;
      });
  }

  EditFacultyInfo(faculty: FacultyDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(this.facultyUrl + "update", faculty, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      })
  }


  GetStaffInfo(staffID: string): Promise<StaffDataStruct> {
    return this.http.get(this.staffUrl + staffID)
      .toPromise()
      .then((response: StaffDataStruct[]) => {
        return response.length > 0 ? response[0] : null;
      });
  }

  EditStaffInfo(staff: StaffDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(this.staffUrl + "update", staff, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  EditFacultyPriority(data: FacultyDataStruct, priority: number): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(this.facultyUrl + "update/" + data.FacultyID, priority, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  EditStaffPriority(data: StaffDataStruct, priority: number): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.put(this.staffUrl + "update/" + data.StaffID, priority, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  DeleteFaculty(data: FacultyDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.facultyUrl + 'delete', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  DeleteStaff(data: StaffDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.staffUrl + 'delete', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  DeleteTA(data: TeacherAssistantDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.taUrl + 'delete', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

}
