import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AlumniDataStruct} from "../view-model/alumni-data-struct";

@Injectable()
export class AlumniService {

  private alumniUpdateUrl: string = 'api/alumni/update';
  private alumniDeleteUrl: string = 'api/alumni/delete';
  private alumniGetUrl: string = 'api/alumni/';

  constructor(private http: HttpClient) {
  }

  GetAlumniDetails(userID: string): Promise<AlumniDataStruct[]> {
    return this.http.get(this.alumniGetUrl + userID)
      .toPromise()
      .then((response: AlumniDataStruct[]) => {
        return response;
      });
  }

  GetAlumniCategories(deptID:string):Promise<AlumniDataStruct[]>{
    return this.http.get(this.alumniGetUrl + "alumni-category/"+deptID)
      .toPromise()
      .then((response: AlumniDataStruct[]) => {
        return response;
      });
  }

  PutAlumniDetails(data: AlumniDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.alumniUpdateUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  GetAlumniByProgramEnrollemnt(programID:number, enroll:number){
    return this.http.get(this.alumniGetUrl + programID.toString()+"/enroll/"+enroll.toString())
      .toPromise()
      .then((response: AlumniDataStruct[]) => {
        return response;
      });
  }

  DeleteAlumniDetails(data: AlumniDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.alumniDeleteUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }
}
