import { Injectable } from '@angular/core';
import {DatePipe} from "@angular/common";

@Injectable()
export class TimeServiceService {

  constructor(private datePipe:DatePipe) { }

  FormatDate(date:Date):Date{
    try {
      date = new Date(this.datePipe.transform(date, 'MM/dd/yyyy, h:mm:ss a'));
      date.setTime(date.getTime()+(6*60*60*1000));
      date = new Date(this.datePipe.transform(date, 'MM/dd/yyyy'));
    }catch(ex){
      console.log(ex);
    }
    return date;
  }
  FormatDateTime(date:Date):Date{
    try {
      date = new Date(this.datePipe.transform(date, 'MM/dd/yyyy, h:mm:ss a'));
      date.setTime(date.getTime()+(6*60*60*1000));
    }catch(ex){
      console.log(ex);
    }
    return date;
  }
}
