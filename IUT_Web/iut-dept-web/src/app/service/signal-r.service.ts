import {EventEmitter, Injectable} from '@angular/core';
import {CommonVariables} from "../view-model/common-variables";
import {BehaviorSubject} from "rxjs/index";
import {AuthenticationService} from "./authentication.service";
import {MessageDataStruct} from "../view-model/message-data-struct";
import {ChatGroupDataStruct} from "../view-model/chat-group-data-struct";

declare var $: any;

@Injectable()
export class SignalRService {

  constructor(private authService: AuthenticationService) {
    this.authService.currentToken.subscribe(token => this.accessToken = token);
    this.currentLoginStatuses.subscribe(val => this.loggedInUsers = val);
    this.currentIsConnected.subscribe(val => this.isConnected = val);
  }

  public addedToGroup: EventEmitter<number> = new EventEmitter<number>();
  public removedFromGroup: EventEmitter<number> = new EventEmitter<number>();
  public addUsersToGroup: EventEmitter<any> = new EventEmitter<any>();
  public removeUsersFromGroup: EventEmitter<any> = new EventEmitter<any>();

  public messageReceived: EventEmitter<MessageDataStruct> = new EventEmitter<MessageDataStruct>();
  public deleteMessage: EventEmitter<any> = new EventEmitter<any>();
  public removedGroup: EventEmitter<number> = new EventEmitter<number>();
  public editedGroup: EventEmitter<ChatGroupDataStruct> = new EventEmitter<ChatGroupDataStruct>();


  private connection: any;
  private proxy: any;
  private url: any = CommonVariables.urlString + 'signalr';

  private loggedInUsers = [];
  public loginStatusSource = new BehaviorSubject([]);
  private isConnectedSource = new BehaviorSubject(false);
  public currentIsConnected = this.isConnectedSource.asObservable();
  private currentLoginStatuses = this.loginStatusSource.asObservable();
  private accessToken: string = "";
  private isConnected: boolean = false;

  public startConnection(withLogin: boolean): void {
    if (this.isConnected)
      return;
    this.connection = $.hubConnection(this.url, {qs: 'access_token=' + this.accessToken, logging: false});
    this.proxy = this.connection.createHubProxy('chatHub');

    this.proxy.on('loggedIn', (usersOnline) => {
      this.loginStatusSource.next(usersOnline)
    });
    this.proxy.on('loggedOut', (user) => {
      try {
        this.loggedInUsers.splice(this.loggedInUsers.indexOf(user), 1);
        this.loginStatusSource.next(this.loggedInUsers);
      } catch (err) {
        console.log(err);
      }
    });
    this.proxy.on('addedToNewGroup', (groupID) => {
      this.addedToGroup.next(groupID);
    });
    this.proxy.on('removedFromChatGroup', (groupID) => {
      this.removedFromGroup.next(groupID);
    });
    this.proxy.on('addUsersToGroup', (groupID, userIDs) => {
      this.addUsersToGroup.next({groupID: groupID, userIDs: userIDs});
    });
    this.proxy.on('removeUsersFromGroup', (groupID, userIDs) => {
      this.removeUsersFromGroup.next({groupID: groupID, userIDs: userIDs});
    });
    this.proxy.on('messageReceived', (message) => {
      this.messageReceived.next(message);
    });
    this.proxy.on('deleteMessage', (messageID, groupID) => {
      this.deleteMessage.next({messageID: messageID, groupID: groupID});
    });
    this.proxy.on('removedGroup', (groupID) => {
      this.removedGroup.next(groupID);
    });
    this.proxy.on('editedGroup', (group) => {
      this.editedGroup.next(group);
    });

    this.connection.start().done((data: any) => {
      this.isConnectedSource.next(true);
      if (withLogin)
        this.login();
    }).catch((error: any) => {
      this.isConnectedSource.next(false);
    });
  }

  public stopConnection(): void {
    this.proxy.off('loggedIn', function () {
    });
    this.proxy.off('loggedOut', function () {
    });
    this.proxy.off('addedToNewGroup', function () {
    });
    this.proxy.off('removedFromChatGroup', function () {
    });
    this.proxy.off('addUsersToGroup', function () {
    });
    this.proxy.off('removeUsersFromGroup', function () {
    });
    this.proxy.off('messageReceived', function () {
    });
    this.proxy.off('deleteMessage', function () {
    });
    this.proxy.off('removedGroup', function () {
    });
    this.proxy.off('editedGroup', function () {
    });

    this.connection.stop(false, true);
    this.isConnectedSource.next(false);
    this.loginStatusSource.next([]);
  }

  public login(): void {
    this.proxy.invoke('LoggedIn')
      .catch((error: any) => {
        console.log('SendMessage error -> ' + error);
      });
  }

}
