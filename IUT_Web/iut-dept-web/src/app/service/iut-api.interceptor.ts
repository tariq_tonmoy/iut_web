import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {CommonVariables} from "../view-model/common-variables";
import { EMPTY } from 'rxjs';
@Injectable()
export class IutApiInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === "DELETE") {
      var r = confirm("You are about to delete information permanently. Do you want to continue?")
      if(r===false){
        throw new Error('Deletion cancelled');
      }
    }
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const userToken = user && user.token;
    const updatedReq = req.clone({
      url: CommonVariables.urlString + req.url,
      setHeaders: {Authorization: `bearer ${userToken}`}
    });
    return next.handle(updatedReq);
  }
}
