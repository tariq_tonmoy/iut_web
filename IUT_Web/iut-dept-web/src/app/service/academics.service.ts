import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/toPromise';
import {DepartmentDataStruct} from "../view-model/department-data-struct";
import {ProgramDataStruct} from "../view-model/program-data-struct";
import {CourseDataStruct} from "../view-model/course-data-struct";
import {SyllabusDataStruct} from "../view-model/syllabus-data-struct";
import {CourseMaterialDataStruct} from "../view-model/course-material-data-struct";
import {SyllabusReviewDataStruct} from "../view-model/syllabus-review-data-struct";
import {FacultyDataStruct} from "../view-model/faculty-data-struct";

@Injectable()
export class AcademicsService {
  academicsUrl: string = 'api/iutacademics/';

  constructor(private http: HttpClient) {

  }

  GetDepartment(deptID: string): Promise<DepartmentDataStruct> {
    return this.http.get(this.academicsUrl + 'department/' + deptID)
      .toPromise()
      .then((response: DepartmentDataStruct) => {
        return response;
      });
  }

  GetDepartmentList(): Promise<DepartmentDataStruct[]> {
    return this.http.get(this.academicsUrl + 'departments')
      .toPromise()
      .then((response: DepartmentDataStruct[]) => {
        return response;
      });
  }

  GetProgramsSummary(deptID: string): Promise<ProgramDataStruct[]> {
    return this.http.get(this.academicsUrl + 'programs-summary/' + deptID)
      .toPromise()
      .then((response: ProgramDataStruct[]) => {
        return response;
      });
  }

  GetDepartmentHead(deptID: string): Promise<FacultyDataStruct> {
    return this.http.get(this.academicsUrl + 'department-head/' + deptID)
      .toPromise()
      .then((response: FacultyDataStruct) => {
        return response;
      });
  }

  GetProgramListByDept(deptID: string): Promise<ProgramDataStruct[]> {
    return this.http.get(this.academicsUrl + 'department-programs/' + deptID)
      .toPromise()
      .then((response: ProgramDataStruct[]) => {
        return response;
      });
  }

  GetProgramByID(programID: number): Promise<ProgramDataStruct> {
    return this.http.get(this.academicsUrl + 'program/' + programID)
      .toPromise()
      .then((response: ProgramDataStruct) => {
        return response;
      });
  }

  GetCourseByProgram(programID: number): Promise<CourseDataStruct[]> {
    return this.http.get(this.academicsUrl + 'program-courses/' + programID)
      .toPromise()
      .then((response: CourseDataStruct[]) => {
        return response;
      });
  }

  GetSyllabusByCourse(courseID: number): Promise<SyllabusDataStruct> {
    return this.http.get(this.academicsUrl + 'course-syllabus/' + courseID)
      .toPromise()
      .then((response: SyllabusDataStruct) => {
        return response;
      });
  }

  GetCoursesByFaculty(facultyID: string): Promise<CourseDataStruct[]> {
    return this.http.get(this.academicsUrl + 'faculty-courses/' + facultyID)
      .toPromise()
      .then((response: CourseDataStruct[]) => {
        return response;
      });
  }

  GetFacultiesByCourse(CourseID: number): Promise<FacultyDataStruct[]> {
    return this.http.get(this.academicsUrl + 'course-faculties/' + CourseID)
      .toPromise()
      .then((response: FacultyDataStruct[]) => {
        return response;
      })
  }

  GetCoursesMaterialsByFacultyCourse(facultyID: string, courseID: number): Promise<CourseMaterialDataStruct[]> {
    return this.http.get(this.academicsUrl + 'coursematerial/' + facultyID + "/" + courseID)
      .toPromise()
      .then((response: CourseMaterialDataStruct[]) => {
        return response;
      });
  }

  GetCourseMaterialFile(data: CourseMaterialDataStruct): Promise<CourseMaterialDataStruct> {
    return this.http.get(this.academicsUrl + 'coursematerial-file/' + data.ID)
      .toPromise()
      .then((response: Blob[]) => {
        data.Material = response;
        return data;
      });
  }

  GetCourseMaterialByCourseByFaculty(courseID: number, facultyID: string): Promise<CourseMaterialDataStruct[]> {
    return this.http.get(this.academicsUrl + 'coursematerial/' + facultyID + "/" + courseID)
      .toPromise()
      .then((response: CourseMaterialDataStruct[]) => {
        return response;
      });
  }


  PostCourseMaterialByCourse(courseID: number, data: CourseMaterialDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'coursematerial/' + courseID, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  PostSyllabusByCourse(courseID: number, data: SyllabusDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'syllabus/' + courseID, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PostCourseByProgram(programID: number, data: CourseDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'course/' + programID, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PostProgramByDepartment(deptID: string, data: ProgramDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'program/' + deptID, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PostDepartment(data: DepartmentDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'department/', data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  AssignFacultyToCourse(facultyID: string, data: CourseDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'assign-faculty/' + facultyID, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  RemoveFacultyFromCourse(facultyID: string, data: CourseDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'remove-faculty/' + facultyID, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutCourseMaterial(data: CourseMaterialDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'coursematerial', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutSyllabus(data: SyllabusDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'syllabus', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutCourse(data: CourseDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'course', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutProgram(data: ProgramDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'program', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutDepartment(data: DepartmentDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'department', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteCourseMaterial(data: CourseMaterialDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'coursematerial', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteSyllabus(data: SyllabusDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'syllabus', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteCourse(data: CourseDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'course', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteProgram(data: ProgramDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'program', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteDepartment(data: DepartmentDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'department', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  GetSyllabusReviews(): Promise<SyllabusReviewDataStruct[]> {
    return this.http.get(this.academicsUrl + 'syllabus-review/')
      .toPromise()
      .then((response: SyllabusReviewDataStruct[]) => {
        return response;
      });
  }

  GetCoursesBySyllabusReviewProgram(progID: number, reviewID: number): Promise<CourseDataStruct[]> {
    return this.http.get(this.academicsUrl + 'syllabus-review-courses/' + progID + '/' + reviewID + '/')
      .toPromise()
      .then((response: CourseDataStruct[]) => {
        return response;
      });
  }

  GetSyllabusReviewsByProgram(progID: number): Promise<SyllabusReviewDataStruct[]> {
    return this.http.get(this.academicsUrl + 'program-syllabus-reviews/' + progID + '/')
      .toPromise()
      .then((response: SyllabusReviewDataStruct[]) => {
        return response;
      });
  }

  PostSyllabusReview(data: SyllabusReviewDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.academicsUrl + 'syllabus-review/', data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  PutSyllabusReview(data: SyllabusReviewDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicsUrl + 'syllabus-review/', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteSyllabusReview(data: SyllabusReviewDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicsUrl + 'syllabus-review', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

}
