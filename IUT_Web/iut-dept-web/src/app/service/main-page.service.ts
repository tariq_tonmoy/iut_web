import {Injectable} from "@angular/core";
import {DynamicComponentItem} from "../dynamic-component-item";
import {IutDeptRoutesEnum, IutDeptRoutesStr} from "../view-model/routing-data";
import {DeptNavComponents} from "../view-model/routing-data-component-list";

@Injectable()
export class MainPageService {
  checkLocation(location: string): number {
    if (location.length === 0)
      return IutDeptRoutesEnum.DEFAULT;
    let str = location.split("/", 2)[1];
    let flagMismatch = -1, count = 0;
    for (let s of IutDeptRoutesStr) {
      if (str === s)
        return count;
      count++;
    }
    return flagMismatch;
  }

  checkUrlForNav(location: string): DynamicComponentItem {
    let idx = this.checkLocation(location);
    idx = idx > -1 ? idx : 0
    return new DynamicComponentItem(DeptNavComponents[idx]);
  }


}
