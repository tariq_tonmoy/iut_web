import { TestBed, inject } from '@angular/core/testing';

import { AlumniService } from './alumni.service';

describe('AlumniService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlumniService]
    });
  });

  it('should be created', inject([AlumniService], (service: AlumniService) => {
    expect(service).toBeTruthy();
  }));
});
