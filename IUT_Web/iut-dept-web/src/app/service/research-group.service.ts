import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ResearchGroupDataStruct} from "../view-model/research-group-data-struct";
import 'rxjs/add/operator/toPromise';
import {PublicationDataStruct} from "../view-model/publication-data-struct";
import {PersonalDetailsDataStruct} from "../view-model/personal-details-data-struct";


@Injectable()
export class ResearchGroupService {


  researchGroupUrl: string = "api/research/research-group/";
  researchUrl: string = "api/research/";

  constructor(private http: HttpClient) {
  }

  GetResearchGroupByGroupID(groupID: number): Promise<ResearchGroupDataStruct> {
    return this.http.get(this.researchUrl + "group-group-id/" + groupID.toString()).toPromise().then((response: ResearchGroupDataStruct) => {
      return response;
    });
  }

  getResearchGroupsByDept(deptID: string): Promise<ResearchGroupDataStruct[]> {
    return this.http.get(this.researchUrl + 'department-groups/' + deptID)
      .toPromise()
      .then((response: ResearchGroupDataStruct[]) => {
        return response;
      });
  }

  getResearchGroupsByUser(userID: string): Promise<ResearchGroupDataStruct[]> {
    return this.http.get(this.researchUrl + 'user-groups/' + userID)
      .toPromise()
      .then((response: ResearchGroupDataStruct[]) => {
        return response;
      });
  }

  getPublicationsByResearchGroup(groupID: number): Promise<PublicationDataStruct[]> {
    return this.http.get(this.researchUrl + 'group-publications/' + groupID)
      .toPromise()
      .then((response: PublicationDataStruct[]) => {
        return response;
      });
  }

  getUsersByResearchGroup(groupID: number): Promise<PersonalDetailsDataStruct[]> {
    return this.http.get(this.researchUrl + 'group-members/' + groupID)
      .toPromise()
      .then((response: PersonalDetailsDataStruct[]) => {
        return response;
      });
  }


  postResearchGroup(data: ResearchGroupDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.researchGroupUrl, data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  editResearchGroup(data: ResearchGroupDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.researchGroupUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  addUsersToResearchGroup(groupID: number, data: string[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.researchGroupUrl + 'add-users/' + groupID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  addPublicationsToResearchGroup(groupID: number, data: number[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.researchGroupUrl + 'add-pub/' + groupID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  removePublicationFromResearchGroup(groupID: number, data: number[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.researchGroupUrl + 'remove-pub/' + groupID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  removeUsersFromResearchGroup(groupID: number, data: string[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.researchGroupUrl + 'remove-user/' + groupID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  deleteResearchGroup(data: number): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.researchGroupUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }
}
