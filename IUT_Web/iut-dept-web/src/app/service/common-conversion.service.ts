import { Injectable } from "@angular/core";

@Injectable()
export class CommonConversionService {

  convertJsonToXWUE(obj: any): any {
    var str = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]))
      }
    }
    return str.join("&");
  }

  replaceQuotes(str: string):string {
    var re = /\"/gi;
    str = str.replace(re, "");
    re = /\'/gi;
    str = str.replace(re, "");
    return str;
  }
}
