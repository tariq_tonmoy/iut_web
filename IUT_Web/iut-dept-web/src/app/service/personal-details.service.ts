import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CommonConversionService} from "./common-conversion.service";
import "rxjs/add/operator/toPromise";
import {PersonalDetailsDataStruct} from "../view-model/personal-details-data-struct";
import {UserRoleDataStruct} from "../view-model/user-role-data-struct";
import {RoleRequestDataStruct} from "../view-model/role-request-data-struct";
import {AcademicDetailsDataStruct} from "../view-model/academic-details-data-struct";
import {LocationDataStruct} from "../view-model/location-data-struct";
import {ProfessionalDetailsDataStruct} from "../view-model/professional-details-data-struct";
import {ContactDataStruct} from "../view-model/contact-data-struct";
import {PersonalSummary} from "../view-model/common-variables";
import {PersonnelService} from "./personnel.service";
import {AlumniService} from "./alumni.service";

@Injectable()
export class PersonalDetailsService {

  public StoredPersonalDetails: PersonalDetailsDataStruct;
  username: string;
  userIdUrl: string = "api/user/userID/";
  userNameUrl: string = "api/user/userName/";
  personalDetailsUrl: string = "api/user/personal/";
  userEmailUrl: string = "api/user/email/";
  rolesUrl: string = "api/user/roles/";
  reqRoleUrl: string = "api/user/request-role/";
  academicDetailsUrl: string = "api/user/academic/";
  professionalDetailsUrl: string = "api/user/profession/";
  contactDetailsUrl: string = "api/user/contact/";
  isDoctorUrl: string = "api/user/isdoctor/";
  isProfessorUrl: string = "api/user/isprofessor/";
  searchUrl: string = "api/user/personal-search/";
  profileNoPicUri: string = "api/user/personal-no-pic/";
  profilePicUri: string = "api/user/profile-picture/";


  constructor(private http: HttpClient,
              private commonConversion: CommonConversionService,
              private personnelService: PersonnelService,
              private  alumniService: AlumniService) {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.username = currentUser && currentUser.username;
  }

  GetUsername(): string {
    return this.username;
  }

  GetUserID(userName: string): Promise<string> {
    return this.http.get(this.userIdUrl + userName, {responseType: "text"})
      .toPromise()
      .then((response: string) => {
        return response;
      });
  }

  GetUserEmail(userID: string): Promise<string> {
    return this.http.get(this.userEmailUrl + userID, {responseType: "text"})
      .toPromise()
      .then((response: string) => {
        response = this.commonConversion.replaceQuotes(response);
        return response;
      });
  }

  getUserNameFromUserID(userID: string): Promise<string> {
    return this.http.get(this.userNameUrl + userID, {responseType: "text"})
      .toPromise()
      .then((response: string) => {
        response = this.commonConversion.replaceQuotes(response);
        return response;
      });
  }

  isProfessor(userID: string): Promise<boolean> | undefined {
    return this.http.get(this.isProfessorUrl + userID, {responseType: "text"})
      .toPromise()
      .then((response: string) => {
        try {
          return JSON.parse(response);
        } catch (e) {
          return undefined;
        }
      });
  }

  isDoctor(userID: string): Promise<boolean> {
    return this.http.get(this.isDoctorUrl + userID, {responseType: "text"})
      .toPromise()
      .then((response: string) => {
        try {
          return JSON.parse(response);
        } catch (e) {
          return undefined;
        }
      });
  }

  GetUserRoles(): Promise<UserRoleDataStruct[]> {
    return this.http.get(this.rolesUrl).toPromise().then((response: UserRoleDataStruct[]) => {
      return response;
    });
  }

  GetUserRolesByUser(userID: string): Promise<UserRoleDataStruct[]> {
    return this.http.get(this.rolesUrl + userID).toPromise().then((response: UserRoleDataStruct[]) => {
      return response;
    });
  }

  GetPersonalDetailsNoPicture(userID: string): Promise<PersonalDetailsDataStruct> {
    return this.http.get(this.profileNoPicUri + userID)
      .toPromise()
      .then((response: PersonalDetailsDataStruct) => {
        return response;
      });
  }

  GetProfilePicture(userID: string): Promise<Blob> {
    return this.http.get(this.profilePicUri + userID)
      .toPromise()
      .then((response: Blob) => {
        return response;
      })
      .catch(err => {
      });
  }


  GetPersonalDetailsBySearch(searchTerm: string): Promise<PersonalDetailsDataStruct[]> {
    return this.http.get(this.searchUrl + searchTerm)
      .toPromise()
      .then((response: PersonalDetailsDataStruct[]) => {
        return response;
      });
  }

  GetPerosnalSummaryByUserID(id: string): Promise<PersonalSummary> {
    return this.GetProfileDetails(id).then(resp => {
      return this.GetPersonalSummaryByPersonalDetails(resp).then(resp_s => {
        return resp_s;
      });
    });
  }

  GetPersonalSummaryByPersonalDetails(data: PersonalDetailsDataStruct): Promise<PersonalSummary> {
    var summary: PersonalSummary = {id: data.UserID};
    return this.GetProfileDetails(data.UserID).then(resp => {
      summary.name = resp.Salutation + resp.FirstName + " " + resp.LastName;
      summary.imageType = resp.ImageType;
      summary.image = resp.Image;
      return this.GetUserRolesByUser(data.UserID).then(role_resp => {
        if (role_resp) {
          if (role_resp.find(x => x.RoleName === "Faculty")) {
            this.personnelService.GetFacultyInfo(data.UserID).then(resp_faculty => {
              summary.remark = resp_faculty.Designation;
            });
          } else if (role_resp.find(x => x.RoleName === "Staff")) {
            this.personnelService.GetStaffInfo(data.UserID).then(resp_staff => {
              summary.remark = resp_staff.Designation;
            });
          }
          else if (role_resp.find(x => x.RoleName === "Alumni")) {
            this.alumniService.GetAlumniDetails(data.UserID).then(resp_alumni => {
              if (resp_alumni && resp_alumni.length > 0) {
                summary.remark = resp_alumni[0].StudentID;
              }
            });
          } else if (role_resp.find(x => x.RoleName === "TA")) {
            summary.remark = "Teacher's Assistant"
          }
        }
        return summary;
      }).catch(err => {
        console.log(err);
        return null;
      })
    });
  }

  GetProfileDetails(userID: string): Promise<PersonalDetailsDataStruct> {
    return this.http.get(this.personalDetailsUrl + userID)
      .toPromise()
      .then((response: PersonalDetailsDataStruct) => {
        response.Salutation = "";
        return this.isDoctor(userID).then((resp_d) => {
          if (resp_d)
            response.Salutation = "Dr. ";
          return this.isProfessor(userID).then((resp_p) => {
            if (resp_p)
              response.Salutation = "Prof. " + response.Salutation;
            return response;
          });
        });

      });
  }

  PostProfileDetails(data: PersonalDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.personalDetailsUrl, data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  PutProfileDetails(data: PersonalDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.personalDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  GetAcademicDetails(userID: string): Promise<AcademicDetailsDataStruct[]> {
    var academicDetails = this.http.get(this.academicDetailsUrl + userID)
      .toPromise()
      .then((response: AcademicDetailsDataStruct[]) => {
        for (let ad of response) {
          this.GetAcademicLocation(ad.AcademicDetailsID).then((resp) => {
            ad.Address = new LocationDataStruct(resp.LocationID, resp.Address, resp.City, resp.Country);
          });
        }
        return response;
      }).catch((err) => {
        console.log(err);
      });


    return academicDetails;
  }

  GetAcademicLocation(academicID: number): Promise<LocationDataStruct> {
    return this.http.get(this.academicDetailsUrl + "location/" + academicID)
      .toPromise()
      .then((locationResp: LocationDataStruct) => {
        return locationResp;
      }).catch((err) => {
        console.log(err);
      });
  }

  PostAcademicDetails(data: AcademicDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.academicDetailsUrl, data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  PutAcademicDetails(data: AcademicDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.academicDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  DeleteAcademicDetails(data: AcademicDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.academicDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  GetProfessionalDetails(userID: string): Promise<ProfessionalDetailsDataStruct[]> {
    var ProfessionalDetails = this.http.get(this.professionalDetailsUrl + userID)
      .toPromise()
      .then((response: ProfessionalDetailsDataStruct[]) => {
        for (let ad of response) {
          this.GetProfessionalLocation(ad.ProfessionalDetailsID).then((resp) => {
            ad.Address = new LocationDataStruct(resp.LocationID, resp.Address, resp.City, resp.Country);
          });
        }
        return response;
      }).catch((err) => {
        console.log(err);
      });


    return ProfessionalDetails;
  }

  GetProfessionalLocation(professionalID: number): Promise<LocationDataStruct> {
    return this.http.get(this.professionalDetailsUrl + "location/" + professionalID)
      .toPromise()
      .then((locationResp: LocationDataStruct) => {
        return locationResp;
      }).catch((err) => {
        console.log(err);
      });
  }

  PostProfessionalDetails(data: ProfessionalDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.professionalDetailsUrl, data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  PutProfessionalDetails(data: ProfessionalDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.professionalDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  DeleteProfessionalDetails(data: ProfessionalDetailsDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.professionalDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  PostRoleRequest(request: RoleRequestDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.reqRoleUrl, request, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  GetPersonalLocation(userID: string): Promise<LocationDataStruct> {
    return this.http.get(this.personalDetailsUrl + "location/" + userID)
      .toPromise()
      .then((locationResp: LocationDataStruct) => {
        return locationResp;
      }).catch((err) => {
        console.log(err);
      });
  }

  PostPersonalLocation(data: LocationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.personalDetailsUrl + "location/", data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  PutPersonalLocation(data: LocationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.personalDetailsUrl + "location/", {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  DeletePersonalLocation(data: LocationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.personalDetailsUrl + "location/", {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


  GetPersonalContacts(userID: string): Promise<ContactDataStruct[]> {
    return this.http.get(this.contactDetailsUrl + userID)
      .toPromise()
      .then((contact: ContactDataStruct[]) => {
        return contact;
      }).catch((err) => {
        console.log(err);
      });
  }

  PostPersonalContact(data: ContactDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.contactDetailsUrl, data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  PutPersonalContact(data: ContactDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.contactDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      }).catch((err) => {
        console.log(err);
      });
  }

  DeletePersonalContact(data: ContactDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.contactDetailsUrl, {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }


}
