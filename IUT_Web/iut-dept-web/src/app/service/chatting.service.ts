import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ChatGroupDataStruct} from "../view-model/chat-group-data-struct";
import {UserDetailsDataStruct} from "../view-model/user-details-data-struct";
import {MessageDataStruct} from "../view-model/message-data-struct";

@Injectable()
export class ChattingService {
  chattingUrl: string = 'api/chatting/';

  constructor(private http: HttpClient) {
  }

  GetChatGroupsByUser(userID: string): Promise<ChatGroupDataStruct[]> {
    return this.http.get(this.chattingUrl + 'chat-groups/' + userID)
      .toPromise()
      .then((response: ChatGroupDataStruct[]) => {
        return response;
      });
  }

  GetChatGroupDetails(groupID: number): Promise<ChatGroupDataStruct> {
    return this.http.get(this.chattingUrl + 'chat-group-details/' + groupID)
      .toPromise()
      .then((response: ChatGroupDataStruct) => {
        return response;
      });
  }

  GetUsersFromChatGroup(groupID: number): Promise<UserDetailsDataStruct[]> {
    return this.http.get(this.chattingUrl + 'chat-group-user-details/' + groupID)
      .toPromise()
      .then((response: UserDetailsDataStruct[]) => {
        return response;
      });
  }

  GetMessagesByChatGroup(groupID: number): Promise<MessageDataStruct[]> {
    return this.http.get(this.chattingUrl + 'get-messages/' + groupID)
      .toPromise()
      .then((response: MessageDataStruct[]) => {
        return response;
      });
  }

  GetMessagesFile(data: MessageDataStruct): Promise<MessageDataStruct> {
    return this.http.get(this.chattingUrl + 'message-file/' + data.MessageID)
      .toPromise()
      .then((response: Blob) => {
        data.MessageFile = response;
        return data;
      });
  }

  AddUsersToChatGroup(groupID: number, data: string[]): Promise<any> {
    if (data.length !== 0) {
      var head;
      head = new HttpHeaders().append('Content-Type', 'application/json');
      return this.http.put(this.chattingUrl + 'add-member/' + groupID.toString(), data, {
        headers: head,
        responseType: 'text',
        withCredentials: true
      }).toPromise()
        .then((response: any) => {
          return response;
        });
    }else{
      return null;
    }
  }

  RemoveUsersFromChatGroup(groupID: number, data: string[]): Promise<any> {
    if (data.length !== 0) {
      var head;
      head = new HttpHeaders().append('Content-Type', 'application/json');
      return this.http.put(this.chattingUrl + 'remove-member/' + groupID.toString(), data, {
        headers: head,
        responseType: 'text',
        withCredentials: true
      }).toPromise()
        .then((response: any) => {
          return response;
        });
    }
  }

  AddNewChatGroup(data: ChatGroupDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.chattingUrl + 'chat-group/', data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  SendMessageChatGroup(data: MessageDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.chattingUrl + 'send-message', data, {
      headers: head,
      responseType: 'text',
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  EditChatGroup(data: ChatGroupDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.put(this.chattingUrl + 'edit-group/', data, {
      headers: head,
      responseType: "text",
      withCredentials: true,
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteChattingGroup(data: ChatGroupDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.chattingUrl + 'delete-group/', {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  DeleteMessage(messageID: number): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.chattingUrl + 'delete-message/' + messageID.toString(),
      {
        headers: head,
        responseType: "text",
        withCredentials: true
      }).toPromise()
      .then((response: any) => {
        return response;
      });
  }
}
