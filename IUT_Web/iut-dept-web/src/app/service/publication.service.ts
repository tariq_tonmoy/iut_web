import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {PublicationDataStruct} from "../view-model/publication-data-struct";
import {PersonalDetailsDataStruct} from "../view-model/personal-details-data-struct";
import {PersonalSummary} from "../view-model/common-variables";
import {PersonalDetailsService} from "./personal-details.service";
import {PersonnelService} from "./personnel.service";
import {AlumniService} from "./alumni.service";

@Injectable()
export class PublicationService {

  private pubUrl: string = "api/research/";

  constructor(private http: HttpClient,
              private personalService: PersonalDetailsService,
              private personnelService: PersonnelService,
              private alumniService: AlumniService) {
  }

  GetPublicationByPubID(pubID: number): Promise<PublicationDataStruct> {
    return this.http.get(this.pubUrl + "publication-pub-id/" + pubID.toString()).toPromise().then((response: PublicationDataStruct) => {
      return response;
    });
  }

  GetPublicationMaterial(pub: PublicationDataStruct): Promise<PublicationDataStruct> {
    return this.http.get(this.pubUrl + "publication-material/" + pub.PublicationID.toString()).toPromise().then((response: Blob) => {
      pub.PublicationMaterial = response;
      return pub;
    });
  }

  GetPublicationsByDept(deptID: string): Promise<PublicationDataStruct[]> {
    return this.http.get(this.pubUrl + "department-publications/" + deptID).toPromise().then((response: PublicationDataStruct[]) => {
      return response;
    });
  }

  PostPublication(data: PublicationDataStruct, authors: PersonalSummary[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.post(this.pubUrl + "publication/", data, {
      headers: head,
      responseType: "text",
      withCredentials: true
    }).toPromise()
      .then((response: any) => {
        var authorList: string[] = [];
        for (let author of authors) {
          if (author.id !== data.CreatorID) {
            authorList.push(author.id);
          }
        }
        if (authorList.length > 0) {
          this.AddUsersToPublication(response, authorList).then(resp_author => {
            return resp_author;
          });
        }
      });
  }

  GetUsersByPublication(pubID: number): Promise<PersonalDetailsDataStruct[]> {
    return this.http.get(this.pubUrl + "publication-authors/" + pubID.toString()).toPromise().then((response: PersonalDetailsDataStruct[]) => {
      return response;
    });
  }

  GetUserSummaryByPublication(pubID: number): Promise<PersonalSummary[]> {
    return this.http.get(this.pubUrl + "publication-authors/" + pubID.toString()).toPromise().then((response: PersonalDetailsDataStruct[]) => {
      var personalSummary: PersonalSummary[] = [];
      for (let person of response) {
        personalSummary.push({id: person.UserID});
        this.personalService.GetProfileDetails(person.UserID).then(resp_person => {
          var v = personalSummary.find(x => x.id === resp_person.UserID);
          v.image = resp_person.Image;
          v.imageType = resp_person.ImageType;
          v.name = resp_person.Salutation + " " + resp_person.FirstName + " " + resp_person.LastName;
        }).catch(err_person => {
          console.log(err_person);
        });
        this.personalService.GetUserRolesByUser(person.UserID).then(resp => {
          if (resp.find(x => x.RoleName === "Faculty")) {
            this.personnelService.GetFacultyInfo(person.UserID).then(resp_faculty => {
              personalSummary.find(x => x.id === resp_faculty.FacultyID).remark = resp_faculty.Designation;
            }).catch(err_faculty => {
              console.log(err_faculty);
            });
          }
          else if (resp.find(x => x.RoleName === "Alumni")) {
            this.alumniService.GetAlumniDetails(person.UserID).then(resp_alumni => {
              try {
                personalSummary.find(x => x.id === resp_alumni[0].UserID).remark = resp_alumni[0].StudentID;
              } catch (exception) {
                console.log(exception);
              }
            }).catch(err_alumni => {
              console.log(err_alumni);
            });
          }
        });
      }
      return personalSummary;
    });
  }

  GetPublicationYearsByDepartment(deptID: string): Promise<PublicationDataStruct[]> {
    return this.http.get(this.pubUrl + "publication-years/" + deptID).toPromise().then((response: PublicationDataStruct[]) => {
      return response;
    });
  }

  AddUsersToPublication(pubID: number, data: string[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.pubUrl + "publication/add-users/" + pubID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  RemoveUserFromPublication(pubID: number, userIDs: string[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.pubUrl + "publication/remove-user/" + pubID.toString(), {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: userIDs
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  GetPublicationsByUserID(userID: string): Promise<PublicationDataStruct[]> {
    return this.http.get(this.pubUrl + "user-publications/" + userID).toPromise().then((response: PublicationDataStruct[]) => {
      return response;
    });
  }

  DeletePublication(data: PublicationDataStruct): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("DELETE", this.pubUrl + "publication/", {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data.PublicationID
    }).toPromise()
      .then((response: any) => {
        return response;
      });
  }

  GetPublicationStatisticsByDept(dept: string):Promise<any[]> {
    return this.http.get(this.pubUrl + "publication-stats/" + dept)
      .toPromise()
      .then((response: any[]) => {
      return response;
    });
  }

  PutPublicationDetails(data: PublicationDataStruct, authors: PersonalSummary[], oldAuthors: string[]): Promise<any> {
    var head;
    head = new HttpHeaders().append("Content-Type", "application/json");
    return this.http.request("PUT", this.pubUrl + "publication/", {
      headers: head,
      responseType: "text",
      withCredentials: true,
      body: data
    }).toPromise()
      .then((response: any) => {
        var authorList: string[] = [];
        var removeAuthList: string[] = [];
        for (let author of authors) {
          if (!oldAuthors.find(x => x === author.id)) {
            authorList.push(author.id);
          }
        }

        for (let oldAuthor of oldAuthors) {
          if (!authors.find(x => x.id === oldAuthor)) {
            removeAuthList.push(oldAuthor);
          }
        }

        if (removeAuthList.length > 0) {

          this.RemoveUserFromPublication(data.PublicationID, removeAuthList).then(resp_remove_auth => {

          });
        }
        if (authorList.length > 0) {
          this.AddUsersToPublication(data.PublicationID, authorList).then(resp_author => {

          });
        }
        return response;
      });
  }
}
