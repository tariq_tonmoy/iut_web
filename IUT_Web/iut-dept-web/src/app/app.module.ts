import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {AppComponent} from "./app.component";
import {ErrorComponent} from "./error.component";
import {AppRoutingModule} from "./app-routing.module";
import {CseDeptModule} from "./cse-dept/cse-dept.module";
import {MceDeptModule} from "./mce-dept/mce-dept.module";
import {IutFooterComponent} from "./components/iut-footer.component";
import {NavComponent} from "./nav.component";
import {MainPageService} from "./service/main-page.service";
import {NavDirective} from "./nav.directive";
import {IutApiInterceptor} from "./service/iut-api.interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {ComponentsModule} from "./components/components.module";
import {ModalModule} from "ngx-bootstrap/modal";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {
  AccordionModule,
  AlertModule,
  CarouselModule,
  PaginationModule,
  TabsModule,
  TypeaheadModule
} from "ngx-bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    NavDirective,
    NavComponent,
    IutFooterComponent,
    ErrorComponent
  ],
  imports: [
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot(),
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    TypeaheadModule.forRoot(),
    CarouselModule.forRoot(),
    BrowserModule,
    FormsModule,
    ComponentsModule,
    CseDeptModule,
    MceDeptModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    MainPageService,
    {provide: HTTP_INTERCEPTORS, useClass: IutApiInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
