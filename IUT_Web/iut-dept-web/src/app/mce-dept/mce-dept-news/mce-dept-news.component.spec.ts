import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptNewsComponent } from './mce-dept-news.component';

describe('MceDeptNewsComponent', () => {
  let component: MceDeptNewsComponent;
  let fixture: ComponentFixture<MceDeptNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
