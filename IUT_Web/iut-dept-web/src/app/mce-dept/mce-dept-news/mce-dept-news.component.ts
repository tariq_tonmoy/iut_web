import {Component, OnInit} from '@angular/core';
import {TimeServiceService} from "../../service/time-service.service";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {StoryDataStruct} from "../../view-model/story-data-struct";
import {NewsService} from "../../service/news.service";
import {AcademicsService} from "../../service/academics.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {CustomEventDataStruct} from "../../view-model/custom-event-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {ProgramDataStruct} from "../../view-model/program-data-struct";

@Component({
  selector: 'app-mce-dept-news',
  templateUrl: './mce-dept-news.component.html',
  styleUrls: ['./mce-dept-news.component.css']
})
export class MceDeptNewsComponent implements OnInit {

  public head: PersonalDetailsDataStruct = new PersonalDetailsDataStruct("", "", "", "", "", "");
  public _dept: DepartmentDataStruct;
  public isProf: boolean = false;
  public isDoc: boolean = false;
  public storyList: StoryDataStruct[] = [];
  public eventList: CustomEventDataStruct[] = [];
  public programList: ProgramDataStruct [] = [];

  private storyID = 0;
  private eventID = 0;
  public selectedStory: StoryDataStruct = new StoryDataStruct(0, "", "", "CSE");
  public status: number = 0;
  public isNewsLoaded: boolean = false;

  constructor(private personalService: PersonalDetailsService,
              private newsService: NewsService,
              private academicService: AcademicsService,
              private timeService: TimeServiceService,
              private route: ActivatedRoute,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.spinner.show();
    this.academicService.GetDepartment("MCE").then(resp_d => {
      this._dept = resp_d;
      try {
        if (this._dept.DepartmentHeadID) {
          this.personalService.GetProfileDetails(this._dept.DepartmentHeadID).then(resp => {
            this.head = resp;
            this.personalService.isProfessor(this._dept.DepartmentHeadID).then(rp => {
              this.isProf = rp;
            }).catch(ep => {
              console.log(ep);
            });
            this.personalService.isDoctor(this._dept.DepartmentHeadID).then(rd => {
              this.isDoc = rd;
            }).catch(ed => {
              console.log(ed);
            });
          }).catch(err => {
            console.log(err);
          });
        }
      } catch (e) {

      }

    }).catch(err_d => {
      console.log(err_d);
    });
    this.newsService.GetStoryByDepartment("MCE").then(resp => {
      this.storyList = resp;
      this.isNewsLoaded = true;
      this.spinner.hide();
      for (let s of this.storyList) {
        this.newsService.GetImageByStoryID(s.StoryID).then(resp_img => {
          s.StoryImg = resp_img;
        }).catch(err_img => {
          console.log(err_img);
        });
      }
      this.newsService.GetFocusedEventByDepartment("MCE").then(resp_e => {
        this.eventList = resp_e;
        for (let event of this.eventList) {
          this.newsService.GetEventLocationByEvent(event.CustomEventID).then(locResp => {
            this.eventList.find(x => x.CustomEventID === event.CustomEventID).EventLocation = locResp;
          });
          if (this.storyID === 0 && this.eventID > 0) {
            this.selectedStory = this.getStory(this.eventList.find(x => x.CustomEventID === this.eventID).StoryID);
            this.eventID = 0;
            this.storyID = 0;
            this.status = 1;
          }
        }
      }).catch(err_e => {

      });

      if (this.eventID === 0 && this.storyID > 0) {
        this.selectedStory = resp.filter(x => x.StoryID === this.storyID)[0];
        this.storyID = 0;
        this.eventID = 0;
        this.status = 1;
      }
    }).catch(err => {
      console.log("error:" + err);
    });


    if (this.route.snapshot.queryParams["storyID"]) {
      this.storyID = parseInt(this.route.snapshot.queryParams["storyID"]);
    } else if (this.route.snapshot.queryParams["eventID"]) {
      this.eventID = parseInt(this.route.snapshot.queryParams["eventID"]);
    } else if (this.route.snapshot.queryParams["status"]) {
      this.status = 2;
      this.getPrograms();
    }
    else this.status = 0;

  }


  getLocalDate(localDate: Date): Date {
    return this.timeService.FormatDate(localDate);
  }

  getStory(storyID: number): StoryDataStruct {
    return this.storyList.find(x => x.StoryID === storyID);
  }

  getEvents(storyID: number): CustomEventDataStruct[] {
    return this.eventList.filter(x => x.StoryID === storyID);
  }

  onAboutDeptClicked(): void {
    window.scroll(0, 0);
    this.status = 2;
    this.getPrograms();
  }

  getPrograms(): void {
    this.academicService.GetProgramListByDept("MCE").then(resp => {
      this.programList = resp;
    }).catch(err => {
      console.log(err);
    });
  }

  onStoryDeptClicked(): void {
    window.scroll(0, 0)
    this.status = 0;
  }

  onSpecificStoryClicked(StoryID: number): void {
    window.scroll(0, 0);
    this.status = 1;
    this.selectedStory = this.storyList.find(x => x.StoryID === StoryID);

  }

  onEventClicked(CustomEvent: CustomEventDataStruct): void {
    this.status = 1;
    this.selectedStory = this.getStory(CustomEvent.StoryID);
  }

  onBackClicked(): void {
    window.scroll(0, 0);
    this.status = 0;
  }

  onProgramNameClicked(name: string) {
    this.router.navigate(["mce/academics"], {queryParams: {programName: name}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });
  }

}
