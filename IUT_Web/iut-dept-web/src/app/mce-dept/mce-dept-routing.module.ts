import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {IutDeptRoutesEnum, IutDeptRoutesStr} from "../view-model/routing-data";
import {MceDeptHomeComponent} from "./mce-dept-home/mce-dept-home.component";
import {MceDeptPersonnelComponent} from "./mce-dept-personnel/mce-dept-personnel.component";
import {MceDeptNewsComponent} from "./mce-dept-news/mce-dept-news.component";
import {MceDeptAcademicsComponent} from "./mce-dept-academics/mce-dept-academics.component";
import {MceDeptResearchComponent} from "./mce-dept-research/mce-dept-research.component";
import {MceDeptAlumniComponent} from "./mce-dept-alumni/mce-dept-alumni.component";
import {MceProfileComponent} from "./mce-profile/mce-profile.component";

const routes: Routes = [
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/home",
    component: MceDeptHomeComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/personnel",
    component: MceDeptPersonnelComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/news", component: MceDeptNewsComponent, data: {class: 'IUT-MCE'}},
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/information",
    component: MceDeptNewsComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/academics",
    component: MceDeptAcademicsComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/research",
    component: MceDeptResearchComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/alumni",
    component: MceDeptAlumniComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/profile/:username",
    component: MceProfileComponent,
    data: {class: 'IUT-MCE', dept: 'mce'}
  },
  {
    path: IutDeptRoutesStr[IutDeptRoutesEnum.MCE],
    redirectTo: IutDeptRoutesStr[IutDeptRoutesEnum.MCE] + "/home",
    pathMatch: "full"
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MceDeptRoutingModule {

}
