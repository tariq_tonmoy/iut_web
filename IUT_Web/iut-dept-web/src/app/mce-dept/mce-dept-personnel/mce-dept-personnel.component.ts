import { Component, OnInit } from '@angular/core';
import {StaffDataStruct} from "../../view-model/staff-data-struct";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {IutDeptRoutesEnum, IutDeptRoutesStr} from "../../view-model/routing-data";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {TeacherAssistantDataStruct} from "../../view-model/teacher-assistant-data-struct";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {PersonnelService} from "../../service/personnel.service";
import {CommonVariables} from "../../view-model/common-variables";
import {AcademicsService} from "../../service/academics.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {CommonConversionService} from "../../service/common-conversion.service";

@Component({
  selector: 'app-mce-dept-personnel',
  templateUrl: './mce-dept-personnel.component.html',
  styleUrls: ['./mce-dept-personnel.component.css']
})
export class MceDeptPersonnelComponent implements OnInit {

  public facultyList: FacultyDataStruct[] = [];
  public staffList: StaffDataStruct[] = [];
  public personList: PersonalDetailsDataStruct[] = [];
  public taList: TeacherAssistantDataStruct[] = [];
  public isFacultyLoaded: boolean = false;
  public isStaffLoaded: boolean = false;
  public isTALoaded: boolean = false;


  public designationList = [];
  public head: FacultyDataStruct = new FacultyDataStruct("", "");
  private hideStaff: boolean = true;
  public id: string = 'personnel_f';
  public status: number = 0;

  constructor(private personnelService: PersonnelService,
              private personService: PersonalDetailsService,
              private academicService: AcademicsService,
              private commonService: CommonConversionService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }


  hideSpinner(): void {
    if (this.isFacultyLoaded && this.isTALoaded && this.isStaffLoaded)
      this.spinner.hide();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.designationList = CommonVariables.facultyDesignations;

    this.academicService.GetDepartmentHead("MCE").then((resp) => {
      this.head = resp;
    }).catch((err) => {
      console.log(err);
    });
    this.spinner.show();

    this.personnelService.GetFacultyByDept(IutDeptRoutesStr[IutDeptRoutesEnum.MCE]).then((resp) => {
      this.facultyList = resp;
      for (let f of this.facultyList) {
        this.personService.GetProfileDetails(f.FacultyID).then((presp) => {
          this.personService.GetUserEmail(presp.UserID).then(email => {
            presp.Email = this.commonService.replaceQuotes(email);
            this.personList.push(presp);
          })
        }).catch((err) => {
          console.log(err)
        });
      }
      this.isFacultyLoaded = true;
      this.hideSpinner();

    }).catch((err) => {
      console.log(err);
    });

    this.personnelService.GetStaffByDept(IutDeptRoutesStr[IutDeptRoutesEnum.MCE]).then((resp) => {
      this.staffList = resp;
      for (let s of this.staffList) {
        this.personService.GetProfileDetails(s.StaffID).then((presp) => {
          this.personService.GetUserEmail(presp.UserID).then(email => {
            presp.Email = this.commonService.replaceQuotes(email);
            this.personList.push(presp);
          })
        }).catch((err) => {
          console.log(err)
        });
      }
      this.isStaffLoaded = true;
      this.hideSpinner();
    }).catch((err) => {
      console.log(err);
    });

    this.personnelService.GetTAByDept(IutDeptRoutesStr[IutDeptRoutesEnum.MCE]).then((resp) => {
      this.taList = resp;
      for (let t of this.taList) {
        this.personService.GetProfileDetails(t.TAID).then((presp) => {
          this.personService.GetUserEmail(presp.UserID).then(email => {
            presp.Email = this.commonService.replaceQuotes(email);
            this.personList.push(presp);
          })
        }).catch((err) => {
          console.log(err)
        });
      }
      this.isTALoaded = true;
      this.hideSpinner();
    }).catch((err) => {
      console.log(err);
    });

    this.hideStaff = true;

  }

  getPerson(userID: string): PersonalDetailsDataStruct {
    return this.personList.find(x => x.UserID == userID);
  }

  showFaculty(mode: string) {
    if (mode === 's') {
      this.hideStaff = false;
      this.status = 1;
      this.id = 'personnel_s';
    }
    else if (mode === 'f') {
      this.hideStaff = true;
      this.status = 0;
      this.id = 'personnel_f';
    }
    else if (mode === 't') {
      this.status = 2;
      this.id = 'personnel_t';
    }
  }

  getSpecificFaculties(desig: string): FacultyDataStruct[] {
    return this.facultyList.filter(x => x.Designation === desig);
  }

  onUserDetailsClicked(id: string): void {
    this.personService.getUserNameFromUserID(id).then(res => {
      this.router.navigate(['mce/profile/' + this.commonService.replaceQuotes(res)]).then(resp => {

      }).catch(err => {
        console.log(err);
      });
    }).catch(err_s => {
      console.log(err_s);
    });

  }

}
