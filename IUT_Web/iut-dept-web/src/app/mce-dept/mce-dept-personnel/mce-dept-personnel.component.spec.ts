import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptPersonnelComponent } from './mce-dept-personnel.component';

describe('MceDeptPersonnelComponent', () => {
  let component: MceDeptPersonnelComponent;
  let fixture: ComponentFixture<MceDeptPersonnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptPersonnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptPersonnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
