import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptHomeComponent } from './mce-dept-home.component';

describe('MceDeptHomeComponent', () => {
  let component: MceDeptHomeComponent;
  let fixture: ComponentFixture<MceDeptHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
