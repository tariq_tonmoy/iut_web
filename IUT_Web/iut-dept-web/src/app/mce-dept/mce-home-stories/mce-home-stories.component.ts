import {Component, Input, OnInit} from "@angular/core";
import {NewsService} from "../../service/news.service";
import {StoryDataStruct} from "../../view-model/story-data-struct";
import {Router} from "@angular/router";
import {TimeServiceService} from "../../service/time-service.service";

@Component({
  selector: "app-mce-home-stories",
  templateUrl: "./mce-home-stories.component.html",
  styleUrls: ["./mce-home-stories.component.css"]
})
export class MceHomeStoriesComponent implements OnInit {

  isScrolled: boolean = false;
  @Input() hasNoEvents:boolean

  constructor(private newsService: NewsService,
              private router: Router,
              private timeService: TimeServiceService) {
  }

  storiesInHome: number = 5;
  stories: StoryDataStruct[] = [];


  ngOnInit() {
    this.isScrolled = false;
    this.newsService.GetStoryByDepartment("MCE").then(resp => {
      this.stories = resp.slice(0, this.storiesInHome);
      for (let s of this.stories) {
        this.newsService.GetImageByStoryID(s.StoryID).then(resp_img => {
          s.StoryImg = resp_img;
        }).catch(err_img => {
          console.log(err_img);
        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  onStoryClicked(StoryID): void {
    this.router.navigate(["mce/news"], {queryParams: {storyID: StoryID}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });

  }

  i: number = 0;

  getLocalDate(localDate: Date): Date {
    return this.timeService.FormatDate(localDate);
  }


  sID: number[] = [];
  colorValue: number[] = [];

  increaseCounter(storyID: number): number {
    if (!this.sID.find(x => x === storyID)) {
      this.sID.push(storyID);
      this.i = this.i + 1;
      this.colorValue.push(this.i);
      return this.i;
    }
    return this.colorValue[this.sID.findIndex(x => x === storyID)];
  }

}
