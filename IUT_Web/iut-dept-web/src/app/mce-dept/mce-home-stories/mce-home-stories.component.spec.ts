import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceHomeStoriesComponent } from './mce-home-stories.component';

describe('MceHomeStoriesComponent', () => {
  let component: MceHomeStoriesComponent;
  let fixture: ComponentFixture<MceHomeStoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceHomeStoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceHomeStoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
