import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {AuthenticationService} from "../../service/authentication.service";

@Component({
  selector: 'app-mce-profile',
  templateUrl: './mce-profile.component.html',
  styleUrls: ['./mce-profile.component.css']
})
export class MceProfileComponent implements OnInit {
  public personalDetails: PersonalDetailsDataStruct = new PersonalDetailsDataStruct('', '', '', '', '', '');

  private sub: any;
  public urlUsername: string;
  public isLoggedIn: boolean = false;

  ngOnInit(): void {
    this.sub = this.router.params.subscribe(params => {
      this.urlUsername = params['username'];
    });
    this.authService.currentUsername.subscribe((username) => {
      if (this.urlUsername === username) {
        this.isLoggedIn = true;
      }
    });
  }

  constructor(private authService: AuthenticationService,
              private router: ActivatedRoute) {
  }


}
