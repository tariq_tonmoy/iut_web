import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceProfileComponent } from './mce-profile.component';

describe('MceProfileComponent', () => {
  let component: MceProfileComponent;
  let fixture: ComponentFixture<MceProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
