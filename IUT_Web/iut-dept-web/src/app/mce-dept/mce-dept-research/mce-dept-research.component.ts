import { Component, OnInit } from '@angular/core';
import {ResearchGroupService} from "../../service/research-group.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {ResearchGroupDataStruct} from "../../view-model/research-group-data-struct";

@Component({
  selector: 'app-mce-dept-research',
  templateUrl: './mce-dept-research.component.html',
  styleUrls: ['./mce-dept-research.component.css']
})
export class MceDeptResearchComponent implements OnInit {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private academicsService: AcademicsService,
              private researchGroupService: ResearchGroupService) {
  }

  public isDeptResearchGroup: boolean;
  public activeTab: string = "r";
  public researchGroup: ResearchGroupDataStruct = new ResearchGroupDataStruct(0, '');
  public department: DepartmentDataStruct = new DepartmentDataStruct('', '');
  public isSelectedGroup: boolean = false;

  ngOnInit() {
    this.isDeptResearchGroup = true;
    window.scroll(0, 0);
    this.academicsService.GetDepartment("MCE").then(resp => {
      this.department = resp;
    }).catch(err => {
      console.log(err);
    });

    if (this.activatedRoute.snapshot.queryParams["groupID"]) {
      this.researchGroupService.GetResearchGroupByGroupID(this.activatedRoute.snapshot.queryParams["groupID"]).then(resp => {
        this.researchGroup = resp;
        this.activeTab = 'r';
        this.isSelectedGroup = true;
      }).catch(err => {
        console.log(err);
      });
    }
    else {
      this.isSelectedGroup = false;
    }
  }

  showResearchGroup(status: string): void {
    this.activeTab = status;
  }

}
