import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptResearchComponent } from './mce-dept-research.component';

describe('MceDeptResearchComponent', () => {
  let component: MceDeptResearchComponent;
  let fixture: ComponentFixture<MceDeptResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
