import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptNavComponent } from './mce-dept-nav.component';

describe('MceDeptNavComponent', () => {
  let component: MceDeptNavComponent;
  let fixture: ComponentFixture<MceDeptNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
