import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {AuthenticationService} from "../../service/authentication.service";
import {Router} from "@angular/router";
import {CommonConversionService} from "../../service/common-conversion.service";
import {PersonalDetailsService} from "../../service/personal-details.service";

declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-mce-dept-nav',
  templateUrl: './mce-dept-nav.component.html',
  styleUrls: ['./mce-dept-nav.component.css']
})
export class MceDeptNavComponent implements OnInit {
  modalRef: BsModalRef;
  username: string;
  errorMessage: string = "";
  animEnd: string = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

  openLoginComponent(template: TemplateRef<any>) {

    this.modalRef = this.modalService.show(template);
    $("div[name=nav-login-form]").addClass("d-inline");
    $("div[name=nav-reg-form]").addClass("d-none");
  }

  constructor(private authService: AuthenticationService,
              private common: CommonConversionService,
              private router: Router,
              private personalDetailsService: PersonalDetailsService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.authService.currentUsername.subscribe(username => this.username = username);


  }

  onSignUpButtonClick(): void {
    this.errorMessage = "";
    $("div[name=nav-login-form]").removeClass("d-inline");
    $("div[name=nav-reg-form]").removeClass("d-none");
    $("div[name=nav-login-form]").addClass("animated fadeOutLeft").one(this.animEnd, function () {
      $(this).removeClass("animated fadeOutLeft");
      $("div[name=nav-login-form]").addClass("d-none");
    });
    $("div[name=nav-reg-form]").addClass("animated fadeInRight").one(this.animEnd, function () {
      $(this).removeClass("animated fadeInRight");
      $("div[name=nav-reg-form]").addClass("d-inline");
    });
  }

  onLogInButtonClick(isLoggedIn: boolean): void {
    if (!isLoggedIn) {
      this.errorMessage = "Oops! Something went wrong. Check the Username and Password!";
    }
    else {
      this.modalRef.hide();

      this.errorMessage = "";

    }
  }

  onRegSubmit(success: boolean): void {
    if (!success) {
      this.errorMessage = "Oops! Something went wrong. Try Different Username and/or Email!";
    }
    else {
      this.modalRef.hide();

      this.errorMessage = "";


    }
  }

  regCancelClick(): void {
    this.errorMessage = "";
    this.modalRef.hide();

  }

  hasAccount(res: boolean): void {
    event.preventDefault();
    this.errorMessage = "";
    $("div[name=nav-login-form]").removeClass("d-none");
    $("div[name=nav-reg-form]").removeClass("d-inline");
    $("div[name=nav-login-form]").addClass("animated fadeInRight").one(this.animEnd, function () {
      $(this).removeClass("animated fadeInRight");
      $("div[name=nav-login-form]").addClass("d-inline");
    });
    $("div[name=nav-reg-form]").addClass("animated fadeOutLeft").one(this.animEnd, function () {
      $(this).removeClass("animated fadeOutLeft");
      $("div[name=nav-reg-form]").addClass("d-none");
    });
  }

  onSignOutClick(): void {
    this.modalRef.hide();

    this.authService.logoutService();
    this.router.navigate(["mce/home"]);
  }

  onProfileDetailsClick(sentParams: any): void {
    if (sentParams.hasProfile) {
      this.modalRef.hide();
      this.router.navigate(["mce/profile", sentParams.personalDetails.Username]);
    }
  }

}
