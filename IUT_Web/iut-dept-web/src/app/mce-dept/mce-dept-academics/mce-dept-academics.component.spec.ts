import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptAcademicsComponent } from './mce-dept-academics.component';

describe('MceDeptAcademicsComponent', () => {
  let component: MceDeptAcademicsComponent;
  let fixture: ComponentFixture<MceDeptAcademicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptAcademicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptAcademicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
