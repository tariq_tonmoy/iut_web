import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {ProgramDataStruct} from "../../view-model/program-data-struct";

@Component({
  selector: 'app-mce-dept-academics',
  templateUrl: './mce-dept-academics.component.html',
  styleUrls: ['./mce-dept-academics.component.css']
})
export class MceDeptAcademicsComponent implements OnInit {

  public programList: ProgramDataStruct[] = [];
  public selectedDept: DepartmentDataStruct = new DepartmentDataStruct("", "");
  public defaultProgName: string = "B.Sc. (Engg) in MCE";
  public defaultSemester: string = "First";

  constructor(private academicService: AcademicsService,
              private route: ActivatedRoute) {
  }

  onProgClicked(p: ProgramDataStruct): void {
    this.defaultProgName = p.ProgramName;
  }

  ngOnInit() {
    window.scroll(0, 0);
    window.onscroll = function () {
      try {
        var el=document.getElementById("top_button");
        if(el) {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            el.style.display = "block";
          } else {
            el.style.display = "none";
          }
        }

      } catch (e) {

      }
    };
    this.academicService.GetDepartment("MCE").then(resp => {
      this.selectedDept = resp;
      this.academicService.GetProgramsSummary(this.selectedDept.DepartmentID).then(resp_p => {
        resp_p.sort((a, b) => a.ProgramName < b.ProgramName ? -1 : a.ProgramName > b.ProgramName ? 1 : 0);
        this.programList = resp_p;
      }).catch(err_p => {
        console.log(err_p);
      });
    }).catch(err => {
      console.log(err);
    });


    if (this.route.snapshot.queryParams["programName"])
      this.defaultProgName = this.route.snapshot.queryParams["programName"];
    if (this.route.snapshot.queryParams["semester"])
      this.defaultSemester = this.route.snapshot.queryParams["semester"];
  }
  public onUpClicked(): void {
    window.scroll(0, 0);
  }

}
