import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CseHeadMessageComponent } from './mce-head-message.component';

describe('CseHeadMessageComponent', () => {
  let component: CseHeadMessageComponent;
  let fixture: ComponentFixture<CseHeadMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CseHeadMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CseHeadMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
