import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {MceDeptRoutingModule} from "./mce-dept-routing.module";
import {MceDeptHomeComponent} from "./mce-dept-home/mce-dept-home.component";
import {MceDeptPersonnelComponent} from './mce-dept-personnel/mce-dept-personnel.component';
import {MceDeptNewsComponent} from './mce-dept-news/mce-dept-news.component';
import {MceDeptAcademicsComponent} from './mce-dept-academics/mce-dept-academics.component';
import {MceDeptResearchComponent} from './mce-dept-research/mce-dept-research.component';
import {MceDeptAlumniComponent} from './mce-dept-alumni/mce-dept-alumni.component';
import {MceDeptNavComponent} from './mce-dept-nav/mce-dept-nav.component';
import {MceProfileComponent} from "./mce-profile/mce-profile.component";
import {MceHeadMessageComponent} from "./mce-head-message/mce-head-message.component";
import {MceHomeStoriesComponent} from "./mce-home-stories/mce-home-stories.component";
import {MceHomeEventsComponent} from "./mce-home-events/mce-home-events.component";
import {MceHomeCarousalComponent} from "./mce-home-carousal/mce-home-carousal.component";
import {BsDatepickerModule, CarouselModule, ModalModule, TooltipModule} from "ngx-bootstrap";
import {NgxSpinnerModule} from "ngx-spinner";
import {ComponentsModule} from "../components/components.module";
import {NgxMasonryModule} from "ngx-masonry";

@NgModule({
  declarations: [MceDeptHomeComponent, MceDeptPersonnelComponent, MceDeptNewsComponent, MceDeptAcademicsComponent, MceDeptResearchComponent, MceDeptAlumniComponent, MceDeptNavComponent, MceHomeCarousalComponent, MceHomeEventsComponent, MceHomeStoriesComponent, MceHeadMessageComponent, MceProfileComponent],
  imports: [ModalModule, BrowserModule, MceDeptRoutingModule, NgxSpinnerModule, ComponentsModule, TooltipModule, BsDatepickerModule, CarouselModule, NgxMasonryModule],
  exports: [ComponentsModule],
  entryComponents: [MceDeptNavComponent]
})

export class MceDeptModule {

}
