import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceDeptAlumniComponent } from './mce-dept-alumni.component';

describe('MceDeptAlumniComponent', () => {
  let component: MceDeptAlumniComponent;
  let fixture: ComponentFixture<MceDeptAlumniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceDeptAlumniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceDeptAlumniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
