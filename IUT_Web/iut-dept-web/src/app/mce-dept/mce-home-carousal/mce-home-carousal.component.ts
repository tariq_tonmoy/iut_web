import {Component, OnInit} from '@angular/core';
import {NewsService} from "../../service/news.service";
import {StoryDataStruct} from "../../view-model/story-data-struct";
import {CarouselConfig} from "ngx-bootstrap";
import {Router} from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-mce-home-carousal',
  templateUrl: './mce-home-carousal.component.html',
  providers: [
    {provide: CarouselConfig, useValue: {interval: 5000, noPause: true, showIndicators: true}}
  ],
  styleUrls: ['./mce-home-carousal.component.css']
})
export class MceHomeCarousalComponent implements OnInit {

  public focusedStories: StoryDataStruct[] = [];

  constructor(private newsService: NewsService,
              private router: Router) {
  }

  ngOnInit() {
    this.newsService.GetFocusedStoryByDepartment("MCE").then(x => {
      this.focusedStories = x;
      for (let f of this.focusedStories) {
        this.newsService.GetImageByStoryID(f.StoryID).then(resp => {
          f.StoryImg = resp;
        }).catch(err_p => {
          console.log(err_p);
        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  onCarouselStoryClicked(StoryID: number): void {
    this.router.navigate(["mce/news"], {queryParams: {storyID: StoryID}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });
  }

}
