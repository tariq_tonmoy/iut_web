import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MceHomeEventsComponent } from './mce-home-events.component';

describe('MceHomeEventsComponent', () => {
  let component: MceHomeEventsComponent;
  let fixture: ComponentFixture<MceHomeEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MceHomeEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MceHomeEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
