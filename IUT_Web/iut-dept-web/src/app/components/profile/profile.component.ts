import {
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef
} from "@angular/core";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {CommonConversionService} from "../../service/common-conversion.service";
import {CommonVariables, Country} from "../../view-model/common-variables";
import {TimeServiceService} from "../../service/time-service.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {AuthenticationService} from "../../service/authentication.service";
import {SignalRService} from "../../service/signal-r.service";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class ProfileComponent implements OnChanges, OnInit {

  @Input() fromProfileDetails: boolean;
  @Input() username: string;
  @Input() isMyProfile: boolean;
  @Output() onSignOutButtonClick = new EventEmitter<void>();
  @Output() onProfileDetailsButtonClick = new EventEmitter<any>();
  @Output() onProfileSaveButtonClick = new EventEmitter<boolean>();

  userID: string;
  personalDetails: PersonalDetailsDataStruct = new PersonalDetailsDataStruct("", "", "", "", "", "");
  hasPersonalDetails: boolean;
  profileLoaded: boolean;
  editClicked: boolean = false;
  bloodGroups: string[];
  countries: Country[];
  modalRef: BsModalRef;
  private userLogins: any[] = [];
  public isOnline: boolean = false;

  constructor(private personalService: PersonalDetailsService,
              private common: CommonConversionService,
              private timeService: TimeServiceService,
              private modalService: BsModalService,
              private authService: AuthenticationService,
              private signalRService: SignalRService,
              private nz: NgZone) {
  }

  ngOnInit(): void {
    this.bloodGroups = CommonVariables.bloodGroups;
    this.countries = CommonVariables.countries;
    this.profileLoaded = false;
    this.signalRService.loginStatusSource.subscribe(val => {
      this.userLogins = val;
      if (this.userID) {
        this.nz.run(()=>{
          this.isOnline = this.userLogins.indexOf(this.userID) !== -1;
        })
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["username"]) {
      if (this.username == null) {
        this.hasPersonalDetails = false;
        this.profileLoaded = false;
        return;
      }
      this.personalService.GetUserID(this.username).then((resp) => {
        this.userID = this.common.replaceQuotes(resp);
        this.isOnline = this.userLogins.indexOf(this.userID) === -1 ? false : true;
        if (this.isOnline) {
          $('#' + this.userID).removeClass('d-none').addClass('d-block');
        }
        else $('#' + this.userID).removeClass('d-block').addClass('d-none');

        this.personalDetails = new PersonalDetailsDataStruct('', '', '', '', '', '');
        this.profileLoaded = false;
        this.hasPersonalDetails = false;
        this.loadPersonalDetails();
      }).catch(err => {
        console.log(err);
      });
    }
  }


  loadPersonalDetails(): void {
    if (this.userID == null) {
      this.profileLoaded = false;
      this.hasPersonalDetails = false;
      return;
    }

    this.personalDetails.UserID = this.userID;

    this.personalService.GetProfileDetails(this.userID).then((p_resp) => {

      this.personalDetails.FirstName = p_resp.FirstName;
      this.personalDetails.LastName = p_resp.LastName;
      this.personalDetails.Image = p_resp.Image;
      this.personalDetails.ImageType = p_resp.ImageType;
      this.personalDetails.Nationality = p_resp.Nationality;
      this.personalDetails.DoB = this.timeService.FormatDate(p_resp.DoB);
      this.personalDetails.BloodGroup = p_resp.BloodGroup;
      this.personalDetails.ImageType = p_resp.ImageType;
      this.personalDetails.Image = p_resp.Image;
      this.personalDetails.Salutation = p_resp.Salutation;
      this.hasPersonalDetails = true;
      this.profileLoaded = true;

    }).catch((errPersonal) => {
      this.profileLoaded = true;
      this.hasPersonalDetails = false;
    });
    this.personalService.GetUserEmail(this.userID)
      .then((email) => {
        this.personalDetails.Email = this.common.replaceQuotes(email);
      })
      .catch((email_error) => {
        console.log(email_error);
        this.personalDetails.Email = "";
      });
    this.personalService.GetUserRolesByUser(this.userID).then((roles) => {
      this.personalDetails.Roles = roles;

      this.personalService.GetUserRoles().then((globalRoles) => {
        for (var i = 0; i < globalRoles.length; i++) {
          var flag: boolean = false;
          for (var j = 0; j < this.personalDetails.Roles.length; j++) {
            if (globalRoles[i].RoleName == this.personalDetails.Roles[j].RoleName) {
              flag = true;
              break;
            }
          }
        }
      }).catch((err) => {
        console.log("Error Getting Roles: " + err);
      });
    }).catch((err) => {
      console.log("Error in getting User Specific roles: " + err);
    });

  }

  onSignOutClick(): void {
    this.userID = null;
    this.personalDetails = new PersonalDetailsDataStruct("", "", "", "", "", "");
    this.onSignOutButtonClick.emit();
  }

  onProfileDetailsClick(): void {
    var hasProfile: boolean = this.hasPersonalDetails;
    var personalDetails: PersonalDetailsDataStruct = this.personalDetails;
    personalDetails.Username = this.username;
    this.onProfileDetailsButtonClick.emit({hasProfile, personalDetails});
  }

  onImageFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      $("img[name=outputImg]").prop("src", dataURL);
      var imgURL = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.personalDetails.Image = imgURL;
      this.personalDetails.ImageType = typeStr;
    });

    reader.readAsDataURL(input.files[0]);
  }

  onProfileSaveClick(): void {
    if (!this.editClicked) {
      this.personalDetails.UserID = this.userID;
      this.personalService.PostProfileDetails(this.personalDetails).then((resp) => {
        this.hasPersonalDetails = true;
      }).catch((err) => {
        console.log(err);
      });
    } else {
      this.personalDetails.UserID = this.userID;
      this.personalService.PutProfileDetails(this.personalDetails).then((resp) => {
        this.hasPersonalDetails = true;
      }).catch((err) => {
        console.log(err);
      });
      this.editClicked = false;
    }
  }

  onProfileEditClick(): void {
    this.editClicked = true;
  }

  onChangePasswordClick(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }


  editCancelClick(): void {
    this.editClicked = false;
  }


  onPasswordChangeEvent(status: number) {
    if (status) {
      this.modalRef.hide();
    } else {
      this.modalRef.hide();
    }
  }

  onAddExternLoginClicked(): void {
    this.authService.getExternalLogins().then(resp => {
      var v = resp.find(r => r.Name === "Google");
      if (v) {
        this.authService.getExternalUserRegInfoForAddExternal(v, this.personalDetails.UserID);
      }
    }).catch(err => {
      console.log(err);
    })
  }

}
