import {Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from "@angular/core";
import {AcademicsService} from "../../service/academics.service";
import {CourseDataStruct} from "../../view-model/course-data-struct";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {SyllabusDataStruct} from "../../view-model/syllabus-data-struct";
import {CourseMaterialDataStruct} from "../../view-model/course-material-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import * as FileSaver from "file-saver";

declare var require: any;
declare var $: any;
declare var jquery: any;

@Component({
  selector: "app-course-faculty",
  templateUrl: "./course-faculty.component.html",
  styleUrls: ["./course-faculty.component.css"]
})
export class CourseFacultyComponent implements OnInit, OnChanges {

  @Input() userID: string;
  @Input() isMyProfile: boolean;

  public selectedCourseList: CourseDataStruct[] = [];
  public selectedCourseDetailsID?: number;
  public programIDs: number[] = [];
  public selectedSyllabus: SyllabusDataStruct;
  public progData: ProgramDataStruct[] = [];
  public courseMaterialList: CourseMaterialDataStruct[] = [];
  public modalRef: BsModalRef;
  public selectedCourseMaterial: CourseMaterialDataStruct;
  private isMaterialUpdate: boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["userID"]) {
      this.academicService.GetCoursesByFaculty(this.userID).then(resp => {
        this.selectedCourseList = resp;
        for (let course of resp) {
          if (!this.programIDs.find(x => x === course.ProgramID)) {
            this.programIDs.push(course.ProgramID);
            this.getProgramDetails(course.ProgramID);
          }
        }
        this.selectedCourseList.sort((a, b) => a.CourseID < b.CourseID ? -1 : a.CourseID > b.CourseID ? 1 : 0);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  constructor(private academicService: AcademicsService,
              private modalService: BsModalService) {
  }

  getCourseNameByID(id: number): string {
    var v = this.selectedCourseList.find(x => x.CourseID === id);
    return v.CourseCode + ": " + v.CourseName;
  }


  private getProgramDetails(id: number): void {
    this.academicService.GetProgramByID(id).then(resp => {
      this.progData.push(resp);
    }).catch(err => {
      console.log(err);
    });
  }

  getCoursesByProgram(id: number): CourseDataStruct[] {
    return this.selectedCourseList.filter(x => x.ProgramID === id);
  }

  ngOnInit() {
  }

  onCourseDetailsExpand(data: CourseDataStruct): void {
    if (data) {
      this.selectedCourseDetailsID = data.CourseID;
      this.academicService.GetSyllabusByCourse(data.CourseID).then(resp => {
        this.selectedSyllabus = resp;
      }).catch(err => {
        console.log(err);
      });
      this.getMaterials(data.CourseID);
    }
    else this.selectedCourseDetailsID = null;
  }

  private getMaterials(id: number): void {
    this.academicService.GetCourseMaterialByCourseByFaculty(id, this.userID).then(resp => {
      this.courseMaterialList = resp;
    }).then(err => {
    });
  }

  onCourseMaterialRequest(data: CourseMaterialDataStruct, template: TemplateRef<any>) {
    if (!data) {
      this.isMaterialUpdate = false;
      this.selectedCourseMaterial = new CourseMaterialDataStruct(0, '', null, this.userID);
    }
    else {
      this.isMaterialUpdate = true;
      this.selectedCourseMaterial = data;
    }

    this.modalRef = this.modalService.show(template);
  }

  onFileChange($event) {
    var input = $event.target;
    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      var materialURL = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.selectedCourseMaterial.Material = materialURL;
      this.selectedCourseMaterial.MaterialType = typeStr;
      this.selectedCourseMaterial.MaterialFileName = input.files[0].name;
    });
    reader.readAsDataURL(input.files[0]);
  }

  onMaterialCancelClick() {
    this.modalRef.hide();
  }

  onMaterialSaveClick() {
    if (this.isMaterialUpdate) {
      this.academicService.PutCourseMaterial(this.selectedCourseMaterial).then(resp => {
        this.getMaterials(this.selectedCourseDetailsID);
      }).catch(err => {
        console.log(err);
      });
    }
    else {
      this.academicService.PostCourseMaterialByCourse(this.selectedCourseDetailsID, this.selectedCourseMaterial).then(resp => {
        this.getMaterials(this.selectedCourseDetailsID);
      }).catch(err => {
        console.log(err)
      });
    }
    this.modalRef.hide();
  }

  onMaterialDeleteClicked(data: CourseMaterialDataStruct) {
    this.academicService.DeleteCourseMaterial(data).then(resp => {
      this.getMaterials(this.selectedCourseDetailsID);
    }).catch(err => {
      console.log(err);
    });
  }

  onMaterialDownloadClicked(data: CourseMaterialDataStruct) {
    this.academicService.GetCourseMaterialFile(data).then(resp => {
      var b64toBlob = require("b64-to-blob");
      var blob = b64toBlob(resp.Material, resp.MaterialType);
      var file = new File([blob], resp.MaterialFileName, {type: "\"" + resp.MaterialType + "\""});
      FileSaver.saveAs(file);
    }).catch(err => {
      console.log(err);
    });
  }
}
