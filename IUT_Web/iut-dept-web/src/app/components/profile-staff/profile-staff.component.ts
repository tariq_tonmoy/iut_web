import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef} from "@angular/core";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {RoleRequestDataStruct} from "../../view-model/role-request-data-struct";
import {HttpErrorResponse} from "@angular/common/http";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {StaffService} from "../../service/staff.service";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {StaffDataStruct} from "../../view-model/staff-data-struct";
import {PersonnelService} from "../../service/personnel.service";
import {BsModalService, BsModalRef} from "ngx-bootstrap";

@Component({
  selector: "app-profile-staff",
  templateUrl: "./profile-staff.component.html",
  styleUrls: ["./profile-staff.component.css"]
})
export class ProfileStaffComponent implements OnInit, AfterViewInit {
  @Input() userID: string;
  @Input() isRoleRequest: boolean;
  @Input() isMyProfile: boolean;
  @Input() userRole: UserRoleDataStruct;
  @Output() onRequestCancelClick = new EventEmitter<void>();

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public deptList: DepartmentDataStruct[] = [];
  public request: RoleRequestDataStruct = new RoleRequestDataStruct(0, "", "");

  public staff: StaffDataStruct = new StaffDataStruct("", "", "", "");
  private tempStaff: StaffDataStruct;
  private modalRef: BsModalRef;
  public isInitialProfile: boolean = true;

  onAlertClosed(): void {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngAfterViewInit(): void {
    window.scroll(0, 0);

    if (this.userRole) {
      this.request.RequestedRoleID = this.userRole.RoleID;
      this.request.RequestUserID = this.userID;
    }
    this.isInitialProfile = !this.isRoleRequest;
    if (this.isRoleRequest) {
      this.academicsService.GetDepartmentList().then((resp) => {
        for (let res of resp) {
          this.deptList.push(res);
        }
      }).catch((err) => {
        console.log("Depr Error: " + err);
      });
    } else {
      this.personnelService.GetStaffInfo(this.userID).then((res) => {
        this.staff = res;
      }).catch((err) => {
        console.log(err);
      });
    }
  }


  constructor(private personalDetailsService: PersonalDetailsService,
              private personnelService: PersonnelService,
              private academicsService: AcademicsService,
              private modalService: BsModalService) {
  }


  ngOnInit() {

  }

  onCancelRequestClick() {
    this.onRequestCancelClick.emit();
  }


  requestForRole(): void {
    this.personalDetailsService.PostRoleRequest(this.request).then((resp) => {
      this.alertMsg = "Request Submitted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.isRoleRequest = false;
    }).catch((err) => {
      var e: HttpErrorResponse = err;
      this.alertMsg = (e.statusText) === "Conflict" ? "You have already submitted a request." : e.message;
      this.msgEvent = true;
      this.msgSuccess = false;
      if (e.statusText == "Conflict")
        this.isRoleRequest = false;
    });
  }

  onUpdateClick(template: TemplateRef<any>): void {
    this.tempStaff = Object.assign({}, this.staff);
    this.modalRef = this.modalService.show(template);
  }

  onSaveDetailsClick(): void {
    this.personnelService.EditStaffInfo(this.staff).then((resp) => {
      this.msgEvent = true;
      this.msgSuccess = true;
      this.alertMsg = "Details Updated";
      this.modalRef.hide();
    }).catch((err) => {
      this.msgEvent = true;
      this.msgSuccess = false;
      this.alertMsg = "Update Failed";
      this.modalRef.hide();
      this.staff = this.tempStaff;
    });
  }

  onCancelDetailsClick(): void {
    this.staff = this.tempStaff;
    this.modalRef.hide();
  }

}
