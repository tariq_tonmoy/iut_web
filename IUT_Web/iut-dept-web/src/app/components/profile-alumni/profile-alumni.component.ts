import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef} from "@angular/core";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {RoleRequestDataStruct} from "../../view-model/role-request-data-struct";
import {HttpErrorResponse} from "@angular/common/http";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AlumniService} from "../../service/alumni.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {AcademicsService} from "../../service/academics.service";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {AlumniDataStruct} from "../../view-model/alumni-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: "app-profile-alumni",
  templateUrl: "./profile-alumni.component.html",
  styleUrls: ["./profile-alumni.component.css"]
})
export class ProfileAlumniComponent implements OnInit, AfterViewInit {
  @Input() userID: string;
  @Input() isRoleRequest: boolean;
  @Input() isMyProfile: boolean;
  @Input() showPublications: boolean;
  @Input() userRole: UserRoleDataStruct;
  @Output() onRequestCancelClick = new EventEmitter<void>();

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public enrollmentYear: number;
  public passingYear: number;
  private modalRef: BsModalRef;
  public reqSubmitted: boolean = false;
  public viewType: string = "person";


  public deptList: DepartmentDataStruct[] = [];
  public progList: ProgramDataStruct[] = [];

  public request: RoleRequestDataStruct = new RoleRequestDataStruct(0, '', '');

  public AlumniList: AlumniDataStruct[] = [];
  public SelectedAlumni: AlumniDataStruct = new AlumniDataStruct(0);

  onAlertClosed(): void {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngAfterViewInit(): void {
    window.scroll(0, 0);
    if (this.userRole) {
      this.request.RequestedRoleID = this.userRole.RoleID;
      this.request.RequestUserID = this.userID;
    }

    this.academicsService.GetDepartmentList().then((resp) => {
      for (let res of resp) {
        this.deptList.push(res);
      }
    }).catch((err) => {
      console.log("Dept Error: " + err);
    });


  }

  private loadAlumniDetails(): void {
    this.progList = [];
    this.AlumniList = [];
    this.alumniService.GetAlumniDetails(this.userID)
      .then((resp) => {
        this.AlumniList = resp;
        for (let a of this.AlumniList) {
          this.academicsService.GetProgramByID(a.ProgramID).then((res_p) => {
            this.progList.push(res_p);
          });
        }
        this.sortAlumniDetails();
      })
      .catch(
        (err) => {
          console.error(err);
        }
      );
  }

  private sortAlumniDetails(): void {
    this.AlumniList = this.AlumniList.sort((a, b) => a.PassingYear > b.PassingYear ? -1 : a.PassingYear < b.PassingYear ? 1 : 0);
  }

  getProgramDetails(progID: number): ProgramDataStruct {
    return this.progList.find(x => x.ProgramID === progID);
  }

  onReqDeptSelectionChanged(): void {
    this.progList = [];
    this.academicsService.GetProgramListByDept(this.request.RequestedDepartmentID).then((resp) => {
      for (let res of resp) {
        this.progList.push(res);
      }
    }).catch((err) => {
      console.log("Prog Error: " + err);
    });
  }


  constructor(private personalDetailsService: PersonalDetailsService,
              private alumniService: AlumniService,
              private modalService: BsModalService,
              private academicsService: AcademicsService) {
  }


  ngOnInit() {
    if (!this.isRoleRequest) {
      this.loadAlumniDetails();
    }
  }

  onCancelRequestClick() {
    this.onRequestCancelClick.emit();
  }


  requestForRole(): void {
    this.personalDetailsService.PostRoleRequest(this.request).then((resp) => {
      this.alertMsg = "Request Submitted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.reqSubmitted = true;
      this.isRoleRequest = false;
    }).catch((err) => {
      var e: HttpErrorResponse = err;
      this.alertMsg = (e.statusText) === "Conflict" ? "You have already submitted a request." : e.message;
      this.msgEvent = true;
      this.msgSuccess = false;
      if (e.statusText == 'Conflict')
        this.isRoleRequest = false;
    });
  }

  onUpdateClick(data: AlumniDataStruct, template: TemplateRef<any>): void {
    this.SelectedAlumni = data;
    if (this.SelectedAlumni.EnrollmentYear)
      this.enrollmentYear = new Date(this.SelectedAlumni.EnrollmentYear).getFullYear();
    else if (this.SelectedAlumni.AdmissionYear) {
      this.SelectedAlumni.EnrollmentYear = new Date();
      this.SelectedAlumni.EnrollmentYear.setFullYear(this.SelectedAlumni.AdmissionYear);
      this.enrollmentYear = new Date(this.SelectedAlumni.EnrollmentYear).getFullYear();
    }
    if (this.SelectedAlumni.PassingYear)
      this.passingYear = new Date(this.SelectedAlumni.PassingYear).getFullYear();
    this.modalRef = this.modalService.show(template);

  }

  onDeleteClick(data: AlumniDataStruct): void {
    this.alumniService.DeleteAlumniDetails(data).then(resp => {
      this.alertMsg = "Details deteled Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.loadAlumniDetails();
    }).catch(err => {
      this.alertMsg = "Failed to delete details";
      this.msgEvent = true;
      this.msgSuccess = false;
    });
  }

  onSaveDataClick(): void {
    if (this.enrollmentYear) {
      this.SelectedAlumni.EnrollmentYear = new Date();
      this.SelectedAlumni.EnrollmentYear.setFullYear(this.enrollmentYear);
      this.SelectedAlumni.AdmissionYear = this.enrollmentYear;
    }

    if (this.passingYear) {
      this.SelectedAlumni.PassingYear = new Date();
      this.SelectedAlumni.PassingYear.setFullYear(this.passingYear);
    }
    this.alumniService.PutAlumniDetails(this.SelectedAlumni).then(resp => {
      this.msgEvent = true;
      this.msgSuccess = true;
      this.alertMsg = "Alumni Record Updated";
      this.AlumniList.splice(this.AlumniList.indexOf(this.AlumniList.find(x => x.AlumniID === this.SelectedAlumni.AlumniID)), 1);
      this.AlumniList.push(this.SelectedAlumni);
      this.sortAlumniDetails();
      this.SelectedAlumni = new AlumniDataStruct(0);
    }).catch(err => {
      console.log(err);
      this.msgEvent = true;
      this.msgSuccess = false;
      this.alertMsg = "Failed to update Alumni Record";
    });

    this.modalRef.hide();
  }

  onCancelDataClick(): void {
    this.SelectedAlumni = new AlumniDataStruct(0);
    this.modalRef.hide();
  }

  onNewReq(): void {
    this.reqSubmitted = false;
    this.isRoleRequest = true;
  }

}
