import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {AcademicsService} from "../../service/academics.service";
import {AlumniService} from "../../service/alumni.service";
import {AlumniDataStruct} from "../../view-model/alumni-data-struct";
import {AlumniSummaryDataStruct} from "../../view-model/alumni-summary-data-struct";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {TypeaheadMatch} from "ngx-bootstrap";

@Component({
  selector: 'app-search-alumni',
  templateUrl: './search-alumni.component.html',
  styleUrls: ['./search-alumni.component.css']
})
export class SearchAlumniComponent implements OnInit {

  public selectedDepartment: DepartmentDataStruct = new DepartmentDataStruct('', '');
  public selectedAlumniCategory: AlumniDataStruct = new AlumniDataStruct(0);
  public selectedAlumni: AlumniSummaryDataStruct = new AlumniSummaryDataStruct('');
  public programList: ProgramDataStruct[] = [];

  public departmentList: DepartmentDataStruct[] = [];
  public alumniCategoryList: AlumniDataStruct[] = [];
  public alumniSummaryList: AlumniSummaryDataStruct[] = [];

  public selectedName: string;
  public isSaveActivated: boolean = false;
  public typeAheadSingleWords = true;
  @Output() onFormRequestSubmitted: EventEmitter<AlumniSummaryDataStruct> = new EventEmitter();


  constructor(private academicsService: AcademicsService,
              private alumniService: AlumniService,
              private personalService: PersonalDetailsService) {
  }


  alumniOnSelect(e: TypeaheadMatch): void {
    if (e.item) {
      this.selectedAlumni = e.item;
      this.isSaveActivated = true;
    }
  }

  onSaveAssignAlumniClick(): void {
    this.isSaveActivated = false;
    this.selectedName = "";
    if (this.selectedAlumni)
      this.onFormRequestSubmitted.emit(this.selectedAlumni);
    else this.onFormRequestSubmitted.emit(null);
  }


  ngOnInit() {
    this.academicsService.GetDepartmentList().then(resp => {
      this.departmentList = resp;
    }).catch(err => {
      console.log(err);
    })
  }

  onDeptChange(): void {
    this.programList = [];
    this.alumniService.GetAlumniCategories(this.selectedDepartment.DepartmentID).then(resp => {
      this.alumniCategoryList = resp;
      for (let p of resp) {
        this.getProgramByID(p.ProgramID).then(r => {
          this.programList.push(r);
        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  getProgramByID(id: number): Promise<ProgramDataStruct> {
    return this.academicsService.GetProgramByID(id).then(resp => {
      return resp;
    }).catch(err => {
      console.log(err);
    });
  }

  getProgramNameByID(id: number): ProgramDataStruct {
    var v = this.programList.find(x => x.ProgramID === id);
    return v === null ? null : v;
  }

  onAlumniCategorySelected(): void {
    if (this.selectedAlumniCategory.ProgramID > -1 && this.selectedAlumniCategory.AdmissionYear > -1) {
      this.alumniSummaryList = [];
      this.alumniService.GetAlumniByProgramEnrollemnt(this.selectedAlumniCategory.ProgramID, this.selectedAlumniCategory.AdmissionYear).then(resp => {
        for (let p of resp) {
          this.personalService.GetProfileDetails(p.UserID).then(person => {
            this.alumniSummaryList.push(new AlumniSummaryDataStruct(
              p.UserID, p.StudentID, p.UserID, person.Salutation + " " + person.FirstName + " " + person.LastName, person.ImageType, person.Image));
          }).catch(err_p => {
            console.log(err_p);
          });
        }
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getUniquePrograms(data: AlumniDataStruct[]): number[] {
    var num: number[] = [];
    for (let p of data) {
      if (num.length === 0 || !num.find(x => x === p.ProgramID))
        num.push(p.ProgramID);
    }
    return num;
  }

  getUniqueBatches(data: AlumniDataStruct[]): number[] {
    var num: number[] = [];
    for (let p of data) {
      if (num.length === 0 || !num.find(x => x === p.AdmissionYear))
        num.push(p.AdmissionYear);
    }
    return num;
  }

}
