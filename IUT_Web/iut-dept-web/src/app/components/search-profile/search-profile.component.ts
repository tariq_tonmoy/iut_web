import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PersonalDetailsService} from "../../service/personal-details.service";
import {PersonalSummary} from "../../view-model/common-variables";
import {TypeaheadMatch} from "ngx-bootstrap";
import {ChatGroupDataStruct} from "../../view-model/chat-group-data-struct";

@Component({
  selector: 'app-search-profile',
  templateUrl: './search-profile.component.html',
  styleUrls: ['./search-profile.component.css']
})
export class SearchProfileComponent implements OnInit {

  @Input()
  public passedProfileSummaryList: PersonalSummary[] = [];
  @Input()
  public chatGroup: ChatGroupDataStruct;
  @Input()
  public userID: string;

  @Output()
  public onProfileSummaryListChanged = new EventEmitter<PersonalSummary[]>();
  public selfAddedProfileSummaryList: PersonalSummary[] = [];

  profileSummary: PersonalSummary[] = [];
  public assignedProfile: PersonalSummary;
  public selectedName: string;
  public isSaveActivated: boolean = false;

  constructor(private profileService: PersonalDetailsService) {
  }

  ngOnInit() {
    this.selfAddedProfileSummaryList = [];
  }

  profileOnSelect(e: TypeaheadMatch): void {
    if (e.item) {
      this.assignedProfile = e.item;
      this.isSaveActivated = true;
    }
  }

  onNameChange(event): void {
    if (this.selectedName && this.selectedName.length % 3 === 0) {
      this.profileService.GetPersonalDetailsBySearch(this.selectedName).then(resp => {
        for (let profile of resp) {
          this.profileService.GetPersonalSummaryByPersonalDetails(profile).then(resp_summary => {
            if (!this.profileSummary.find(x => x.id === resp_summary.id))
              this.profileSummary.push(resp_summary);
          }).catch(err_summary => {
            console.log(err_summary);
          })
        }
      }).catch(err => {
        console.log(err);
      });
    }
  }

  onAssignProfileClick() {
    if (!this.passedProfileSummaryList.find(x => x.id === this.assignedProfile.id)) {
      this.passedProfileSummaryList.push(this.assignedProfile);
      this.onProfileSummaryListChanged.emit(this.passedProfileSummaryList);
      this.selfAddedProfileSummaryList.push(this.assignedProfile);
    }
    this.selectedName = "";
    this.isSaveActivated = false;
  }

  onProfileRemoveClicked(data: PersonalSummary) {
    var selfAdded = this.selfAddedProfileSummaryList.find(x => x.id === data.id);
    if (selfAdded) {
      var idx = this.selfAddedProfileSummaryList.indexOf(selfAdded);
      this.selfAddedProfileSummaryList.splice(idx, 1);
    }
    selfAdded = this.passedProfileSummaryList.find(x => x.id === data.id);
    if (selfAdded) {
      var idx = this.passedProfileSummaryList.indexOf(selfAdded);
      this.passedProfileSummaryList.splice(idx, 1);
      this.onProfileSummaryListChanged.emit(this.passedProfileSummaryList);
    }
  }
}
