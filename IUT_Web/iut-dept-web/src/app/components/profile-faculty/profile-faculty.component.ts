import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef} from "@angular/core";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {RoleRequestDataStruct} from "../../view-model/role-request-data-struct";
import {HttpErrorResponse} from "@angular/common/http";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {PersonnelService} from "../../service/personnel.service";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {CommonVariables} from "../../view-model/common-variables";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: "app-profile-faculty",
  templateUrl: "./profile-faculty.component.html",
  styleUrls: ["./profile-faculty.component.css"]
})
export class ProfileFacultyComponent implements OnInit, AfterViewInit {
  @Input() userID: string;
  @Input() isRoleRequest: boolean;
  @Input() isMyProfile: boolean;
  @Input() userRole: UserRoleDataStruct;
  @Output() onRequestCancelClick = new EventEmitter<void>();

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public isBioExpanded: boolean = false;
  public bioLen: number = 1000;
  public isReqShown: boolean = false;
  private modalRef: BsModalRef;
  public viewType: string = "person";


  public facultyDetails: FacultyDataStruct = new FacultyDataStruct("", "");
  public deptList: DepartmentDataStruct[] = [];
  public designationList: string[] = CommonVariables.facultyDesignations;
  private tempFaculty: FacultyDataStruct;

  public request: RoleRequestDataStruct = new RoleRequestDataStruct(0, "", "");


  public isDeptPage: boolean;

  onBioExpand(res: boolean): void {
    if (res) {
      this.bioLen = this.facultyDetails.Biography.length;
      this.isBioExpanded = true;
    } else {
      this.bioLen = 1000;
      this.isBioExpanded = false;
    }
  }

  onAlertClosed(): void {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngAfterViewInit(): void {
    window.scroll(0, 0);

    if (this.userRole) {
      this.request.RequestedRoleID = this.userRole.RoleID;
      this.request.RequestUserID = this.userID;
    }
    if (this.isRoleRequest) {
      this.isReqShown = true;
      this.academicsService.GetDepartmentList().then((resp) => {
        for (let res of resp) {
          this.deptList.push(res);
        }
      }).catch((err) => {
        console.log("Dept Error: " + err);
      });
    }
    else {
      this.facultyService.GetFacultyInfo(this.userID).then((res) => {
        this.facultyDetails = res;
      }).catch((err) => {

      });
    }
  }

  onDesignationSelected(val: string): void {
    this.facultyDetails.Designation = val;
  }

  onUpdateClick(template: TemplateRef<any>): void {
    this.tempFaculty = Object.assign({}, this.facultyDetails);
    this.modalRef = this.modalService.show(template, Object.assign({}, {class: "modal-lg"}));
  }

  onSaveDetailsClick(): void {
    this.facultyService.EditFacultyInfo(this.facultyDetails).then((resp) => {
      this.msgEvent = true;
      this.msgSuccess = true;
      this.alertMsg = "Details Updated";
      this.modalRef.hide();
    }).catch((err) => {
      this.msgEvent = true;
      this.msgSuccess = false;
      this.alertMsg = "Update Failed";
      this.modalRef.hide();
      this.facultyDetails = this.tempFaculty;
    });
  }

  onCancelDetailsClick(): void {
    this.facultyDetails = this.tempFaculty;
    this.modalRef.hide();
  }

  constructor(private personalDetailsService: PersonalDetailsService,
              private facultyService: PersonnelService,
              private modalService: BsModalService,
              private academicsService: AcademicsService) {
  }


  ngOnInit() {
    this.isDeptPage = false;
    this.facultyService.GetFacultyInfo(this.userID).then((res) => {
      this.facultyDetails = res;

    }).catch((err) => {

    });
  }

  onCancelRequestClick() {
    this.onRequestCancelClick.emit();
  }


  requestForRole(): void {
    this.personalDetailsService.PostRoleRequest(this.request).then((resp) => {
      this.alertMsg = "Request Submitted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.isReqShown = false;
    }).catch((err) => {
      var e: HttpErrorResponse = err;
      this.alertMsg = (e.statusText) === "Conflict" ? "You have already submitted a request." : e.message;
      this.msgEvent = true;
      this.msgSuccess = false;
      if (e.statusText == "Conflict")
        this.isReqShown = false;
    });
  }

}
