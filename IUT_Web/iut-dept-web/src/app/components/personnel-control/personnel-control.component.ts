import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {AcademicsService} from "../../service/academics.service";
import {TimeServiceService} from "../../service/time-service.service";
import {CommonVariables, PersonalSummary} from "../../view-model/common-variables";
import {PersonnelService} from "../../service/personnel.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {StaffDataStruct} from "../../view-model/staff-data-struct";
import {TeacherAssistantDataStruct} from "../../view-model/teacher-assistant-data-struct";

declare var $: any;

@Component({
  selector: "app-personnel-control",
  templateUrl: "./personnel-control.component.html",
  styleUrls: ["./personnel-control.component.css"]
})


export class PersonnelControlComponent implements OnInit, AfterViewInit {
  public selectedDeptID: string = "";
  public selectedPersonnelType: string = "";
  public selectedDept: DepartmentDataStruct = new DepartmentDataStruct("", "");
  public facultyList: FacultyDataStruct[] = [];
  public staffList: StaffDataStruct[] = [];
  public taList: TeacherAssistantDataStruct[] = [];
  selectedPriority?: number = -1;
  facultyPriorityList: number[] = [];
  staffPriorityList: number[] = [];
  facultyCounter: number = 0;
  staffCounter: number = 0;


  public facultySummaryList: PersonalSummary[] = [];
  public staffSummaryList: PersonalSummary[] = [];
  public taSummaryList: PersonalSummary[] = [];

  constructor(private academicService: AcademicsService,
              private timeService: TimeServiceService,
              private personnelService: PersonnelService,
              private profileService: PersonalDetailsService) {
  }

  public personnelTypes: string[];
  public alertMsg: string = "";
  public msgEvent: boolean = false;
  public msgSuccess: boolean = false;
  public isSearched: boolean = false;
  public msgWarning: boolean = false;
  public msgFailed: boolean = false;


  ngAfterViewInit(): void {
    this.academicService.GetDepartmentList().then(resp => {
      this.deptList = resp;
      this.formatDeptList();
    }).catch(err => {

    });

  }

  onAlertClosed(): void {
    this.msgEvent = false;
    this.msgSuccess = false;
    this.msgWarning = false;
    this.msgFailed = false;
    this.alertMsg = "";
  }

  @Input() deptList: DepartmentDataStruct[];

  formatDeptList(): void {
    for (let d of this.deptList) {
      d.EstablishedIn = this.timeService.FormatDate(d.EstablishedIn);
    }
    this.deptList = this.deptList.sort((a, b) =>
      a.EstablishedIn > b.EstablishedIn ? 1 : a.EstablishedIn < b.EstablishedIn ? -1 : 0
    );
  }

  ngOnInit() {
    this.personnelTypes = CommonVariables.personnelTypes;
  }

  setFacultyPriority(data: FacultyDataStruct): void {
    this.selectedPriority = $("#" + data.FacultyID).children("option:selected").val();
    this.personnelService.EditFacultyPriority(data, this.selectedPriority === -1 ? null : this.selectedPriority).then(resp => {
      this.getFaculties();
    }).catch(err => {
      console.log(err);
    });
  }

  setStaffPriority(data: StaffDataStruct): void {
    this.selectedPriority = $("#" + data.StaffID).children("option:selected").val();
    this.personnelService.EditStaffPriority(data, this.selectedPriority === -1 ? null : this.selectedPriority).then(resp => {
      this.getStaffs();
    }).catch(err => {
      console.log(err);
    });
  }

  private getFaculties(): void {
    this.selectedPriority = -1;
    this.personnelService.GetFacultyByDept(this.selectedDeptID).then(resp => {
      this.facultyList = resp;
      this.facultyCounter = 0;
      this.facultyPriorityList = [];
      this.facultySummaryList = [];
      for (let faculty of this.facultyList) {
        this.facultyPriorityList.push(++this.facultyCounter);
        this.getPersonnelSummary(faculty.FacultyID).then(resp => {
          if (this.facultyPriorityList.find(x => x === faculty.Priority)) {
            this.facultyPriorityList.splice(this.facultyPriorityList.findIndex(x => x === faculty.Priority), 1);
          }
          this.facultySummaryList.push(resp);
        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  private getTAs(): void {
    this.personnelService.GetTAByDept(this.selectedDeptID).then(resp => {
      this.taList = resp;
      this.taSummaryList = [];
      for (let ta of this.taList) {
        this.getPersonnelSummary(ta.TAID).then(resp => {
          this.taSummaryList.push(resp);

        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  private getStaffs(): void {
    this.selectedPriority = -1;
    this.personnelService.GetStaffByDept(this.selectedDeptID).then(resp => {
      this.staffList = resp;
      this.staffSummaryList = [];
      this.staffCounter = 0;
      this.staffPriorityList = [];
      for (let staff of this.staffList) {
        this.staffPriorityList.push(++this.staffCounter);
        this.getPersonnelSummary(staff.StaffID).then(resp => {
          if (this.staffPriorityList.find(x => x === staff.Priority))
            this.staffPriorityList.splice(this.staffPriorityList.findIndex(x => x === staff.Priority), 1);
          this.staffSummaryList.push(resp);
        });
      }
    }).catch(err => {
      console.log(err);
    });
  }

  private getPersonnelSummary(id: string): Promise<PersonalSummary> {
    return this.profileService.GetProfileDetails(id).then(resp_details => {
      var summary: PersonalSummary = {name: "", id: ""};
      summary.name = resp_details.Salutation + resp_details.FirstName + " " + resp_details.LastName;
      summary.id = id;
      summary.image = resp_details.Image;
      summary.imageType = resp_details.ImageType;
      return summary;
    }).catch(err_details => {
      console.log(err_details);
    });

  }


  getFacultySummary(id: string): PersonalSummary {
    return this.facultySummaryList.find(x => x.id === id);
  }

  getStaffSummary(id: string): PersonalSummary {
    return this.staffSummaryList.find(x => x.id === id);
  }

  getTASummary(id: string): PersonalSummary {
    return this.taSummaryList.find(x => x.id === id);
  }


  onSearchClicked(): void {
    if (!this.selectedDeptID || !this.selectedPersonnelType) {
      this.isSearched = false;
      return;
    }
    this.selectedPriority = -1;
    this.onAlertClosed();
    if (this.selectedPersonnelType == "Faculty") {
      this.getFaculties();
    } else if (this.selectedPersonnelType === "Staff") {
      this.getStaffs();
    } else if (this.selectedPersonnelType === "TA") {
      this.getTAs();
    }
    this.isSearched = true;
  }

  private facultyToDelete: FacultyDataStruct;
  private staffToDelete: StaffDataStruct;
  private taToDelete: TeacherAssistantDataStruct;
  public selectedPersonnelForMsg: string = "";

  onDeleteClicked(id: string): void {
    var name: string = "";
    this.onAlertClosed();
    if (this.selectedPersonnelType === "Faculty") {
      name = this.facultySummaryList.find(x => x.id === id).name;
      this.facultyToDelete = this.facultyList.find(x => x.FacultyID === id);
    } else if (this.selectedPersonnelType === "Staff") {
      name = this.staffSummaryList.find(x => x.id === id).name;
      this.staffToDelete = this.staffList.find(x => x.StaffID === id);
    } else if (this.selectedPersonnelType === "TA") {
      name = this.taSummaryList.find(x => x.id === id).name;
      this.taToDelete = this.taList.find(x => x.TAID === id);
    }

    if (name) {
      this.alertMsg = "You are about to delete the profile of " + name + ". ";
      this.msgWarning = true;
      this.msgEvent = true;
      this.selectedPersonnelForMsg = id;
    }
  }

  deletePersonnel(id: string): void {
    if (!id) return;
    this.onAlertClosed();
    if (this.selectedPersonnelType === "Faculty") {
      this.personnelService.DeleteFaculty(this.facultyToDelete).then(resp=>{
        this.showSuccessMessage();
        this.getFaculties();
      }).catch(err=>{
        console.log(err);
        this.showFailureMessage()
      });
    } else if (this.selectedPersonnelType === "Staff") {
      this.personnelService.DeleteStaff(this.staffToDelete).then(resp=>{
        this.showSuccessMessage();
        this.getStaffs();
      }).catch(err=>{
        console.log(err);
        this.showFailureMessage();
      });
      this.showSuccessMessage();
    } else if (this.selectedPersonnelType === "TA") {

      this.personnelService.DeleteTA(this.taToDelete).then(resp=>{
        this.showSuccessMessage();
        this.getTAs();
      }).catch(err=>{
        console.log(err);
        this.showFailureMessage();
      });
    }
  }

  private showSuccessMessage():void{
    this.alertMsg = this.selectedPersonnelType+ " member profile deleted";
    this.msgSuccess= true;
    this.msgFailed= false;
    this.msgWarning=false;
    this.msgEvent = true;
  }

  private showFailureMessage():void{
    this.alertMsg = "Failed to delete "+this.selectedPersonnelType+ " member profile";
    this.msgFailed= true;
    this.msgSuccess=false;
    this.msgWarning=false;
    this.msgEvent = true;

  }

}
