import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelControlComponent } from './personnel-control.component';

describe('PersonnelControlComponent', () => {
  let component: PersonnelControlComponent;
  let fixture: ComponentFixture<PersonnelControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonnelControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
