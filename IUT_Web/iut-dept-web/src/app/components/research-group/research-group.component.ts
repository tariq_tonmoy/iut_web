import {AfterViewInit, Component, Input, OnChanges, SimpleChanges, TemplateRef} from "@angular/core";
import {ResearchGroupDataStruct} from "../../view-model/research-group-data-struct";
import {ResearchGroupService} from "../../service/research-group.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {PersonnelService} from "../../service/personnel.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {PublicationDataStruct} from "../../view-model/publication-data-struct";
import {PublicationService} from "../../service/publication.service";
import {Router} from "@angular/router";
import {CommonConversionService} from "../../service/common-conversion.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";

declare var $: any;

@Component({
  selector: "app-research-group",
  templateUrl: "./research-group.component.html",
  styleUrls: ["./research-group.component.css"]
})
export class ResearchGroupComponent implements AfterViewInit, OnChanges {

  constructor(private researchService: ResearchGroupService,
              private modalService: BsModalService,
              private personnelService: PersonnelService,
              private profileService: PersonalDetailsService,
              private publicationService: PublicationService,
              private router: Router,
              private commonService: CommonConversionService,
              private spinner: NgxSpinnerService) {
  }

  @Input()
  researchGroup: ResearchGroupDataStruct = new ResearchGroupDataStruct(0, "");
  researchGroupList: ResearchGroupDataStruct[] = [];
  researchGroupUsers: string[] = [];
  removeUsers: string[] = [];
  researchGroupPubs: number[] = [];
  removePubs: number[] = [];
  @Input()
  department: DepartmentDataStruct = new DepartmentDataStruct("", "");

  public selectedFacultyList: FacultyDataStruct[] = [];
  public oldMemberList: string[] = [];
  public selectedFacultySummeryList: FacultySummaryDataStruct[] = [];

  public selectedPublicationList: PublicationDataStruct[] = [];
  public oldPublicationList: number[] = [];

  public isUpdate: boolean = false;

  @Input()
  isDeptPage: boolean = false;
  @Input()
  userID: string = "";
  @Input()
  isMyProfile: string = "";
  @Input()
  isSelectedGroup: boolean = false;


  public msgEvent: boolean = false;
  public msgSuccess: boolean = false;
  public alertMsg: string = "";
  private modalRef: BsModalRef;

  ngAfterViewInit() {
    if (!this.isDeptPage)
      this.getResearchGroupsByUser();
    if (this.isDeptPage && this.department) {
      this.getResearchGroupsByDepartment();
    }
  }

  addPersonToSelectedFacultyList(data: PersonalDetailsDataStruct): void {
    if (this.selectedFacultyList.find(x => x.FacultyID === data.UserID))
      return;
    this.personnelService.GetFacultyInfo(data.UserID).then(resp => {
      this.selectedFacultyList.push(resp);
      this.selectedFacultySummeryList.push(new FacultySummaryDataStruct(data.UserID, resp.Designation, data.Salutation + " " + data.FirstName + " " + data.LastName, data.ImageType, data.Image));
    }).catch(err => {
      console.log(err);
    });
  }

  addFacultyToSelectedFacultyList(data: FacultySummaryDataStruct): void {
    if (this.selectedFacultyList.find(x => x.FacultyID === data.FacultyID))
      return;
    this.personnelService.GetFacultyInfo(data.FacultyID).then(resp => {
      this.selectedFacultyList.push(resp);
      this.selectedFacultySummeryList.push(data);
    }).catch(err => {
      console.log(err);
    });
  }

  changeGroupLeaderClicked(template: TemplateRef<any>): void {
    this.getUsersByResearchGroup();

    this.modalRef.hide();
    this.modalRef = this.modalService.show(template);
  }

  addPublicationToSelectedPublicationList(pubID: number): void {
    if (this.selectedPublicationList.find(x => x.PublicationID === pubID))
      return;
    this.publicationService.GetPublicationByPubID(pubID).then(resp => {
      if (resp)
        this.selectedPublicationList.push(resp);
    }).catch(err => {
      console.log(err);
    });
  }

  removePublication(pubID: number): void {
    this.selectedPublicationList.splice(this.selectedPublicationList.indexOf(this.selectedPublicationList.find(x => x.PublicationID === pubID)), 1);
  }

  removeFaculty(userID: string): void {
    this.selectedFacultyList.splice(this.selectedFacultyList.indexOf(this.selectedFacultyList.find(x => x.FacultyID === userID)), 1);
    this.selectedFacultySummeryList.splice(this.selectedFacultySummeryList.indexOf(this.selectedFacultySummeryList.find(x => x.FacultyID === userID)), 1);
  }


  getResearchGroupsByUser(): void {
    this.researchGroupList = [];
    this.researchService.getResearchGroupsByUser(this.userID).then(resp => {
      this.researchGroupList = resp;
    }).catch(err => {
      console.log(err);
    });

  }

  getUsersByResearchGroup(): void {
    this.researchService.getUsersByResearchGroup(this.researchGroup.GroupID).then(resp => {
      for (let p of resp) {
        this.profileService.GetProfileDetails(p.UserID).then(resp_p=>{
          this.addPersonToSelectedFacultyList(resp_p);
        }).catch(err_p=>{
          console.log(err_p);
        });
        this.oldMemberList.push(p.UserID);
      }
    }).catch(err => {
      console.log(err);
    });
  }

  getResearchGroupsByDepartment(): void {
    if (!this.department.DepartmentID)
      return;
    this.researchGroupList = [];
    this.researchService.getResearchGroupsByDept(this.department.DepartmentID).then(resp => {
      this.researchGroupList = resp;
      this.spinner.hide();
    }).catch(err => {
      console.log(err);
    });
  }

  getPublicationsByResearchGroup(): void {
    this.researchService.getPublicationsByResearchGroup(this.researchGroup.GroupID).then(resp => {
      for (let p of resp) {
        this.addPublicationToSelectedPublicationList(p.PublicationID);
        this.oldPublicationList.push(p.PublicationID);
      }
    }).catch(err => {
      console.log(err);
    });
  }

  editResearchGroup(): void {
    this.researchService.editResearchGroup(this.researchGroup).then(resp => {
      this.getResearchGroupsByUser();
      this.alertMsg = "Research Group Updated Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;

    }).catch(err => {
      console.log(err);
      this.alertMsg = "Failed to update Research Group.";
      this.msgEvent = true;
      this.msgSuccess = false;
    });
  }

  addNewResearchGroup(): Promise<number> {
    return this.researchService.postResearchGroup(this.researchGroup).then(resp => {
      this.researchGroup.GroupID = resp;
      this.getResearchGroupsByUser();
      this.alertMsg = "Research Group Saved Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      return resp;
    }).catch(err => {
      this.alertMsg = "Failed to Save Research Group.";
      this.msgEvent = true;
      this.msgSuccess = false;
      console.log(err);
    });
  }

  addUsersToResearchGroup(): void {
    this.researchService.addUsersToResearchGroup(this.researchGroup.GroupID, this.researchGroupUsers).then(resp => {

    }).catch(err => {
      console.log(err);
    });
  }

  addPublicationsToResearchGroup(): void {
    this.researchService.addPublicationsToResearchGroup(this.researchGroup.GroupID, this.researchGroupPubs).then(resp => {

    }).catch(err => {
      console.log(err);
    });
  }

  removePublicationFromResearchGroup(): void {
    this.researchService.removePublicationFromResearchGroup(this.researchGroup.GroupID, this.removePubs).then(resp => {

    }).catch(err => {
      console.log(err);
    });
  }

  removeUserFromResearchGroup(): void {
    this.researchService.removeUsersFromResearchGroup(this.researchGroup.GroupID, this.removeUsers).then(resp => {

    }).catch(err => {
      console.log(err);
    });
  }

  deleteResearchGroup(groupID: number): void {
    this.researchService.deleteResearchGroup(groupID).then(resp => {
      this.getResearchGroupsByUser();
      this.alertMsg = "Research Group Deleted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
    }).catch(err => {
      console.log(err);
      this.alertMsg = "Failed to delete Research Group";
      this.msgEvent = true;
      this.msgSuccess = false;

    });
  }

  onImageFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      $("img[name=researchGroupImage]").prop("src", dataURL);
      var imgURL = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.researchGroup.Image = imgURL;
      this.researchGroup.ImageType = typeStr;
    });
    reader.readAsDataURL(input.files[0]);
  }

  onCancelClick(): void {
    this.modalRef.hide();
  }

  onSaveClick(): void {
    if (!this.isUpdate) {
      this.addNewResearchGroup().then(resp => {
        this.researchGroupUsers = [];
        for (let member of this.selectedFacultyList)
          this.researchGroupUsers.push(member.FacultyID);
        this.researchGroupPubs = [];
        for (let pub of this.selectedPublicationList)
          this.researchGroupPubs.push(pub.PublicationID);
        this.researchGroup.GroupID = resp;
        this.addUsersToResearchGroup();
        this.addPublicationsToResearchGroup();
      });
      this.modalRef.hide();
    }
    else {
      this.editResearchGroup();
      this.researchGroupUsers = [];
      this.removeUsers = [];
      this.researchGroupPubs = [];
      this.removePubs = [];
      for (let member of this.selectedFacultyList) {
        if (!this.oldMemberList.find(x => x === member.FacultyID))
          this.researchGroupUsers.push(member.FacultyID);
      }
      for (let oldMember of this.oldMemberList) {
        if (!this.selectedFacultyList.find(x => x.FacultyID === oldMember))
          this.removeUsers.push(oldMember);
      }
      for (let pub of this.selectedPublicationList) {
        if (!this.oldPublicationList.find(x => x === pub.PublicationID))
          this.researchGroupPubs.push(pub.PublicationID);
      }
      for (let oldPub of this.oldPublicationList) {
        if (!this.selectedPublicationList.find(x => x.PublicationID === oldPub))
          this.removePubs.push(oldPub);
      }

      if (this.researchGroupUsers.length > 0)
        this.addUsersToResearchGroup();
      if (this.removeUsers.length > 0)
        this.removeUserFromResearchGroup();
      if (this.researchGroupPubs.length > 0)
        this.addPublicationsToResearchGroup();
      if (this.removePubs.length > 0)
        this.removePublicationFromResearchGroup();

      this.modalRef.hide();
    }

  }

  onMsgAlertClosed(): void {
    this.msgEvent = false;
    this.msgSuccess = false;
    this.alertMsg = "";
  }

  onFormRequest(data: ResearchGroupDataStruct, template: TemplateRef<any>): void {
    this.selectedFacultyList = [];
    this.selectedFacultySummeryList = [];
    this.selectedPublicationList = [];
    if (data) {
      this.oldMemberList = [];
      this.oldPublicationList = [];
      this.researchGroup = data;
      this.isUpdate = true;
      this.getUsersByResearchGroup();
      this.getPublicationsByResearchGroup();
    }
    else {
      this.isUpdate = false;
      this.profileService.GetProfileDetails(this.userID).then(resp => {
        this.addPersonToSelectedFacultyList(resp);
      }).catch(err => {
        console.log(err);
      });
      this.researchGroup = new ResearchGroupDataStruct(
        0,
        "");
    }
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));
  }

  viewGroupDetails(groupID: number): void {
    this.router.navigate(["cse/research"], {queryParams: {groupID: groupID}}).then(resp => {
    }).catch(err => {
      console.log("fail");
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.isDeptPage && !this.isSelectedGroup && changes["department"]) {
      this.getResearchGroupsByDepartment();
      this.spinner.show();

    }
    if (changes["isSelectedGroup"]) {
      if (this.isSelectedGroup) {
        this.getUsersByResearchGroup();
        this.getPublicationsByResearchGroup();
      }
    }
  }

  onBackClicked(): void {
    this.isSelectedGroup = false;
  }

  onProfileDetailsClicked(facultyID: string): void {
    this.profileService.getUserNameFromUserID(facultyID).then(resp => {
      this.router.navigate(["cse/profile/" + this.commonService.replaceQuotes(resp)]).then(resp_u => {

      }).catch(err_u => {
        console.log(err_u);
      });
    }).catch(err => {
      console.log(err);
    });
  }

  onGroupDetailsClicked(group: ResearchGroupDataStruct): void {
    this.researchGroup = group;
    this.isSelectedGroup = true;
    this.selectedPublicationList = [];
    this.selectedFacultySummeryList = [];
    this.selectedFacultyList = [];
    this.getUsersByResearchGroup();
    this.getPublicationsByResearchGroup();
  }

}
