import {Component, OnInit, AfterViewInit, TemplateRef, Input, Output, EventEmitter} from "@angular/core";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {TimeServiceService} from "../../service/time-service.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {PersonnelService} from "../../service/personnel.service";
import {CommonConversionService} from "../../service/common-conversion.service";
import {Router} from "@angular/router";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-department",
  templateUrl: "./department.component.html",
  styleUrls: ["./department.component.css"]
})
export class DepartmentComponent implements OnInit, AfterViewInit {

  @Input() deptList: DepartmentDataStruct[];
  @Output() onDeptLoaded = new EventEmitter<DepartmentDataStruct[]>();

  constructor(private academicsService: AcademicsService,
              private timeService: TimeServiceService,
              private facultyService: PersonnelService,
              private commonService: CommonConversionService,
              private profileSerivce: PersonalDetailsService,
              private modalService: BsModalService,
              private router: Router) {
  }

  ngOnInit() {
  }

  public selectedDept: DepartmentDataStruct = new DepartmentDataStruct("", "");
  private modalRef: BsModalRef;

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public enableEdit: boolean;
  public deptHeads: Map<string, FacultySummaryDataStruct> = new Map<string, FacultySummaryDataStruct>();

  public facultyListForSearch: FacultyDataStruct[] = [];

  onAddDeptHeadClick(deptDetails: DepartmentDataStruct, template: TemplateRef<any>): void {
    this.selectedDept = deptDetails;
    if (!this.externLinkClicked) {
      if (this.deptHeads.get(this.selectedDept.DepartmentID)) {
        this.facultyListForSearch = [];
        this.facultyService.GetFacultyInfo(this.deptHeads.get(this.selectedDept.DepartmentID).FacultyID).then(resp_f => {
          this.facultyListForSearch.push(resp_f);
        }).catch(err => {
        });
      }
      this.modalRef = this.modalService.show(template);
    } else this.externLinkClicked = false;
  }

  ondeptAlertClosed() {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  onDeptFormRequest(template: TemplateRef<any>): void {
    this.enableEdit = false;
    this.selectedDept = new DepartmentDataStruct("", "");
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));

  }

  onCancelClick(): void {
    this.modalRef.hide();
  }


  deleteDeptHead(): void {
    this.selectedDept.DepartmentHeadID = null;
    this.academicsService.PutDepartment(this.selectedDept).then(resp => {
      this.loadAllHeads();
      this.alertMsg = "Departmental Head Deleted";
      this.msgEvent = true;
      this.msgSuccess = true;
    }).catch(err_d => {
      this.alertMsg = "Failed to Delete Department";
      this.msgEvent = true;
      this.msgSuccess = false;
    });
    this.modalRef.hide();
  }


  addDeptHead(facultyID: string): void {
    if (facultyID) {
      this.selectedDept.DepartmentHeadID = facultyID;
      this.academicsService.PutDepartment(this.selectedDept).then(resp => {
        this.loadAllHeads();
        this.alertMsg = "Departmental Head Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
      }).catch(err_d => {
        this.alertMsg = "Failed to update Department";
        this.msgEvent = true;
        this.msgSuccess = false;
      });
    }
    this.modalRef.hide();
  }

  onSaveClick(): void {
    if (this.enableEdit) {
      this.academicsService.PutDepartment(this.selectedDept).then((resp) => {
        this.deptList.splice(this.deptList.indexOf(this.deptList.find(x => x.DepartmentID === this.selectedDept.DepartmentID)), 1);
        this.deptList.push(this.selectedDept);
        this.formatDeptList();
        this.alertMsg = "Department Successfully Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {

        this.alertMsg = "Department Update Faleld";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
    else {
      this.academicsService.PostDepartment(this.selectedDept).then((resp) => {
        this.deptList.push(this.selectedDept);
        this.formatDeptList();
        this.alertMsg = "Department Successfully Added";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Failed to save new Department";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
  }

  updateDeptDetails(dept: DepartmentDataStruct, template: TemplateRef<any>): void {
    this.enableEdit = true;
    this.selectedDept = dept;
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));
  }


  deleteDeptDetails(dept: DepartmentDataStruct): void {

    this.academicsService.DeleteDepartment(dept).then((resp) => {
      this.deptList.splice(this.deptList.indexOf(this.deptList.find(x => x.DepartmentID === dept.DepartmentID)), 1);
      this.formatDeptList();
      this.alertMsg = "Department Successfully Deleted";
      this.msgEvent = true;
      this.msgSuccess = true;

    }).catch((err) => {
      this.alertMsg = "Failed to Delete Department";
      this.msgEvent = true;
      this.msgSuccess = false;
    });
  }

  fetchHead(deptID: string): void {
    this.academicsService.GetDepartmentHead(deptID).then(resp_f => {
      this.profileSerivce.GetProfileDetails(resp_f.FacultyID).then(resp_p => {
        this.deptHeads.set(deptID, new FacultySummaryDataStruct(resp_f.FacultyID, resp_f.Designation, resp_p.Salutation + resp_p.FirstName + " " + resp_p.LastName, resp_p.ImageType, resp_p.Image));
      }).catch(err_p => {
      });
    }).catch(err_f => {

    });
  }

  private externLinkClicked: boolean = false;

  externalLinkHead(facultyID: string): void {
    this.externLinkClicked = true;
    this.profileSerivce.getUserNameFromUserID(facultyID).then(resp => {
      var username = this.commonService.replaceQuotes(resp);
      this.facultyService.GetFacultyInfo(facultyID).then(resp_f => {
        var dept = resp_f.DepartmentID.toLowerCase();
        var url = dept + "/profile/" + username;
        this.router.navigate([url]).then(resp => {
        }).catch(err => {
          console.log("fail");
        });

      }).catch(err => {
      });
    }).catch(err => {

    });
  }

  formatDeptList(): void {
    for (let d of this.deptList) {
      d.EstablishedIn = this.timeService.FormatDate(d.EstablishedIn);
    }
    this.deptList = this.deptList.sort((a, b) =>
      a.EstablishedIn > b.EstablishedIn ? 1 : a.EstablishedIn < b.EstablishedIn ? -1 : 0
    );
    this.onDeptLoaded.emit(this.deptList);
  }

  loadAllHeads(): void {
    this.deptHeads.clear();
    for (let d of this.deptList) {
      this.fetchHead(d.DepartmentID);
    }
  }

  ngAfterViewInit(): void {
    if (this.deptList.length === 0) {
      this.academicsService.GetDepartmentList().then((resp) => {
        this.deptList = resp;
        this.formatDeptList();
        this.loadAllHeads();
      }).catch((err) => {

      });
    }
    else {
      this.loadAllHeads();
    }
  }

  onImageFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      $("img[name=deptImg]").prop("src", dataURL);
      var imgURL = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.selectedDept.Image = imgURL;
      this.selectedDept.ImageType = typeStr;
    });

    reader.readAsDataURL(input.files[0]);
  }


}
