import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {LoginDataStruct} from "../../view-model/login-data-struct";
import {AuthenticationService} from "../../service/authentication.service";
import {ExternalAPIDataStruct} from "../../view-model/external-api-data-struct";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  title: string;
  login: LoginDataStruct;
  isLoading: boolean = false;
  @Input() errorMsg: string;
  @Input() googleApiInfo:ExternalAPIDataStruct;
  @Output() onLogInButtonClick = new EventEmitter<boolean>();
  @Output() onSignUpButtonClick = new EventEmitter<void>();

  ngOnInit(): void {
    this.title = "Login";
    this.login = new LoginDataStruct('password', '', '');
  }

  onLoginClick(): void {
    this.isLoading=true;
    this.authService.loginService(this.login).then((res) => {
      this.login = new LoginDataStruct('password', '', '');
      this.isLoading=false;
      this.onLogInButtonClick.emit(true);
    }).catch((err) => {
      this.isLoading=false;
      this.onLogInButtonClick.emit(false);
    });
  }

  onLoginByEnter(event){
    if(event.keyCode===13){
      this.onLoginClick();
    }
  }

  onSignUpClick(): void {
    this.onSignUpButtonClick.emit();
  }

  constructor(private authService: AuthenticationService) {

  }
  onGoogleRegClicked() {
    this.authService.getExternalUserRegInfo(this.googleApiInfo);
  }

}
