import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef} from '@angular/core';
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {SyllabusReviewDataStruct} from "../../view-model/syllabus-review-data-struct";
import {AcademicsService} from "../../service/academics.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'app-syllabus-review',
  templateUrl: './syllabus-review.component.html',
  styleUrls: ['./syllabus-review.component.css']
})
export class SyllabusReviewComponent implements OnInit, AfterViewInit,OnDestroy {

  constructor(private academicsService: AcademicsService,
              private modalService: BsModalService) {
  }

  @Input() selectedProgram: ProgramDataStruct;
  public selectedSyllabusReview: SyllabusReviewDataStruct;
  public reviews: SyllabusReviewDataStruct[] = [];
  private modalRef: BsModalRef;
  @Output() syllabusReviewBeingDestroyed=new EventEmitter<void>();

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public enableEdit: boolean;

  onAlertClosed() {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  onOptionClick(progOption: string, review: SyllabusReviewDataStruct, template: TemplateRef<any>) {
    this.selectedSyllabusReview = review !== null ? review : new SyllabusReviewDataStruct(0, '', this.selectedProgram.ProgramID);

    if (progOption === 'Add') {
      this.enableEdit = false;
      this.modalRef = this.modalService.show(template);
    } else if (progOption === 'Update') {
      this.enableEdit = true;
      this.modalRef = this.modalService.show(template);
    } else if (progOption === 'Delete') {
      this.academicsService.DeleteSyllabusReview(this.selectedSyllabusReview).then((resp) => {
        this.reviews.splice(this.reviews.indexOf(this.reviews.find(x => x.SyllabusReviewID === this.selectedSyllabusReview.SyllabusReviewID)), 1);
        this.alertMsg = "Syllabus Revision Successfully Deleted";
        this.msgEvent = true;
        this.msgSuccess = true;
      }).catch((err) => {
        this.alertMsg = "Failed to delete Syllabus Revision";
        this.msgEvent = true;
        this.msgSuccess = false;
      });
    }
  }

  loadSyllabusReview(): void {
    this.academicsService.GetSyllabusReviewsByProgram(this.selectedProgram.ProgramID).then((resp) => {
      if (resp.length <= 0) {
        this.alertMsg = "No Syllabus Revision found for " + this.selectedProgram.ProgramName;
        this.msgEvent = true;
        this.msgSuccess = false;
      }
      else {
        this.reviews = resp;
      }
    }).catch((err) => {

    })
  }

  ngOnInit() {
  }

  onReviewCancelClick(): void {
    this.modalRef.hide();
    this.selectedSyllabusReview = new SyllabusReviewDataStruct(0, '', this.selectedProgram.ProgramID);

  }

  onReviewSaveClick(): void {
    if (this.enableEdit) {
      this.academicsService.PutSyllabusReview(this.selectedSyllabusReview).then((resp) => {
        this.reviews.splice(this.reviews.indexOf(this.reviews.find(x => x.SyllabusReviewID === this.selectedSyllabusReview.SyllabusReviewID)), 1);
        this.reviews.push(this.selectedSyllabusReview);
        this.alertMsg = "Revision Successfully Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Syllabus Revision Update Faleld";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
    else {
      this.academicsService.PostSyllabusReview(this.selectedSyllabusReview).then((resp) => {
        this.reviews.push(this.selectedSyllabusReview);
        this.loadSyllabusReview();
        this.alertMsg = "Syllabus Revision Successfully Added";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Failed to save new Syllabus Revision";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
  }

  ngAfterViewInit(): void {
    this.loadSyllabusReview();
  }

  ngOnDestroy(): void {
    this.syllabusReviewBeingDestroyed.emit();
  }

}
