import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyllabusReviewComponent } from './syllabus-review.component';

describe('SyllabusReviewComponent', () => {
  let component: SyllabusReviewComponent;
  let fixture: ComponentFixture<SyllabusReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyllabusReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyllabusReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
