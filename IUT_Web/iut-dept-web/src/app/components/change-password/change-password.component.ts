import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {ChangePasswordBindingModel} from "../../view-model/change-password-binding-model";
import {AuthenticationService} from "../../service/authentication.service";
import {Router} from "@angular/router";
import {SignalRService} from "../../service/signal-r.service";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.css"]
})
export class ChangePasswordComponent implements OnInit {

  @Output() onChangePasswordEvent = new EventEmitter<number>();

  constructor(private authService: AuthenticationService,
              private router: Router,
              private signalRService: SignalRService) {
  }

  title: string;
  errorMsg: string;
  changePassword: ChangePasswordBindingModel = new ChangePasswordBindingModel("", "", "");

  ngOnInit() {
    this.title = "Change Password";
    this.errorMsg = "";
  }

  onCancelClick(): void {
    //'0' closes Modal
    this.onChangePasswordEvent.emit(0);
  }

  onChangePasswordClick(): void {
    this.authService.changePassword(this.changePassword).then(resp => {
      //'1' confirms password change
      this.onChangePasswordEvent.emit(1);
      this.signalRService.stopConnection();
      this.authService.logoutService();
      this.router.navigate(["cse/home"]);
    }).catch(err => {
      this.errorMsg = err;
    });
  }
}
