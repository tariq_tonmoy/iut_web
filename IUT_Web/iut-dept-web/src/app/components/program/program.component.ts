import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from "@angular/core";
import {AcademicsService} from "../../service/academics.service";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {TimeServiceService} from "../../service/time-service.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";


@Component({
  selector: "app-program",
  templateUrl: "./program.component.html",
  styleUrls: ["./program.component.css"]
})


export class ProgramComponent implements OnInit, AfterViewInit {

  public progList: ProgramDataStruct[] = [];
  public selectedProgram: ProgramDataStruct;
  @Input() deptList: DepartmentDataStruct[];
  @Output() onDeptLoaded = new EventEmitter<DepartmentDataStruct[]>();


  constructor(private academicsService: AcademicsService,
              private modalService: BsModalService,
              private timeService: TimeServiceService) {
  }

  ngOnInit() {
  }


  public selectedDept: DepartmentDataStruct = new DepartmentDataStruct("", "");
  private modalRef: BsModalRef;

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public enableEdit: boolean;
  public showSyllabusReview: Map<string, boolean> = new Map<string, boolean>();
  private changedDept: boolean = false;
  private progActionChanged: boolean = false;

  onAlertClosed() {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  onDepartmentSelect(dept: DepartmentDataStruct): void {
    if (this.selectedDept.DepartmentID === "" || dept.DepartmentID !== this.selectedDept.DepartmentID) {
      this.showSyllabusReview[this.selectedDept.DepartmentID] = false;
      this.selectedDept = dept;
      this.changedDept = true;

      for (let dep of this.deptList) {
        if (!this.showSyllabusReview.has(dep.DepartmentID))
          this.showSyllabusReview.set(dep.DepartmentID, false);
        else this.showSyllabusReview[dep.DepartmentID] = false;
      }
      this.academicsService.GetProgramListByDept(dept.DepartmentID).then((resp) => {
        this.progList = resp;
        this.formatProgList();
      }).catch((err) => {

      });
    }
    else {
      this.changedDept = false;
    }
  }


  onProgramOptionClick(progOption: string, prog: ProgramDataStruct, template: TemplateRef<any>) {
    this.selectedProgram = prog !== null
      ? new ProgramDataStruct(prog.ProgramID, prog.ProgramName, prog.Concentration, prog.DepartmentID, prog.ProgramDescription)
      : new ProgramDataStruct(0, "", "", this.selectedDept.DepartmentID);

    if (progOption === "Syllabus") {
      this.progActionChanged = true;
      if (!this.showSyllabusReview[this.selectedDept.DepartmentID])
        this.showSyllabusReview[this.selectedDept.DepartmentID] = true;
      else this.showSyllabusReview[this.selectedDept.DepartmentID] = false;


    }
    else {
      this.showSyllabusReview[this.selectedDept.DepartmentID] = false;
      this.progActionChanged = false;
      if (progOption === "Add") {
        this.enableEdit = false;
        this.modalRef = this.modalService.show(template,
          Object.assign({}, {class: "modal-lg"}));
      } else if (progOption === "Update") {
        this.enableEdit = true;
        this.modalRef = this.modalService.show(template,
          Object.assign({}, {class: "modal-lg"}));
      } else if (progOption === "Delete") {
        this.academicsService.DeleteProgram(this.selectedProgram).then((resp) => {
          this.progList.splice(this.progList.indexOf(this.progList.find(x => x.ProgramID === this.selectedProgram.ProgramID)), 1);
          this.formatProgList();
          this.alertMsg = "Program Successfully Deleted";
          this.msgEvent = true;
          this.msgSuccess = true;
        }).catch((err) => {
          this.alertMsg = "Program Deletion Failed";
          this.msgEvent = true;
          this.msgSuccess = false;
        });
      }
    }
  }

  onSyllabusReviewDestroyed(): void {
    if (!this.changedDept && this.progActionChanged) {
      this.showSyllabusReview[this.selectedDept.DepartmentID] = true;
      this.changedDept = true;
    }
  }

  onProgCancelClick(): void {
    this.modalRef.hide();
    this.selectedProgram = new ProgramDataStruct(0, "", "", "");
  }

  onProgSaveClick(): void {
    if (this.enableEdit) {
      this.academicsService.PutProgram(this.selectedProgram).then((resp) => {
        this.progList.splice(this.progList.indexOf(this.progList.find(x => x.ProgramID === this.selectedProgram.ProgramID)), 1);
        this.progList.push(this.selectedProgram);
        this.formatProgList();
        this.alertMsg = "Program Successfully Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Program Update Faleld";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
    else {
      this.academicsService.PostProgramByDepartment(this.selectedProgram.DepartmentID, this.selectedProgram).then((resp) => {
        this.progList.push(this.selectedProgram);
        this.onDepartmentSelect(this.selectedDept);
        this.alertMsg = "Program Successfully Added";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Failed to save new Program";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
  }

  formatProgList(): void {
    this.progList = this.progList.sort((a, b) =>
      a.ProgramName > b.ProgramName ? 1 : a.ProgramName < b.ProgramName ? -1 : 0
    );
  }

  formatDeptList(): void {
    for (let d of this.deptList) {
      d.EstablishedIn = this.timeService.FormatDate(d.EstablishedIn);
    }
    this.deptList = this.deptList.sort((a, b) =>
      a.EstablishedIn > b.EstablishedIn ? 1 : a.EstablishedIn < b.EstablishedIn ? -1 : 0
    );
    this.onDeptLoaded.emit(this.deptList);
  }

  ngAfterViewInit(): void {
    this.showSyllabusReview = new Map<string, boolean>();
    if (this.deptList.length === 0) {
      this.academicsService.GetDepartmentList().then((resp) => {
        this.deptList = resp;
        for (let dep of this.deptList) {
          this.showSyllabusReview.set(dep.DepartmentID, false);
        }
        this.formatDeptList();
      }).catch((err) => {

      });
    }
    else {
      for (let dep of this.deptList) {
        this.showSyllabusReview.set(dep.DepartmentID, false);
      }
    }
  }


}
