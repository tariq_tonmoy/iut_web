import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RegistrationDataStruct} from "../../view-model/registration-data-struct";
import {AuthenticationService} from "../../service/authentication.service";
import {ExternalAPIDataStruct} from "../../view-model/external-api-data-struct";
import {CommonVariables, RegisterExternalBindingModel} from "../../view-model/common-variables";

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  title: string;
  isLoading: boolean = false;
  registration: RegistrationDataStruct;
  @Input() isExternalRegistration: boolean = false;
  @Input() errorMsg: string;
  @Input() googleApiInfo: ExternalAPIDataStruct;
  @Output() isRegSuccess = new EventEmitter<boolean>();
  @Output() cancelButtonClick = new EventEmitter<void>();
  @Output() hasAccount = new EventEmitter<boolean>();

  ngOnInit(): void {
    this.title = "Sign Up";
    this.registration = new RegistrationDataStruct('', '', '', '');

  }

  onRegistrationClick(): void {
    this.isLoading = true;
    if (!this.isExternalRegistration) {
      this.authService.registrationService(this.registration).then((res) => {
        this.isLoading = false;
        this.isRegSuccess.emit(true);
      }).catch((err) => {
        this.isLoading = false;
        this.isRegSuccess.emit(false);
      });
    }
    else {
      var regExtern: RegisterExternalBindingModel = {Username: this.registration.Username, UserID: ""};
      this.authService.registrationExternal(regExtern).then((res) => {

        this.isLoading = false;
        this.isRegSuccess.emit(true);
      }).catch(err => {
        this.isLoading = false;
        this.isRegSuccess.emit(false);
      });
    }
    this.registration = new RegistrationDataStruct('', '', '', '');

  }

  onCancelClick(): void {
    this.cancelButtonClick.emit();

  }

  onHasAccountClick(): void {
    this.registration = new RegistrationDataStruct('', '', '', '');
    this.hasAccount.emit(true);
  }

  constructor(private authService: AuthenticationService) {

  }

  onGoogleRegClicked() {
    this.authService.getExternalUserRegInfo(this.googleApiInfo);
  }
}
