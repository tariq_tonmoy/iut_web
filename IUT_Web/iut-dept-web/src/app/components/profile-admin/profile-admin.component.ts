import {Component, Input, OnInit, Output, AfterViewInit, EventEmitter, TemplateRef, ViewChild} from "@angular/core";
import {RoleRequestDataStruct} from "../../view-model/role-request-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {AdminService} from "../../service/admin.service";
import {HttpErrorResponse} from "@angular/common/http";
import {TabDirective, TabsetComponent} from "ngx-bootstrap/tabs";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {BsModalRef, BsModalService, PageChangedEvent} from "ngx-bootstrap";
import {TimeServiceService} from "../../service/time-service.service";
import {AcademicsService} from "../../service/academics.service";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {SyllabusReviewDataStruct} from "../../view-model/syllabus-review-data-struct";

@Component({
  selector: "app-profile-admin",
  templateUrl: "./profile-admin.component.html",
  styleUrls: ["./profile-admin.component.css"]
})
export class ProfileAdminComponent implements OnInit, AfterViewInit {
  @Input() userID: string;
  @Input() isRoleRequest: boolean;
  @Input() userRole: UserRoleDataStruct;
  @Input() allRoles: UserRoleDataStruct[];
  @Output() onRequestCancelClick = new EventEmitter<void>();
  @ViewChild("academicsTabs") academicsTabs: TabsetComponent;

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public request: RoleRequestDataStruct = new RoleRequestDataStruct(0, "", "");
  public initAdminStatus: boolean;

  public deptList: DepartmentDataStruct[] = [];
  public progList: ProgramDataStruct[] = [];
  public syllabusVersions: SyllabusReviewDataStruct[] = [];

  onAlertClosed(): void {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngAfterViewInit(): void {
    window.scroll(0, 0);
    this.initAdminStatus = this.isRoleRequest;
    if (this.userRole && this.isRoleRequest) {
      this.request.RequestedRoleID = this.userRole.RoleID;
      this.request.RequestUserID = this.userID;
    }
    else if (!this.isRoleRequest) {
      this.loadRequests();
    }

  }


  constructor(private personalDetailsService: PersonalDetailsService,
              private adminService: AdminService,
              private academicsService: AcademicsService,
              private modalService: BsModalService,
              private timeService: TimeServiceService) {
  }


  ngOnInit() {

  }

  onCancelRequestClick() {
    this.onRequestCancelClick.emit();
  }


  requestForRole(): void {
    this.personalDetailsService.PostRoleRequest(this.request).then((resp) => {
      this.alertMsg = "Request Submitted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.isRoleRequest = false;
    }).catch((err) => {
      var e: HttpErrorResponse = err;
      this.alertMsg = (e.statusText) === "Conflict" ? "You have already submitted a request." : e.message;
      this.msgEvent = true;
      this.msgSuccess = false;
      if (e.statusText == "Conflict")
        this.isRoleRequest = false;
    });
  }

  public academicsDisplay: string = "";

  onTabSelect(data: TabDirective): void {
    if (data.id === "tab-requests") {
      if (this.allRequests.length <= 0) {
        this.loadRequests();
      }
    }
    else if (data.id === "tab-academics") {
      this.academicsDisplay = "Department";
      this.academicsTabs.tabs[0].active = true;
    }
    else if (data.id === "tab-events") {
    }

  }

  onAcademicsTabSelect(data: TabDirective): void {
    this.academicsDisplay = data.heading;
  }


  public selectedRequest: RoleRequestDataStruct;
  private modalRef: BsModalRef;

  onSelectedRequest(pendingRequest: RoleRequestDataStruct, template: TemplateRef<any>) {
    this.selectedRequest = pendingRequest;
    this.modalRef = this.modalService.show(template);
  }

  onRequestTabSelect(data: TabDirective): void {
    for (let role of this.allUniqRequestRoles) {
      if (role.RoleName === data.heading) {
        this.displayRequests = [];
        this.displayRequests = this.allRequests.filter(x => x.RequestedRoleID === role.RoleID);
        this.setupPagination();
        this.currentPage = 0;
      }
    }
  }

  public paginationDisplay: RoleRequestDataStruct[] = [];
  public paginationLength: number = 0;
  public itemPerPage: number = 15;
  public currentPage: number = 0;

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.paginationDisplay = this.displayRequests.slice(startItem, endItem);
  }

  setupPagination(): void {
    var len = this.displayRequests.length;
    this.paginationLength = len;
    this.paginationDisplay = this.displayRequests.slice(0, this.itemPerPage);


  }

  onPendingRequestDecisionClick(decision: string): void {
    if (decision === "Cancel") {
      this.modalRef.hide();
    }
    else if (decision === "Reject") {
      this.adminService.RemoveRequest(this.selectedRequest.RequestID).then((res) => {
        if (res == 201) {
          this.alertMsg = "Request Successfully Removed";
          this.msgEvent = true;
          this.msgSuccess = true;
          this.loadRequests();
          this.modalRef.hide();
        }
        else {
          this.alertMsg = "Request Removal Failed";
          this.msgEvent = true;
          this.msgSuccess = false;
          this.modalRef.hide();
        }
      }).catch((err) => {

      });
    }
    else if (decision === "Approve") {
      this.adminService.ApproveRequest(this.selectedRequest.RequestID).then((res) => {
        if (res == 201) {
          this.alertMsg = "Request Successfully Approved";
          this.msgEvent = true;
          this.msgSuccess = true;
          this.loadRequests();
          this.modalRef.hide();
        }
        else {
          this.alertMsg = "Request Approval Failed";
          this.msgEvent = true;
          this.msgSuccess = false;
          this.modalRef.hide();
        }
      }).catch((err) => {

      });
    }
  }

  public displayRequests: RoleRequestDataStruct[] = [];
  public allRequests: RoleRequestDataStruct[] = [];
  public allRequestPersonalDetails: PersonalDetailsDataStruct[] = [];
  public allUniqRequestRoles: UserRoleDataStruct[] = [];
  public allProgramList: ProgramDataStruct[] = [];

  getRoleNameFromRoleID(id: string): (string) {
    return this.allRoles.find(x => x.RoleID === id).RoleName;
  }

  getPersonalDetailsFromUserID(userID: string): PersonalDetailsDataStruct {
    return this.allRequestPersonalDetails.find(x => x.UserID == userID);
  }

  getProgramDetailsFromProgramID(progID: number): ProgramDataStruct {
    return this.allProgramList.find(x => x.ProgramID === progID);
  }

  loadRequests(): void {
    this.adminService.GetRequestList().then((res) => {
      this.allRequestPersonalDetails = [];

      this.displayRequests = [];
      this.allRequests = [];
      this.allRequestPersonalDetails = [];
      this.allUniqRequestRoles = [];
      this.allProgramList = [];

      var userIdList: string[] = [];
      var roleIdList: string[] = [];
      for (let id of res) {
        if (userIdList.find(x => x === id.RequestUserID) === undefined)
          userIdList.push(id.RequestUserID);
        if (roleIdList.find(x => x === id.RequestedRoleID) === undefined)
          roleIdList.push(id.RequestedRoleID);
        id.SubmissionDate = this.timeService.FormatDate(id.SubmissionDate);
        var alumniReqs = res.filter(x => x.RequestedProgramID !== null && x.RequestedProgramID !== undefined);

        for (let almn of alumniReqs) {
          this.academicsService.GetProgramByID(almn.RequestedProgramID).then((almnRes) => {
            res.find(x => x.RequestID === almn.RequestID).RequestedDepartmentID = almnRes.DepartmentID;
            if (this.allProgramList.find(x => x.ProgramID === almnRes.ProgramID) === undefined) {
              this.allProgramList.push(almnRes);
            }
          }).catch((almnErr) => {

          });
        }
      }

      for (let req of userIdList) {
        this.personalDetailsService.GetProfileDetails(req).then((p_resp) => {
          this.allRequestPersonalDetails.push(p_resp);
        }).catch((err) => {
          this.allRequestPersonalDetails.push(null);
        });
      }

      for (let role of roleIdList) {
        this.allUniqRequestRoles.push(new UserRoleDataStruct(role, this.getRoleNameFromRoleID(role)));
      }
      this.allRequests = res.sort((a, b) => a.SubmissionDate > b.SubmissionDate ? -1 : a.SubmissionDate < b.SubmissionDate ? 1 : 0);

      this.displayRequests = this.allRequests.filter(x => x.RequestedRoleID === this.allUniqRequestRoles[0].RoleID);
      this.setupPagination();
    }).catch((err) => {
      console.log(err);
    });
  }

  onDeptListLoaded(depts: DepartmentDataStruct[]): void {
    this.deptList = depts;
  }

  onProgListLoaded(progs: ProgramDataStruct[]): void {
    this.progList = progs;
  }

  onSyllabusVersionsLoaded(reviews: SyllabusReviewDataStruct[]): void {
    this.syllabusVersions = reviews;
  }

}
