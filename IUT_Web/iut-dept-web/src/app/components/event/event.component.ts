import {AfterViewInit, Component, OnInit, TemplateRef} from "@angular/core";
import {NewsService} from "../../service/news.service";
import {TimeServiceService} from "../../service/time-service.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {StoryDataStruct} from "../../view-model/story-data-struct";
import {CustomEventDataStruct} from "../../view-model/custom-event-data-struct";
import {LocationDetailsComponent} from "../location-details/location-details.component";
import {LocationDataStruct} from "../../view-model/location-data-struct";


declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, AfterViewInit {

  private locationComponent: LocationDetailsComponent;
  public deptList: DepartmentDataStruct[] = [];
  public selectedDept = new DepartmentDataStruct('', '');
  public storyList: StoryDataStruct[] = [];
  public eventList: CustomEventDataStruct[] = [];
  public selectedStory = new StoryDataStruct(0, '', '', '');
  public selectedEvent = new CustomEventDataStruct(0, '', 0);

  public location = new LocationDataStruct(0, '', '', '');

  ngAfterViewInit(): void {
    this.academicService.GetDepartmentList().then(resp => {
      this.deptList = resp;
      this.formatDeptList();
    }).catch(err => {

    })

  }

  formatDeptList(): void {
    for (let d of this.deptList) {
      d.EstablishedIn = this.timeService.FormatDate(d.EstablishedIn);
    }
    this.deptList = this.deptList.sort((a, b) =>
      a.EstablishedIn > b.EstablishedIn ? 1 : a.EstablishedIn < b.EstablishedIn ? -1 : 0
    );
  }

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public onEdit: boolean;
  public onEventEdit: boolean;
  private modalRef: BsModalRef;

  constructor(private newsService: NewsService,
              public timeService: TimeServiceService,
              private academicService: AcademicsService,
              private modalService: BsModalService) {
  }

  onAlertClosed() {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngOnInit() {
  }

  deptChanged(deptID: string): void {
    this.selectedDept = this.deptList.find(x => x.DepartmentID === deptID);
    this.newsService.GetStoryByDepartment(this.selectedDept.DepartmentID).then(resp => {
      this.storyList = resp;
      for (let s of this.storyList) {
        this.newsService.GetImageByStoryID(s.StoryID).then(resp_img => {
          s.StoryImg = resp_img;
        }).catch(err_img => {
          console.log(err_img);
        });
      }
    }).catch(err => {

    });

    this.newsService.GetEventByDepartment(this.selectedDept.DepartmentID).then(resp => {
      this.eventList = resp;
      for (let event of this.eventList) {
        this.newsService.GetEventLocationByEvent(event.CustomEventID).then(locResp => {
          this.eventList.find(x => x.CustomEventID === event.CustomEventID).EventLocation = locResp;
        })
      }
    }).catch(err => {

    });
  }

  getEvents(storyID: number): CustomEventDataStruct[] {
    return this.eventList.filter(x => x.StoryID === storyID);
  }

  openNewsDetailsModal(story: StoryDataStruct, template: TemplateRef<any>): void {
    this.onEdit = false;
    if (story == null)
      story = new StoryDataStruct(0, "", "", this.selectedDept.DepartmentID);
    this.selectedStory = story;
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: 'modal-lg'}));
  }

  onCancelClick(): void {
    this.selectedStory = new StoryDataStruct(0, '', '', '');
    this.modalRef.hide();
    this.deptChanged(this.selectedDept.DepartmentID);
  }

  onStorySaveClick(): void {
    if (!this.onEdit) {
      this.selectedStory.PublishDate = this.timeService.FormatDate(new Date());
      this.newsService.PostStory(this.selectedStory).then(resp => {
        this.alertMsg = "Data Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
        this.selectedStory = new StoryDataStruct(0, '', '', '');
        this.deptChanged(this.selectedDept.DepartmentID);
      }).catch(err => {
        this.alertMsg = "Failed to update data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
        this.modalRef.hide();
      });
    }
    else {
      this.newsService.PutStory(this.selectedStory).then(resp => {
        this.alertMsg = "Data Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
        this.selectedStory = new StoryDataStruct(0, '', '', '');
        this.deptChanged(this.selectedDept.DepartmentID);
      }).catch(err => {
        this.alertMsg = "Failed to update data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
        this.modalRef.hide();
      });
    }
  }

  onImageFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      $("img[name=storyImg]").prop("src", dataURL);
      var imgURL = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.selectedStory.StoryImg = imgURL;
      this.selectedStory.ImageType = typeStr;
    });

    reader.readAsDataURL(input.files[0]);
  }


  onEditStoryClicked(story: StoryDataStruct, template: TemplateRef<any>): void {
    this.onEdit = true;
    this.selectedStory = story;

    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: 'modal-lg'}));
  }

  onDeleteStoryClicked(story: StoryDataStruct): void {
    var r = confirm("Are you sure?");
    if (r) {
      this.newsService.DeleteStory(story).then(resp => {
        this.alertMsg = "Data Deleted";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.deptChanged(this.selectedDept.DepartmentID);
      }).catch(err => {
        this.alertMsg = "Failed to Delete data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
      });
    }
  }

  onEventEditClicked(s: StoryDataStruct, e: CustomEventDataStruct, template: TemplateRef<any>): void {
    this.onEventEdit = true;
    this.selectedStory = s;
    this.selectedEvent = e;
    this.selectedEvent.EventEnds = this.timeService.FormatDate(this.selectedEvent.EventEnds);
    this.selectedEvent.EventStart = this.timeService.FormatDate(this.selectedEvent.EventStart);
    this.location = this.selectedEvent.EventLocation;
    this.modalRef = this.modalService.show(template);
  }

  onLocationComponentLoaded(locationComp: LocationDetailsComponent): void {
    this.locationComponent = locationComp;
    this.locationComponent.SetLocation(this.location);
  }

  onEventSaveClick(): void {
    this.selectedEvent.StoryID = this.selectedStory.StoryID;
    if (this.onEventEdit) {
      this.location = this.locationComponent.onSaveClick();
      this.newsService.PutEvent(this.selectedEvent).then(resp => {
        this.selectedStory = new StoryDataStruct(0, '', '', '');
        if (this.selectedEvent.EventLocation) {
          this.newsService.PutEventLocation(this.selectedEvent.CustomEventID, this.location).then(resp_loc => {
            this.alertMsg = "Data Updated";
            this.msgEvent = true;
            this.msgSuccess = true;
            this.modalRef.hide();
            this.selectedEvent = new CustomEventDataStruct(0, '', 0);
            this.deptChanged(this.selectedDept.DepartmentID);
          }).catch(err_loc => {
            this.alertMsg = "Failed to update data";
            this.msgSuccess = false;
            this.msgEvent = true;
            console.log(err_loc);
            this.modalRef.hide();
          });
        } else {
          this.newsService.PostEventLocation(this.selectedEvent.CustomEventID, this.location).then(resp_loc => {
            this.alertMsg = "Data Updated";
            this.msgEvent = true;
            this.msgSuccess = true;
            this.modalRef.hide();
            this.selectedEvent = new CustomEventDataStruct(0, '', 0);
            this.deptChanged(this.selectedDept.DepartmentID);
          }).catch(err_loc => {
            this.alertMsg = "Failed to update data";
            this.msgSuccess = false;
            this.msgEvent = true;
            console.log(err_loc);
            this.modalRef.hide();
          });
        }
      }).catch(err => {
        this.alertMsg = "Failed to update data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
        this.modalRef.hide();
      });
    }
    else {
      this.newsService.PostEvent(this.selectedEvent).then(resp => {
        this.alertMsg = "Data Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
        this.selectedEvent = new CustomEventDataStruct(0, '', 0);
        this.selectedStory = new StoryDataStruct(0, '', '', '');

        this.deptChanged(this.selectedDept.DepartmentID);
      }).catch(err => {
        this.alertMsg = "Failed to update data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
        this.modalRef.hide();
      });
    }

  }

  OnAddEventClicked(s: StoryDataStruct, admin_event_modal: TemplateRef<any>): void {
    this.onEventEdit = false;
    this.selectedStory = s;
    this.selectedEvent = new CustomEventDataStruct(0, '', 0);
    this.location = this.selectedEvent.EventLocation;
    this.modalRef = this.modalService.show(admin_event_modal);
  }

  onEventDeleteClicked(): void {
    var r = confirm("Are you sure?");
    if (r) {
      this.newsService.DeleteEvent(this.selectedEvent).then(resp => {
        this.alertMsg = "Data Deleted";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.deptChanged(this.selectedDept.DepartmentID);
        this.modalRef.hide();
      }).catch(err => {
        this.alertMsg = "Failed to Delete data";
        this.msgSuccess = false;
        this.msgEvent = true;
        console.log(err);
        this.modalRef.hide();
      });
    }
  }

}
