import {AfterViewInit, Component, Input, NgZone, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ChattingService} from "../../service/chatting.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {ChatGroupDataStruct} from "../../view-model/chat-group-data-struct";
import {SignalRService} from "../../service/signal-r.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {PersonalSummary} from "../../view-model/common-variables";
import {UserDetailsDataStruct} from "../../view-model/user-details-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {MessageDataStruct} from "../../view-model/message-data-struct";
import {ActivatedRoute, Router} from "@angular/router";
import {TimeServiceService} from "../../service/time-service.service";
import * as FileSaver from "file-saver";

declare var require: any;
declare var $: any;

@Component({
  selector: 'app-profile-chatting',
  templateUrl: './profile-chatting.component.html',
  styleUrls: ['./profile-chatting.component.css']
})
export class ProfileChattingComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() userID: string;

  public isChatWindowCompressed: boolean = true;
  public chatGroups: ChatGroupDataStruct[] = [];
  private chatGroupsSource = new BehaviorSubject([]);
  private currentChatGroups = this.chatGroupsSource.asObservable();
  public chatGroup: ChatGroupDataStruct = new ChatGroupDataStruct();
  public peopleSummaryList: PersonalSummary[] = [];
  private isUpdate: boolean = false;
  private modalRef: BsModalRef;
  public changedProfileSummaryList: PersonalSummary[] = [];
  public message: string = "";
  public userStatuses: any[] = [];
  private deptID: string = "";
  public selectedMessage: MessageDataStruct = new MessageDataStruct();

  constructor(private chatting: ChattingService,
              private profileService: PersonalDetailsService,
              private signalRService: SignalRService,
              private modalService: BsModalService,
              private nz: NgZone,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private timeService: TimeServiceService) {
    this.currentChatGroups.subscribe(val => this.chatGroups = val);
  }


  ngOnInit() {
    $('body').removeClass('no-scroll').addClass('do-scroll');
    window.scroll(0, 0);
    this.isChatWindowCompressed = true;
    this.chatGroup = new ChatGroupDataStruct();
    this.chatting.GetChatGroupsByUser(this.userID).then(resp => {
      this.chatGroupsSource.next(resp);
    }).catch(err => {
      console.log(err);
    });
    this.deptID = this.activatedRoute.root.firstChild.snapshot.data['dept'];
    this.signalRService.loginStatusSource.subscribe(val => {
      for (let v of this.userStatuses) {
        if (val.indexOf(v) === -1) {
          this.nz.run(() => {
            this.userStatuses.splice(this.userStatuses.indexOf(v), 1);
          });
        }
      }
      for (let v of val) {
        if (this.userStatuses.indexOf(v) === -1) {
          this.nz.run(() => {
            this.userStatuses.push(v);
          });
        }
      }
    });

    this.signalRService.addedToGroup.subscribe(value => {
      this.chatting.GetChatGroupDetails(value).then(resp => {
        this.nz.run(() => {
          this.chatGroups.push(resp);
          this.chatGroupsSource.next(this.chatGroups);
        });
      }).catch(err => {
        console.log(err);
      });
    });

    this.signalRService.removedFromGroup.subscribe(value => {
      var idx = this.chatGroups.indexOf(this.chatGroups.find(x => x.ChatGroupID === value));
      if (idx > -1) {
        this.nz.run(() => {
          if (this.chatGroup && this.chatGroup.ChatGroupID) {
            this.chatGroup = new ChatGroupDataStruct();
          }
          this.chatGroups.splice(idx, 1);
          this.chatGroupsSource.next(this.chatGroups);
        });
      }
    });

    this.signalRService.addUsersToGroup.subscribe(value => {
      if (this.chatGroup.ChatGroupID === value.groupID) {
        for (let id of value.userIDs) {
          this.nz.run(() => {
            this.chatGroup.GroupMembers.push(new UserDetailsDataStruct(id));
            this.getPeopleSummary(id);
          });
        }
      }
    });

    this.signalRService.removeUsersFromGroup.subscribe(value => {
      if (this.chatGroup.ChatGroupID === value.groupID) {
        for (let id of value.userIDs) {
          this.nz.run(() => {
            var user = this.chatGroup.GroupMembers.find(x => x.UserID == id);
            if (user) {
              this.chatGroup.GroupMembers.splice(this.chatGroup.GroupMembers.indexOf(user), 1);
            }
          });
        }
      }
    });

    this.signalRService.messageReceived.subscribe(value => {
      if (this.chatGroup.ChatGroupID === value.GroupID) {
        this.nz.run(() => {
          value.TimeStamp = this.timeService.FormatDateTime(value.TimeStamp);
          this.chatGroup.Messages.push(value);
          if (value.MessageFileType && value.MessageFileType.match('image')) {
            this.chatting.GetMessagesFile(value).then(resp => {
            }).catch(err_file => {
              console.log(err_file);
            });
          }
        });
      }
    });

    this.signalRService.deleteMessage.subscribe(value => {
      if (this.chatGroup.ChatGroupID === value.groupID) {
        var idx = this.chatGroup.Messages.indexOf(this.chatGroup.Messages.find(x => x.MessageID === value.messageID));
        if (idx > -1) {
          this.nz.run(() => {
            this.chatGroup.Messages.splice(idx, 1);
          });
        }
      }
    });

    this.signalRService.removedGroup.subscribe(value => {
      var idx = this.chatGroups.indexOf(this.chatGroups.find(x => x.ChatGroupID === value));
      if (idx > -1) {
        this.nz.run(() => {
          this.chatGroups.splice(idx, 1);
          if (this.chatGroup && this.chatGroup.ChatGroupID == value) {
            this.chatGroup = new ChatGroupDataStruct();
          }
        });
      }
    });

    this.signalRService.editedGroup.subscribe(value => {
      var idx = this.chatGroups.indexOf(this.chatGroups.find(x => x.ChatGroupID === value.ChatGroupID));
      if (idx > -1) {
        this.nz.run(() => {
          this.chatGroups[idx] = value;
        });
      }
    });
  }

  onProfileLinkCliked(userID: string): void {
    this.profileService.getUserNameFromUserID(userID).then(resp => {
      this.router.navigate([this.deptID.toLowerCase() + '/profile/' + resp]).then(resp_n => {

      }).catch(err_n => {
        console.log(err_n);
      })
    }).catch(err => {
      console.log(err);
    })
  }

  getPeopleSummaryForDOM(id: string): PersonalSummary {
    return this.peopleSummaryList.find(x => x.id === id);
  }

  getPeopleSummary(id: string): Promise<PersonalSummary> {
    if (!this.peopleSummaryList.find(x => x.id === id)) {
      return this.profileService.GetPerosnalSummaryByUserID(id).then(resp => {
        this.peopleSummaryList.push(resp);
        return resp;
      })
    } else return new Promise<PersonalSummary>((resolve => {
      resolve(this.peopleSummaryList.find(x => x.id === id));
    }));
  }

  requestGroupAddOrEdit(isUpdate: boolean, template: TemplateRef<any>): void {
    this.isUpdate = isUpdate;
    this.changedProfileSummaryList = [];
    if (this.isUpdate) {
      this.modalRef = this.modalService.show(template);
      this.getProfileSummaryListByGroup(this.chatGroup);
    } else {
      this.chatGroup = new ChatGroupDataStruct();
      this.chatGroup.CreatorID = this.userID;
      this.getPeopleSummary(this.userID).then(resp => {
        this.changedProfileSummaryList.push(resp);
      }).catch(err => {
        console.log(err);
      });
      this.modalRef = this.modalService.show(template);
    }
  }

  deleteChatGroup(data: ChatGroupDataStruct): void {
    this.chatting.DeleteChattingGroup(data).then(resp => {

      var idx = this.chatGroups.indexOf(this.chatGroups.find(x => x.ChatGroupID == data.ChatGroupID));
      if (idx > -1)
        this.chatGroups.splice(idx, 1);
    }).catch(err => {
      console.log(err);
    })
  }

  getProfileSummaryListByGroup(group: ChatGroupDataStruct): void {
    this.changedProfileSummaryList = [];
    if (this.isUpdate) {
      for (let p of group.GroupMembers) {
        var summary = this.getPeopleSummaryForDOM(p.UserID);
        if (summary)
          this.changedProfileSummaryList.push(summary);
        else {
          this.profileService.GetPerosnalSummaryByUserID(p.UserID).then(resp => {
            this.changedProfileSummaryList.push(resp);
          }).then(err => {
          });
        }
      }
    }
  }

  onChatGroupSelected(id: number, event): void {
    if (!event)
      return;
    this.chatGroup = this.chatGroups.find(x => x.ChatGroupID === id);
    if (this.chatGroup) {
      this.isChatWindowCompressed = false;
      this.setChatWindow();

      this.chatting.GetUsersFromChatGroup(id).then(resp => {
        this.chatGroup.GroupMembers = resp;
        for (let p of resp) {
          this.getPeopleSummary(p.UserID);
        }
      }).catch(err => {
        console.log(err);
      });
      this.chatting.GetMessagesByChatGroup(id).then(resp_m => {
        this.chatGroup.Messages = resp_m;
        for (let m of this.chatGroup.Messages) {
          m.TimeStamp = this.timeService.FormatDateTime(m.TimeStamp);
          this.getPeopleSummary(m.SenderID);
          if (m.MessageFileType && m.MessageFileType.match('image')) {
            this.chatting.GetMessagesFile(m).then(resp => {

            }).catch(err_file => {
              console.log(err_file);
            });
          }
        }
      }).catch(err_m => {
        console.log(err_m);
      })
    }
  }


  onProfileSummaryListChanged(profileSummaryList: PersonalSummary[]): void {
    this.changedProfileSummaryList = profileSummaryList;
  }

  getUserIDsToAdd(): string[] {
    var addIDs: string[] = [];
    try {
      for (let p of this.changedProfileSummaryList) {
        if (!this.chatGroup.GroupMembers.find(x => x.UserID === p.id)) {
          addIDs.push(p.id);
        }
      }
    } catch (e) {
      console.log(e);
    }
    return addIDs;
  }

  getUserIDsToRemove(): string[] {
    var removeIDs: string[] = [];
    try {
      for (let p of this.chatGroup.GroupMembers) {
        if (!this.changedProfileSummaryList.find(x => x.id === p.UserID)) {
          removeIDs.push(p.UserID);
        }
      }
    } catch (e) {
      console.log(e);
    }
    return removeIDs;
  }

  onSaveClick(): void {
    if (!this.isUpdate) {
      let toAdd: string[] = this.getUserIDsToAdd();

      this.chatting.AddNewChatGroup(this.chatGroup).then(resp => {
        this.chatGroup.ChatGroupID = parseInt(resp);
        if (toAdd.length > 0)
          this.chatting.AddUsersToChatGroup(this.chatGroup.ChatGroupID, toAdd).then(resp_add => {
          }).catch(err_add => {
            console.log(err_add);
          });
      }).catch(err => {
        console.log(err);
      });
    } else {
      let toAdd: string[] = this.getUserIDsToAdd();
      let toRemove: string[] = this.getUserIDsToRemove();
      if (this.chatGroup.CreatorID === this.userID)
        this.chatting.EditChatGroup(this.chatGroup).then(resp => {

        }).catch(err => {
          console.log(err);
        });
      if (toAdd.length > 0) {
        this.chatting.AddUsersToChatGroup(this.chatGroup.ChatGroupID, toAdd).then(resp_add => {
          if (this.chatGroup.CreatorID === this.userID)
            if (toRemove.length > 0)
              this.chatting.RemoveUsersFromChatGroup(this.chatGroup.ChatGroupID, toRemove).then(resp_remove => {
              }).catch(err_remove => {
                console.log(err_remove);
              });
        }).catch(err_add => {
          console.log(err_add);
        });
      } else {
        if (toRemove.length > 0)
          this.chatting.RemoveUsersFromChatGroup(this.chatGroup.ChatGroupID, toRemove).then(resp_remove => {
          }).catch(err_remove => {
            console.log(err_remove);
          });
      }

    }
    this.modalRef.hide();
  }

  leaveChatGroup(group: ChatGroupDataStruct): void {
    var leaveId: string[] = [this.userID];
    this.chatting.RemoveUsersFromChatGroup(this.chatGroup.ChatGroupID, leaveId).then(resp_remove => {
      var idx = this.chatGroups.indexOf(this.chatGroups.find(x => x.ChatGroupID === group.ChatGroupID));
      if (idx > -1)
        this.chatGroups.splice(idx, 1);
    }).catch(err_remove => {
      console.log(err_remove);
    });
  }

  isEmptyOrSpaces(str: string): boolean {
    return str === null || str.match(/^ *$/) !== null;
  }

  messageEntered() {
    if (!this.selectedMessage.MessageFileType && !this.isEmptyOrSpaces(this.selectedMessage.MessageText)) {
      this.selectedMessage.MessageFileType = "text";
      this.selectedMessage.GroupID = this.chatGroup.ChatGroupID;
      this.selectedMessage.SenderID = this.userID;
    }
    this.chatting.SendMessageChatGroup(this.selectedMessage).then(resp => {
      this.selectedMessage = new MessageDataStruct();
    }).catch(err => {
      console.log(err);
    });
  }

  onCancelClick(): void {
    this.modalRef.hide();
    if (this.isUpdate)
      this.chatting.GetChatGroupDetails(this.chatGroup.ChatGroupID).then(resp => {
        this.chatGroups.splice(this.chatGroups.indexOf(this.chatGroup), 1);
        this.chatGroups.push(resp);
        this.chatGroup = resp;
        this.chatGroup = new ChatGroupDataStruct();
      }).catch(err => {
        console.log(err);
        this.chatGroup = new ChatGroupDataStruct();
      });
  }


  deleteMessage(m: MessageDataStruct): void {
    this.chatting.DeleteMessage(m.MessageID).then(resp => {

    }).catch(err => {
      console.log(err);
    })
  }

  onFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      var url = dataURL.split(",")[1];
      var typeStr = dataURL.split(",")[0];
      this.selectedMessage.MessageText = "";
      this.selectedMessage.MessageFileType = typeStr;
      this.selectedMessage.GroupID = this.chatGroup.ChatGroupID;
      this.selectedMessage.SenderID = this.userID;
      this.selectedMessage.MessageFile = url;
      this.selectedMessage.MessageFileName = input.files[0].name;
    });

    reader.readAsDataURL(input.files[0]);
  }

  saveFileToLocalMachine(m: MessageDataStruct): void {
    var b64toBlob = require("b64-to-blob");
    var blob = b64toBlob(m.MessageFile, m.MessageFileType);
    var file = new File([blob], m.MessageFileName, {type: "\"" + m.MessageFileType + "\""});
    FileSaver.saveAs(file);
  }

  downloadFile(m: MessageDataStruct): void {
    if (m.MessageFileType === 'text')
      return;
    if (m.MessageFile) {
      this.saveFileToLocalMachine(m);
    } else {
      this.chatting.GetMessagesFile(m).then(resp => {
        m.MessageFile = resp.MessageFile;
        this.saveFileToLocalMachine(m);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  ngAfterViewInit(): void {
    $(window).resize(() => {
      if (this.chatGroup.ChatGroupID) {
        this.setChatWindow();
      }
    });
  }


  setChatWindow(): void {
    var top = $('#chat-window-container').offset().top;
    var window_height = $(window).height();
    if (!this.isChatWindowCompressed && $(window).width() < 768) {
      this.mobileChatView();
    } else if (this.isChatWindowCompressed || $(window).width() >= 768) {
      var chat_window_height = (window_height - top) < 500 ? 500 : (window_height - top);
      $('#chat-window-container').css('top', 0);
      if (chat_window_height > 1) {
        $('#chat-window-container').height(chat_window_height - 5);
      }
    }
  }

  mobileChatView(): void {
    if (!this.isChatWindowCompressed) {
      var nav_len = $('nav').height();
      $('#chat-window-container').css('top', nav_len);
      $('body').removeClass('do-scroll').addClass('no-scroll');

      var window_height = $(window).height();
      var chat_window_height = (window_height - nav_len);
      if (chat_window_height > 1) {
        $('#chat-window-container').height(chat_window_height);
      }
    } else {
      $('body').removeClass('no-scroll').addClass('do-scroll');
      this.setChatWindow();
    }
  }

  getChatWindowHeight(): number {
    var msg_window_len = $('.message-window').height();
    return msg_window_len + 10;
  }

  removeFile(): void {
    this.selectedMessage = new MessageDataStruct();
  }

  ngOnDestroy(): void {
    $('body').removeClass('no-scroll').addClass('do-scroll');
    this.chatGroup = new ChatGroupDataStruct();
    this.isChatWindowCompressed = true;
  }

}
