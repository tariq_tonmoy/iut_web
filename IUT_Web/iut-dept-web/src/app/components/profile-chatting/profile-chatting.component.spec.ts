import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileChattingComponent } from './profile-chatting.component';

describe('ProfileChattingComponent', () => {
  let component: ProfileChattingComponent;
  let fixture: ComponentFixture<ProfileChattingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileChattingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileChattingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
