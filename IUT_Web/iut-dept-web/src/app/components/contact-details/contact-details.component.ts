import {Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {ContactDataStruct} from "../../view-model/contact-data-struct";
import {CommonVariables} from "../../view-model/common-variables";
import {CommonConversionService} from "../../service/common-conversion.service";

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit, OnChanges {
  @Input() userID: string;
  @Input() isMyProfile: boolean;

  private modalRef: BsModalRef;
  private contactEditClick: boolean;

  public contactCollection: ContactDataStruct[] = [];
  public contact: ContactDataStruct = new ContactDataStruct(0, "", "", "");
  public contactEvent: boolean = false;
  public contactSuccess: boolean = false;

  public alertMsg: string;
  public contactTypes: string[];
  public primaryEmail: string;

  constructor(private personalDetailsService: PersonalDetailsService,
              private modalService: BsModalService,
              private common: CommonConversionService) {
  }

  ngOnInit() {
    this.contactTypes = CommonVariables.ContactTypes;
  }

  loadContact(): void {
    this.personalDetailsService.GetPersonalContacts(this.userID).then((resp) => {
      this.contactCollection = [];
      this.contactCollection = resp;
    }).catch((err) => {
    });
    this.personalDetailsService.GetUserEmail(this.userID).then((resp) => {
      this.primaryEmail = this.common.replaceQuotes(resp);
      ;
    }).catch((err) => {
    });


  }


  onSaveClick(): void {
    this.contact.UserID = this.userID;
    if (!this.contactEditClick) {
      this.personalDetailsService.PostPersonalContact(this.contact).then((resp) => {
        this.alertMsg = "Details saved in Database";
        this.contactEvent = true;
        this.contactSuccess = true;

        this.modalRef.hide();
        this.loadContact();
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";

        this.contactEvent = true;
        this.contactSuccess = false;
        this.modalRef.hide();
      });
    } else {
      this.personalDetailsService.PutPersonalContact(this.contact).then((resp) => {
        this.alertMsg = "Details updated in Database";
        this.contactEvent = true;
        this.contactSuccess = true;
        this.modalRef.hide();
        var v = this.contactCollection.find(x => x.ContactID === this.contact.ContactID);
        if (v) {
          v = this.contact;
        }

      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.contactEvent = true;
        this.contactSuccess = false;
        this.modalRef.hide();
        ;
      });
    }
  }

  updateContact(i: ContactDataStruct, template: TemplateRef<any>): void {
    this.contactEditClick = true;
    var v = this.contactCollection.find(x => x.ContactID === i.ContactID);
    if (v) {
      this.contact = v;
      this.modalRef = this.modalService.show(template);
    }
    else {
      this.contactEvent = true;
      this.contactSuccess = false;
    }
  }

  deleteContact(i: ContactDataStruct): void {
    var v = this.contactCollection.find(x => x.ContactID === i.ContactID);

    if (v) {
      this.personalDetailsService.DeletePersonalContact(v).then((resp) => {

        var idx = this.contactCollection.indexOf(v);
        if (idx > -1) {
          this.contactCollection.splice(idx, 1);
          this.alertMsg = "Details deleted from Database";
          this.contactEvent = true;
          this.contactSuccess = true;

        }
        else {
          this.alertMsg = "Failed to access Database";
          this.contactEvent = true;
          this.contactSuccess = false;
        }
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.contactEvent = true;
        this.contactSuccess = false;
      });
    }


  }

  onContactAlertClosed(): void {
    this.contactEvent = false;
    this.contactSuccess = false;
  }

  onContactFormRequest(template: TemplateRef<any>) {
    this.contactEditClick = false;
    this.contact = new ContactDataStruct(0, "", "", "");
    this.modalRef = this.modalService.show(template);
  }

  onCancelClick(): void {
    this.modalRef.hide();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["userID"])
      this.loadContact();
  }

  contactClicked(data: ContactDataStruct): void {
    if (!data) data = new ContactDataStruct(0, "Email", this.primaryEmail, this.userID);
    if (data.ContactType === 'Email') {
      window.location.href = ('mailto:' + data.ContactDetails);
    }
    else if (data.ContactType === 'Phone' || data.ContactType === 'Mobile') {
      window.location.href = ('skype:' + data.ContactDetails + '?call');
    }
    else if (data.ContactType === 'Skype') {
      window.location.href = ('skype:' + data.ContactDetails + '?chat');
    }
    else if (data.ContactType === 'Twitter') {
      if (data.ContactDetails.toLowerCase().includes('twitter')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://twitter.com/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'ResearchGate') {
      if (data.ContactDetails.toLowerCase().includes('researchgate')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://www.researchgate.net/profile/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'Facebook') {
      if (data.ContactDetails.toLowerCase().includes('facebook')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://www.facebook.com/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'Instagram') {
      if (data.ContactDetails.toLowerCase().includes('instagram')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://www.instagram.com/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'LinkedIn') {
      if (data.ContactDetails.toLowerCase().includes('linkedin')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://www.linkedin.com/in/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'GitHub') {
      if (data.ContactDetails.toLowerCase().includes('github')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://github.com/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'Bitbucket') {
      if (data.ContactDetails.toLowerCase().includes('bitbucket')) {
        var splits = data.ContactDetails.split('/');
        data.ContactDetails = splits[splits.length - 1];
      }
      window.open('https://bitbucket.org/' + data.ContactDetails, '_blank');
    }
    else if (data.ContactType === 'Website' || data.ContactType === 'Google Scholar') {
      if (data.ContactDetails.toLowerCase().includes('http://') || data.ContactDetails.toLowerCase().includes('https://')) {
        window.open(data.ContactDetails, '_blank');
      }
      else window.open('http://' + data.ContactDetails, '_blank');
    }
  }


}
