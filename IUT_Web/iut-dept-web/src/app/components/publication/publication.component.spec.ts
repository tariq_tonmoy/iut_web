import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultyResearchComponent } from './faculty-research.component';

describe('FacultyResearchComponent', () => {
  let component: FacultyResearchComponent;
  let fixture: ComponentFixture<FacultyResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultyResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
