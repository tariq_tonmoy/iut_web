import {Component, Input, OnInit, TemplateRef} from "@angular/core";
import {CommonVariables, PersonalSummary} from "../../view-model/common-variables";
import {PublicationDataStruct} from "../../view-model/publication-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {PublicationService} from "../../service/publication.service";
import * as FileSaver from "file-saver";
import {PersonnelService} from "../../service/personnel.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {ResearchGroupService} from "../../service/research-group.service";
import {Router} from "@angular/router";
import {CommonConversionService} from "../../service/common-conversion.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AlumniSummaryDataStruct} from "../../view-model/alumni-summary-data-struct";

declare var require: any;
declare var $: any;
declare var jquery: any;

@Component({
  selector: "app-publication",
  templateUrl: "./publication.component.html",
  styleUrls: ["./publication.component.css"]
})
export class PublicationComponent implements OnInit {

  @Input() isMyProfile: boolean = false;
  @Input() userID: string;
  @Input() viewType: string;
  @Input() researchGroupID: number;
  @Input() departmentID: string;
  @Input()
  public publicationList: PublicationDataStruct[] = [];

  public msgEvent: boolean = false;
  public msgSuccess: boolean = false;
  public alertMsg: string = "";
  private modalRef: BsModalRef;
  private isUpdate: boolean = false;
  public pubTypes: string[] = [];
  public oldAuthorList: string[] = [];
  public selectedUserList: PersonalSummary[] = [];
  public hideSpinner: boolean = false;
  public pubYears: PublicationDataStruct[] = [];
  public fullPubList: PublicationDataStruct[] = [];
  public selectedYear: number = -1;
  public types: string[] = CommonVariables.publicationType;
  public selectedPublication: PublicationDataStruct = new PublicationDataStruct(
    0,
    "",
    "",
    "",
    "",
    "");
  public searchUser: number = 0;
  public barChartLabels: number[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [];
  public chartLoaded = false;

  public chartColors: any[] = [
    {
      backgroundColor: '#f16975'
    },
    {
      backgroundColor: '#8D6E63'
    },
    {
      backgroundColor: '#0288D1'
    },
    {
      backgroundColor: '#009688'
    },
    {
      backgroundColor: '#6A1B9A'
    }
  ];

  constructor(private modalService: BsModalService,
              private pubService: PublicationService,
              private personnelService: PersonnelService,
              private profileService: PersonalDetailsService,
              private researchGroupService: ResearchGroupService,
              private router: Router,
              private commonService: CommonConversionService,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.loadPublications();
    if (this.viewType === "department") {
      $("#faculty-publications").addClass("animated fadeInUp");
      this.spinner.show();
      this.hideSpinner = false;

    }
  }

  onAlumniAdded(data: AlumniSummaryDataStruct): void {
    if (this.selectedUserList.find(x => x.id === data.UserID))
      return;
    this.selectedUserList.push({
      id: data.UserID,
      name: data.Name,
      imageType: data.ImageType,
      image: data.Image,
      remark: data.StudentID
    });
  }

  onFacultyAdded(data: FacultySummaryDataStruct): void {
    if (!data)
      return;
    if (this.selectedUserList.find(x => x.id === data.FacultyID))
      return;
    this.selectedUserList.push({
      id: data.FacultyID,
      name: data.Name,
      imageType: data.ImageType,
      image: data.Image,
      remark: data.Designation
    });

  }

  addUserToSelectedUserList(userID: string): void {
    if (this.selectedUserList.find(x => x.id === userID))
      return;
    this.profileService.GetProfileDetails(userID).then(resp_details => {
      this.selectedUserList.push({
        id: resp_details.UserID,
        name: resp_details.Salutation + resp_details.FirstName + " " + resp_details.LastName,
        imageType: resp_details.ImageType,
        image: resp_details.Image
      });
    }).catch(err => {
      console.log(err);
    });
  }

  removeAuthor(userID: string): void {
    this.selectedUserList.splice(this.selectedUserList.indexOf(this.selectedUserList.find(x => x.id === userID)), 1);
  }

  formatPublicationList(resp: PublicationDataStruct[]): void {
    this.publicationList = resp;
    this.pubTypes = [];
    for (let data of resp) {
      if (!this.pubTypes.some(x => x === data.Type))
        this.pubTypes.push(data.Type);
    }
  }

  loadPublications(): void {
    this.publicationList = [];
    this.selectedPublication = new PublicationDataStruct(0,
      "",
      "",
      "",
      "",
      "");
    if (this.viewType === "person") {
      this.pubService.GetPublicationsByUserID(this.userID).then((resp) => {
        this.formatPublicationList(resp);
      }).catch((err) => {

      });
    }
    else if (this.viewType === "department") {
      this.pubYears = [];
      this.pubService.GetPublicationsByDept(this.departmentID).then(resp => {
        this.formatPublicationList(resp);
        this.fullPubList = resp;
        this.hideSpinner = true;
        this.spinner.hide();
      }).catch(err => {
        console.log(err);
      });
      this.pubService.GetPublicationYearsByDepartment(this.departmentID).then(resp => {
        this.pubYears = resp;
      }).catch(err => {
        console.log(err);
      });
    }
    else if (this.viewType === "researchGroup") {
      this.researchGroupService.getPublicationsByResearchGroup(this.researchGroupID).then(resp => {
        this.formatPublicationList(resp);
        this.hideSpinner = true;
      }).catch(err => {

      });
    }
  }

  formatAuthors(authorList: string): string {
    var k = authorList.length - 1;
    for (; k >= 0; k--) {
      if (authorList[k] !== " " && authorList[k] !== ";" && authorList[k] !== ".")
        break;
    }
    authorList = authorList.slice(0, k + 1);

    var auth = authorList.split(";");
    var newAuth = "";
    if (auth.length > 1) {
      for (var i = 0; i < auth.length; i++) {
        if (i < auth.length - 2) {
          newAuth += auth[i] + ", ";
        }
        else if (i === auth.length - 2) {
          newAuth += auth[i] + " and ";
        }
        else newAuth += auth[i];
      }
    }
    else if (auth.length > 0) {
      newAuth += auth[0];
    }
    else return null;
    return newAuth;
  }

  onCancelClick(): void {
    this.modalRef.hide();
  }

  onSaveClick(): void {
    if (!this.isUpdate) {
      this.selectedPublication.CreatorID = this.userID;
      this.pubService.PostPublication(this.selectedPublication, this.selectedUserList).then((resp) => {
        this.msgEvent = true;
        this.msgSuccess = true;
        this.alertMsg = "Publication added successfully";
        this.loadPublications();
        this.publicationList.push(this.selectedPublication);
      }).catch((err) => {
        this.msgEvent = true;
        this.msgSuccess = false;
        this.alertMsg = "Failed to save publication";
      });
    } else {
      this.pubService.PutPublicationDetails(this.selectedPublication, this.selectedUserList, this.oldAuthorList).then((resp) => {
        this.msgEvent = true;
        this.msgSuccess = true;
        this.alertMsg = "Publication updated successfully";
        this.publicationList.splice(this.publicationList.indexOf(this.publicationList.find(x => x.PublicationID === this.selectedPublication.PublicationID)), 1);
        this.publicationList.push(this.selectedPublication);
        this.loadPublications();
      }).catch((err) => {
        this.msgEvent = true;
        this.msgSuccess = false;
        this.alertMsg = "Failed to update publication";
      });
    }
    this.modalRef.hide();

  }

  onMsgAlertClosed(): void {
    this.msgEvent = false;
    this.msgSuccess = false;
    this.alertMsg = "";
  }

  onFormRequest(data: PublicationDataStruct, template: TemplateRef<any>): void {
    this.selectedUserList = [];
    if (data) {
      this.oldAuthorList = [];
      this.selectedPublication = data;
      this.isUpdate = true;
      this.pubService.GetUserSummaryByPublication(data.PublicationID).then(resp => {
        this.selectedUserList = resp;
        for (let auth of resp) {
          this.oldAuthorList.push(auth.id);
        }
      }).catch(err => {
        console.log(err);
      });
    }
    else {
      this.isUpdate = false;
      this.addUserToSelectedUserList(this.userID);
      this.selectedPublication = new PublicationDataStruct(
        0,
        "",
        "",
        "",
        "",
        "");
    }
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));
  }

  onFileChange($event): void {
    var input = $event.target;

    var reader = new FileReader();
    reader.addEventListener("load", (res) => {
      var dataURL = reader.result;
      this.selectedPublication.PublicationMaterial = dataURL.split(",")[1];
      this.selectedPublication.MaterialType = dataURL.split(",")[0];
      this.selectedPublication.MaterialName = input.files[0].name;
    });
    reader.readAsDataURL(input.files[0]);
  }


  deleteDetails(data: PublicationDataStruct) {
    this.selectedPublication = data;
    this.pubService.DeletePublication(data).then((resp) => {
      this.msgEvent = true;
      this.msgSuccess = true;
      this.alertMsg = "Publication deleted successfully";
      this.publicationList.splice(this.publicationList.indexOf(this.publicationList.find(x => x.PublicationID === this.selectedPublication.PublicationID)), 1);
    }).catch((err) => {
      console.log(err);
      this.msgEvent = true;
      this.msgSuccess = false;
      this.alertMsg = "Failed to delete publication";
    });
  }

  downloadMaterial(data: PublicationDataStruct): void {
    this.pubService.GetPublicationMaterial(data).then(resp => {
      var b64toBlob = require("b64-to-blob");
      var blob = b64toBlob(resp.PublicationMaterial, resp.MaterialType);
      var file = new File([blob], resp.MaterialName, {type: "\"" + resp.MaterialType + "\""});
      FileSaver.saveAs(file);
    }).catch(err => {
      console.log(err);
    });
  }

  showPubDetails(data: PublicationDataStruct): void {
    this.selectedPublication = data;
    if (data) {
      this.selectedUserList = [];
      this.pubService.GetUserSummaryByPublication(data.PublicationID).then(resp => {
        this.selectedUserList = resp;
        let el = document.getElementById(data.PublicationID.toString());
        if (el)
          el.scrollIntoView(false);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  profileClicked(facultyID: string): void {
    this.profileService.getUserNameFromUserID(facultyID).then(resp => {
      this.router.navigate(["cse/profile/" + this.commonService.replaceQuotes(resp)]).then(resp_u => {

      }).catch(err_u => {
        console.log(err_u);
      });
    }).catch(err => {
      console.log(err);
    });
  }

  public onYearClicked(year: number): void {
    this.selectedYear = year;
    if (this.selectedYear === -2)
      this.populateChart();
    else if (this.selectedYear > -1)
      this.formatPublicationList(this.fullPubList.filter(x => x.Year === year));
    else this.formatPublicationList(this.fullPubList);
  }

  showUserGroup(id: number): void {
    this.searchUser = id;
  }


  private populateChart() {
    if (this.chartLoaded) return;
    this.pubService.GetPublicationStatisticsByDept(this.departmentID).then(resp => {
      for (let stat of resp) {
        if (!this.barChartLabels.find(x => x === stat.year))
          this.barChartLabels.push(stat.year);
        if (!this.barChartData.find(x => x.label === stat.type)) {
          this.barChartData.push({
            data: [],
            label: stat.type
          });
        }
      }

      for (let label of this.barChartData) {
        for (let year of this.barChartLabels) {
          var r = resp.find(x => x.type === label.label && x.year === year);
          if (r) {
            this.barChartData.find(x => x.label === label.label).data.push(r.count);
          }
          else this.barChartData.find(x => x.label === label.label).data.push(0);
        }
      }
      this.chartLoaded = true;
    }).catch(err => {
      console.log(err);
    });
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  removeSelectedPublicationFile(): void {
    this.selectedPublication.PublicationMaterial = null;
    this.selectedPublication.MaterialName = "";
    this.selectedPublication.MaterialType = "";
  }


}
