import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchGroupDetailsComponent } from './research-group-details.component';

describe('ResearchGroupDetailsComponent', () => {
  let component: ResearchGroupDetailsComponent;
  let fixture: ComponentFixture<ResearchGroupDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchGroupDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchGroupDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
