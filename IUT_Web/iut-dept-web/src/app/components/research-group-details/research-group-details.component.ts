import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ResearchGroupDataStruct} from "../../view-model/research-group-data-struct";
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {PublicationDataStruct} from "../../view-model/publication-data-struct";

@Component({
  selector: 'app-research-group-details',
  templateUrl: './research-group-details.component.html',
  styleUrls: ['./research-group-details.component.css']
})
export class ResearchGroupDetailsComponent implements OnInit {

  constructor() {
  }

  @Output() onDetailsClicked = new EventEmitter<ResearchGroupDataStruct>();
  @Output() onBackClicked = new EventEmitter();
  @Output() onProfileClicked = new EventEmitter<string>();
  @Input() isCompressed: boolean = true;
  @Input() facultySummaryList: FacultySummaryDataStruct[]=[];
  @Input() publicationList: PublicationDataStruct[] = [];

  @Input() researchGroup: ResearchGroupDataStruct = new ResearchGroupDataStruct(0, '');

  ngOnInit() {
  }

  detailsClicked(): void {
    this.onDetailsClicked.emit(this.researchGroup);
  }

  backClicked(): void {
    this.onBackClicked.emit();
  }

  getResearchGroupLeader(): FacultySummaryDataStruct {
    return this.facultySummaryList.find(x => x.FacultyID === this.researchGroup.CreatorID);
  }

  getGroupMembersWhoAreNotLeader(): FacultySummaryDataStruct[] {
    return this.facultySummaryList.filter(x => x.FacultyID !== this.researchGroup.CreatorID);
  }

  profileClicked(facultyID: string): void {
    this.onProfileClicked.emit(facultyID);
  }
}
