import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PersonnelService} from "../../service/personnel.service";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import 'rxjs/add/observable/of';
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {TypeaheadMatch} from "ngx-bootstrap";

@Component({
  selector: 'app-search-faculty',
  templateUrl: './search-faculty.component.html',
  styleUrls: ['./search-faculty.component.css']
})


export class SearchFacultyComponent implements OnInit, AfterViewInit {


  @Input() departmentList: DepartmentDataStruct[] = [];

  @Output() onFormRequestSubmitted: EventEmitter<FacultySummaryDataStruct> = new EventEmitter();

  public selectedDepartment: DepartmentDataStruct = new DepartmentDataStruct('', '');
  public facultyList: FacultyDataStruct[] = [];
  public typeaheadSingleWords = true;
  public facultySummary: FacultySummaryDataStruct[] = [];
  public assignedFaculty: FacultySummaryDataStruct;

  constructor(private facultyService: PersonnelService,
              private personalService: PersonalDetailsService,
              private academicsService: AcademicsService) {
  }

  ngOnInit() {
  }

  public selectedName: string;
  public isSaveActivated: boolean = false;

  facultyOnSelect(e: TypeaheadMatch): void {
    if (e.item) {
      this.assignedFaculty = e.item;
      this.isSaveActivated = true;
    }
  }



  onCancelAssignFacultyClick(): void {
    this.isSaveActivated = false;
    this.selectedName = "";
    this.onFormRequestSubmitted.emit(null);
  }

  onSaveAssignFacultyClick(): void {
    this.isSaveActivated = false;
    this.selectedName = "";
    if (this.assignedFaculty)
      this.onFormRequestSubmitted.emit(this.assignedFaculty);
    else this.onFormRequestSubmitted.emit(null);

  }

  onDeptChange(): void {
    this.facultyService.GetFacultyByDept(this.selectedDepartment.DepartmentID).then((resp) => {
      this.facultyList = resp;
      this.facultySummary = [];
      for (let p of this.facultyList) {
        this.personalService.GetProfileDetails(p.FacultyID).then((resp_p) => {
          this.facultySummary.push(new FacultySummaryDataStruct(p.FacultyID, p.Designation, resp_p.Salutation + resp_p.FirstName + " " + resp_p.LastName, resp_p.ImageType, resp_p.Image));
        }).catch((err_p) => {
          console.log(err_p);
        });
      }

    }).catch((err) => {
      console.log(err);
    });
  }

  ngAfterViewInit(): void {
    if (this.departmentList.length < 1) {
      this.academicsService.GetDepartmentList().then((resp) => {
        this.departmentList = resp;
      }).catch((err) => {
        console.log(err);
      });
    }
  }

}
