import {Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from '@angular/core';
import {PersonalDetailsService} from "../../service/personal-details.service";
import {LocationDetailsComponent} from "../location-details/location-details.component";
import {LocationDataStruct} from "../../view-model/location-data-struct";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit, OnChanges {


  @Input() userID: string;
  @Input() isMyProfile: boolean;

  private locationComponent: LocationDetailsComponent;
  public locationEvent: boolean;
  public locationSuccess: boolean;
  public alertMsg: string;
  public addLocation: boolean;
  private modalRef: BsModalRef;


  location: LocationDataStruct = new LocationDataStruct(0, '', '', '');

  constructor(private personalDetailsService: PersonalDetailsService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.locationEvent = false;
    this.locationSuccess = false;
  }

  onLocationAlertClosed(): void {
    this.locationEvent = false;
    this.locationSuccess = false;
  }

  loadLocation(): void {
    this.personalDetailsService.GetPersonalLocation(this.userID).then((resp) => {
      if (!resp) {
        this.location = null;
        this.addLocation = true;
      }
      else {
        this.location = resp;
        this.addLocation = false;
      }
    }).catch((ex) => {
      this.addLocation = true;
    });


  }

  onLocationComponentLoaded(locationComp: LocationDetailsComponent): void {
    this.locationComponent = locationComp;
    this.locationComponent.SetLocation(this.location);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["userID"])
      window.scroll(0, 0);
      this.loadLocation();
  }

  onLocationFormRequest(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  deleteLocation(): void {
    this.personalDetailsService.DeletePersonalLocation(this.location).then((resp) => {
      this.locationEvent = true;
      this.locationSuccess = true;
      this.addLocation = true;
      this.alertMsg = "Location Deleted Successfully";
      this.location = new LocationDataStruct(0, '', '', '');
    }).catch((err) => {
      this.locationEvent = true;
      this.locationSuccess = false;
      this.addLocation = false;
      this.alertMsg = "Failed to delete location";
    });

  }

  onLocationCancelClick(): void {
    this.modalRef.hide();
  }

  onLocationSaveClick(): void {
    this.location = this.locationComponent.onSaveClick();
    if (this.addLocation) {
      this.personalDetailsService.PostPersonalLocation(this.location).then((resp) => {
        this.locationEvent = true;
        this.locationSuccess = true;
        this.addLocation = false;
        this.alertMsg = "Location Saved Successfully";
        this.loadLocation();
      }).catch((err) => {
        this.locationEvent = true;
        this.locationSuccess = false;
        this.addLocation = true;
        this.alertMsg = "Failed to save location";
      });
    } else {
      this.personalDetailsService.PutPersonalLocation(this.location).then((resp) => {
        this.locationEvent = true;
        this.locationSuccess = true;
        this.addLocation = false;
        this.alertMsg = "Location Updated Successfully";
      }).catch((err) => {
        this.locationEvent = true;
        this.locationSuccess = false;
        this.addLocation = false;
        this.alertMsg = "Location Updated Successfully";
      });
    }
    this.modalRef.hide();
  }
}
