import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../service/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {SignalRService} from "../../service/signal-r.service";

@Component({
  selector: 'app-external-auth',
  templateUrl: './external-auth.component.html',
  styleUrls: ['./external-auth.component.css']
})
export class ExternalAuthComponent implements OnInit {
  token: string;

  constructor(private authService: AuthenticationService,
              private route: ActivatedRoute,
              private personalService: PersonalDetailsService,
              private router: Router,
              private signalRService: SignalRService) {

  }


  ngOnInit() {
    var fragmentArray: string[] = this.route.snapshot.fragment.split('&');
    if (fragmentArray.length > 0)
      this.token = fragmentArray[0].split('=')[1];
    else return;
    this.authService.setAcessToken(this.token);
    this.signalRService.startConnection(true);
    var id: string = this.route.snapshot.queryParams['id'];
    var dept: string = this.route.snapshot.queryParams['dept'];
    if (id) {
      this.authService.registrationExternal({UserID: id, Username: ''}).then(resp => {
        this.authService.logoutService();
        this.personalService.getUserNameFromUserID(id).then(resp_un => {
          this.router.navigate(['/' + dept + '/profile/' + resp_un]);
        });
      }).catch(err => {
        this.personalService.getUserNameFromUserID(id).then(resp_un => {
          this.router.navigate(['/' + dept + '/profile/' + resp_un]);
        });
      });
      return;
    }
    this.authService.getExternalUserInfo().then(resp => {
      this.router.navigate(["/" + dept], {queryParams: {userInfo: JSON.stringify(resp)}}).then(res => {
      }).catch(err_route => {
        console.log(err_route);
      });
    }).catch(err => {
      console.log(err);
    });
  }

}
