import {Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from "@angular/core";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {AcademicDetailsDataStruct} from "../../view-model/academic-details-data-struct";
import {LocationDetailsComponent} from "../location-details/location-details.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {TimeServiceService} from "../../service/time-service.service";

@Component({
  selector: "app-academic-details",
  templateUrl: "./academic-details.component.html",
  styleUrls: ["./academic-details.component.css"]
})
export class AcademicDetailsComponent implements OnInit, OnChanges {
  @Input() userID: string;
  @Input() isMyProfile: boolean;


  private modalRef: BsModalRef;
  private academicEditClick: boolean;

  public academicDetailsCollection: AcademicDetailsDataStruct[] = [];
  public academicDetails: AcademicDetailsDataStruct = new AcademicDetailsDataStruct(0, "", "", "", "", new Date());
  public academicEvent: boolean = false;
  public academicSuccess: boolean = false;

  private locationComponent: LocationDetailsComponent;
  public alertMsg: string;


  constructor(private personalDetailsService: PersonalDetailsService,
              private modalService: BsModalService,
              private timeService: TimeServiceService) {
  }

  ngOnInit() {
  }

  loadAcademicDetails(): void {
    this.personalDetailsService.GetAcademicDetails(this.userID).then((resp) => {
      this.academicDetailsCollection = [];
      this.academicDetailsCollection = resp;
      for (let v of this.academicDetailsCollection) {
        v.From = v.From === null ? null : this.timeService.FormatDate(v.From);
        v.To = v.To === null ? null : this.timeService.FormatDate(v.To);
      }
    }).catch((err) => {
    });


  }


  onSaveClick(): void {
    this.academicDetails.UserID = this.userID;
    this.academicDetails.Address = this.locationComponent.onSaveClick();
    if (!this.academicEditClick) {
      this.personalDetailsService.PostAcademicDetails(this.academicDetails).then((resp) => {
        this.alertMsg = "Details saved in Database";
        this.academicEvent = true;
        this.academicSuccess = true;

        this.modalRef.hide();
        this.loadAcademicDetails();
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";

        this.academicEvent = true;
        this.academicSuccess = false;
        this.modalRef.hide();
      });
    } else {
      this.personalDetailsService.PutAcademicDetails(this.academicDetails).then((resp) => {
        this.alertMsg = "Details updated in Database";
        this.academicEvent = true;
        this.academicSuccess = true;
        this.modalRef.hide();
        var v = this.academicDetailsCollection.find(x => x.AcademicDetailsID === this.academicDetails.AcademicDetailsID);
        if (v) {
          v = this.academicDetails;
        }

      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.academicEvent = true;
        this.academicSuccess = false;
        this.modalRef.hide();

      });
    }
  }

  updateAcademicDetails(i: AcademicDetailsDataStruct, template: TemplateRef<any>): void {
    this.academicEditClick = true;
    var v = this.academicDetailsCollection.find(x => x.AcademicDetailsID === i.AcademicDetailsID);
    if (v) {
      this.academicDetails = v;
      this.modalRef = this.modalService.show(template);
    }
    else {
      this.academicEvent = true;
      this.academicSuccess = false;
    }
  }

  deleteAcademicDetails(i: AcademicDetailsDataStruct): void {
    var v = this.academicDetailsCollection.find(x => x.AcademicDetailsID === i.AcademicDetailsID);

    if (v) {
      this.personalDetailsService.DeleteAcademicDetails(v).then((resp) => {

        var idx = this.academicDetailsCollection.indexOf(v);
        if (idx > -1) {
          this.academicDetailsCollection.splice(idx, 1);
          this.alertMsg = "Details deleted from Database";
          this.academicEvent = true;
          this.academicSuccess = true;

        }
        else {
          this.alertMsg = "Failed to access Database";
          this.academicEvent = true;
          this.academicSuccess = false;
        }
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.academicEvent = true;
        this.academicSuccess = false;
      });
    }


  }

  onAcademicAlertClosed(): void {
    this.academicEvent = false;
    this.academicSuccess = false;
  }

  onAcademicFormRequest(template: TemplateRef<any>) {
    this.academicEditClick = false;
    this.academicDetails = new AcademicDetailsDataStruct(0, "", "", "", "", new Date());
    this.modalRef = this.modalService.show(template);
  }

  onCancelClick(): void {
    this.loadAcademicDetails();
    this.modalRef.hide();
  }

  onLocationComponentLoaded(locationComp: LocationDetailsComponent): void {
    this.locationComponent = locationComp;
    this.locationComponent.SetLocation(this.academicDetails.Address);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["userID"])
      this.loadAcademicDetails();
  }


}
