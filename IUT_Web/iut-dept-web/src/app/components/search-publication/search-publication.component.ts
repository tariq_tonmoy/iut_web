import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AcademicsService} from "../../service/academics.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {TypeaheadMatch} from "ngx-bootstrap";
import {PublicationDataStruct} from "../../view-model/publication-data-struct";
import {PublicationService} from "../../service/publication.service";

@Component({
  selector: 'app-search-publication',
  templateUrl: './search-publication.component.html',
  styleUrls: ['./search-publication.component.css']
})
export class SearchPublicationComponent implements OnInit, AfterViewInit {

  @Input() departmentList: DepartmentDataStruct[] = [];

  @Output() onFormRequestSubmitted: EventEmitter<number> = new EventEmitter();

  public selectedDepartment: DepartmentDataStruct = new DepartmentDataStruct('', '');
  public publicationList: PublicationDataStruct[] = [];
  public typeaheadSingleWords = true;
  public assignedPublication: PublicationDataStruct;

  constructor(private publicationService: PublicationService,
              private academicsService: AcademicsService) {
  }

  ngOnInit() {
  }

  public selectedName: string;
  public isSaveActivated: boolean = false;

  publicationOnSelect(e: TypeaheadMatch): void {
    if (e.item) {
      this.assignedPublication = e.item;
      this.isSaveActivated = true;
    }
  }


  onCancelAssignPublicationClick(): void {
    this.isSaveActivated = false;
    this.selectedName = "";
    this.onFormRequestSubmitted.emit(null);
  }

  onSaveAssignPublicationClick(): void {
    this.isSaveActivated = false;
    this.selectedName = "";
    if (this.assignedPublication)
      this.onFormRequestSubmitted.emit(this.assignedPublication.PublicationID);
    else this.onFormRequestSubmitted.emit(null);

  }

  onDeptChange(): void {
    this.publicationService.GetPublicationsByDept(this.selectedDepartment.DepartmentID).then((resp) => {
      this.publicationList = resp;
    }).catch((err) => {

    });
  }

  ngAfterViewInit(): void {
    if (this.departmentList.length < 1) {
      this.academicsService.GetDepartmentList().then((resp) => {
        this.departmentList = resp;
      }).catch((err) => {

      });
    }
  }

}
