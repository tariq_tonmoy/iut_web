import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {RoleRequestDataStruct} from "../../view-model/role-request-data-struct";
import {HttpErrorResponse} from "@angular/common/http";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {TaService} from "../../service/ta.service";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {AcademicsService} from "../../service/academics.service";

@Component({
  selector: 'app-profile-ta',
  templateUrl: './profile-ta.component.html',
  styleUrls: ['./profile-ta.component.css']
})
export class ProfileTaComponent implements OnInit , AfterViewInit {
  @Input() userID: string;
  @Input() isRoleRequest: boolean;
  @Input() userRole: UserRoleDataStruct;
  @Output() onRequestCancelClick = new EventEmitter<void>();

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public deptList: DepartmentDataStruct[] = [];
  public request: RoleRequestDataStruct = new RoleRequestDataStruct(0, '', '');


  onAlertClosed(): void {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  ngAfterViewInit(): void {
    window.scroll(0, 0);
    if (this.userRole) {
      this.request.RequestedRoleID = this.userRole.RoleID;
      this.request.RequestUserID = this.userID;
    }
    this.academicsService.GetDepartmentList().then((resp) => {
      for (let res of resp) {
        this.deptList.push(res);
      }
    }).catch((err) => {
      console.log("Depr Error: " + err);
    });
  }


  constructor(private personalDetailsService: PersonalDetailsService,
              private taService: TaService,
              private academicsService: AcademicsService) {
  }


  ngOnInit() {

  }

  onCancelRequestClick() {
    this.onRequestCancelClick.emit();
  }


  requestForRole(): void {
    this.personalDetailsService.PostRoleRequest(this.request).then((resp) => {
      this.alertMsg = "Request Submitted Successfully";
      this.msgEvent = true;
      this.msgSuccess = true;
      this.isRoleRequest = false;
    }).catch((err) => {
      var e: HttpErrorResponse = err;
      this.alertMsg = (e.statusText) === "Conflict" ? "You have already submitted a request." : e.message;
      this.msgEvent = true;
      this.msgSuccess = false;
      if (e.statusText == 'Conflict')
        this.isRoleRequest = false;
    });
  }

}
