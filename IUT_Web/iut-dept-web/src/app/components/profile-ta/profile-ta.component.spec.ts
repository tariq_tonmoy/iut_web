import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileTaComponent } from './profile-ta.component';

describe('ProfileTaComponent', () => {
  let component: ProfileTaComponent;
  let fixture: ComponentFixture<ProfileTaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
