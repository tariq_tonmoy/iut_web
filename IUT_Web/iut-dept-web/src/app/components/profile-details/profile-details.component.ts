import {Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {PersonalDetailsDataStruct} from "../../view-model/personal-details-data-struct";
import {UserRoleDataStruct} from "../../view-model/user-role-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {CommonConversionService} from "../../service/common-conversion.service";
import {NgxSpinnerService} from "ngx-spinner";
import {SignalRService} from "../../service/signal-r.service";

declare var $: any;

@Component({
  selector: "app-profile-details",
  templateUrl: "./profile-details.component.html",
  styleUrls: ["./profile-details.component.css"]
})
export class ProfileDetailsComponent implements OnInit, OnChanges {
  @Input() urlUsername: string;

  @Input() isLoggedIn: boolean;
  public personalDetails: PersonalDetailsDataStruct = new PersonalDetailsDataStruct("", "", "", "", "", "");
  public isMyProfile: boolean = false;
  public roles: UserRoleDataStruct[] = [];
  public allRoles: UserRoleDataStruct[] = [];
  public hasProfile: boolean = false;
  public selectedUserRole: string;
  public selectedRoleDetails: UserRoleDataStruct;
  public showPublicationsInAlumni: boolean = false;
  public isConnected: boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["isLoggedIn"]) {
      this.isMyProfile = this.isLoggedIn;
    }
    if (changes["urlUsername"]) {
      this.selectedUserRole = "Personal";
      window.scroll(0, 0);
      var currentUser = JSON.parse(localStorage.getItem("currentUser"));
      var saved_username: string = currentUser && currentUser.username;
      this.personalDetails.Username = this.urlUsername;

      if (this.urlUsername === saved_username) {
        this.isMyProfile = true;
      }
      else {
        this.isMyProfile = false;
      }
      this.personalDetailsService.GetUserID(this.urlUsername).then(resp => {
        this.personalDetails.UserID = this.commonService.replaceQuotes(resp);
        this.personalDetailsService.GetUserRolesByUser(this.personalDetails.UserID).then(resp_r => {
          this.personalDetails.Roles = resp_r;
          this.personalDetailsService.GetUserRoles().then((resp_roles) => {
            this.roles = [];
            for (let r of resp_roles) {
              if (!this.isInRole(r))
                this.roles.push(r);
            }
            if (this.roles.find(x => x.RoleName === "Faculty")) {
              this.showPublicationsInAlumni = true;
            }
          });

        }).catch(err_r => {
          this.hasProfile = false;
        });
        this.personalDetailsService.GetUserEmail(this.personalDetails.UserID).then(resp_e => {
          this.personalDetails.Email = resp_e;
        }).catch(err_e => {
          this.hasProfile = false;
        });
        this.personalDetailsService.GetProfileDetails(this.personalDetails.UserID).then(resp_p => {
          this.hasProfile = true;
          this.personalDetails.Salutation = resp_p.Salutation;
          this.personalDetails.FirstName = resp_p.FirstName;
          this.personalDetails.LastName = resp_p.LastName;
          this.personalDetails.Image = resp_p.Image;
          this.personalDetails.ImageType = resp_p.ImageType;
          this.personalDetails.Nationality = resp_p.Nationality;
          this.personalDetails.BloodGroup = resp_p.BloodGroup;
          this.personalDetails.DoB = resp_p.DoB;
          this.spinner.hide();
        }).catch(err_p => {
          this.hasProfile = false;
        });
      }).catch(err => {
        this.hasProfile = false;
        this.spinner.hide();

      });

    }
  }


  constructor(private personalDetailsService: PersonalDetailsService,
              private commonService: CommonConversionService,
              private spinner: NgxSpinnerService,
              private signalRService: SignalRService) {
    this.signalRService.currentIsConnected.subscribe(val => this.isConnected = val);
  }

  ngOnInit() {
    this.spinner.show();
    this.selectedUserRole = "Personal";
    this.personalDetailsService.GetUserRoles().then(resp => {
      this.allRoles = resp;
    });
  }

  onCancelClickFromRole() {
    this.selectedUserRole = "Personal";
    this.selectedRoleDetails = null;
  }


  public isRoleRequest: boolean;

  showRolePanel(roleName: string, isRoleRequest: boolean): void {
    this.selectedUserRole = roleName;
    if (roleName !== "Personal") {
      this.isRoleRequest = isRoleRequest;
      if (!isRoleRequest)
        this.selectedRoleDetails = this.personalDetails.Roles.find(x => x.RoleName === roleName);
      else this.selectedRoleDetails = this.roles.find(x => x.RoleName === roleName);
    }
  }

  isInRole(i: UserRoleDataStruct): boolean {
    return this.personalDetails.Roles.some(x => x.RoleName === i.RoleName);
  }

}
