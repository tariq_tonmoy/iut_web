import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {LocationDataStruct} from "../../view-model/location-data-struct";
import {CommonVariables, Country} from "../../view-model/common-variables";

@Component({
  selector: "app-location-details",
  templateUrl: "./location-details.component.html",
  styleUrls: ["./location-details.component.css"]
})
export class LocationDetailsComponent implements OnInit {
  countries: Country[];

  @Output() onLocationLoaded = new EventEmitter<LocationDetailsComponent>();

  constructor() {
  }

  public location: LocationDataStruct;

  ngOnInit() {
    this.countries = CommonVariables.countries;
    this.onLocationLoaded.emit(this);

  }

  public SetLocation(location: LocationDataStruct) {
    if (!location)
      this.location = new LocationDataStruct(0, "", "", "");
    else this.location = location;
  }

  onSaveClick(): LocationDataStruct {
    return this.location;
  }

}
