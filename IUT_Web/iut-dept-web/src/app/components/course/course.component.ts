import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, TemplateRef} from "@angular/core";
import {TimeServiceService} from "../../service/time-service.service";
import {AcademicsService} from "../../service/academics.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {DepartmentDataStruct} from "../../view-model/department-data-struct";
import {ProgramDataStruct} from "../../view-model/program-data-struct";
import {SyllabusReviewDataStruct} from "../../view-model/syllabus-review-data-struct";
import {CourseDataStruct} from "../../view-model/course-data-struct";
import {SyllabusDataStruct} from "../../view-model/syllabus-data-struct";
import {CommonVariables} from "../../view-model/common-variables";
import {FacultyDataStruct} from "../../view-model/faculty-data-struct";
import {FacultySummaryDataStruct} from "../../view-model/faculty-summary-data-struct";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {CommonConversionService} from "../../service/common-conversion.service";
import {Router} from "@angular/router";

declare var json: any;
declare var $: any;


@Component({
  selector: "app-course",
  templateUrl: "./course.component.html",
  styleUrls: ["./course.component.css"]
})
export class CourseComponent implements OnInit, OnChanges {
  private _passedDepartment: DepartmentDataStruct = new DepartmentDataStruct("", "");
  public progSelection: number = 0;
  public reviewSelection: number = 0;
  public semesterSelection: string;
  @Input() deptList: DepartmentDataStruct[];
  @Input() isDeptFixed: boolean;
  @Input() passedProgName: string;
  @Input() passedSemester: string;

  @Input() set passedDept(value: DepartmentDataStruct) {
    this._passedDepartment = value;
    this.selectedDept = value;
    if (this.isDeptFixed && this.progList.length === 0) {
      if (this.selectedDept) {
        this.fetchProgramList(this.selectedDept.DepartmentID);

      }
    }
  }

  public selectedDept: DepartmentDataStruct = new DepartmentDataStruct("", "");

  get passedDept(): DepartmentDataStruct {
    return this._passedDepartment;
  }

  @Output() onDeptLoaded = new EventEmitter<DepartmentDataStruct[]>();

  constructor(private academicsService: AcademicsService,
              private profileService: PersonalDetailsService,
              private timeService: TimeServiceService,
              private commonService: CommonConversionService,
              private modalService: BsModalService,
              private router: Router) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["passedProgName"] && this.passedProgName && this.progList.length > 0) {
      this.selectedProgram = this.progList.find(x => x.ProgramName === this.passedProgName);
      this.progSelection = this.selectedProgram.ProgramID;
      this.fetchReviewList(this.progSelection);
    }
  }

  public semesters: string[] = CommonVariables.semesters;
  public progList: ProgramDataStruct[] = [];
  public reviewList: SyllabusReviewDataStruct[] = [];
  public courseList: CourseDataStruct[] = [];
  public selectedCourseList: CourseDataStruct[] = [];
  public semesterList: string[] = [];
  public selectedSemester: string = "";
  public selectedCourseDetailsID: number;
  public selectedProgram: ProgramDataStruct = new ProgramDataStruct(0, "", "", "");
  public selectedReview: SyllabusReviewDataStruct = new SyllabusReviewDataStruct(0, "", 0);
  public selectedCourse: CourseDataStruct = new CourseDataStruct(0, "", "", 0, "", false);
  public selectedSyllabus: SyllabusDataStruct = new SyllabusDataStruct(0, 0);
  public assignedFacultyList: FacultyDataStruct[] = [];
  public assignedFacultySummary: FacultySummaryDataStruct[] = [];

  private modalRef: BsModalRef;

  public alertMsg: string;
  public msgEvent: boolean;
  public msgSuccess: boolean;
  public enableEdit: boolean;

  onAlertClosed() {
    this.alertMsg = "";
    this.msgEvent = false;
    this.msgSuccess = false;
  }

  onFormRequest(template: TemplateRef<any>): void {
    this.enableEdit = false;
    this.selectedCourse = new CourseDataStruct(0, "", "", this.selectedProgram.ProgramID, this.selectedSemester, false);
    this.selectedSyllabus = new SyllabusDataStruct(0, this.selectedReview.SyllabusReviewID);
    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));

  }

  externalLinkFaculty(f: FacultySummaryDataStruct): void {
    this.profileService.getUserNameFromUserID(f.FacultyID).then(resp => {
      var fac = this.assignedFacultyList.find(x => x.FacultyID === f.FacultyID);
      if (fac) {
        var dept = fac.DepartmentID;
        var url = dept.toLowerCase() + "/profile/" + this.commonService.replaceQuotes(resp);
        this.router.navigate([url]).then(resp => {
        }).catch(err => {
          console.log("fail");
        });
      }
    }).catch(err => {

    });
  }

  updateDetails(course: CourseDataStruct, template: TemplateRef<any>): void {
    this.enableEdit = true;
    this.selectedCourse = course;
    this.fetchSyllabusByCourseID(course.CourseID);

    this.modalRef = this.modalService.show(template,
      Object.assign({}, {class: "modal-lg"}));
  }

  assignFaculty(courseID: number): void {
    this.academicsService.GetFacultiesByCourse(courseID).then(resp => {
      this.assignedFacultyList = resp;
      this.assignedFacultySummary = [];
      for (let f of this.assignedFacultyList) {
        this.profileService.GetProfileDetails(f.FacultyID).then(resp => {
          this.assignedFacultySummary.push(new FacultySummaryDataStruct(f.FacultyID, f.Designation, resp.Salutation + resp.FirstName + " " + resp.LastName, resp.ImageType, resp.Image));
        }).catch(err => {

        });
      }
    }).catch(err => {

    });
  }

  removeFaculty(f: FacultySummaryDataStruct): void {
    this.academicsService.RemoveFacultyFromCourse(f.FacultyID, this.selectedCourse).then(resp => {
      this.assignFaculty(this.selectedCourse.CourseID);
    }).catch(err => {

    });
  }

  onAssignFacultyRequest(faculty: FacultySummaryDataStruct): void {
    if (!faculty)
      this.modalRef.hide();
    else {
      if (this.assignedFacultyList.find(x => x.FacultyID === faculty.FacultyID))
        return;
      this.academicsService.AssignFacultyToCourse(faculty.FacultyID, this.selectedCourse).then(resp => {
        this.assignFaculty(this.selectedCourse.CourseID);
      }).catch(err => {

      });
    }
  }

  deleteDetails(course: CourseDataStruct): void {

    this.academicsService.DeleteCourse(course).then((resp) => {
      this.courseList.splice(this.courseList.indexOf(this.courseList.find(x => x.CourseID === this.selectedCourse.CourseID)), 1);
      this.fetchCourseListBySemester(this.selectedSemester);
      this.fetchCourseSemester();
      this.alertMsg = "Course Successfully Deleted";
      this.msgEvent = true;
      this.msgSuccess = true;

    }).catch((err) => {
      this.alertMsg = "Failed to Delete Course";
      this.msgEvent = true;
      this.msgSuccess = false;
      this.modalRef.hide();
    });
  }

  onCancelClick(): void {
    this.modalRef.hide();
  }


  onAssignFaculty(courseID: number, template: TemplateRef<any>) {
    this.selectedCourse = this.courseList.find(x => x.CourseID === courseID);
    this.assignFaculty(courseID);
    this.modalRef = this.modalService.show(template);
  }

  onSaveClick(): void {
    if (this.enableEdit) {
      this.academicsService.PutCourse(this.selectedCourse).then((resp) => {
        this.courseList.splice(this.courseList.indexOf(this.courseList.find(x => x.CourseID === this.selectedCourse.CourseID)), 1);
        this.courseList.push(this.selectedCourse);
        this.fetchCourseListBySemester(this.selectedSemester);
        this.fetchCourseSemester();

        this.alertMsg = "Course Successfully Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Course Update Failed";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
      this.academicsService.PutSyllabus(this.selectedSyllabus).then((resp_syl) => {
        this.alertMsg = "Syllabus Successfully Updated";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err_syl) => {
        this.alertMsg = "Syllabus Updated Failed";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
    else {
      this.selectedCourse.Syllabus = this.selectedSyllabus;
      this.academicsService.PostCourseByProgram(this.selectedProgram.ProgramID, this.selectedCourse).then((resp) => {

        this.fetchCourseList(this.selectedReview.SyllabusReviewID);
        this.fetchCourseListBySemester(this.selectedSemester);
        this.fetchCourseSemester();
        this.alertMsg = "Course Successfully Added";
        this.msgEvent = true;
        this.msgSuccess = true;
        this.modalRef.hide();
      }).catch((err) => {
        this.alertMsg = "Failed to save new Course";
        this.msgEvent = true;
        this.msgSuccess = false;
        this.modalRef.hide();
      });
    }
  }


  formatDeptList(): void {
    for (let d of this.deptList) {
      d.EstablishedIn = this.timeService.FormatDate(d.EstablishedIn);
    }
    this.deptList = this.deptList.sort((a, b) =>
      a.EstablishedIn > b.EstablishedIn ? 1 : a.EstablishedIn < b.EstablishedIn ? -1 : 0
    );
  }

  fetchDeptList(): void {
    if (this.deptList.length === 0 && !this.isDeptFixed) {
      this.academicsService.GetDepartmentList().then((resp) => {
        this.deptList = resp;
        this.formatDeptList();
      }).catch((err) => {

      });
    }
  }

  fetchProgramList(deptID: string): void {
    if (!deptID)
      return;
    this.progList = [];
    this.reviewList = [];
    this.semesterList = [];
    this.courseList = [];
    this.selectedCourseList = [];
    this.selectedReview = new SyllabusReviewDataStruct(0, "", 0);
    this.selectedProgram = new ProgramDataStruct(0, "", "", this.selectedDept.DepartmentID);
    this.removeSyllabusData();
    this.academicsService.GetProgramListByDept(deptID).then((resp) => {
      this.progList = resp;
      if (this.deptList)
        this.selectedDept = this.deptList.length > 0 ? this.deptList.find(x => x.DepartmentID === deptID) : this.selectedDept;
      if (this.isDeptFixed) {
        this.selectedProgram = this.progList.find(x => x.ProgramName === this.passedProgName);
        if (this.selectedProgram) {
          this.progSelection = this.selectedProgram.ProgramID;
          this.fetchReviewList(this.selectedProgram.ProgramID);
        }
      }

    }).catch((err) => {
      console.log(err);
    });
  }

  fetchReviewList(progID: number): void {
    this.semesterList = [];
    this.courseList = [];
    this.selectedCourseList = [];
    this.selectedReview = new SyllabusReviewDataStruct(0, "", progID);
    this.removeSyllabusData();
    this.academicsService.GetSyllabusReviewsByProgram(progID).then((resp) => {
      this.reviewList = resp;
      this.selectedProgram = this.progList.find(x => x.ProgramID === +progID);
      if (this.isDeptFixed) {
        this.selectedReview = this.reviewList.find(x => x.EndAY === null);
        if (this.selectedReview) {
          this.reviewSelection = this.selectedReview.SyllabusReviewID;
          this.fetchCourseList(this.selectedReview.SyllabusReviewID);
        }
      }
    }).catch((err) => {

    });
  }

  fetchCourseList(reviewID: number): void {
    this.semesterList = [];
    this.courseList = [];
    this.selectedCourseList = [];
    this.removeSyllabusData();

    this.academicsService.GetCoursesBySyllabusReviewProgram(this.selectedProgram.ProgramID, reviewID).then((resp) => {
      this.courseList = resp;
      this.selectedReview = this.reviewList.find(x => x.SyllabusReviewID === +reviewID);
      this.fetchCourseSemester();
    }).catch((err) => {

    });
  }

  fetchCourseSemester(): void {
    this.semesterList = [];
    if (this.courseList.length > 0) {
      for (let c of this.courseList) {
        if (c.Semester === "" || c.Semester === null || c === undefined) {
          if (!this.semesterList.find(x => x === "Uncategorized")) {
            this.semesterList.push("Uncategorized");
          }
          c.Semester = "Uncategorized";
        } else if (!this.semesterList.find(x => x === c.Semester)) {
          this.semesterList.push(c.Semester);
        }
      }
    }
    if (this.isDeptFixed) {
      if (this.passedSemester) {
        this.semesterSelection = this.passedSemester;
        this.fetchCourseListBySemester(this.passedSemester);
      }
    }
  }


  fetchCourseListBySemester(semester: string): void {
    this.removeSyllabusData();
    this.selectedCourseList = this.courseList.filter(x =>
      x.Semester === semester).length === 0 ? this.courseList.filter(x =>
      x.Semester === "Uncategorized").sort((a, b) =>
        a.CourseCode > b.CourseCode ? 1 : a.CourseCode < b.CourseCode ? -1 : 0) :
      this.courseList.filter(x =>
        x.Semester === semester).sort((a, b) =>
        a.CourseCode > b.CourseCode ? 1 : a.CourseCode < b.CourseCode ? -1 : 0);

    this.semesterSelection = this.selectedSemester = this.courseList.filter(x =>
      x.Semester === semester).length === 0 ? "Uncategorized" : semester;
  }

  onCourseDetailsExpand(courseID: number): void {
    this.fetchSyllabusByCourseID(courseID);
    this.assignFaculty(courseID);
  }

  fetchSyllabusByCourseID(courseID: number): void {
    this.selectedCourseDetailsID = courseID;
    this.academicsService.GetSyllabusByCourse(courseID).then((resp) => {
      if (resp) {
        this.selectedSyllabus = resp;
      }
      else {
        this.selectedSyllabus.SyllabusDetails = "No Syllabus found!";
      }
    }).catch((err) => {

    });
  }


  removeSyllabusData(): void {
    this.selectedCourseDetailsID = -1;
    this.assignedFacultyList = [];
  }

  ngAfterViewInit(): void {
    if (!this.isDeptFixed)
      this.fetchDeptList();
  }

  @Input()
  public id: string = "program_details";

  onAcademicDetailsChoiceClicked(str: string): void {
    this.id = str;
  }

}
