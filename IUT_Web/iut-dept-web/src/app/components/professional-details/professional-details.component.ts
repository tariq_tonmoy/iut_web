import {Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from "@angular/core";
import {PersonalDetailsService} from "../../service/personal-details.service";
import {ProfessionalDetailsDataStruct} from "../../view-model/professional-details-data-struct";
import {LocationDetailsComponent} from "../location-details/location-details.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {TimeServiceService} from "../../service/time-service.service";
import {CommonVariables} from "../../view-model/common-variables";

@Component({
  selector: 'app-professional-details',
  templateUrl: './professional-details.component.html',
  styleUrls: ['./professional-details.component.css']
})
export class ProfessionalDetailsComponent implements OnInit, OnChanges {

  @Input() userID: string;
  @Input() isMyProfile: boolean;

  private modalRef: BsModalRef;
  private professionalEditClick: boolean;

  public professionalDetailsCollection: ProfessionalDetailsDataStruct[] = [];
  public professionalDetails: ProfessionalDetailsDataStruct = new ProfessionalDetailsDataStruct(0, "", "", "", new Date(), "");
  public professionalEvent: boolean = false;
  public professionalSuccess: boolean = false;

  private locationComponent: LocationDetailsComponent;
  public alertMsg: string;
  public professionTypes: string[] = [];


  constructor(private personalDetailsService: PersonalDetailsService,
              private modalService: BsModalService,
              private timeService: TimeServiceService) {
  }

  ngOnInit() {
    this.professionTypes = CommonVariables.professionTypes;
  }

  loadProfessionalDetails(): void {
    this.personalDetailsService.GetProfessionalDetails(this.userID).then((resp) => {
      this.professionalDetailsCollection = resp;
      for (let v of this.professionalDetailsCollection) {
        v.From = v.From === null ? null : this.timeService.FormatDate(v.From);
        v.To = v.To === null ? null : this.timeService.FormatDate(v.To);
      }
    }).catch((err) => {
    });
  }


  onSaveClick(): void {
    this.professionalDetails.UserID = this.userID;
    this.professionalDetails.Address = this.locationComponent.onSaveClick();
    if (!this.professionalEditClick) {
      this.personalDetailsService.PostProfessionalDetails(this.professionalDetails).then((resp) => {
        this.alertMsg = "Details saved in Database";
        this.professionalEvent = true;
        this.professionalSuccess = true;

        this.modalRef.hide();
        this.loadProfessionalDetails();
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";

        this.professionalEvent = true;
        this.professionalSuccess = false;
        this.modalRef.hide();
      });
    } else {
      this.personalDetailsService.PutProfessionalDetails(this.professionalDetails).then((resp) => {
        this.alertMsg = "Details updated in Database";
        this.professionalEvent = true;
        this.professionalSuccess = true;
        this.modalRef.hide();
        var v = this.professionalDetailsCollection.find(x => x.ProfessionalDetailsID === this.professionalDetails.ProfessionalDetailsID);
        if (v) {
          v = this.professionalDetails;
        }

      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.professionalEvent = true;
        this.professionalSuccess = false;
        this.modalRef.hide();
        ;
      });
    }
  }

  updateProfessionalDetails(i: ProfessionalDetailsDataStruct, template: TemplateRef<any>): void {
    this.professionalEditClick = true;
    var v = this.professionalDetailsCollection.find(x => x.ProfessionalDetailsID === i.ProfessionalDetailsID);
    if (v) {
      this.professionalDetails = v;
      this.modalRef = this.modalService.show(template);
    }
    else {
      this.professionalEvent = true;
      this.professionalSuccess = false;
    }
  }

  deleteProfessionalDetails(i: ProfessionalDetailsDataStruct): void {
    var v = this.professionalDetailsCollection.find(x => x.ProfessionalDetailsID === i.ProfessionalDetailsID);

    if (v) {
      this.personalDetailsService.DeleteProfessionalDetails(v).then((resp) => {

        var idx = this.professionalDetailsCollection.indexOf(v);
        if (idx > -1) {
          this.professionalDetailsCollection.splice(idx, 1);
          this.alertMsg = "Details deleted from Database";
          this.professionalEvent = true;
          this.professionalSuccess = true;

        }
        else {
          this.alertMsg = "Failed to access Database";
          this.professionalEvent = true;
          this.professionalSuccess = false;
        }
      }).catch((err) => {
        this.alertMsg = "Failed to access Database";
        this.professionalEvent = true;
        this.professionalSuccess = false;
      });
    }


  }

  onProfessionalAlertClosed(): void {
    this.professionalEvent = false;
    this.professionalSuccess = false;
  }

  onProfessionalFormRequest(template: TemplateRef<any>) {
    this.professionalEditClick = false;
    this.professionalDetails = new ProfessionalDetailsDataStruct(0, "", "", "", new Date(), "");
    this.modalRef = this.modalService.show(template);
  }

  onCancelClick(): void {
    this.modalRef.hide();
    this.loadProfessionalDetails();
  }

  onLocationComponentLoaded(locationComp: LocationDetailsComponent): void {
    this.locationComponent = locationComp;
    this.locationComponent.SetLocation(this.professionalDetails.Address);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["userID"])
      this.loadProfessionalDetails();
  }

}
