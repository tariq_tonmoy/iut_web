import {NgModule} from "@angular/core";
import {CommonModule, DatePipe} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoginFormComponent} from "./login-form/login-form.component";
import {RegistrationFormComponent} from "./registration-form/registration-form.component";
import {ProfileComponent} from "./profile/profile.component";
import {CommonConversionService} from "../service/common-conversion.service";
import {AuthenticationService} from "../service/authentication.service";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {PersonalDetailsService} from "../service/personal-details.service";
import {ProfileDetailsComponent} from "./profile-details/profile-details.component";
import {AcademicsService} from "../service/academics.service";
import {AdminService} from "../service/admin.service";
import {PersonnelService} from "../service/personnel.service";
import {
  AccordionModule,
  AlertModule,
  BsDatepickerModule,
  PaginationModule,
  TabsModule,
  TypeaheadModule
} from "ngx-bootstrap";
import {ProfileTaComponent} from "./profile-ta/profile-ta.component";
import {ProfileAdminComponent} from "./profile-admin/profile-admin.component";
import {ProfileFacultyComponent} from "./profile-faculty/profile-faculty.component";
import {ProfileAlumniComponent} from "./profile-alumni/profile-alumni.component";
import {ProfileStaffComponent} from "./profile-staff/profile-staff.component";
import {PersonalDetailsComponent} from "./personal-details/personal-details.component";
import {ContactDetailsComponent} from "./contact-details/contact-details.component";
import {ProfessionalDetailsComponent} from "./professional-details/professional-details.component";
import {AcademicDetailsComponent} from "./academic-details/academic-details.component";
import {LocationDetailsComponent} from "./location-details/location-details.component";
import {ModalModule} from "ngx-bootstrap/modal";
import {TimeServiceService} from "../service/time-service.service";
import {AlumniService} from "../service/alumni.service";
import {TaService} from "../service/ta.service";
import {FacultyService} from "../service/faculty.service";
import {StaffService} from "../service/staff.service";
import {CourseFacultyComponent} from "./course-faculty/course-faculty.component";
import {CourseComponent} from "./course/course.component";
import {DepartmentComponent} from "./department/department.component";
import {ProgramComponent} from "./program/program.component";
import {SyllabusReviewComponent} from "./syllabus-review/syllabus-review.component";
import {SearchFacultyComponent} from "./search-faculty/search-faculty.component";
import {PublicationService} from "../service/publication.service";
import {EventComponent} from "./event/event.component";
import {NewsService} from "../service/news.service";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {ResearchGroupService} from "../service/research-group.service";
import {ResearchGroupComponent} from "./research-group/research-group.component";
import {SearchPublicationComponent} from "./search-publication/search-publication.component";
import {ResearchGroupDetailsComponent} from "./research-group-details/research-group-details.component";
import {NgxMasonryModule} from "ngx-masonry";
import {NgxSpinnerModule} from "ngx-spinner";
import {PersonnelControlComponent} from "./personnel-control/personnel-control.component";
import {SafeHtmlPipe} from "../pipes/safe-html.pipe";
import {QuillModule} from "ngx-quill";
import {PublicationComponent} from "./publication/publication.component";
import {SearchAlumniComponent} from './search-alumni/search-alumni.component';
import { ExternalAuthComponent } from './external-auth/external-auth.component';
import {ChartsModule} from "ng2-charts";
import {SignalRService} from "../service/signal-r.service";
import { ProfileChattingComponent } from './profile-chatting/profile-chatting.component';
import {ChattingService} from "../service/chatting.service";
import { SearchProfileComponent } from './search-profile/search-profile.component';

@NgModule({
  imports: [
    BsDatepickerModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ModalModule,
    AlertModule,
    AccordionModule,
    TabsModule,
    PaginationModule,
    TypeaheadModule,
    NgxMasonryModule,
    NgxSpinnerModule,
    QuillModule,
    ChartsModule
  ],
  declarations: [
    LoginFormComponent,
    RegistrationFormComponent,
    ProfileComponent,
    ProfileDetailsComponent,
    ProfileAdminComponent,
    ProfileFacultyComponent,
    ProfileTaComponent,
    ProfileStaffComponent,
    ProfileAlumniComponent,
    PersonalDetailsComponent,
    ProfessionalDetailsComponent,
    AcademicDetailsComponent,
    ContactDetailsComponent,
    LocationDetailsComponent,
    DepartmentComponent,
    EventComponent,
    ProgramComponent,
    CourseComponent,
    CourseFacultyComponent,
    SyllabusReviewComponent,
    SearchFacultyComponent,
    PublicationComponent,
    ChangePasswordComponent,
    ResearchGroupComponent,
    SearchPublicationComponent,
    ResearchGroupDetailsComponent,
    PersonnelControlComponent,
    SafeHtmlPipe,
    SearchAlumniComponent,
    ExternalAuthComponent,
    ProfileChattingComponent,
    SearchProfileComponent
  ],
  providers: [
    CommonConversionService,
    AuthenticationService,
    PersonalDetailsService,
    AcademicsService,
    AdminService,
    PersonnelService,
    TimeServiceService,
    DatePipe,
    AlumniService,
    FacultyService,
    TaService,
    StaffService,
    PublicationService,
    NewsService,
    SignalRService,
    ResearchGroupService,
    ChattingService
  ],
  exports: [
    CommonModule,
    CourseComponent,
    FormsModule,
    LoginFormComponent,
    RegistrationFormComponent,
    ProfileComponent,
    ProfileDetailsComponent,
    ResearchGroupComponent,
    PublicationComponent,
    SafeHtmlPipe
  ]
})
export class ComponentsModule {
}
