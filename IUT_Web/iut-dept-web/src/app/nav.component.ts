import {Component, Input, AfterViewInit, ViewChild, ComponentFactoryResolver} from '@angular/core';
import {Location} from "@angular/common";
import {MainPageService} from "./service/main-page.service";
import {NavDirective} from "./nav.directive";
import {DynamicComponentItem} from "./dynamic-component-item";

@Component({
  selector: "iut-nav",
  template: "<ng-template nav-host></ng-template>"
})
export class NavComponent implements AfterViewInit {

  @ViewChild(NavDirective) navHost: NavDirective;

  comp: DynamicComponentItem;

  constructor(private location: Location,
              private mainPageService: MainPageService,
              private componentFactoryResolver: ComponentFactoryResolver) {

  }

  ngAfterViewInit(): void {
    this.comp = this.mainPageService.checkUrlForNav(this.location.path());
    this.loadNavComponent();
  }

  loadNavComponent(): void {
    let componenFactory = this.componentFactoryResolver.resolveComponentFactory(this.comp.component);

    let viewContainerRef = this.navHost.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componenFactory);
  }
}
