import {Component} from "@angular/core";

@Component({
  selector: "app-error",
  template: "<h5>This is {{title|uppercase}}</h5>"
})
export class ErrorComponent {
  title = "hello error";
}
