import {StoryDataStruct} from "./story-data-struct";
import {LocationDataStruct} from "./location-data-struct";

export class CustomEventDataStruct {
  constructor(
    public CustomEventID:number,
    public Title:string,
    public StoryID: number,
    public EventStart?:Date,
    public EventEnds?:Date,
    public EventLocation?:LocationDataStruct
  ) {}
}
