export const IutDeptRoutesStr: string[] =
  [
    "/cse/home",
    "cse",
    "mce"
  ];
export const IutDeptStr: string[] =
  [
    "Default",
    "CSE",
    "MCE"
  ];


export enum IutDeptRoutesEnum {
  DEFAULT = 0,
  CSE = 1,
  MCE = 2
}
