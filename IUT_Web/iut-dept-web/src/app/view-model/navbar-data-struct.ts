export interface NavbarDataStruct {
  home: string;
  academics: string;
  research: string;
  faculty: string;
  alumni: string;
  news: string;
  contact: string;
}

