export class SyllabusDataStruct {
  constructor(public SyllabusID: number,
              public SyllabusReviewID:number,
              public SyllabusDetails?: string,
              public TextBooks?:string) {
  }
}
