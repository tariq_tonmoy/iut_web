export class CourseMaterialDataStruct {
  constructor(
    public ID: number,
    public MaterialType: string,
    public Material: Blob[],
    public FacultyID:string,
    public Name?:string,
    public Summary?: string,
    public MaterialFileName?:string
  ) { }
}
