export class ResearchInterestDataStruct {
  constructor(
    public InterestID:number,
    public CreatorID: string,
    public InterestTitle:string,
    public InterestDescription: string
  ){}
}
