export class LoginDataStruct {
  constructor(
    public grant_type: string,
    public Username: string,
    public Password: string
  ) { }
}
