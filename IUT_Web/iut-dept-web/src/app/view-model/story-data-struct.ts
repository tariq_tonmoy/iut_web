export class StoryDataStruct {
  constructor(public StoryID: number,
              public Title: string,
              public Subtitle: string,
              public DeptID: string,
              public IsFocused?: boolean,
              public Author?:string,
              public Description?: string,
              public PublishDate?: Date,
              public StoryImg?: Blob,
              public ImageType?: string,
              public DescriptionType?: string) {
  }
}
