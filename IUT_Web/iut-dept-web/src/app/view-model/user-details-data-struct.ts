import {ChatGroupDataStruct} from "./chat-group-data-struct";
import {MessageDataStruct} from "./message-data-struct";
import {MessageStatusDataStruct} from "./message-status-data-struct";

export class UserDetailsDataStruct {
  constructor(
    public UserID?:string,
    public IsLoggedIn?:boolean,
    public CreatedChatGroups?:ChatGroupDataStruct[],
    public ChatGroups?:ChatGroupDataStruct[],
    public Messages?:MessageDataStruct[],
    public MessageStatuses?:MessageStatusDataStruct[]
  ) {}
}
