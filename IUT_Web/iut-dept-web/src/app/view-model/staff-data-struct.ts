export class StaffDataStruct {
  constructor(
    public StaffID: string,
    public Designation: string,
    public OfficeAddress?: string,
    public DepartmentID?: string,
    public Priority?:number
  ) { }
}
