import { UserRoleDataStruct } from "./user-role-data-struct";

export class PersonalDetailsDataStruct {
  constructor(
    public UserID: string,
    public Username:string,
    public FirstName: string,
    public LastName: string,
    public Nationality: string,
    public Email: string,
    public BloodGroup?: string,
    public Salutation?: string,
    public DoB?: Date,
    public Image?: Blob,
    public ImageType?: string,
    public Roles?: UserRoleDataStruct[]
  ) { }
}
