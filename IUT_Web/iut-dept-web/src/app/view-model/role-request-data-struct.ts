export class RoleRequestDataStruct {
  constructor(
    public RequestID: number,
    public RequestedRoleID: string,
    public RequestUserID: string,
    public RequestedDepartmentID?: string,
    public RequestedProgramID?: number,
    public MessageToAdmin?: string,
    public SubmissionDate?: Date) {
  }
}
