import {CseDeptNavComponent} from "../cse-dept/cse-dept-nav/cse-dept-nav.component";
import {MceDeptNavComponent} from "../mce-dept/mce-dept-nav/mce-dept-nav.component";

export const DeptNavComponents: any[] = [
  CseDeptNavComponent,
  CseDeptNavComponent,
  MceDeptNavComponent
];
