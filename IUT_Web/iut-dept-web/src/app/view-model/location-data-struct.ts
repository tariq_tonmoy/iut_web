export class LocationDataStruct {
  constructor(public LocationID: number,
              public Address: string,
              public City: string,
              public Country: string,
              public Event_EventID?: number,
              public UserDetails_UserID?: string,
              public ProfessionalDetails_ProfessionalDetailsID?: number,
              public AcademicDetails_AcademicDetailsID?: number) {
  }
}
