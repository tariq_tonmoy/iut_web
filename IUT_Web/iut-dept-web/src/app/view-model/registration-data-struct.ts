export class RegistrationDataStruct {
  constructor(
    public Username: string,
    public Email?: string,
    public Password?: string,
    public ConfirmPassword?: string) { }
}

