export class AlumniDataStruct {
  constructor(
    public AlumniID: number,
    public UserID?: string,
    public PassingYear?: Date,
    public EnrollmentYear?: Date,
    public StudentID?: string,
    public ProgramID?: number,
    public AdmissionYear?:number,
    public IsFocused?:Boolean
  ) {
  }
}
