import {UserDetailsDataStruct} from "./user-details-data-struct";
import {MessageDataStruct} from "./message-data-struct";

export class MessageStatusDataStruct {
  constructor(
    public MessageID?:number,
    public ChatGroupMemberID?:string,
    public ChatGroupMember?:UserDetailsDataStruct,
    public Message?:MessageDataStruct,
    public HasSeen?:Boolean,
    public TimeStamp?:Date
  ) {}
}
