import {LocationDataStruct} from "./location-data-struct";

export class ProfessionalDetailsDataStruct {
  constructor(
    public ProfessionalDetailsID: number,
    public UserID: string,
    public Designation: string,
    public Institution: string,
    public From: Date,
    public Department: string,
    public Address?: LocationDataStruct,
    public To?: Date,
    public IsCurrentProfessionalStatus?: boolean,
    public ProfessionType?: string
  ) {
  }
}
