import {NavbarDataStruct} from "./navbar-data-struct";

export const NavData: NavbarDataStruct = {
  contact: "Contact Us",
  home: "Home",
  news: "News & Events",
  academics: "Academics",
  faculty: "Faculty & Staff",
  research: "Research",
  alumni: "Alumni"
}
