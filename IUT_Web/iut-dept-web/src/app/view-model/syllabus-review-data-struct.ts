export class SyllabusReviewDataStruct {
  constructor(public SyllabusReviewID: number,
              public StartAY: string,
              public ProgramID:number,
              public EndAY?: string,
              public IsCurrentlyActive?: boolean,
              public Version?:number,
              public Comment?: string) {
  }
}
