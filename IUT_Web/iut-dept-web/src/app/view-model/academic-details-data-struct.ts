import {LocationDataStruct} from "./location-data-struct";

export class AcademicDetailsDataStruct {
  constructor(
    public AcademicDetailsID: number,
    public Degree:string,
    public Concentration: string,
    public Institution: string,
    public UserID: string,
    public From?:Date,
    public To?: Date,
    public IsCurrentAcademicStatus?: boolean,
    public Address?:LocationDataStruct
  ){}
}
