import {ResearchInterestDataStruct} from "./research-interest-data-struct";

export class PublicationDataStruct {
  constructor(
    public PublicationID:number,
    public Title:string,
    public AuthorList:string,
    public CreatorID: string,
    public PublishedIn:string,
    public Type: string,
    public Year?: number,
    public Abstract?: string,
    public Url?: string,
    public PublicationMaterial?: Blob,
    public MaterialName?:string,
    public MaterialType?:string,
    public ResearchInterests?:ResearchInterestDataStruct[]
  ) { }
}
