import {ResearchInterestDataStruct} from "./research-interest-data-struct";
import {PublicationDataStruct} from "./publication-data-struct";

export class ResearchGroupDataStruct {
  constructor(
    public GroupID:number,
    public GroupTitle:string,
    public CreatorID?: string,
    public Description?: string,
    public Image?:Blob[],
    public ImageType?:string,
    public Interests?:ResearchInterestDataStruct[],
    public Publications?:PublicationDataStruct[],
    public Members?:string[]
  ){}
}
