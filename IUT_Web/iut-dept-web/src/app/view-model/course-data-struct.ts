import {SyllabusDataStruct} from "./syllabus-data-struct";

export class CourseDataStruct {
  constructor(
    public CourseID: number,
    public CourseCode: string,
    public CourseName: string,
    public ProgramID: number,
    public Semester:string,
    public IsOffered:boolean,
    public Credit?:number,
    public CreditHour?:number,
    public TotalSections?:number,
    public Syllabus?:SyllabusDataStruct
  ) { }
}
