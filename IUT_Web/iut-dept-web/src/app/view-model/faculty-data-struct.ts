export class FacultyDataStruct {
  constructor(
    public FacultyID: string,
    public DepartmentID: string,
    public Designation?: string,
    public OfficeAddress?: string,
    public Biography?: string,
    public Priority?:number
  ) {
  }
}
