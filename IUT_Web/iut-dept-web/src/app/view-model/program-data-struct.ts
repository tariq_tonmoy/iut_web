export class ProgramDataStruct {
  constructor(
    public ProgramID: number,
    public ProgramName: string,
    public Concentration: string,
    public DepartmentID: string,
    public ProgramDescription?:string
  ) { }
}
