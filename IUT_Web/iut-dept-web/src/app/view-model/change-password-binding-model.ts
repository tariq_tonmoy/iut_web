export class ChangePasswordBindingModel {
  constructor(public OldPassword: string,
              public NewPassword: string,
              public ConfirmPassword: string) {
  }
}
