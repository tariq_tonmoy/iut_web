import { UserDetailsDataStruct } from "./user-details-data-struct";
import { MessageDataStruct } from "./message-data-struct";

export class ChatGroupDataStruct {
  constructor(
    public ChatGroupID?: number,
    public Name?: string,
    public CreatorID?: string,
    public Creator?: UserDetailsDataStruct,
    public GroupMembers?: UserDetailsDataStruct[],
    public Messages?: MessageDataStruct[]
  ) {
    this.GroupMembers = [];
    this.Creator = new UserDetailsDataStruct();
    this.Messages = [];
  }
}
