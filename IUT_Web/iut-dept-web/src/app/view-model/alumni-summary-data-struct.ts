export class AlumniSummaryDataStruct {
  constructor(
    public AlumniID?: string,
    public StudentID?: string,
    public UserID?:string,
    public Name?: string,
    public ImageType?: string,
    public Image?: Blob
  ) {
  }
}
