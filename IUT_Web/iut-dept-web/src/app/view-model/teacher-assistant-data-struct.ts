export class TeacherAssistantDataStruct {
  constructor(
    public TAID:string,
    public CreditHour:number,
    public DepartmentID:string
  ) {}
}
