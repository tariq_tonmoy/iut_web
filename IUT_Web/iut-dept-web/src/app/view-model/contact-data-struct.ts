export class ContactDataStruct {
  constructor(
    public ContactID: number,
    public ContactType:string,
    public ContactDetails: string,
    public UserID: string,
  ){}
}
