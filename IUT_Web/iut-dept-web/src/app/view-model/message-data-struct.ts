import {UserDetailsDataStruct} from "./user-details-data-struct";
import {ChatGroupDataStruct} from "./chat-group-data-struct";

export class MessageDataStruct {
  constructor(
    public MessageID?:number,
    public MessageFileType?:string,
    public MessageFileName?:string,
    public MessageFile?:Blob,
    public MessageText?:string,
    public TimeStamp?:Date,
    public GroupID?:number,
    public SenderID?:string,
    public TargetChatGroup?:ChatGroupDataStruct,
    public Sender?:UserDetailsDataStruct,
    public MessageStatuses?:MessageDataStruct[],
  ) {}
}
