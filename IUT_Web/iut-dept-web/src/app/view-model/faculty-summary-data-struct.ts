export class FacultySummaryDataStruct {
  constructor(
    public FacultyID?: string,
    public Designation?: string,
    public Name?: string,
    public ImageType?: string,
    public Image?: Blob
  ) {
  }
}
