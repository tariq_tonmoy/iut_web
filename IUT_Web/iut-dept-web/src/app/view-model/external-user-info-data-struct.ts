export class ExternalUserInfoDataStruct {
  constructor(
              public UserID:string,
              public Email: string,
              public HasRegistered: boolean,
              public LoginProvider: string) {
  }
}
