
export class DepartmentDataStruct {
  constructor(
    public DepartmentID: string,
    public FullName: string,
    public Location?: string,
    public Email?: string,
    public EstablishedIn?: Date,
    public DepartmentHeadID?: string,
    public About?:string,
    public MessageFromHead?:string,
    public Image?:Blob,
    public ImageType?:string
  ) {
  }
}
