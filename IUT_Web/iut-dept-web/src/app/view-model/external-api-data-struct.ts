export class ExternalAPIDataStruct {
  constructor(public Name: string,
              public Url: string,
              public State: string) {
  }
}
