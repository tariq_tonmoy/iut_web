import {Component, OnDestroy, OnInit, Renderer2,} from "@angular/core";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import 'rxjs/add/operator/filter';
import {Title} from "@angular/platform-browser";
import {SignalRService} from "./service/signal-r.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(private renderer: Renderer2,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private title: Title) {
  }

  private prevClass: string = "";

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'IUT-CSE');
    this.renderer.removeClass(document.body, 'IUT-MCE');

  }

  ngOnInit(): void {
    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .subscribe((event: NavigationEnd) => {
        var v: string = this.activatedRoute.root.firstChild.snapshot.data['class'];
        if (v && this.prevClass != v) {
          try {
            if (this.prevClass)
              this.renderer.removeClass(document.body, this.prevClass);
          } catch (e) {

          }
          this.prevClass = v;
          this.renderer.addClass(document.body, this.prevClass);
          this.title.setTitle(this.prevClass);
        }
        else if (!v) {
          try {
            if (this.prevClass)
              this.renderer.removeClass(document.body, this.prevClass);
          } catch (e) {

          }
          this.prevClass = "";
        }
      });
  }
}
