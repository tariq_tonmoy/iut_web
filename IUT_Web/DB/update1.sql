

UPDATE NewCrs
SET NewCrs.Semester=OldSyl.StartSemester
FROM [IUT_Web].[dbo].[Syllabus] As NewSyl
INNER JOIN [IUT_Web].[dbo].[Courses] As NewCrs
	ON NewSyl.SyllabusID=NewCrs.CourseID
	INNER JOIN [IUT_Web_Old].[dbo].[Courses] As OldCrs
		ON OldCrs.CourseCode=NewCrs.CourseCode
		INNER JOIN [IUT_Web_Old].[dbo].[Syllabus] As OldSyl
		ON OldCrs.CourseID=OldSyl.CourseID 
GO


