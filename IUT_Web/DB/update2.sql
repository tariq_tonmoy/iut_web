USE [IUT_Web]
GO

UPDATE [dbo].[Syllabus]
   SET [SyllabusReviewID] = sr.SyllabusReviewID
   FROM [dbo].[SyllabusReviews] AS sr
   INNER JOIN [dbo].[Courses] AS cr
   ON sr.ProgramID=cr.ProgramID
   INNER JOIN [dbo].[Syllabus] as sl
   ON sl.SyllabusID=cr.CourseID
GO


