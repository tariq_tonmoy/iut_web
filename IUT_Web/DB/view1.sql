/****** Script for SelectTopNRows command from SSMS  ******/
SELECT syl.SyllabusDetails, Crs.CourseCode, Crs.CourseName 
FROM [IUT_Web].[dbo].Syllabus As syl
INNER JOIN [IUT_Web].[dbo].Courses as Crs
	ON syl.SyllabusID=Crs.CourseID
	AND Crs.ProgramID=1
Go 
