# Welcome to IUT Web

This repo is an implementation of departmental websites of Islamic University of Technology (IUT), Gazipur, Bangladesh. We have created services using ASP.NET WEB API 2 in order to give different departments of IUT capability to maintain their own website. The source code for the APIs are located in **./IUT_WEB/IUT_Dept_Services/**. The departmental websites are located in **./IUT_WEB/iut-dept-web/**. As an example we have created website for department of Computer Science and Engineering of IUT using Angular 4 in Angular-CLI architecture. The website is highly modular and the components can be reused for other departments. The reusable components can be found in **./IUT_WEB/iut-dept-web/src/app/components/** directory. For CSE department specific pages, we created **./IUT_WEB/iut-dept-web/src/app/cse-dept/** directory. To build the WEB API project, we used Visulal Studio 2017 and for Database, we used MS SQL Server 2016\. The WEB API has followed Dependency Injection architecture and we used Entity Framework Code First to access database. For package manager for the CSE departmental Website, we used Node Package Modules.

[Deployed Site](https://csenew.iutoic-dhaka.edu)

### Features:

The project works as a **Content Management System** for a department. **Not a single paragraph in this website is hard coded in HTML.** The features of the website are:

-   Creating user accounts according to various user categories:
    *   Admin
    *   Faculty
    *   Alumni
    *   Teaching Assistant
    *   Staff
-   Each user has a user profile and a profile based on his category
-   The details of the department and news and events are added by Admin from her panel
-   Academic information like course list, syllabus are also added from Admin Panel
-   Faculty members can include publications, research groups and course materials from Faculty Panel
-   Users can create acout with Gmail or IUT email address (OAuth2)
-   Existing Users can link Gmail or IUT email address with their account to sign in later
-   Users can chat with others as indivuduals or groups

### Technologies Used:
-   ASP.NET WEB API 2
-   Entity Framework Code First 6
-   ASP.NET Identity
-   Angular 4
-   Bootstrap 4
-   Animate.css
-   Font Awesome
-   ASP.NET SignalR

### Tools Used:
-   [For API Development] - Microsoft Visual Studio Enterprise 2017
-   [For Website Development] - IntelliJ IDEA 
-   [For Database] - Microsoft SQL Server 2016

**Speial thanks to Raihan Islam Arnob [raihanislam@iut-dhaka.edu] for his contribution in database creation.** The backup database can be found in **./IUT_WEB/DB/**. Developers can use this backup db to test the project.  